<?php
return [
    'backend' => [
        'frontName' => 'admin'
    ],
    'db' => [
        'connection' => [
            'indexer' => [
                'host' => 'localhost',
                'dbname' => 'magento2',
                'username' => 'magento',
                'password' => 'magento@123',
                'active' => '1',
                'persistent' => NULL
            ],
            'default' => [
                'host' => 'localhost',
                'dbname' => 'magento2',
                'username' => 'magento',
                'password' => 'magento@123',
                'active' => '1'
            ]
        ],
        'table_prefix' => ''
    ],
    'crypt' => [
        'key' => '6c0ba96bc906336a6abe0fc339bd13b0'
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'developer',
    'session' => [
        'save' => 'files'
    ],
    'cache' => [
        'frontend' => [
            'default' => [
                'id_prefix' => 'fe1_'
            ],
            'page_cache' => [
                'id_prefix' => 'fe1_'
            ]
        ]
    ],
    'lock' => [
        'provider' => 'db',
        'config' => [
            'prefix' => ''
        ]
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'compiled_config' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'target_rule' => 1,
        'full_page' => 0,
        'config_webservice' => 1,
        'translate' => 1,
        'vertex' => 1,
        'gigyaim_fieldmapping_cache' => 1
    ],
    'install' => [
        'date' => 'Sat, 10 Aug 2019 12:10:46 +0000'
    ]
];
