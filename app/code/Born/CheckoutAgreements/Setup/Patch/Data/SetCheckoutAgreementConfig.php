<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_CheckoutAgreements
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\CheckoutAgreements\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class SetCheckoutAgreementConfig
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements\Setup\Patch\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class SetCheckoutAgreementConfig implements DataPatchInterface
{
    /**
     * Set the Constant CONFIG_PATH
     *
     * CONFIG_PATH
     */
    const CONFIG_PATH = 'checkout/options/enable_agreements';

    /**
     * TypeListInterface
     *
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * WriteInterface
     *
     * @var WriterInterface
     */
    protected $writer;

    /**
     * SetCheckoutAgreementConfig constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup ModuleDataSetupInterface
     * @param TypeListInterface        $cacheTypeList   TypeListInterface
     * @param WriterInterface          $writer          WriterInterface
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->cacheTypeList = $cacheTypeList;
        $this->writer = $writer;
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->writer->save(
            self::CONFIG_PATH,
            1,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
        // Clean Config type Cache
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);

        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
