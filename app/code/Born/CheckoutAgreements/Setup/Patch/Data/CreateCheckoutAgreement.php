<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_CheckoutAgreements
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\CheckoutAgreements\Setup\Patch\Data;

use Magento\CheckoutAgreements\Model\AgreementFactory;
use Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class CreateCheckoutAgreement
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements\Setup\Patch\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CreateCheckoutAgreement implements DataPatchInterface
{
    /**
     * CheckoutAgreementsRepository
     *
     * @var CheckoutAgreementsRepository
     */
    protected $agreementsRepository;

    /**
     * AgreementFactory
     *
     * @var AgreementFactory
     */
    protected $agreementFactory;

    /**
     * CreateCheckoutAgreement constructor.
     *
     * @param ModuleDataSetupInterface     $moduleDataSetup      ModuleDataSetupInterface
     * @param CheckoutAgreementsRepository $agreementsRepository CheckoutAgreementsRepository
     * @param AgreementFactory             $agreementFactory     AgreementFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CheckoutAgreementsRepository $agreementsRepository,
        AgreementFactory $agreementFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->agreementsRepository = $agreementsRepository;
        $this->agreementFactory = $agreementFactory;
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $agreementModel = $this->agreementFactory->create();
        $agreementModel->setName('Order level consent');
        $agreementModel->setIsActive(true);
        $agreementModel->setIsHtml(false);
        $agreementModel->setMode(1);
        $agreementModel->setCheckboxText("I acknowledge that this order contains special order items. I agree to be billed upon placing the order, and accept a longer delivery time");
        $agreementModel->setContent("I acknowledge that this order contains special order items. I agree to be billed upon placing the order, and accept a longer delivery time");
        $this->agreementsRepository->save($agreementModel);

        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
