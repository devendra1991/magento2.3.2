<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_CheckoutAgreements
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Born_CheckoutAgreements',
    __DIR__
);
