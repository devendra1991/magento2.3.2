<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_CheckoutAgreements
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\CheckoutAgreements\Plugin;

use Born\DbiConnector\Model\InventoryEDDFromDatahub;
use Magento\Checkout\Model\Cart;
use Born\QuoteItemGraphQl\Model\Resolver\UpdateCartItems;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class AgreementsConfigProviderPlugin
 *
 * @category  PHP
 * @package   Born\CheckoutAgreements\Plugin
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class AgreementsConfigProviderPlugin
{
    /**
     * Checkout Cart
     *
     * @var Cart
     */
    protected $cart;

    /**
     * InventoryEDDFromDatahub
     *
     * @var InventoryEDDFromDatahub
     */
    protected $getFromDatahub;

    /**
     * AgreementsConfigProviderPlugin constructor.
     *
     * @param Cart                    $cart           Cart
     * @param InventoryEDDFromDatahub $getFromDatahub InventoryEDDFromDatahub
     * @param ScopeConfigInterface    $scopeConfig    ScopeConfigInterface
     */
    public function __construct(
        Cart $cart,
        InventoryEDDFromDatahub         $getFromDatahub,
        ScopeConfigInterface            $scopeConfig
    ) {
        $this->cart = $cart;
        $this->getFromDatahub    = $getFromDatahub;
        $this->scopeConfig       = $scopeConfig;
    }

    /**
     * Modify the Checkout agreements on special Order Item
     *
     * @param \Magento\CheckoutAgreements\Model\AgreementsConfigProvider $subject AgreementsConfigProvider
     * @param array                                                      $result  ConfigProvider Result
     *
     * @return array
     */
    public function afterGetConfig(
        \Magento\CheckoutAgreements\Model\AgreementsConfigProvider $subject,
        $result
    ) {
        $quote = $this->cart->getQuote();
        $isSpecialOrder = false;
        $agreements = [];
        $requestSystem = $this->scopeConfig->getValue(
            UpdateCartItems::REQUESTING_SYSTEM_PATH,
            ScopeInterface::SCOPE_STORE
        );
        $requestCountry = $this->scopeConfig->getValue(
            UpdateCartItems::REQUESTING_COUNTRY_PATH,
            ScopeInterface::SCOPE_STORE
        );
        foreach ($quote->getAllItems() as $item) {
            $sku = $item->getSku();
            $params = [
                'requestingSystem' => $requestSystem,
                'requestingCountry' => $requestCountry,
                'skus' => [$sku]
            ];
            $data = $this->getFromDatahub->getEDDFromDatahub($params);
            if (!empty($data['specialOrder'])) {
                $isSpecialOrder = true;
            }
        }

        if ($isSpecialOrder) {
            return $result;
        } else {
            return $agreements;
        }
    }
}
