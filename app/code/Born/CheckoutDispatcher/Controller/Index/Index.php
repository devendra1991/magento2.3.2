<?php
namespace Born\CheckoutDispatcher\Controller\Index;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Model\QuoteFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Index
 *
 * @category  PHP
 * @package   Born\CheckoutDispatcher\Controller\Index
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Index extends Action
{
    protected $_pageFactory;
    /**
     * @var
     */
    private $logger;
    /**
     * @var
     */
    private $pageFactory;
    /**
     * @var
     */
    private $quoteFactory;
    /**
     * @var
     */
    private $customer;
    /**
     * @var
     */
    private $customerSession;
    /**
     * @var
     */
    private $checkoutSession;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param LoggerInterface $logger
     * @param QuoteFactory $quoteFactory
     * @param Customer $customer
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        LoggerInterface $logger,
        QuoteFactory $quoteFactory,
        Customer $customer,
        Session $customerSession,
        CheckoutSession $checkoutSession
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->logger = $logger;
        $this->quoteFactory = $quoteFactory;
        $this->customer = $customer;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
    }

    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try {
            $quoteNumber = $this->getRequest()->getParam('quote');
            if ($quoteNumber) {
                $quote = $this->quoteFactory->create()->load($quoteNumber);
                $customerId = $quote->getCustomerId();
                if ($customerId) {
                    $customer = $this->customer->load($customerId);
                    $this->customerSession->setCustomerAsLoggedIn($customer);
                    $resultRedirect->setPath('checkout');
                    return $resultRedirect;
                } else {
                    if ($this->customerSession->getId()) {
                        $this->customerSession->logout();
                    }
                    $this->checkoutSession->clearQuote();
                    $this->checkoutSession->replaceQuote($quote);
                    $resultRedirect->setPath('checkout');
                    return $resultRedirect;
                }
            }

        } catch (Exception $e) {
            $this->logger->debug($e->getMessage());
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
