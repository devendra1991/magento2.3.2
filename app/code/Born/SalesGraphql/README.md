# About Born_SalesGraphql
**SalesGraphQl** provides type and resolver information for the GraphQl module
to generate sales orders information.

Please follow below steps:-

=> Born should be the Vendor and alesGraphql should be the module name.

=> after placing the module in the right place next step is the enable module using following command

php bin/magento module:enable Born_SalesGraphql

=> Next, after that run following command.

php bin/magento setup:upgrade

=> And the last one is to deploy by using following command.

php bin/magento setup:static-content:deploy -f


=> You can check your GraphQL query response by installing chrome extension ChromeiQL or Altair GraphQL addon.
=> Put URL like "http://yourwebsiteurl/graphql"(EX: http://local.magentoce.com/graphql) to get order data 
=> In Request Body, You need to pass the required data(field) to request payload for getting a response of Sales Order record,
=> We have pass sales_order id as 2(response order id on success page) for fetch records of Sales Order Id 2. Based on define id in request result will be shown data of order of specific id.
=> Request Payload,

{
  salesOrder(id: 2) {
    increment_id
    customer_name
    sub_total
    grand_total
    is_guest_customer
    shipping_method
    shipping_amount
    tax_amount
    discount_amount
    created_at
    shipping_method
    items {
      title
      sku
      price
      qty
      pid
      image
      itemoptions
    }
  }
}
=>Result for Order id 2, I have added two products for order id 2 so in response they display both items in items array with Item  info:

{
  "data": {
    "salesOrder": {
      "increment_id": "000000195",
      "customer_name": "Swadesh Swain",
      "sub_total": "110.0000",
      "grand_total": "120.0000",
      "is_guest_customer": false,
      "shipping_method": "flatrate_flatrate",
      "shipping_amount": "10.0000",
      "tax_amount": "0.0000",
      "discount_amount": "0.0000",
      "created_at": "2019-06-03 13:15:39",
      "items": [
        {
          "title": " Dyeable Satin Mid Heel Crystal T Strap Sandal ",
          "sku": "DBTIA-Red-4",
          "price": 55,
          "qty": "1.0000",
          "pid": "18",
          "image": "http://local.dbi-magento/media/catalog/product/i/_/i_gmodule_870x617_brideguide_1_1.jpg",
          "itemoptions": "{\"config\":{\"Color\":\"Red\",\"size\":\"4\"}}"
        },
        {
          "title": " Dyeable Satin Mid Heel Crystal T Strap Sandal ",
          "sku": "DBTIA-Blue-6",
          "price": 55,
          "qty": "1.0000",
          "pid": "18",
          "image": "http://local.dbi-magento/media/catalog/product/i/_/i_gmodule_870x617_brideguide_1_1.jpg",
          "itemoptions": "{\"config\":{\"Color\":\"Blue\",\"size\":\"6\"}}"
        }
      ]
    }
  }
}



