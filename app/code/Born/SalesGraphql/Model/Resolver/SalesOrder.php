<?php
declare(strict_types=1);

namespace Born\SalesGraphql\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Sales Order field resolver, used for GraphQL request processing
 * @property \Magento\Sales\Api\OrderRepositoryInterface orderRepository
 */
class SalesOrder implements ResolverInterface
{
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $salesId = $this->getSalesId($args);
        $salesData = $this->getSalesData($salesId);
        return $salesData;
    }

    /**
     * @param array $args
     * @return int
     * @throws GraphQlInputException
     */
    private function getSalesId(array $args): int
    {
        if (!isset($args['id'])) {
            throw new GraphQlInputException(__('"sales id should be specified'));
        }

        return (int)$args['id'];
    }

    /**
     * @param int $orderId
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    private function getSalesData(int $orderId): array
    {
        try {
            $order = $this->orderRepository->get($orderId);
            $billigAddress = $order->getBillingAddress()->getData();
            $shippingAddress = $order->getShippingAddress()->getData();
            $optionArray = [];

            foreach ($order->getAllVisibleItems() as $_item) {
                $customOptions = [];
                $itemsData = $_item->getData();
                if ($_item->getProductType() == 'configurable') {
                    $itemsConfig =  $_item->getProductOptions();
                    $customOptions = $itemsConfig['attributes_info'];

                    if(!empty($customOptions)) {
                        foreach ($customOptions as $options) {
                            $optionArray['config'][$options['label']] = $options['value'];
                        }
                    }
                }
                $CustomOption = json_encode($optionArray);


                $Itemsalldata[] = array_merge($itemsData, array("itemoptions"=>$CustomOption));
            }
            $pageData = [
                'increment_id' => $order->getIncrementId(),
                'grand_total' => $order->getGrandTotal(),
                'customer_name' => $order->getCustomerFirstname().' '
                                   .$order->getCustomerLastname(),
                'created_at' => $order->getCreatedAt(),
                'shipping_amount' => $order->getShippingAmount(),
                'discount_amount' => $order->getDiscountAmount(),
                'sub_total'=> $order->getSubtotal(),
                'tax_amount'=> $order->getTaxAmount(),
                'is_guest_customer' => !empty($order->getCustomerIsGuest()) ? 1 : 0,
                'shipping_method' => $order->getShippingMethod(),
                'shipping_address' => $shippingAddress,
                'billing_address' => $billigAddress,
                'items' => $Itemsalldata
            ];
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $pageData;
    }
}