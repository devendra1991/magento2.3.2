<?php
declare(strict_types=1);

namespace Born\SalesGraphql\Model\Resolver;

use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ValueFactory;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\CatalogGraphQl\Model\Resolver\Product\Websites\Collection;

/**
 * Retrieves the Items information object
 */
class Items implements ResolverInterface
{
    /**
     * Get All Product Items of Order.
     * @inheritdoc
     */

    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($value['items'])) {
            return null;
        }
        $itemArray = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        foreach ($value['items'] as $key => $item) {
            $prdoduct = $objectManager->get('Magento\Catalog\Model\Product')->load($item['product_id']);
            $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
            $productImageUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $prdoduct->getImage();
            $itemArray[$key]['sku'] = $item['sku'];
            $itemArray[$key]['title'] = $item['name'];
            $itemArray[$key]['price'] = $item['price'];
            $itemArray[$key]['qty'] = $item['qty_ordered'];
            $itemArray[$key]['pid'] = $item['product_id'];
            $itemArray[$key]['image'] = $productImageUrl;
            $itemArray[$key]['itemoptions'] = $item['itemoptions'];


        }
        return $itemArray;
    }
}