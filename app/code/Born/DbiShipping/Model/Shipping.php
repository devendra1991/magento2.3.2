<?php
/**
 * DbiShipping Shipping.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\DbiShipping\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class Shipping
 *
 * @category  PHP
 * @package   Born\DbiShipping\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Shipping extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Shipping Model
     *
     * @param Context          $context            Context
     * @param Registry         $registry           Registry
     * @param AbstractResource $resource           AbstractResource
     * @param AbstractDb       $resourceCollection AbstractDb
     * @param array            $data               data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    /**
     * Shipping Constructor
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Born\DbiShipping\Model\ResourceModel\Shipping');
    }
}