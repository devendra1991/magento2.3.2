<?php
/**
 * DbiShipping Collection.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Model\ResourceModel\Shipping;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 * @category  PHP
 * @package   Born\DbiShipping\Model\ResourceModel\Shipping
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Born\DbiShipping\Model\Shipping', 'Born\DbiShipping\Model\ResourceModel\Shipping');
    }
}
