<?php
/**
 * DbiShipping Shippingrate.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Model\Config\Backend;


use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Filesystem;
use Magento\Framework\DataObject;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Cache\TypeListInterface ;
use Born\DbiShipping\Model\ShippingFactory;
use Magento\Framework\File\Csv;
use Magento\Config\Model\Config\Backend\File\RequestData\RequestDataInterface ;
use Magento\Framework\Model\ResourceModel\AbstractResource ;
use Magento\Framework\Data\Collection\AbstractDb ;
use Magento\Framework\Exception\LocalizedException;


/**
 * Class Shippingrate
 *
 * @category  PHP
 * @package   Born\DbiShipping\Model\Config\Backend
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Shippingrate extends \Magento\Framework\App\Config\Value
{

    protected $shippingFactory;
    protected $requestData;
    protected $filesystem;
    protected $resource;
    protected $csvProcessor;


    /**
     * Shippingrate Constructor
     *
     * @param Context               $context            Context
     * @param Registry              $registry           Registry
     * @param ScopeConfigInterface  $config             ScopeConfigInterface
     * @param TypeListInterface     $cacheTypeList      TypeListInterface
     * @param ShippingFactory       $shippingFactory    ShippingFactory
     * @param Csv                   $csvProcessor       Csv
     * @param RequestDataInterface  $requestData        RequestDataInterface
     * @param Filesystem            $filesystem         Filesystem
     * @param AbstractResource|null $resource           AbstractResource
     * @param AbstractDb|null       $resourceCollection AbstractDb
     * @param array                 $data               data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        ShippingFactory $shippingFactory,
        Csv $csvProcessor,
        RequestDataInterface $requestData,
        Filesystem $filesystem,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->shippingFactory = $shippingFactory;
        $this->requestData = $requestData;
        $this->resource = $resource;
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;
        parent::__construct(
            $context, $registry,
            $config, $cacheTypeList,
            $resource, $resourceCollection,
            $data
        );
    }


    /**
     * Processing object after save data
     *
     * {@inheritdoc}. In addition, it sets status 'invalidate' for config caches
     *
     * @return $this
     * @throws LocalizedException
     */
    public function afterSave()
    {

        $this->uploadAndImport($this);
        return parent::afterSave();
    }

    /**
     * Upload Import Csv file
     *
     * @param DataObject $object data object
     *
     * @return $this
     * @throws LocalizedException
     */
    public function uploadAndImport(DataObject $object)
    {
        /**
         * Read config value
         *
         * @var \Magento\Framework\App\Config\Value $object
         */
        $data = $object->getData('groups');

        if (empty($data['standard']['fields']['import']['value']['tmp_name'])) {
            return $this;
        }
        $filePath = $data['standard']['fields']['import']['value']['tmp_name'];


        $file = $this->getCsvFile($filePath);
        try {
            $csvData = $this->csvProcessor->getData($file);

        } catch (\Exception $e) {
            throw new LocalizedException(
                __(
                    'Something went wrong 
                while uploading and reading 
                shipping rates import file.'
                )
            );
        }
        $insertData = [];
        try {

            $getRate = $this->shippingFactory->create()->getCollection();
            foreach ($getRate as $item) {

                $this->removeRate($item->getId());

            }

            foreach ($csvData as $rowIndex => $dataRow) {
                if ($rowIndex > 0) {
                    if (!$dataRow['0']) {
                        $shippingRate = $this->shippingFactory->create();
                        $shippingRate->setShippingCode($dataRow[1]);
                        $shippingRate->setOrderPriceStart($dataRow[2]);
                        $shippingRate->setOrderPriceEnd($dataRow[3]);
                        $shippingRate->setShippingFees($dataRow[4]);
                        $shippingRate->save();
                    }
                }
            }
        } catch (\Exception $e) {
            throw new LocalizedException(
                __('Something went wrong while Import to Database')
            );

        }

    }//end uploadAndImport()

    /**
     * Read Csv File
     *
     * @param string $filePath Read Csv File Path
     *
     * @return string
     */
    public function getCsvFile($filePath)
    {
        $tmpDirectory = $this->filesystem->getDirectoryRead(DirectoryList::SYS_TMP);
        $path         = $tmpDirectory->getAbsolutePath($filePath);

        return $path;
    }//end getCsvFile()

    /**
     * Remove Shipping Rate
     *
     * @param string $itemId Remove
     *
     * @throws \Exception
     *
     * @return string
     */
    public function removeRate($itemId)
    {

        $shipLoad =  $this->shippingFactory->create()->load($itemId);
        $shipLoad->delete();
    }


}
