<?php
/**
 * DbiShipping Expedited.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Born\DbiConnector\Helper\Data as DataHelper;
use Born\DbiConnector\Model\InventoryEDDFromDatahub;

/**
 * Class Expedited
 *
 * @category  PHP
 * @package   Born\DbiShipping\Model\Carrier
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Expedited extends AbstractCarrier implements
    CarrierInterface
{
    const REQUESTING_SYSTEM_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_system";

    const REQUESTING_COUNTRY_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_country";

    /**
     * Return String type data
     *
     * @var string
     */
    protected $_code = 'expedited';
    /**
     * ResultFactory
     *
     * @var ResultFactory
     */
    protected $rateResultFactory;
    /**
     * MethodFactory
     *
     * @var MethodFactory
     */
    protected $rateMethodFactory;
    /**
     * ProductRepositoryInterface
     *
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * Cart
     *
     * @var Cart
     */
    protected $cart;
    /**
     * InventoryEDDFromDatahub
     *
     * @var InventoryEDDFromDatahub
     */
    protected $getFromDatahub;
    /**
     * DataHelper
     *
     * @var DataHelper
     */
    protected $dataHelper;

    /**
     * Expedited Consturctor
     *
     * @param ScopeConfigInterface       $scopeConfig       ScopeConfigInterface
     * @param ErrorFactory               $rateErrorFactory  ErrorFactory
     * @param LoggerInterface            $logger            LoggerInterface
     * @param ResultFactory              $rateResultFactory ResultFactory
     * @param MethodFactory              $rateMethodFactory MethodFactory
     * @param ProductRepositoryInterface $productRepository ProductRepositoryInter
     * @param Cart                       $cart              Cart
     * @param DataHelper                 $dataHelper        DataHelper
     * @param InventoryEDDFromDatahub    $getFromDatahub    InventoryEDD
     * @param array                      $data              Data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        ProductRepositoryInterface $productRepository,
        Cart $cart,
        DataHelper                            $dataHelper,
        InventoryEDDFromDatahub         $getFromDatahub,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->productRepository = $productRepository;
        $this->cart = $cart;
        $this->getFromDatahub    = $getFromDatahub;
        $this->dataHelper        = $dataHelper;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Get method Config data
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['expedited' => $this->getConfigData('name')];
    }

    /**
     * CollectRates Request
     *
     * @param RateRequest $request RateRequest
     *
     * @return bool|Result
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $result = $this->rateResultFactory->create();
        $method = $this->rateMethodFactory->create();
        $subTotal = $this->cart->getQuote()->getSubtotal();
        $method->setCarrier('expedited');
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod('expedited');
        $method->setMethodTitle($this->getConfigData('name'));

        $items = $this->cart->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            $params = [
                'requestingSystem' => 'magento',
                'requestingCountry' => 'us',
                'skus' => [$item->getSku()]
            ];

            $resultSpcl = $this->getFromDatahub->getEDDFromDatahub($params);
            $resultDc = $this->getFromDatahub->getinventoryFromDatahub($params);
            $CheckDcAvil = $resultDc['dcAvailable'];
            if ($CheckDcAvil === 0) {
                return false;
            }

            if (!empty($resultSpcl['specialOrder'])) {
                return false;
            }

        }

        $regionCode =  $request->getDestRegionCode();
        $countryCode =$request->getDestCountryId();
        $allowedRegions = ['AK', 'HI'];
        $minOrderPrice = $this->getConfigData('state_price_min');
        if (in_array($regionCode, $allowedRegions) && $countryCode == 'US' && $subTotal >= $minOrderPrice) {

            $amount = $this->getConfigData('state_price');
        } else {
            $amount = $this->getConfigData('price');
        }

        $method->setPrice($amount);
        $method->setCost($amount);
        $result->append($method);

        return $result;
    }

}