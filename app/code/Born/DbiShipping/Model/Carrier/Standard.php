<?php
/**
 * DbiShipping Standard.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Born\DbiShipping\Model\ShippingFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

/**
 * Class Standard
 *
 * @category  PHP
 * @package   Born\DbiShipping\Model\Carrier
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Standard extends AbstractCarrier implements
    CarrierInterface
{
    /**
     * Standard code
     *
     * @var string
     */
    protected $_code = 'standard';
    /**
     * ShippingFactory
     *
     * @var ShippingFactory
     */
    protected $modelShippingFactory;
    /**
     * ResultFactory
     *
     * @var ResultFactory
     */
    protected $rateResultFactory;
    /**
     * ProductRepositoryInterface
     *
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * MethodFactory
     *
     * @var MethodFactory
     */
    protected $rateMethodFactory;
    /**
     * Session
     *
     * @var Session
     */
    protected $checkoutSession;
    /**
     * Cart
     *
     * @var Cart
     */
    protected $cart;

    /**
     * Standard Constructor
     *
     * @param ScopeConfigInterface       $scopeConfig          ScopeConfigInterface
     * @param ErrorFactory               $rateErrorFactory     ErrorFactory
     * @param LoggerInterface            $logger               LoggerInterface
     * @param ResultFactory              $rateResultFactory    ResultFactory
     * @param MethodFactory              $rateMethodFactory    MethodFactory
     * @param Session                    $checkoutSession      Session
     * @param Cart                       $cart                 Cart
     * @param ProductRepositoryInterface $productRepository    ProductRepositoryInter
     * @param ShippingFactory            $modelShippingFactory ShippingFactory
     * @param array                      $data                 data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        Session  $checkoutSession,
        Cart $cart,
        ProductRepositoryInterface $productRepository,
        ShippingFactory $modelShippingFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->productRepository = $productRepository;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->checkoutSession = $checkoutSession;
        $this->modelShippingFactory = $modelShippingFactory;
        $this->cart = $cart;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Admin Config For allow
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['standard' => $this->getConfigData('name')];
    }

    /**
     * CollectRates Request
     *
     * @param RateRequest $request RateRequest
     *
     * @return bool|Result
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $shippingModel = $this->modelShippingFactory->create()->getCollection();

        $amount = $this->getConfigData('price');
        $subTotal = (int)$this->cart->getQuote()->getSubtotal();
        $regionCode =  $request->getDestRegionCode();
        $countryCode =$request->getDestCountryId();


        $allowedRegions = ['AK', 'HI'];
        $minOrderPrice = $this->getConfigData('state_price_min');
        if (in_array($regionCode, $allowedRegions) && $countryCode == 'US' && $subTotal >= $minOrderPrice) {
            $amount = $this->getConfigData('state_price');
        } else {
            foreach ($shippingModel as $result) {
                $startPrice = $result->getOrderPriceStart();
                $endPrice = $result->getOrderPriceEnd();

                if ($subTotal >= $startPrice && empty($endPrice) || $subTotal >= $startPrice && $endPrice == 0) {

                    $amount = $result->getShippingFees();
                } elseif ($subTotal >= $startPrice && $subTotal <= $endPrice ) {

                    $amount = $result->getShippingFees();
                }

            }
        }

        /**
         * Create RateResultFactory
         *
         * @var \Magento\Shipping\Model\Rate\Result $result
         */
        $result = $this->rateResultFactory->create();

        /**
         * Create RateMethodFactory
         *
         * @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method
         */
        $method = $this->rateMethodFactory->create();
        $method->setCarrier('standard');
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod('standard');
        $method->setMethodTitle($this->getConfigData('name'));

        /*you can fetch shipping price from different
         sources over some APIs, we used price from
         config.xml - xml node price*/

        $method->setPrice($amount);
        $method->setCost($amount);
        $result->append($method);

        return $result;
    }


}