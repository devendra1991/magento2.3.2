<?php
/**
 * DbiShipping Import.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Block\Adminhtml\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;


/**
 * Class Import
 *
 * @category  PHP
 * @package   Born\DbiShipping\Block\Adminhtml\Form\Field
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Import extends AbstractElement
{
    /**
     * Import Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setType('file');

    }

}
