<?php
/**
 * DbiShipping Export.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiShipping
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\DbiShipping\Block\Adminhtml\Form\Field;

use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Escaper;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Asset\Repository;

/**
 * Class Export
 *
 * @category  PHP
 * @package   Born\DbiShipping\Block\Adminhtml\Form\Field
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Export extends AbstractElement
{

    /**
     * Dir Url
     *
     * @var Dir
     */
    protected $dir;
    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected $urlInterface;
    /**
     * Asset Repository
     *
     * @var AssetRepository
     */
    protected $assetRepository;

    /**
     * Export Constructor
     *
     * @param Factory           $factoryElement    FactoryElement
     * @param CollectionFactory $factoryCollection CollectonFactory
     * @param Escaper           $escaper           Escaper
     * @param DirectoryList     $dir               DirecotoryList
     * @param UrlInterface      $urlInterface      UrlInterface
     * @param array             $data              Data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        DirectoryList $dir,
        UrlInterface $urlInterface,
        Repository $assetRepository,
        array $data = []
    ) {
        $this->dir = $dir;
        $this->urlInterface = $urlInterface;
        $this->assetRepository = $assetRepository;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);

    }

    /**
     * String In return
     *
     * @return string
     */
    public function getElementHtml()
    {

        $buttonBlock = $this->getForm()->getParent()->getLayout()->createBlock(
            Button::class
        );

        $params = ['website' => $buttonBlock->getRequest()->getParam('website')];
         $url =  $this->getFilePath();
        $data = [
            'label' => __('Download Sample CSV'),
            'onclick' => "setLocation('" .$url ."' )",
            'class' => '',
        ];

        $html = $buttonBlock->setData($data)->toHtml();
        return $html;
    }

    public function getFilePath()
    {
        $fileId = 'Born_DbiShipping::csv/dbi_shipping_price.csv';
        $params = [
            'area' => 'frontend'
        ];
        $asset = $this->assetRepository->createAsset($fileId, $params);
        try {
            return $asset->getUrl();
        } catch (\Exception $e) {
            return null;
        }
    }

}
