<?php


namespace Born\DbiEmail\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;
use Magento\Framework\MessageQueue\PublisherInterface;
use Born\DbiEmail\Model\Config;
use Born\DbiConnector\Helper\Data as DbiConnectorHelper;
use Born\DbiEmail\Model\EmailExportApi;

/**
 * Class CustomerCreationEmail
 * @package Born\DbiEmail\Observer
 * Oberver for the account creation email push to the queue or send to the datahub
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class CustomerCreationEmail implements ObserverInterface
{
    /**
     *
     */
    const TOPIC_NAME = 'dbiaccountcreation_email';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PublisherInterface
     */
    private $publisher;

    /**
     * @var DbiConnectorHelper
     */
    private $dbiConnectorHelper;

    /**
     * @var EmailExportApi
     */
    private $emailExporter;

    /**
     * CustomerCreationEmail constructor.
     * @param Config $config
     * @param Json $json
     * @param LoggerInterface $logger
     * @param PublisherInterface $publisher
     * @param DbiConnectorHelper $dbiConnectorHelper
     * @param EmailExportApi $emailExporter
     */
    public function __construct(
        Config $config,
        Json $json,
        LoggerInterface $logger,
        PublisherInterface $publisher,
        DbiConnectorHelper $dbiConnectorHelper,
        EmailExportApi $emailExporter

    )
    {
        $this->config = $config;
        $this->json = $json;
        $this->logger = $logger;
        $this->publisher = $publisher;
        $this->dbiConnectorHelper = $dbiConnectorHelper;
        $this->emailExporter = $emailExporter;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     * pushing the account creation info to the queue or datahub
     */
    public function execute(Observer $observer)
    {
        /** @var Customer $customer */

        if ($this->config->getAccountCreationEmailDisable()) { //check if disable magento OOTB sending account creation email
            $request = $observer->getEvent()->getAccountController()->getRequest();
            $customer = $observer->getCustomer();
            $customerId = $customer->getId();
            $email = $request->getParam('email');
            $firstName = $request->getParam('firstname');
            $lastName = $request->getParam('lastname');
            $url = 'https://something.com/resetme';
            $customerInfo = [];
            $customerInfo['emailAddress'] = $email;
            $customerInfo['firstName'] = $firstName;
            $customerInfo['lastName'] = $lastName;
            $customerInfo['url'] = $url;
            if ($this->dbiConnectorHelper->isQueueProcessingEnable()) {
                try {
                    $customerInfoJson = $this->json->serialize($customerInfo);
                    $this->publisher->publish(self::TOPIC_NAME, $customerInfoJson);
                    $message = 'CustomerID ' . $customerId . 'Account Creation Info ' . $customerInfoJson;
                    $this->logger->debug($message);
                } catch (Exception $e) {
                    $this->logger->debug($e->getMessage());
                }
            } else {
                try {
                    $response = $this->emailExporter->accountCreationExport('POST', $customerInfo);
                    $this->logger->debug($response);
                } catch (Exception $e) {
                    $this->logger->debug($e->getMessage());
                }
            }
        }
        return $this;
    }
}