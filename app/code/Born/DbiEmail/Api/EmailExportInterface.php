<?php


namespace Born\DbiEmail\Api;


/**
 * Interface EmailExportInterface
 * @package Born\DbiEmail\Api
 */
interface EmailExportInterface
{
    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function accountCreationExport($method, $params = []);
}