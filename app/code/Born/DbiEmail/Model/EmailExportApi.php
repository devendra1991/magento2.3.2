<?php


namespace Born\DbiEmail\Model;

use Born\DbiConnector\Helper\Data;
use Born\DbiEmail\Api\EmailExportInterface;
use Born\DbiEmail\Model\Config;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
 * Class EmailExportApi
 * @package Born\DbiEmail\Model
 * @author    Borngroup <support@borngroup.com>
 * Api caller for the datahub
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class EmailExportApi implements EmailExportInterface
{
    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var Curl
     */
    private $curlFactory;

    /**
     * @var ScopeInterface
     */
    private $dataHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Json
     */
    private $jsonHelper;

    /**
     * EmailExportApi constructor.
     * @param \Born\DbiEmail\Model\Config $scopeConfig
     * @param CurlFactory $curlFactory
     * @param Data $dataHelper
     * @param LoggerInterface $logger
     * @param Request $request
     * @param Json $jsonHelper
     * @param SessionManagerInterface $coreSession
     */
    public function __construct(
        Config $scopeConfig,
        CurlFactory $curlFactory,
        Data $dataHelper,
        LoggerInterface $logger,
        Request $request,
        Json $jsonHelper,
        SessionManagerInterface $coreSession
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
        $this->request = $request;
        $this->jsonHelper = $jsonHelper;
        $this->coreSession = $coreSession;
    }

    /**
     * @param $method
     * @param array $params
     * @return bool|false|mixed|string
     */
    public function accountCreationExport($method, $params = [])
    {
        $endPoint = $this->scopeConfig->getAccountCreationEndpointUrl();
        $apiCaller = $this->apiCaller($params, $endPoint, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->serialize(['response' => 'false', "Message" => "Please try again, no response from the API."]);
    }

    /**
     * @param $method
     * @param array $params
     * @return bool|false|string
     */
    public function ResetPasswordEmail($method, $params = [])
    {
        $end_point = $this->scopeConfig->getPasswordResetEndpointUrl();

        $apiCaller = $this->apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->serialize(['response' => 'false', "Message" => "Please try again, no response from the API."]);
    }

    /**
     * @param array $params
     * @param $endPoint
     * @param string $method
     * @return bool
     */
    private function apiCaller($params = [], $endPoint, $method = "POST")
    {
        try {
            if (!empty($params) && $params != '') {

                /**
                 * @var TYPE_NAME $accessToken
                 */

                $accessToken = $this->getAccessHeader();

                /* Create curl factory */
                $httpAdapter = $this->curlFactory->create();

                $postFields = '';

                if ($method == 'POST') {
                    $postFields = $this->jsonHelper->serialize($params);
                }

                $headers = ['authorizationToken:' . $accessToken];
                $httpAdapter->write($method, $endPoint, '1.1', $headers, $postFields);
                return $httpAdapter->read();
            } else {
                $this->logger->error(__('Request don\'t have data.'));
            }
            $this->logger->error(__('Something went wrong.'));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return false;
    }

    /**
     * This method is used to create the access token
     *
     * @return array|mixed
     */
    private function getAccessHeader()
    {
        $tokenResponse = $this->dataHelper->getAccessToken();
        $jsonResponse = $this->jsonHelper->unserialize($tokenResponse);

        if (isset($jsonResponse['code']) && $jsonResponse['code'] != '') {
            $this->logger->error(__('Unable to get the token', $jsonResponse['body']));
            return $tokenResponse;
        } else {
            return $jsonResponse['token'];
        }
    }
}