<?php


namespace Born\DbiEmail\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\MessageQueue\CallbackInvoker;
use Magento\Framework\MessageQueue\ConsumerConfigurationInterface;
use Magento\Framework\MessageQueue\ConsumerInterface;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\MessageController;
use Magento\Framework\MessageQueue\QueueInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Born\DbiEmail\Model\Config;
use Born\DbiEmail\Model\EmailExportApi;


/**
 * Class AccountCreationEmailConsumer
 * @package Born\DbiEmail\Model
 * Consumer for the account creation email to datahub
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class AccountCreationEmailConsumer implements ConsumerInterface
{
    /**
     * @var \Born\DbiEmail\Model\EmailExportApi
     */
    private $emailExporter;

    /**
     * @var Json
     */
    private $json;

    /**
     * Initialize dependencies.
     *
     * @param CallbackInvoker $invoker CallbackInvoker
     * @param ResourceConnection $resource ResourceConnection
     * @param MessageController $messageController MessageController
     * @param ConsumerConfigurationInterface $configuration ConsumerConfigurationInterface
     * @param LoggerInterface $logger LoggerInterface
     */
    public function __construct(
        CallbackInvoker $invoker,
        ResourceConnection $resource,
        MessageController $messageController,
        ConsumerConfigurationInterface $configuration,
        LoggerInterface $logger,
        Config $config,
        EmailExportApi $emailExporter,
        Json $json
    )
    {
        $this->invoker = $invoker;
        $this->resource = $resource;
        $this->messageController = $messageController;
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->config = $config;
        $this->emailExporter = $emailExporter;
        $this->json = $json;
    }

    /**
     * Connects to a queue, consumes a message on the queue,
     * and invoke a method to process the message contents.
     * @param int|null $maxNumberOfMessages
     * if not specified - process all queued incoming messages and terminate,
     * otherwise terminate execution after processing the specified number of messages
     * @return void
     * @since 102.0.1
     */
    public function process($maxNumberOfMessages = null)
    {
        $queue = $this->configuration->getQueue();
        if (!isset($maxNumberOfMessages)) {
            $queue->subscribe($this->getTransactionCallback($queue));
        } else {
            $this->invoker->invoke
            ($queue, $maxNumberOfMessages, $this->getTransactionCallback($queue));
        }
    }

    /**
     * @param QueueInterface $queue
     * @return \Closure
     */
    private function getTransactionCallback(QueueInterface $queue)
    {
        return function (EnvelopeInterface $message) use ($queue) {

            $lock = null;
            try {
                $lock = $this->messageController->lock($message, $this->configuration->getConsumerName());
                $messageBody = $message->getBody();
                //$this->logger->debug($messageBody);
                $customerInfoArray = $this->json->unserialize($this->json->unserialize($messageBody));
                /**
                 * $this->processQueueMsg->process() use for process message which you publish in queue
                 * $data = $this->processQueueMsg->($message);
                 */
                $response = $this->emailExporter->accountCreationExport('POST', $customerInfoArray);
                $data = $this->json->unserialize($response);
                $this->logger->debug('Account Creation Response ' . $data);
                if (isset($data['response']) && $data['response'] === false) {
                    $queue->reject($message); // if get error in message process
                }
                $queue->acknowledge($message); // send acknowledge to queue
            } catch (MessageLockException $exception) {
                $queue->acknowledge($message);
            } catch (ConnectionLostException $e) {
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            } catch (NotFoundException $e) {
                $queue->acknowledge($message);
                $this->logger->warning($e->getMessage());
            } catch (\Exception $e) {
                $queue->reject($message, false, $e->getMessage());
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            }
        };
    }
}