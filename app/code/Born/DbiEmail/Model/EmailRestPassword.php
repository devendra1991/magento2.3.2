<?php

declare(strict_types=1);

namespace Born\DbiEmail\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\MessageQueue\CallbackInvoker;
use Magento\Framework\MessageQueue\ConsumerConfigurationInterface;
use Magento\Framework\MessageQueue\ConsumerInterface;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\MessageController;
use Magento\Framework\MessageQueue\QueueInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;
use Born\DbiEmail\Model\EmailExportApi;


/**
 * Delegating to MassConsumer to consume the data from the queue
 */
class EmailRestPassword implements \Magento\Framework\MessageQueue\ConsumerInterface
{

    private $configuration;

    private $json;

    private $invoker;

    private $messageController;

    private $resource;

    private $emailExporter;

    private $logger;

    public function __construct(
        CallbackInvoker $invoker,
        ResourceConnection $resource,
        MessageController $messageController,
        ConsumerConfigurationInterface $configuration,
        LoggerInterface $logger,
        EmailExportApi $emailExporter,
        Json $json
    )
    {
        $this->messageController = $messageController;
        $this->configuration = $configuration;
        $this->json = $json;
        $this->invoker = $invoker;
        $this->resource = $resource;
        $this->emailExporter = $emailExporter;
        $this->logger = $logger;
    }

    public function process($maxNumberOfMessages = null)
    {
        $queue = $this->configuration->getQueue();
        if (!isset($maxNumberOfMessages)) {
            $queue->subscribe($this->getTransactionCallback($queue));
        } else {
            $this->invoker->invoke
            ($queue, $maxNumberOfMessages, $this->getTransactionCallback($queue));
        }
    }


    private function getTransactionCallback(QueueInterface $queue)
    {
        return function (EnvelopeInterface $message) use ($queue) {

            $lock = null;
            try {
                $lock = $this->messageController->lock($message, $this->configuration->getConsumerName());
                $messageBody = $message->getBody();
                $customerInfoArray = $this->json->unserialize($this->json->unserialize($messageBody));
                /**
                 * $this->processQueueMsg->process() use for process message which you publish in queue
                 * $data = $this->processQueueMsg->($message);
                 */
                $response = $this->emailExporter->ResetPasswordEmail('POST', $customerInfoArray);
                $data = $this->json->unserialize($response);
                $this->logger->debug('Reset Password Response ' . $data);
                if (isset($data['response']) && $data['response'] === false) {
                    $queue->reject($message); // if get error in message process
                }
                $queue->acknowledge($message); // send acknowledge to queue
            } catch (MessageLockException $exception) {
                $queue->acknowledge($message);
            } catch (ConnectionLostException $e) {
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            } catch (NotFoundException $e) {
                $queue->acknowledge($message);
                $this->logger->warning($e->getMessage());
            } catch (\Exception $e) {
                $queue->reject($message, false, $e->getMessage());
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            }
        };
    }

}
