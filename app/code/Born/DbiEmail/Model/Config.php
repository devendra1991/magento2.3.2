<?php


namespace Born\DbiEmail\Model;

use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterfaceAlias;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Config
 * @package Born\DbiEmail\Model
 * @author    Borngroup <support@borngroup.com>
 * Config helper for the admin configuration
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class Config
{
    /**
     *
     */
    const PASSWORD_RESET_ENDPOINT_URL = 'born_dbi_email/dbi_email_endpoint/dbi_email_password_reset';
    /**
     *
     */
    const PASSWORD_RESET_EMAIL_DISABLE = 'born_dbi_email/dbi_email_endpoint/dbi_email_password_reset_disable';
    /**
     *
     */
    const ACCOUNT_CREATION_ENDPOINT_URL = 'born_dbi_email/dbi_email_endpoint/dbi_email_account_creation';
    /**
     *
     */
    const ACCOUNT_CREATION_EMAIL_DISABLE = 'born_dbi_email/dbi_email_endpoint/dbi_email_account_creation_disable';
    /**
     * @var
     */
    private $scopeConig;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Data constructor.
     * @param ScopeConfigInterfaceAlias $scopeConfig
     */
    public function __construct(
        ScopeConfigInterfaceAlias $scopeConfig,
        LoggerInterface $logger
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetEndpointUrl()
    {
        return $this->scopeConfig->getValue(
            self::PASSWORD_RESET_ENDPOINT_URL, ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getPasswordResetEmailDisable()
    {
        return $this->scopeConfig->getValue(
            self::PASSWORD_RESET_EMAIL_DISABLE, ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getAccountCreationEndpointUrl()
    {
        return $this->scopeConfig->getValue(
            self::ACCOUNT_CREATION_ENDPOINT_URL, ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getAccountCreationEmailDisable()
    {
        return $this->scopeConfig->getValue(
            self::ACCOUNT_CREATION_EMAIL_DISABLE, ScopeInterface::SCOPE_STORE
        );
    }

}