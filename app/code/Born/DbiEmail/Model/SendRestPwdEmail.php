<?php


namespace Born\DbiEmail\Model;


use Born\DbiConnector\Helper\Data as DbiConnectorHelper;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\MessageQueue\PublisherInterface;
use Born\DbiEmail\Model\EmailExportApi;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class SendRestPwdEmail
 * @package Born\DbiEmail\Model
 */
class SendRestPwdEmail
{

    const TOPIC_NAME = 'dbiemail_resetpwd';

    private $urlBuilder;

    private $publisher;

    private $jsonHelper;

    private $logger;

    private $dbiConnector;

    private $emailExporter;

    private $storeManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        PublisherInterface $publisher,
        JsonHelper $jsonHelper,
        LoggerInterface $logger,
        UrlInterface $urlBuilder,
        DbiConnectorHelper $dbiConnector,
        EmailExportApi $emailExporter
    )
    {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->publisher = $publisher;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->dbiConnector = $dbiConnector;
        $this->emailExporter = $emailExporter;
        $this->urlBuilder = $urlBuilder;
    }
    /**
     * @param $customerEmailData
     * @param CustomerInterface $customer
     * @param $storeId
     */
    public function sendEmail($customerEmailData , $customer, $storeId)
    {
        $customerId = $customer->getId();
        $email = $customer->getEmail();
        $firstName = $customer->getFirstname();
        $lastName = $customer->getLastname();
        $queryParams = [
            'token' => $customerEmailData['rp_token'],
            'store' => $storeId,
            '_nosid' => '1'
        ];

        $url = $this->urlBuilder->getUrl('customer/account/createPassword', ['_current' => true,'_use_rewrite' => true, '_query' => $queryParams]);

        //url should be <a href="{{var this.getUrl($store,'customer/account/createPassword/',[_query:[token:$customer.rp_token],_nosid:1])}}" target="_blank">{{trans "Set a New Password"}}</a>

        $customerInfo = [];
        $customerInfo['emailAddress'] = $email;
        $customerInfo['firstName'] = $firstName;
        $customerInfo['lastName'] = $lastName;
        $customerInfo['url'] = $url;


        if ($this->dbiConnector->isQueueProcessingEnable()) {
            try {
                $publishData = $this->jsonHelper->jsonEncode($customerInfo);
                $this->publisher->publish(self::TOPIC_NAME, $publishData);
                $message = 'CustomerID ' . $customerId . 'Reset Password Info ' . $publishData;
                $this->logger->debug($message);
            } catch (Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        } else {
            try {
                $response = $this->emailExporter->ResetPasswordEmail('POST', $customerInfo);
                $this->logger->debug($response);
            } catch (Exception $e) {
                $this->logger->debug($e->getMessage());
            }
        }

    }

}