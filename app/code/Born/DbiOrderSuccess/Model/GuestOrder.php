<?php
/**
 * DbiOrderSuccess GuestOrder.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiOrderSuccess
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);
namespace Born\DbiOrderSuccess\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class GuestOrder
 *
 * @category  PHP
 * @package   Born\DbiOrderSuccess\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class GuestOrder extends AbstractModel
{

    /**
     * Shipping Constructor
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Born\DbiOrderSuccess\Model\ResourceModel\GuestOrder');
    }
}