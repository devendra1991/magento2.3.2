<?php
/**
 * DbiOrderSuccess Success.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiOrderSuccess
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\DbiOrderSuccess\ViewModel\Checkout\Onepage;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Sales\Model\OrderFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Born\DbiOrderSuccess\Model\GuestOrderFactory;
use Magento\Customer\Model\Session as CustomerSession;


/**
 * Class Success
 *
 * @category  PHP
 * @package   Born\DbiOrderSuccess\ViewModel\Checkout\Onepage
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Success implements ArgumentInterface
{
    /**
     * Checkout Session
     *
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Sales Factory
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * ProductRepository
     *
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductFactory
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * GuestOrderFactory
     *
     * @var GuestOrderFactory
     */
    protected $guestOrderFactory;
    /**
     * CustomerSession
     *
     * @var CustomerSession
     */
    protected $customerSession;


    /**
     * Success constructor.
     *
     * @param CheckoutSession       $checkoutSession   CheckoutSession
     * @param OrderFactory          $orderFactory      OrderFactory
     * @param ProductRepository     $productRepository ProductRepository
     * @param ProductFactory        $productFactory    ProductFactory
     * @param StoreManagerInterface $storeManager      StoreManager
     * @param GuestOrderFactory     $guestOrderFactory GuestOrderFactory
     * @param CustomerSession       $customerSession   CustomerSession
     */
    public function __construct(

        CheckoutSession $checkoutSession,
        OrderFactory $orderFactory,
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        StoreManagerInterface $storeManager,
        GuestOrderFactory $guestOrderFactory,
        CustomerSession $customerSession


    ) {
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->guestOrderFactory = $guestOrderFactory;
        $this->customerSession = $customerSession;

    }

    /**
     * GetOrder OrderArray
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->checkoutSession->getLastRealOrder();

    }

    /**
     * GetOrderID OrderId
     *
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->checkoutSession->getLastOrderId();

    }

    /**
     * GetPrintOption
     *
     * @param  OrderId $orderid OrderId
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPrint($orderid)
    {
        $store = $this->storeManager->getStore();

        $OrderIncrementID =  $this->getOrder()->getIncrementId();

        $getHash = $this->guestOrderFactory->create()->getCollection()
            ->addFieldToFilter('order_id', array('eq' => $OrderIncrementID));

          $dataArray = $getHash->getData();
          $hashCode = $dataArray[0]['hash'];

        if ($this->customerSession->isLoggedIn()) {
            $printUrl = $store->getBaseUrl().'sales/order/print/order_id/'.$orderid;
        } else {

            $printUrl = $store->getBaseUrl()
                .'guest/order/printOrder/order_hash/'.$hashCode;
        }

        return $printUrl;
    }


}