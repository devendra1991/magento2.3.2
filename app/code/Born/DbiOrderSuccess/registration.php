<?php
/**
 * DbiOrderSuccess Registration.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiOrderSuccess
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Born_DbiOrderSuccess', __DIR__);
