<?php
/**
 * DbiOrderSuccess GuestOrder.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiOrderSuccess
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiOrderSuccess\Controller\Order;

use Born\DbiOrderSuccess\Model\GuestOrderFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;

/**
 * Class PrintOrder
 *
 * @category  PHP
 * @package   Born\DbiOrderSuccess\Controller\Order
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class PrintOrder extends \Magento\Framework\App\Action\Action
{

    /**
     * ForwardFactory
     *
     * @var ForwardFactory
     */
    protected $resultForwardFactory;
    /**
     * Registry
     *
     * @var Registry|null
     */
    protected $coreRegistry = null;
    /**
     * Context
     *
     * @var Context
     */
    protected $context;
    /**
     * Order
     *
     * @var Order
     */
    protected $order;
    /**
     * GuestOrderFactory
     *
     * @var GuestOrderFactory
     */
    protected $guestOrderFactory;
    /**
     * LoggerInterface
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * PrintOrder constructor.
     *
     * @param Context           $context              Context
     * @param ForwardFactory    $resultForwardFactory ForwardFactory
     * @param Registry          $registry             Registry
     * @param Order             $order                Order
     * @param GuestOrderFactory $guestOrderFactory    GuestOrderFactory
     * @param LoggerInterface   $logger               LoggerInterface
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory,
        Registry $registry,
        Order $order,
        GuestOrderFactory $guestOrderFactory,
        LoggerInterface $logger
    ) {
        $this->resultForwardFactory = $resultForwardFactory;
        $this->context = $context;
        $this->coreRegistry = $registry;
        $this->order = $order;
        $this->guestOrderFactory = $guestOrderFactory;
        $this->logger = $logger;

        parent::__construct($context);
    }

    /**
     * ExecuteGuestOrder
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $guestOrderHash = $this->getRequest()->getParam('order_hash');
        try {

                 $getHash = $this->guestOrderFactory->create()->getCollection()
                     ->addFieldToFilter('hash', array('eq' => $guestOrderHash));

                    $dataArray = $getHash->getData() ;
                    $orderIncrmentId  = (int)$dataArray[0]['order_id'];

                    $orderPrint = $this->getOrderId($orderIncrmentId);
                    $this->coreRegistry->register('current_order', $orderPrint);
                    $this->context->getView()->loadLayout(array('print', 'sales_order_print'));
                    $this->context->getView()->renderLayout();

        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }

    }

    /**
     * GetOrderID
     *
     * @param IncID $incrementId IncID
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderId($incrementId)
    {
        $orderData = $this->order->loadByIncrementId($incrementId);
        return $orderData;
    }
}
