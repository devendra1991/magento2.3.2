<?php
/**
 * DbiOrderSuccess GuestOrder.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiOrderSuccess
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiOrderSuccess\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;
use Born\DbiOrderSuccess\Model\GuestOrderFactory;

/**
 * Class OrderComplete
 *
 * @category  PHP
 * @package   Born\DbiOrderSuccess\Observer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class OrderComplete implements ObserverInterface
{
    /**
     * ObjectManager
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;
    /**
     * GuestOrderFactory
     *
     * @var GuestOrderFactory
     */
    protected $guestOrderFactory;
    /**
     * Order
     *
     * @var Order
     */
    protected $order;
    /**
     * OrderComplete constructor.
     *
     * @param ObjectManagerInterface $objectManager     ObjectManagerInterface
     * @param GuestOrderFactory      $guestOrderFactory GuestOrderFactory
     * @param Order                  $order             Order
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        GuestOrderFactory $guestOrderFactory,
        Order $order
    ) {
        $this->objectManager = $objectManager;
        $this->guestOrderFactory = $guestOrderFactory;
        $this->order = $order;
    }

    /**
     * GetOrderData
     *
     * @param \Magento\Framework\Event\Observer $observer Observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderData      = $observer->getEvent()->getOrder();
        $orderId    = $orderData->getIncrementId();
        $hash       = md5($orderData->getIncrementId());
        $guestOrder = $this->guestOrderFactory->create();
        $guestOrder->setOrderId($orderId);
        $guestOrder->setHash($hash);
        $guestOrder->save();
    }
}