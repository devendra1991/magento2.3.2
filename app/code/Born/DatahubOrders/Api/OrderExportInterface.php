<?php


namespace Born\DatahubOrders\Api;


/**
 * Interface OrderExportInterface
 * @package Born\DatahubOrders\Api
 */
interface OrderExportInterface
{
    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function orderExport($method, $params = []);
}