<?php

namespace Born\DatahubOrders\Plugin\Order;

use Born\DbiConnector\Helper\Data as DbiConnectorHelper;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Born\DatahubOrders\Model\Config;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Born\DatahubOrders\Model\OrderPrep;
use Born\DatahubOrders\Model\OrderExportApi;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class PlaceAfterPlugin
 * @package Born\DatahubOrders\Plugin\Order
 */
class  PlaceAfterPlugin
{
    /**
     *
     */
    const TOPIC_NAME = 'processOrders.topic';
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SerializerJson
     */
    private $json;
    /**
     * @var PublisherInterface
     */
    private $publisher;
    /**
     * @var Config
     */
    private $config;
    /**
     * @var DbiConnectorHelper
     */
    private $dbiConnectorHelper;

    /**
     * @var OrderPrep
     */
    private $orderPrep;

    /**
     * @var OrderExportApi
     */
    private $orderExport;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * PlaceAfterPlugin constructor.
     * @param LoggerInterface $logger
     * @param SerializerJson $json
     * @param PublisherInterface $publisher
     * @param Config $config
     * @param DbiConnectorHelper $dbiConnectorHelper
     */
    public function __construct(
        LoggerInterface $logger,
        SerializerJson $json,
        PublisherInterface $publisher,
        Config $config,
        DbiConnectorHelper $dbiConnectorHelper,
        OrderPrep $orderPrep,
        OrderExportApi $orderExport,
        OrderRepositoryInterface $orderRepository
    )
    {

        $this->logger = $logger;
        $this->json = $json;
        $this->publisher = $publisher;
        $this->config = $config;
        $this->dbiConnectorHelper = $dbiConnectorHelper;
        $this->orderPrep = $orderPrep;
        $this->orderExport = $orderExport;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param OrderManagementInterface $orderManagementInterface
     * @param $order
     * @return mixed
     */
    public function afterPlace(
        OrderManagementInterface $orderManagementInterface,
        $order
    )
    {
        /** @var Order $order */
        if ($this->config->getEnable()) {
            $orderArray = $this->orderPrep->orderPrep($order);

            if ($this->dbiConnectorHelper->isQueueProcessingEnable()) {
                try {
                    $orderJson = $this->json->serialize($orderArray);
                    $this->publisher->publish(self::TOPIC_NAME, $orderJson);
                    $message = 'OrderID: ' . $order->getId() . 'Order Rabbitmq Value' . $orderJson;
                    $this->config->setDebugValue($message);
                } catch (Exception $e) {
                    $this->logger->debug($e->getMessage());
                }
            } else {
                try {
                    $response = $this->orderExport->orderExport('POST', $orderArray);
                    $data = $this->json->unserialize($response);
                    $transactionUuid = $data['transactionUuid'] ?? null;
                    if (!empty($transactionUuid)) {
                        $order->setData('transaction_uuid', $transactionUuid);
                        $this->orderRepository->save($order);
                    } else {
                        if (!empty($data['errorDetails'])) {
                            $order->addCommentToStatusHistory('DataHub Warning : ' . implode(",\n", $data['errorDetails']), true);
                            $this->orderRepository->save($order);
                        }
                    }
                } catch (Exception $e) {
                    $this->logger->debug($e->getMessage());
                }
            }
        }

        return $order;
    }
}