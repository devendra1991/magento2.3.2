<?php


namespace Born\DatahubOrders\Model;

use Born\DatahubOrders\Model\Config;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Born\DatahubOrders\Model\OrderExportApi;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Item;
use Psr\Log\LoggerInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

/**
 * Class OrderPrep
 * @package Born\DatahubOrders\Model
 */
class OrderPrep
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var TransactionRepositoryInterface
     */
    private $transactionRepository;

    /**
     * @var \Born\DatahubOrders\Model\OrderExportApi
     */
    private $orderExportApi;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Config
     */
    private $config;


    /**
     * @var SerializerJson
     */
    private $json;

    /**
     * OrderPrep constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param TransactionRepositoryInterface $transactionRepository
     * @param \Born\DatahubOrders\Model\OrderExportApi $orderExportApi
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        TransactionRepositoryInterface $transactionRepository,
        OrderExportApi $orderExportApi,
        Config $config,
        LoggerInterface $logger,
        SerializerJson $json
    )
    {
        $this->customerRepository = $customerRepository;
        $this->transactionRepository = $transactionRepository;
        $this->orderExportApi = $orderExportApi;
        $this->config = $config;
        $this->logger = $logger;
        $this->json = $json;
    }

    /**
     * @param Order $order
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function orderPrep(Order $order)
    {
        /** @var Order $order */
        /** @var Customer $customer */
        /** @var Item $item */

        $isBoarderFreeOrder = $this->isBoarderFreeOrder($order);

        //order object array
        $orderArray = [];
        $orderArray['createdBy'] = $isBoarderFreeOrder ? 'Magento API BoarderFree' : 'Magento API'; //will change in future

        //transaction object array
        $transaction = [];
        $messageId = $this->orderExportApi->generateUuid();
        $messageDate = $order->getCreatedAt();
        $transaction['messageId'] = $messageId;
        $transaction['messageDate'] = $messageDate;

        //transactionHeader object array
        $transactionHeader = [];
        $transactionId = $order->getIncrementId();
        $transactionExternalId = $order->getId();
        $transactionDate = $order->getCreatedAt();
        $company = 'DBI_US';
        $channel = 'ECOM';
        $source = 'MAG';
        $origin = $isBoarderFreeOrder ? 'International' : 'Domestic';
        $chargeUponOrder = $this->isChargeUponOrder($order); // 'No' -> authorized 'Yes' -> captured
        $isGuest = $order->getCustomerIsGuest(); // boolean false : customer true : guest
        $guestCheckout = $isGuest ? true : false;
        $currency = $order->getOrderCurrencyCode();
        $subtotalAmount = $order->getSubtotal();
        $discountAmount = $order->getDiscountAmount();
        $shippingAmount = $order->getShippingAmount();
        $taxAmount = $order->getTaxAmount();
        $totalAmount = $order->getGrandTotal();
        $transactionHeader['transactionId'] = $transactionId;
        $transactionHeader['transactionExternalId'] = $transactionExternalId;
        $transactionHeader['transactionDate'] = $transactionDate;
        $transactionHeader['company'] = $company;
        $transactionHeader['channel'] = $channel;
        $transactionHeader['source'] = $source;
        $transactionHeader['origin'] = $origin;
        $transactionHeader['chargeUponOrder'] = $chargeUponOrder;
        $transactionHeader['guestCheckout'] = $guestCheckout;
        $transactionHeader['currency'] = $currency;
        $transactionHeader['subtotalAmount'] = $subtotalAmount;
        $transactionHeader['discountAmount'] = $discountAmount;
        $transactionHeader['shippingAmount'] = $shippingAmount;
        $transactionHeader['taxAmount'] = $taxAmount;
        $transactionHeader['totalAmount'] = $totalAmount;
        $transaction['transactionHeader'] = $transactionHeader;
        //customer object array
        // check if guest checkout
        $customerArray = [];
        if ($isGuest) {
            $transactionHeader['customer'] = $customerArray; // TODO: verify with DBI if we should send empty data for customer if guest
        } else {
            $customerId = $order->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $customerUuid = $customer->getCustomAttribute('customer_uuid')->getValue(); //TODO :: It might failed !!
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $email = $customer->getEmail();
            $phone = $order->getShippingAddress()->getTelephone();
            $customerArray['customerId'] = $customerId;
            $customerArray['customerUuid'] = $customerUuid;
            $customerArray['firstName'] = $firstName;
            $customerArray['lastName'] = $lastName;
            $customerArray['email'] = $email;
            $customerArray['phone'] = $phone;
            $transactionHeader['customer'] = $customerArray;
        }
        $transactionHeader['payment'][] = $this->getPaymentObject($order);
        $transaction['transactionHeader'] = $transactionHeader;

        $shipToArray = [];
        $shipToArray = $this->getShipToObject($order);

        //transactionLines object array
        $transactionLines = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $itemArray = [];
            $hasChildren = false;
            $childrenItems = $item->getChildrenItems();
            if ($childrenItems != null) {
                $hasChildren = true;
            }
            $fulfilledBy = $item->getProduct()->getAttributeText('descriptive_fulfilled_by');
            if ($fulfilledBy == 'David\'s Bridal') { //fulfilled by david's bridal, split by qty
                if ($hasChildren) {
                    // for configurable product
                    foreach ($childrenItems as $cItem) {
                        if ($item->getQtyOrdered() == 1) {
                            $itemArray['transactionLineId'] = $cItem->getItemId();
                            $itemArray['transactionType'] = 'Customer Order';
                            $itemArray['transactionAction'] = 'Purchase';
                            $itemArray['product'] = [
                                'sku' => $cItem->getSku(),
                                'productName' => $cItem->getName(),
                                'productDescription' => $cItem->getDescription(),
                                'returnable' => $cItem->canRefund() ? 'Yes' : 'No',
                                'maxLeadTime' => '2' //TODO
                            ];
                            $itemArray['quantity'] = 1;
                            $itemArray['unitPrice'] = $item->getBasePrice();
                            //TODO
                            $itemArray['taxAmount'] = $item->getTaxAmount() / $item->getQtyOrdered();
                            $itemArray['discountAmount'] = $item->getDiscountAmount() / $item->getQtyOrdered();
                            $itemArray['shippingAmount'] = $order->getShippingAmount() / $item->getQtyOrdered();
                            $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount() / $item->getQtyOrdered();
                            $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax() / $item->getQtyOrdered();
                            $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                            $itemArray['requiredDate'] = '20191212'; // TODO
                            $itemArray['orderType'] = 'OH';
                            $itemArray['shipTo'] = $shipToArray;
                            $transactionLines[] = $itemArray;
                        } else {
                            for ($i = 1; $i <= $item->getQtyOrdered(); $i++) {
                                $itemArray['transactionLineId'] = $cItem->getItemId() . '-' . $i;
                                $itemArray['transactionType'] = 'Customer Order';
                                $itemArray['transactionAction'] = 'Purchase';
                                $itemArray['product'] = [
                                    'sku' => $cItem->getSku(),
                                    'productName' => $cItem->getName(),
                                    'productDescription' => $cItem->getDescription(),
                                    'returnable' => $cItem->canRefund() ? 'Yes' : 'No',
                                    'maxLeadTime' => '2' //TODO
                                ];
                                $itemArray['quantity'] = 1;
                                $itemArray['unitPrice'] = $item->getBasePrice();
                                //TODO
                                $itemArray['taxAmount'] = $item->getTaxAmount() / $item->getQtyOrdered();
                                $itemArray['discountAmount'] = $item->getDiscountAmount() / $item->getQtyOrdered();
                                $itemArray['shippingAmount'] = $order->getShippingAmount() / $item->getQtyOrdered();
                                $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount() / $item->getQtyOrdered();
                                $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax() / $item->getQtyOrdered();
                                $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                                $itemArray['requiredDate'] = '20191212'; // TODO
                                $itemArray['orderType'] = 'OH';
                                $itemArray['shipTo'] = $shipToArray;
                                $transactionLines[] = $itemArray;
                            }
                        }

                    }
                } else {
                    // for simple product
                    if ($item->getQtyOrdered() == 1) {
                        $itemArray['transactionLineId'] = $item->getItemId();
                        $itemArray['transactionType'] = 'Customer Order';
                        $itemArray['transactionAction'] = 'Purchase';
                        $itemArray['product'] = [
                            'sku' => $item->getSku(),
                            'productName' => $item->getName(),
                            'productDescription' => $item->getDescription(),
                            'returnable' => $item->canRefund() ? 'Yes' : 'No',
                            'maxLeadTime' => '2' //TODO
                        ];
                        $itemArray['quantity'] = 1;
                        $itemArray['unitPrice'] = $item->getBasePrice();
                        //TODO
                        $itemArray['taxAmount'] = $item->getTaxAmount() / $item->getQtyOrdered();
                        $itemArray['discountAmount'] = $item->getDiscountAmount() / $item->getQtyOrdered();
                        $itemArray['shippingAmount'] = $order->getShippingAmount() / $item->getQtyOrdered();
                        $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount() / $item->getQtyOrdered();
                        $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax() / $item->getQtyOrdered();
                        $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                        $itemArray['requiredDate'] = '20191212'; // TODO
                        $itemArray['orderType'] = 'OH';
                        $itemArray['shipTo'] = $shipToArray;
                        $transactionLines[] = $itemArray;
                    } else {
                        for ($i = 1; $i <= $item->getQtyOrdered(); $i++) {
                            $itemArray['transactionLineId'] = $item->getItemId() . '-' . $i;
                            $itemArray['transactionType'] = 'Customer Order';
                            $itemArray['transactionAction'] = 'Purchase';
                            $itemArray['product'] = [
                                'sku' => $item->getSku(),
                                'productName' => $item->getName(),
                                'productDescription' => $item->getDescription(),
                                'returnable' => $item->canRefund() ? 'Yes' : 'No',
                                'maxLeadTime' => '2' //TODO
                            ];
                            $itemArray['quantity'] = 1;
                            $itemArray['unitPrice'] = $item->getBasePrice();
                            //TODO
                            $itemArray['taxAmount'] = $item->getTaxAmount() / $item->getQtyOrdered();
                            $itemArray['discountAmount'] = $item->getDiscountAmount() / $item->getQtyOrdered();
                            $itemArray['shippingAmount'] = $order->getShippingAmount() / $item->getQtyOrdered();
                            $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount() / $item->getQtyOrdered();
                            $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax() / $item->getQtyOrdered();
                            $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                            $itemArray['requiredDate'] = '20191212'; // TODO
                            $itemArray['orderType'] = 'OH';
                            $itemArray['shipTo'] = $shipToArray;
                            $transactionLines[] = $itemArray;
                        }
                    }
                }
            } else { // fulfilled by vendor, don't need to split by qty
                if ($hasChildren) {
                    // for configurable product
                    foreach ($childrenItems as $cItem) {
                        $itemArray['transactionLineId'] = $cItem->getItemId();
                        $itemArray['transactionType'] = 'Customer Order';
                        $itemArray['transactionAction'] = 'Purchase';
                        $itemArray['product'] = [
                            'sku' => $cItem->getSku(),
                            'productName' => $cItem->getName(),
                            'productDescription' => $cItem->getDescription(),
                            'returnable' => $cItem->canRefund() ? 'Yes' : 'No',
                            'maxLeadTime' => '2' //TODO
                        ];
                        $itemArray['quantity'] = $item->getQtyOrdered();
                        $itemArray['unitPrice'] = $item->getBasePrice();
                        $itemArray['taxAmount'] = $item->getTaxAmount();
                        $itemArray['discountAmount'] = $item->getDiscountAmount();
                        $itemArray['shippingAmount'] = $order->getShippingAmount();
                        $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount();
                        $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax();
                        $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                        $itemArray['requiredDate'] = '20191212'; // TODO
                        $itemArray['orderType'] = 'OH';
                        $itemArray['shipTo'] = $shipToArray;
                        $transactionLines[] = $itemArray;
                    }
                } else {
                    // for simple product
                    $itemArray['transactionLineId'] = $item->getItemId();
                    $itemArray['transactionType'] = 'Customer Order';
                    $itemArray['transactionAction'] = 'Purchase';
                    $itemArray['product'] = [
                        'sku' => $item->getSku(),
                        'productName' => $item->getName(),
                        'productDescription' => $item->getDescription(),
                        'returnable' => $item->canRefund() ? 'Yes' : 'No',
                        'maxLeadTime' => '2' //TODO
                    ];
                    $itemArray['quantity'] = $item->getQtyOrdered();
                    $itemArray['unitPrice'] = $item->getBasePrice();
                    $itemArray['taxAmount'] = $item->getTaxAmount();
                    $itemArray['discountAmount'] = $item->getDiscountAmount();
                    $itemArray['shippingAmount'] = $order->getShippingAmount();
                    $itemArray['shippingTaxAmount'] = $order->getShippingTaxAmount();
                    $itemArray['totalAmount'] = $item->getBaseRowTotalInclTax();
                    $itemArray['shippingMethod'] = $this->getShippingMethod($order);
                    $itemArray['requiredDate'] = '20191212'; // TODO
                    $itemArray['orderType'] = 'OH';
                    $itemArray['shipTo'] = $shipToArray;
                    $transactionLines[] = $itemArray;
                }
            }
        }
        $transaction['transactionLines'] = $transactionLines;
        $orderArray['transaction'] = $transaction;
        if ($this->config->getDebugMode()) { // debug
            $this->config->setDebugValue("Payload Order Export to DataHub :: " . $this->json->serialize($orderArray));
        }
        return $orderArray;
    }

    /**
     * @param Order $order
     * @return string
     */
    public function getShippingMethod(Order $order)
    {
        $shippingMethod = $order->getShippingMethod();
        switch ($shippingMethod) {
            case 'standard_standard':
                return 'Standard';
            case 'expedited_expedited':
                return 'Expedited';
            case 'express_express':
                return 'Express';
            case 'freeshipping_freeshipping':
                return 'Free';
            default:
                return 'Standard';
        }
    }

    /**
     * @param Order $order
     * @return array
     * @throws LocalizedException
     */
    public function getPaymentObject(Order $order)
    {
        //payment object array
        /** @var Payment $payment */
        $payment = $order->getPayment();
        $methodCode = $payment->getMethodInstance()->getCode();

        $debugMode = $this->config->getDebugMode();
        if ($debugMode) {
            $paymentJson = $this->json->serialize((array)$payment->getData());
            $message = 'OrderID: ' . $order->getId() . ' payment information : ' . $paymentJson;
            $this->config->setDebugValue($message);
        }


        $paymentArray = [];
        if ($methodCode == 'chcybersource') {
            $paymentArray['paymentType'] = 'Credit Card';
            $paymentArray['creditCardLastFour'] = $payment->getCcLast4();
            $paymentArray['paymentAmount'] = $payment->getAmountAuthorized();
            $paymentArray['paymentSubType'] = $this->getPaymentSubType($payment->getAdditionalInformation('cardType'));
            $paymentArray['paymentId'] = $payment->getAdditionalInformation('transaction_id');
            $paymentArray['paymentStatus'] = $payment->getAdditionalInformation('decision');   //  ACCEPT / Review  TODO : authorized / captured
            $paymentArray['paymentDate'] = $order->getCreatedAt();  // i.e "2019-06-27T094806Z"
        } elseif ($methodCode == 'cybersourcepaypal') {
            $paymentArray['paymentType'] = 'PayPal';
            $paymentArray['paymentAmount'] = $payment->getAmountAuthorized();
            $paymentArray['customer_email'] = $payment->getAdditionalInformation('paypal_express_checkout_customer_email'); // TODO review filed name 'customer_email'
            $paymentArray['paymentId'] = $payment->getAdditionalInformation('paypal_txn_id');
            $paymentArray['paymentStatus'] = $payment->getAdditionalInformation('payment_status'); // processing TODO : authorized / captured

            // not applicable for paypal
            $paymentArray['creditCardLastFour'] = $payment->getCcLast4();
            $paymentArray['paymentSubType'] = 'NA';
            $paymentArray['paymentDate'] = $order->getCreatedAt(); // null not in paypal payment object TODO

        } elseif ($methodCode == 'cybersourceappleapy') {
            //todo

        } elseif ($methodCode == 'cybersourcegooglepay') {
            //todo

        }

        $paymentArray['billTo'] = $this->getBillingObject($order);

        return $paymentArray;

    }


    /**
     * @param string $code
     * @return string
     */
    public function getPaymentSubType($code)
    {
        //ref:  \CyberSource\SecureAcceptance\Block\Checkout\Billing::getCcTypes
        switch ($code) {
            case '001':
                return 'Visa';
            case '002':
                return 'MasterCard';
            case '003':
                return 'American Express';
            case '004':
                return 'Discover';
            case '005':
                return 'Diners Club';
            case '007':
                return 'JCB';
            case '042':
                return 'Maestro International';
            default:
                return 'NA';
        }

    }


    /**
     * @param Order $order
     * @return array
     */
    private function getBillingObject(Order $order)
    {
        $billTo = [];
        $billingStreet = [];
        $billingStreet = $order->getBillingAddress()->getStreet();
        $billTo['firstName'] = $order->getBillingAddress()->getFirstname();
        $billTo['lastName'] = $order->getBillingAddress()->getLastname();
        $billTo['address1'] = $billingStreet[0] ?? '';
        $billTo['address2'] = $billingStreet[1] ?? '';
        $billTo['city'] = $order->getBillingAddress()->getCity();
        $billTo['stateProvince'] = $order->getBillingAddress()->getRegionCode();
        $billTo['postalCode'] = $order->getBillingAddress()->getPostcode();
        $billTo['country'] = $order->getBillingAddress()->getCountryId();
        $billTo['email'] = $order->getBillingAddress()->getEmail();
        $billTo['phone'] = $order->getBillingAddress()->getTelephone();
        return $billTo;

    }

    /**
     * @param Order $order
     * @return array
     */
    private function getShipToObject(Order $order)
    {
        $shippingStreet = [];
        $shippingStreet = $order->getShippingAddress()->getStreet();
        return
            [
                'firstName' => $order->getShippingAddress()->getFirstname(),
                'lastName' => $order->getShippingAddress()->getLastname(),
                'address1' => $shippingStreet[0] ?? '',
                'address2' => $shippingStreet[1] ?? '',
                'city' => $order->getShippingAddress()->getCity(),
                'stateProvince' => $order->getShippingAddress()->getRegionCode(),
                'postalCode' => $order->getShippingAddress()->getPostcode(),
                'country' => $order->getShippingAddress()->getCountryId(),
                'email' => $order->getShippingAddress()->getEmail(),
                'phone' => $order->getShippingAddress()->getTelephone()
            ];
    }

    /**
     * @param Order $order
     * @return boolean;
     */
    private function isBoarderFreeOrder(Order $order)
    {
        //TODO: implement

        return false;

    }

    /**
     * @param Order $order
     * @return boolean;
     */
    private function isChargeUponOrder(Order $order)
    {
        //TODO: implement

        return false;

    }

}
