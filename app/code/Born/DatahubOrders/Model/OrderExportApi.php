<?php


namespace Born\DatahubOrders\Model;

use Born\DbiConnector\Helper\Data;
use Born\DatahubOrders\Api\OrderExportInterface;
use Born\DatahubOrders\Model\Config;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
 * Class OrderExportApi
 * @package Born\DatahubOrders\Model
 */
class OrderExportApi implements OrderExportInterface
{
    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var Curl
     */
    private $curlFactory;

    /**
     * @var ScopeInterface
     */
    private $data_helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Json
     */
    private $jsonHelper;

    /**
     * OrderExportApi constructor.
     * @param \Born\DatahubOrders\Model\Config $scopeConfig
     * @param CurlFactory $curlFactory
     * @param Data $dataHelper
     * @param LoggerInterface $logger
     * @param Request $request
     * @param Json $jsonHelper
     * @param SessionManagerInterface $coreSession
     */
    public function __construct(
        Config $scopeConfig,
        CurlFactory $curlFactory,
        Data $dataHelper,
        LoggerInterface $logger,
        Request $request,
        Json $jsonHelper,
        SessionManagerInterface $coreSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
        $this->data_helper = $dataHelper;
        $this->logger = $logger;
        $this->request = $request;
        $this->jsonHelper = $jsonHelper;
        $this->coreSession = $coreSession;
    }

    /**
     * @param $method
     * @param array $params
     * @return bool|false|mixed|string
     */
    public function orderExport($method, $params = [])
    {
        $end_point = $this->scopeConfig->getLiveUrl();
        $apiCaller = $this->_apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->serialize(['response' => 'false', "Message" => "Please try again, no response from the API."]);
    }

    /**
     * @param array $params
     * @param $end_point
     * @param string $method
     * @return bool
     */
    private function _apiCaller($params = [], $end_point, $method = "GET")
    {
        try {
            if (!empty($params) && $params != '') {

                /**
                 * @var TYPE_NAME $accessToken
                 */

                $accessToken = $this->_getAccessHeader();

                /* Create curl factory */
                $httpAdapter = $this->curlFactory->create();

                $postFields = '';

                if ($method == 'GET') {
                    $end_point .= '?' . http_build_query($params);
                }

                if ($method == 'POST') {
                    $postFields = $this->jsonHelper->serialize($params);
                }

                $headers = ['authorizationToken:' . $accessToken];
                $httpAdapter->write($method, $end_point, '1.1', $headers, $postFields);
                return $httpAdapter->read();
            } else {
                $this->logger->error(__('Request don\'t have data.'));
            }
            $this->logger->error(__('Something went wrong.'));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return false;
    }

    /**
     * This method is used to create the access token
     *
     * @return array|mixed
     */
    private function _getAccessHeader()
    {
        $tokenResponse = $this->data_helper->getAccessToken();
        $json_response = $this->jsonHelper->unserialize($tokenResponse);

        if (isset($json_response['code']) && $json_response['code'] != '') {
            $this->logger->error(__('Unable to get the token', $json_response['body']));
            return $tokenResponse;
        } else {
            return $json_response['token'];
        }
    }
    /**
     * This method is used to create Transaction Unique ID uuid
     * identification which returns the hexadecimal string
     *
     * @return string
     * @throws \Exception
     */
    public function generateUuid()
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}