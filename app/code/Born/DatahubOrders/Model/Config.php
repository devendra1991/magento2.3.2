<?php
/**
 * Created by PhpStorm.
 * User: swadeshranjan.s@wearefmg.net
 * Date: 7/6/19
 * Time: 4:09 PM
 */
namespace Born\DatahubOrders\Model;

use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterfaceAlias;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
* Class Config
* @package Born\DatahubOrders\Model
*/
class Config
{
    const ENABLE = 'born_dbi_extension/born_dbi_order_export/enable';
    const DEBUG_MODE = 'born_dbi_extension/born_dbi_order_export/debug';
    const API_ENDPOINT_URL = 'born_dbi_extension/born_dbi_order_export/endpoint_url';

    /**
     * Data constructor.
     * @param    ScopeConfigInterfaceAlias  $scopeConfig
     */
    public function __construct(
        ScopeConfigInterfaceAlias $scopeConfig,
        LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getEnable()
    {
        return (int) $this->scopeConfig->getValue(
            self::ENABLE, ScopeInterface::SCOPE_STORE
        );
    }
    /**
     * @return mixed
     */
    public function getDebugMode()
    {
        return (int) $this->scopeConfig->getValue(
            self::DEBUG_MODE, ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     * string
     * get live config url
     */
    public function getLiveUrl()
    {
        return $this->scopeConfig->getValue(
            self::API_ENDPOINT_URL, ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $data
     */
    public function setDebugValue($data)
    {
        if ($this->getDebugMode()) {
            $this->logger->debug($data);
        }
    }

}