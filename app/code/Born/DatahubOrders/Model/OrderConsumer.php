<?php
/**
 * Created by PhpStorm.
 * User: swadeshranjan.s@wearefmg.net
 * Date: 22/5/19
 * Time: 4:34 PM
 */

namespace Born\DatahubOrders\Model;


use Magento\Framework\App\ResourceConnection;
use Magento\Framework\MessageQueue\CallbackInvoker;
use Magento\Framework\MessageQueue\ConsumerConfigurationInterface;
use Magento\Framework\MessageQueue\ConsumerInterface;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\MessageController;
use Magento\Framework\MessageQueue\QueueInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Born\DatahubOrders\Model\Config;
use Born\DatahubOrders\Model\OrderExportApi;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

/**
 * Class OrderConsumer
 *
 * @package Born\DatahubOrders\Model
 */
class OrderConsumer implements ConsumerInterface
{
    /**
     * @var \Born\DatahubOrders\Model\OrderExportApi
     */
    private $orderExport;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SerializerJson
     */
    private $json;

    /**
     * Initialize dependencies.
     *
     * @param CallbackInvoker $invoker CallbackInvoker
     * @param ResourceConnection $resource ResourceConnection
     * @param MessageController $messageController MessageController
     * @param ConsumerConfigurationInterface $configuration ConsumerConfigurationInterface
     * @param LoggerInterface $logger LoggerInterface
     * @param \Born\DatahubOrders\Model\Config $config
     */
    public function __construct(
        CallbackInvoker $invoker,
        ResourceConnection $resource,
        MessageController $messageController,
        ConsumerConfigurationInterface $configuration,
        LoggerInterface $logger,
        Config $config,
        OrderExportApi $orderExport,
        OrderRepositoryInterface $orderRepository,
        SerializerJson $json
    )
    {
        $this->invoker = $invoker;
        $this->resource = $resource;
        $this->messageController = $messageController;
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->config = $config;
        $this->orderExport = $orderExport;
        $this->orderRepository = $orderRepository;
        $this->json = $json;
    }

    /**
     * Connects to a queue, consumes a message on the queue,
     * and invoke a method to process the message contents.
     * @param int|null $maxNumberOfMessages
     * if not specified - process all queued incoming messages and terminate,
     * otherwise terminate execution after processing the specified number of messages
     * @return void
     * @since 102.0.1
     */
    public function process($maxNumberOfMessages = null)
    {
        $queue = $this->configuration->getQueue();
        if (!isset($maxNumberOfMessages)) {
            $queue->subscribe($this->getTransactionCallback($queue));
        } else {
            $this->invoker->invoke
            ($queue, $maxNumberOfMessages, $this->getTransactionCallback($queue));
        }
    }

    /**
     * @param QueueInterface $queue
     * @return \Closure
     */
    private function getTransactionCallback(QueueInterface $queue)
    {
        return function (EnvelopeInterface $message) use ($queue) {
            /** @var Order $order */
            $lock = null;
            try {
                $lock = $this->messageController->lock($message, $this->configuration->getConsumerName());
                $messageBody = $message->getBody();
                //$this->config->setDebugValue($messageBody);
                $orderArray = $this->json->unserialize($this->json->unserialize($messageBody));

                /**
                 * $this->processQueueMsg->process() use for process message which you publish in queue
                 * $data = $this->processQueueMsg->($message);
                 */

                $response = $this->orderExport->orderExport('POST', $orderArray);
                $data = $this->json->unserialize($response);
                $transactionUuid = $data['transactionUuid'] ?? null;
                if (!empty($transactionUuid)) {
                    $orderId = $data['transaction']['transactionHeader']['transactionExternalId'];
                    $order = $this->orderRepository->get($orderId);
                    $order->setData('transaction_uuid', $transactionUuid);
                    $this->orderRepository->save($order);
                } else {
                    if (!empty($data['errorDetails'])) {
                        $orderId = $orderArray['transaction']['transactionHeader']['transactionExternalId'];
                        $order = $this->orderRepository->get($orderId);
                        $order->addCommentToStatusHistory('DataHub Warning : ' . implode(",\n", $data['errorDetails']), true);
                        $this->orderRepository->save($order);
                    }
                }

                if (isset($data['response']) && $data['response'] === false) {
                    $queue->reject($message); // if get error in message process
                }
                $queue->acknowledge($message); // send acknowledge to queue
            } catch (MessageLockException $exception) {
                $queue->acknowledge($message);
            } catch (ConnectionLostException $e) {
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            } catch (NotFoundException $e) {
                $queue->acknowledge($message);
                $this->logger->warning($e->getMessage());
            } catch (\Exception $e) {
                $queue->reject($message, false, $e->getMessage());
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            }
        };
    }
}