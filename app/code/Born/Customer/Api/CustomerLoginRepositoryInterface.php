<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Api;

/**
 * Class CustomerLoginRepositoryInterface
 *
 * @category  PHP
 * @package   Born\Customer\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CustomerLoginRepositoryInterface
{
    /**
     * Save CustomerLogin
     *
     * @param Data\CustomerLoginInterface $customerLogin CustomerLoginInterface
     *
     * @return \Born\Customer\Api\Data\CustomerLoginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        Data\CustomerLoginInterface $customerLogin
    );

    /**
     * Retrieve CustomerLogin
     *
     * @param string $customerLoginId CustomerLogin
     *
     * @return \Born\Customer\Api\Data\CustomerLoginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($customerLoginId);

    /**
     * Retrieve CustomerLogin matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria SearchCriteria
     *
     * @return \Born\Customer\Api\Data\CustomerLoginSearchResultsInterface
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete CustomerLogin
     *
     * @param Data\CustomerLoginInterface $customerLogin CustomerLoginInterface
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        Data\CustomerLoginInterface $customerLogin
    );

    /**
     * Delete CustomerLogin by ID
     *
     * @param string $customerLoginId CustomerLogin
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customerLoginId);
}
