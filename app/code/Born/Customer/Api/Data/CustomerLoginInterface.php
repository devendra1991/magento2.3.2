<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Api\Data;

/**
 * Interface CustomerLoginInterface
 *
 * @category  PHP
 * @package   Born\Customer\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CustomerLoginInterface
{
    /**
     * CUSTOMER_ID customer_id
     */
    const CUSTOMER_ID = 'customer_id';
    /**
     * SESSION_ID session_id
     */
    const SESSION_ID = 'session_id';
    /**
     * ENTITY_ID entity_id
     */
    const ENTITY_ID = 'entity_id';
    /**
     * TOKEN token
     */
    const TOKEN = 'token';

    /**
     * Get entity_id
     *
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     *
     * @param string $entityId Set Entity id
     *
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get customer_id
     *
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     *
     * @param string $customerId Set Customer Id
     *
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get token
     *
     * @return string|null
     */
    public function getToken();

    /**
     * Set token
     *
     * @param string $token Set Token
     *
     * @return $this
     */
    public function setToken($token);

    /**
     * Get session_id
     *
     * @return string|null
     */
    public function getSessionId();

    /**
     * Set session_id
     *
     * @param string $sessionId Set Session Id
     *
     * @return $this
     */
    public function setSessionId($sessionId);
}
