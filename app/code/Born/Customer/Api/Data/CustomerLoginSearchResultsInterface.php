<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;
/**
 * Interface CustomerLoginSearchResultsInterface
 *
 * @category  PHP
 * @package   Born\Customer\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CustomerLoginSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get CustomerLogin list.
     *
     * @return \Born\Customer\Api\Data\CustomerLoginInterface[]
     */
    public function getItems();

    /**
     * Set customer_id list.
     *
     * @param \Born\Customer\Api\Data\CustomerLoginInterface[] $items Set Items
     *
     * @return $this
     */
    public function setItems(array $items);
}
