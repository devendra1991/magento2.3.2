<?php
/**
 * CustomerOptStatus ViewModel Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_Customer
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\ViewModel;

use Born\DbiNewsletter\Model\ResourceModel\Newsletter\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class CustomerOptStatus
 * @package Born\Customer\ViewModel
 */

class CustomerOptStatus implements ArgumentInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var UrlInterface
     */
    private $urlInterface;

    /**
     * CustomerOptStatus constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        UrlInterface $urlInterface
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->urlInterface = $urlInterface;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSelectedOptions()
    {
        $userId = $this->getLoggedinCustomerId();
        $customerObject = $this->customerRepository->getById($userId);

        if (!$customerObject->getCustomAttribute('newsletter_options')) {
            return false;
        }

        $newsletter_options = $customerObject->getCustomAttribute(
            'newsletter_options'
        )->getValue();
        $collection = $this->collectionFactory->create()
            ->addFieldToFilter("newsletter_id", ['in' => $newsletter_options]);
        return $collection;
    }

    public function getAllOptions()
    {
        $collection = $this->collectionFactory->create()->load();
        return $collection;
    }

    /**
     * @return bool|int|null
     */
    public function getLoggedinCustomerId()
    {
        if ($this->customerSession->isLoggedIn()) {
            return $this->customerSession->getId();
        }
        return false;
    }

    public function getSaveUrl()
    {
        return $this->urlInterface->getUrl('customer/');
    }
}
