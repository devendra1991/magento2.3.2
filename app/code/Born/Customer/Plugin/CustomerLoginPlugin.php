<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Plugin;

use Born\Customer\Model\CustomerLoginFactory;
use Born\Customer\Model\ResourceModel\CustomerLogin as ResourceCustomerLogin;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Session;

/**
 * Class SetDefaultConfig
 *
 * @category  PHP
 * @package   Born\Customer\Plugin
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerLoginPlugin
{
    /**
     * AccountManagementInterface
     *
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * CustomerSession
     *
     * @var Session
     */
    protected $session;

    /**
     * CustomerLoginFactory
     *
     * @var CustomerLoginFactory
     */
    protected $customerLoginFactory;

    /**
     * ResourceCustomerLogin
     *
     * @var ResourceCustomerLogin
     */
    protected $resource;
    /**
     * CustomerLoginPlugin constructor.
     *
     * @param AccountManagementInterface $customerAccountManagement AccountManagementInterface
     * @param Session                    $session                   Session
     * @param CustomerLoginFactory       $customerLoginFactory      CustomerLoginFactory
     * @param ResourceCustomerLogin      $resource                  ResourceCustomerLogin
     */
    public function __construct(
        AccountManagementInterface $customerAccountManagement,
        Session $session,
        CustomerLoginFactory $customerLoginFactory,
        ResourceCustomerLogin $resource
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->session = $session;
        $this->customerLoginFactory = $customerLoginFactory;
        $this->resource = $resource;
    }

    /**
     * After Successfully generate the Token,
     * Able to Login the Customer and maintained the Customer Session
     *
     * @param \Magento\Integration\Model\CustomerTokenService $subject  PluginClass
     * @param string                                          $result   Generated Customer Token
     * @param string                                          $username Customer Email Id
     * @param string                                          $password Customer Password
     *
     * @return string                                          $result
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterCreateCustomerAccessToken(
        \Magento\Integration\Model\CustomerTokenService $subject,
        $result,
        $username,
        $password
    ) {
        $customer = $this->customerAccountManagement
            ->authenticate($username, $password);
        $customerId = $customer->getId();
        $this->session->setCustomerDataAsLoggedIn($customer);
        $this->session->regenerateId();
        $sessionId = $this->session->getSessionId();
        if ($this->session->isLoggedIn()) {
            $customerLoginData = [
                'customer_id' => $customerId,
                'token' => $result,
                'session_id' => $sessionId
            ];
            $customerLoginModel = $this->customerLoginFactory->create()
                ->setData($customerLoginData);
            try {
                $this->resource->save($customerLoginModel);
            } catch (\Exception $exception) {
                throw new CouldNotSaveException(
                    __(
                        'Could not save the customerLogin: %1',
                        $exception->getMessage()
                    )
                );
            }
        }
        return $result;
    }
}
