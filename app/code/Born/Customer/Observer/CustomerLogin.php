<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_Customer
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\Customer\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Class CustomerLogin
 *
 * @category  PHP
 * @package   Born\Customer\Observer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerLogin implements ObserverInterface
{
    /**
     * Set Cookie Name
     *
     * COOKIE_NAME
     */
    const COOKIE_NAME = 'cust_dh_id';

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;
    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;
    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * CustomerLogin constructor.
     *
     * @param CookieManagerInterface  $cookieManager
     * @param CookieMetadataFactory   $cookieMetadataFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer(); //Get customer object
        $this->set($customer->getCustomerUuid());
    }

    /**
     * @param $value
     * @param int   $duration
     */
    public function set($value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setHttpOnly(true);

        $this->cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $value,
            $metadata
        );
    }
}
