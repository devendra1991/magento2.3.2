<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Model\ResourceModel\CustomerLogin;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
/**
 * Class CustomerLogin
 *
 * @category  PHP
 * @package   Born\Customer\Model\ResourceModel\CustomerLogin
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Born\Customer\Model\CustomerLogin::class,
            \Born\Customer\Model\ResourceModel\CustomerLogin::class
        );
    }
}
