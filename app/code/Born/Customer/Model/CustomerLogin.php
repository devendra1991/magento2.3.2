<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Model;

use Magento\Framework\Api\DataObjectHelper;
use Born\Customer\Api\Data\CustomerLoginInterfaceFactory;

/**
 * Class CustomerLogin
 *
 * @category  PHP
 * @package   Born\Customer\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerLogin extends \Magento\Framework\Model\AbstractModel
{
    /**
     * EventPrefix
     *
     * @var string EventPrefix
     */
    protected $eventPrefix = 'born_customerlogin_customerlogin';
    /**
     * CustomerLoginInterfaceFactory
     *
     * @var CustomerLoginInterfaceFactory
     */
    protected $customerLoginDataFactory;
    /**
     * DataObjectHelper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * CustomerLogin constructor.
     *
     * @param \Magento\Framework\Model\Context       $context                  Context
     * @param \Magento\Framework\Registry            $registry                 Registry
     * @param CustomerLoginInterfaceFactory          $customerLoginDataFactory CustomerLoginInterfaceFactory
     * @param DataObjectHelper                       $dataObjectHelper         DataObjectHelper
     * @param ResourceModel\CustomerLogin            $resource                 CustomerLogin
     * @param ResourceModel\CustomerLogin\Collection $resourceCollection       Collection
     * @param array                                  $data                     Data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CustomerLoginInterfaceFactory $customerLoginDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\CustomerLogin $resource,
        ResourceModel\CustomerLogin\Collection $resourceCollection,
        array $data = []
    ) {
        $this->customerLoginDataFactory = $customerLoginDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve customerLogin model with customerLogin data
     *
     * @return mixed
     */
    public function getDataModel()
    {
        $customerLoginData = $this->getData();
        $customerLoginDataObject = $this->customerLoginDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $customerLoginDataObject,
            $customerLoginData,
            CustomerLoginInterface::class
        );
        return $customerLoginDataObject;
    }
}
