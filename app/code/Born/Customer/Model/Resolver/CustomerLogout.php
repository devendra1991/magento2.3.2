<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Model\Resolver;

use Born\Customer\Model\CustomerLoginFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Born\Customer\Model\ResourceModel\CustomerLogin as ResourceCustomerLogin;
use Born\Customer\Api\CustomerLoginRepositoryInterface;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;

/**
 * Class SetDefaultConfig
 *
 * @category  PHP
 * @package   Born\Customer\Plugin
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerLogout implements ResolverInterface
{
    /**
     * Session
     *
     * @var Session
     */
    protected $session;

    /**
     * CustomerLoginFactory
     *
     * @var CustomerLoginFactory
     */
    protected $customerLoginFactory;

    /**
     * ResourceCustomerLogin
     *
     * @var ResourceCustomerLogin
     */
    protected $resource;

    /**
     * CustomerLoginRepositoryInterface
     *
     * @var CustomerLoginRepositoryInterface
     */
    protected $customerLoginRepository;

    /**
     * CustomerLogout constructor.
     *
     * @param Session                          $customerSession         Session
     * @param CustomerLoginFactory             $customerLoginFactory    CustomerLoginFactory
     * @param ResourceCustomerLogin            $resource                ResourceCustomerLogin
     * @param CustomerLoginRepositoryInterface $customerLoginRepository CustomerLoginRepositoryInterface
     */
    public function __construct(
        Session $customerSession,
        CustomerLoginFactory $customerLoginFactory,
        ResourceCustomerLogin $resource,
        CustomerLoginRepositoryInterface $customerLoginRepository
    ) {
        $this->session = $customerSession;
        $this->customerLoginFactory = $customerLoginFactory;
        $this->resource = $resource;
        $this->customerLoginRepository = $customerLoginRepository;
    }

    /**
     * Resolver Class
     *
     * @param Field                                                      $field   Field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context ContextInterface
     * @param ResolveInfo                                                $info    ResolveInfo
     * @param array|null                                                 $value   Value
     * @param array|null                                                 $args    Arguments
     *
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['input']) || !is_array($args['input']) || empty($args['input'])) {
            throw new GraphQlInputException(__('"input" value should be specified'));
        }

        $token = $args['input']['token'];
        $customerLogin = $this->customerLoginFactory->create();
        $this->resource->load($customerLogin, $token, 'token');
        $tokenSessionData = $customerLogin->getData();

        $result= [];
        if (!empty($tokenSessionData)) {
            if ($tokenSessionData['customer_id']) {
                $this->session->logout();
                $this->resource->load(
                    $customerLogin,
                    $tokenSessionData['entity_id']
                );
                try {
                    $this->resource->delete($customerLogin);
                    $result = [
                        'customer_id' => $tokenSessionData['customer_id'],
                        'result' => true
                    ];

                    return $result;
                } catch (\Exception $exception) {
                    throw new CouldNotDeleteException(
                        __(
                            'Could not delete the CustomerLogin: %1',
                            $exception->getMessage()
                        )
                    );
                }
            }
        } else {
            throw new GraphQlInputException(
                __(
                    'Customer is already logout, Token is not exists.'
                )
            );
        }
    }
}
