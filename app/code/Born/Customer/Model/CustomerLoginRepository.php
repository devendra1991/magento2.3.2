<?php
/**
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\Customer\Model;

use Born\Customer\Model\ResourceModel\CustomerLogin\CollectionFactory as CustomerLoginCollectionFactory;
use Born\Customer\Model\ResourceModel\CustomerLogin as ResourceCustomerLogin;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Born\Customer\Api\Data\CustomerLoginSearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Born\Customer\Api\CustomerLoginRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Born\Customer\Api\Data\CustomerLoginInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

/**
 * Class CustomerLoginRepository
 *
 * @category  PHP
 * @package   Born\Customer\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerLoginRepository implements CustomerLoginRepositoryInterface
{
    /**
     * ResourceCustomerLogin
     *
     * @var ResourceCustomerLogin
     */
    protected $resource;
    /**
     * JoinProcessorInterface
     *
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * ExtensibleDataObjectConverter
     *
     * @var ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;
    /**
     * CustomerLoginCollectionFactory
     *
     * @var CustomerLoginCollectionFactory
     */
    protected $customerLoginCollectionFactory;
    /**
     * CustomerLoginFactory
     *
     * @var CustomerLoginFactory
     */
    protected $customerLoginFactory;
    /**
     * DataObjectProcessor
     *
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;
    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    private $_storeManager;
    /**
     * CollectionProcessorInterface
     *
     * @var CollectionProcessorInterface
     */
    private $_collectionProcessor;
    /**
     * DataObjectHelper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;
    /**
     * CustomerLoginInterfaceFactory
     *
     * @var CustomerLoginInterfaceFactory
     */
    protected $dataCustomerLoginFactory;
    /**
     * CustomerLoginSearchResultsInterfaceFactory
     *
     * @var CustomerLoginSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * CustomerLoginRepository constructor.
     *
     * @param ResourceCustomerLogin                      $resource                         ResourceCustomerLogin
     * @param CustomerLoginFactory                       $customerLoginFactory             CustomerLoginFactory
     * @param CustomerLoginInterfaceFactory              $dataCustomerLoginFactory         CustomerLoginInterfaceFactory
     * @param CustomerLoginCollectionFactory             $customerLoginCollectionFactory   CustomerLoginCollectionFactory
     * @param CustomerLoginSearchResultsInterfaceFactory $searchResultsFactory             CustomerLoginSearchResultsInterfaceFactory
     * @param DataObjectHelper                           $dataObjectHelper                 DataObjectHelper
     * @param DataObjectProcessor                        $dataObjectProcessor              DataObjectProcessor
     * @param StoreManagerInterface                      $storeManager                     StoreManagerInterface
     * @param CollectionProcessorInterface               $collectionProcessor              CollectionProcessorInterface
     * @param JoinProcessorInterface                     $extensionAttributesJoinProcessor JoinProcessorInterface
     * @param ExtensibleDataObjectConverter              $extensibleDataObjectConverter    ExtensibleDataObjectConverter
     */
    public function __construct(
        ResourceCustomerLogin $resource,
        CustomerLoginFactory $customerLoginFactory,
        CustomerLoginInterfaceFactory $dataCustomerLoginFactory,
        CustomerLoginCollectionFactory $customerLoginCollectionFactory,
        CustomerLoginSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->customerLoginFactory = $customerLoginFactory;
        $this->customerLoginCollectionFactory = $customerLoginCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCustomerLoginFactory = $dataCustomerLoginFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->_storeManager = $storeManager;
        $this->_collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * Save CustomerLogin
     *
     * @param \Born\Customer\Api\Data\CustomerLoginInterface $customerLogin CustomerLoginInterface
     *
     * @return \Born\Customer\Api\Data\CustomerLoginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Born\Customer\Api\Data\CustomerLoginInterface $customerLogin
    ) {
        $customerLoginData = $this->extensibleDataObjectConverter->toNestedArray(
            $customerLogin,
            [],
            \Born\Customer\Api\Data\CustomerLoginInterface::class
        );
        $customerLoginModel = $this->customerLoginFactory->create()
            ->setData($customerLoginData);
        try {
            $this->resource->save($customerLoginModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the customerLogin: %1',
                    $exception->getMessage()
                )
            );
        }
        return $customerLoginModel->getDataModel();
    }

    /**
     * Retrieve CustomerLogin
     *
     * @param string $customerLoginId CustomerLogin
     *
     * @return \Born\Customer\Api\Data\CustomerLoginInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($customerLoginId)
    {
        $customerLogin = $this->customerLoginFactory->create();
        $this->resource->load($customerLogin, $customerLoginId);
        if (!$customerLogin->getId()) {
            throw new NoSuchEntityException(
                __(
                    'CustomerLogin with id "%1" does not exist.',
                    $customerLoginId
                )
            );
        }
        return $customerLogin->getDataModel();
    }

    /**
     * Retrieve CustomerLogin matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria SearchCriteria
     *
     * @return \Born\Customer\Api\Data\CustomerLoginSearchResultsInterface
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->customerLoginCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Born\Customer\Api\Data\CustomerLoginInterface::class
        );
        $this->_collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete CustomerLogin
     *
     * @param \Born\Customer\Api\Data\CustomerLoginInterface $customerLogin CustomerLoginInterface
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Born\Customer\Api\Data\CustomerLoginInterface $customerLogin
    ) {
        try {
            $customerLoginModel = $this->customerLoginFactory->create();
            $this->resource->load(
                $customerLoginModel,
                $customerLogin->getCustomerloginId()
            );
            $this->resource->delete($customerLoginModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the CustomerLogin: %1',
                    $exception->getMessage()
                )
            );
        }
        return true;
    }

    /**
     * Delete CustomerLogin by ID
     *
     * @param string $customerLoginId CustomerLogin
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customerLoginId)
    {
        return $this->delete($this->getById($customerLoginId));
    }
}
