<?php
/**
 * Index Controller Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_Customer
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\Customer\Controller\Index;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

/**
 * Class Index
 * @package Born\Customer\Controller\Index
 */

class Index extends Action
{
    protected $customer;

    protected $customerFactory;
    /**
     * @var Session
     */
    private $session;

    /**
     * Save constructor.
     * @param Context $context
     * @param Customer $customer
     * @param CustomerFactory $customerFactory
     * @param Session $session
     */
    public function __construct(
        Context $context,
        Customer $customer,
        CustomerFactory $customerFactory,
        Session $session
    ) {
        $this->customer = $customer;
        $this->customerFactory = $customerFactory;
        $this->session = $session;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $post = $this->getRequest()->getPostValue();
        $customer = $this->customer->load($this->session->getId());
        $customerData = $customer->getDataModel();
        if (isset($post['optin'])) {
            $data = implode(',', $post['optin']);
            $customerData->setCustomAttribute('newsletter_options', $data);
            $message = __(
                'You have successfully optin newsletter .'
            );
            $this->messageManager->addSuccessMessage($message);
        } elseif (isset($post['optOutAll'])) {
            $customerData->setCustomAttribute('newsletter_options', "");
            $message = __(
                'You have successfully optout all newsletter .'
            );
            $this->messageManager->addSuccessMessage($message);
        } else {
            $message = __(
                'Something went wrong, Please try again.'
            );
            $this->messageManager->addErrorMessage($message);
        }
        $customer->updateData($customerData);
        $customerResource = $this->customerFactory->create();
        $customerResource->saveAttribute($customer, 'newsletter_options');

        $resultRedirect->setPath('customer/newsletter/newsletter/');
        return $resultRedirect;
    }
}
