/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_EstimateDelivery
 *
 * PHP version 5.x-7.x
 *
 * @category  JS
 * @package   Born\EstimateDelivery
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/template/summary/item/details.html':
                'Born_EstimateDelivery/template/summary/item/details.html'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/select-shipping-method': {
                'Born_EstimateDelivery/js/action/select-shipping-method-mixin': true
            },
            'Magento_Checkout/js/action/select-payment-method': {
                'Born_EstimateDelivery/js/action/select-payment-method-mixin': true
            }
        }
    }
};
