/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_EstimateDelivery
 *
 * PHP version 5.x-7.x
 *
 * @category  JS
 * @package   Born\EstimateDelivery
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

define(
    [
    'jquery',
    'Magento_Checkout/js/model/quote'
    ], function ($, quote) {
        'use strict';
        return function (target) {
            return function (shippingMethod) {
                // call original method
                target(shippingMethod);
                var carrier_code = quote.shippingMethod() ? quote.shippingMethod().carrier_code: null;
                if (carrier_code === 'expedited') {
                    carrier_code = "twoDayAir";
                }
                if (carrier_code === 'express') {
                    carrier_code = "nextDayAir";
                }
                var quoteItemData = window.checkoutConfig.quoteItemData;
                var additionalOptions = ['standard', 'twoDayAir', 'nextDayAir', 'specialOrder'];
                for (var key in quoteItemData) {
                    // skip loop if the property is from prototype
                    if (!quoteItemData.hasOwnProperty(key)) { continue;
                    }
                    var obj = quoteItemData[key];
                    var options = obj.options;
                    options.forEach(
                        function (value, key) {
                            if (carrier_code !== null) {
                                if (additionalOptions.includes(value.label)) {
                                    if (value.label !== carrier_code) {
                                        $('.born_carrier_code_'+ value.label).hide();
                                        $('.born_carrier_code_'+ carrier_code).show();
                                    }
                                }
                                if (value.label == 'specialOrder' || !(additionalOptions.includes(value.label))) {
                                    $('.born_carrier_code_'+ value.label).show();
                                }
                            }
                        }
                    );
                }
            };
        }
    }
);