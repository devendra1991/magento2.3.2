<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_EstimateDelivery
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\EstimateDelivery
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\EstimateDelivery\Plugin;

use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class QuoteItemToOrderItemPlugin
 *
 * @category  PHP
 * @package   Born\EstimateDelivery\Plugin
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class QuoteItemToOrderItemPlugin
{
    /**
     * SerializerInterface
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * QuoteItemToOrderItemPlugin constructor.
     *
     * @param SerializerInterface $serializer SerializerInterface
     */
    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    /**
     * Plugin that Convert Quote Item to Order Item.
     *
     * @param \Magento\Quote\Model\Quote\Item\ToOrderItem $subject   ToOrderItem
     * @param callable                                    $proceed   callable
     * @param mixed                                       $quoteItem QuoteItem
     * @param array                                       $data      Data
     *
     * @return mixed
     */
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        callable $proceed,
        $quoteItem,
        $data
    ) {
        // get order item
        $orderItem = $proceed($quoteItem, $data);
        if (!$orderItem->getParentItemId() && $orderItem->getProductType() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
            if ($additionalOptionsQuote = $quoteItem->getOptionByCode('additional_options')) {
                //To do
                // - check to make sure element are not added twice
                // - $additionalOptionsQuote - may not be an array
                $this->convertItemToOrderOption($orderItem, $additionalOptionsQuote);
            }
        }

        return $orderItem;
    }

    /**
     * Convert Quote Item to Order Item.
     *
     * @param mixed $orderItem              OrderItem
     * @param array $additionalOptionsQuote AdditionOptions
     *
     * @return void
     */
    public function convertItemToOrderOption($orderItem, $additionalOptionsQuote)
    {
        if ($additionalOptionsOrder = $orderItem->getProductOptionByCode('additional_options')) {
            $additionalOptions = array_merge($additionalOptionsQuote, $additionalOptionsOrder);
        } else {
            $additionalOptions = $additionalOptionsQuote;
        }

        if (!empty($this->serializer->unserialize($additionalOptions->getValue()))) {
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = $this->serializer->unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
    }
}