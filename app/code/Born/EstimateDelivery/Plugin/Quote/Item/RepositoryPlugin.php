<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_EstimateDelivery
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\EstimateDelivery
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\EstimateDelivery\Plugin\Quote\Item;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Item\Repository;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\ResourceModel\Quote\Item as ItemResource;

/**
 * Class RepositoryPlugin
 *
 * @category  PHP
 * @package   Born\EstimateDelivery\Plugin\Quote\Item
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class RepositoryPlugin
{
    /**
     * SerializerInterface
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * ItemFactory
     *
     * @var ItemFactory
     */
    protected $itemFactory;

    /**
     * ItemResource
     *
     * @var ItemResource
     */
    protected $itemResource;

    /**
     * Repository
     *
     * @var Repository
     */
    protected $repository;

    /**
     * QuoteFactory
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * RepositoryPlugin constructor.
     *
     * @param SerializerInterface $serializer   SerializerInterface
     * @param ItemFactory         $itemFactory  ItemFactory
     * @param ItemResource        $itemResource ItemResource
     * @param Repository          $repository   Repository
     * @param QuoteFactory        $quoteFactory QuoteFactory
     */
    public function __construct(
        SerializerInterface $serializer,
        ItemFactory $itemFactory,
        ItemResource $itemResource,
        Repository $repository,
        QuoteFactory $quoteFactory
    ) {
        $this->serializer = $serializer;
        $this->itemFactory = $itemFactory;
        $this->itemResource = $itemResource;
        $this->repository = $repository;
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * Plugin Save the info_buyrequest to addtioanl_data on Item Level
     *
     * @param Repository                                $subject  Repository
     * @param mixed                                     $result   Plugin Result
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem CartItemInterface
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterSave(
        \Magento\Quote\Model\Quote\Item\Repository $subject,
        $result,
        \Magento\Quote\Api\Data\CartItemInterface $cartItem
    ) {
        $itemId = $result->getItemId();
        $quoteId  = $result->getQuoteId();
        $quoteItemModel = $this->quoteFactory->create();
        $quote = $quoteItemModel->load($quoteId);
        $item = $quote->getItemById($itemId);
        $additionalOptions = $options = [];
        if ($item) {
            if ($result->getProductOption()) {
                $option = $result->getProductOption()->getExtensionAttributes()->getCustomOptions();
                if ($additionalOption = $item->getOptionByCode('additional_options')) {
                    $additionalOptions = (array) unserialize($additionalOption->getValue());
                }
                foreach ($option as $key => $value) {
                    $options[$value->getOptionId()] = $value->getOptionValue();
                    $additionalOptions[] = [
                        'label' => $value->getOptionId(),
                        'value' => $value->getOptionValue()
                    ];
                }
            }
            if (count($additionalOptions) > 0) {
                $item->addOption(
                    [
                        'code' => 'additional_options',
                        'value' => $this->serializer->serialize($additionalOptions)
                    ]
                );
            }
            $data = ['options' => $options, 'qty' => $result->getQty()];
            $additionalData = $this->serializer->serialize($data);
            $item->setAdditionalData($additionalData);
            $this->itemResource->save($item);
        }

        return $result;
    }
}
