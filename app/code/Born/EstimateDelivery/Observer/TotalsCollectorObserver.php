<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_EstimateDelivery
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\EstimateDelivery
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\EstimateDelivery\Observer;

use Magento\Checkout\Model\Cart as CheckoutCart;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\QuoteIdToMaskedQuoteId;

/**
 * Class TotalsCollectorObserver
 *
 * @category  PHP
 * @package   Born\EstimateDelivery\Observer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class TotalsCollectorObserver implements ObserverInterface
{
    /**
     * CheckoutCart
     *
     * @var CheckoutCart
     */
    protected $cart;

    /**
     * SerializerInterface
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * UrlInterface
     *
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * QuoteIdToMaskedQuoteId
     *
     * @var QuoteIdToMaskedQuoteId
     */
    protected $quoteIdToMaskedQuoteId;

    /**
     * CustomerSession
     *
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * TotalsCollectorObserver constructor.
     *
     * @param CheckoutCart           $cart                   CheckoutCart
     * @param SerializerInterface    $serializer             SerializerInterface
     * @param UrlInterface           $urlInterface           UrlInterface
     * @param QuoteIdToMaskedQuoteId $quoteIdToMaskedQuoteId QuoteIdToMaskedQuoteId
     * @param CustomerSession        $customerSession        CustomerSession
     */
    public function __construct(
        CheckoutCart $cart,
        SerializerInterface $serializer,
        UrlInterface $urlInterface,
        QuoteIdToMaskedQuoteId $quoteIdToMaskedQuoteId,
        CustomerSession $customerSession
    ) {
        $this->cart = $cart;
        $this->serializer = $serializer;
        $this->urlInterface = $urlInterface;
        $this->quoteIdToMaskedQuoteId = $quoteIdToMaskedQuoteId;
        $this->customerSession = $customerSession;
    }

    /**
     * Observer Collect the quote Itema and update Additional_options based on Shipping Method
     *
     * @param Observer $observer Observer
     *
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        $currentUrl = $this->urlInterface->getCurrentUrl();
        $baseUrl = $this->urlInterface->getBaseUrl();
        $callObserver = false;
        if ($this->customerSession->isLoggedIn()) {
            $shippingUrl = $baseUrl . 'rest/default/V1/carts/mine/shipping-information';
            $paymentUrl = $baseUrl . 'rest/default/V1/carts/mine/payment-information';
            if ($currentUrl == $shippingUrl || $currentUrl == $paymentUrl) {
                $callObserver = true;
            }
        } else {
            $maskId = '';
            if ($quote->getId()) {
                $maskId = $this->quoteIdToMaskedQuoteId->execute($quote->getId());
            }
            $shippingUrl = $baseUrl . 'rest/default/V1/guest-carts/' . $maskId . '/shipping-information';
            $paymentUrl = $baseUrl . 'rest/default/V1/guest-carts/' . $maskId . '/payment-information';
            if ($currentUrl == $shippingUrl || $currentUrl == $paymentUrl) {
                $callObserver = true;
            }
        }
        $method = $quote->getShippingAddress()->getShippingMethod();
        $items = $this->cart->getQuote()->getAllItems();
        if ($callObserver) {
            foreach ($items as $item) {
                if ($additionalOption = $item->getOptionByCode('additional_options')) {
                    $additionalOptions = $this->serializer->unserialize($additionalOption->getValue());
                }
                if (!empty($additionalOptions)) {
                    $additionalOptions = $this->setAdditionalOptionByMethod($additionalOptions, $method);
                    $item->addOption(
                        [
                        'code' => 'additional_options',
                        'value' => $this->serializer->serialize($additionalOptions)
                        ]
                    );
                }
            }
        }
    }

    /**
     * Set Additional Option By Selected Shipping method
     *
     * @param array  $additionalOptions AdditionalOptions
     * @param string $method            Shipping Method
     *
     * @return mixed
     */
    public function setAdditionalOptionByMethod($additionalOptions, $method)
    {
        switch ($method) {
        case "standard_standard":
            $methodName = 'standard';
            $additionalOptions = $this->changeAddionalOption($additionalOptions, $methodName);
            break;
        case "expedited_expedited":
            $methodName = 'twoDayAir';
            $additionalOptions = $this->changeAddionalOption($additionalOptions, $methodName);
            break;
        case "express_express":
            $methodName = 'nextDayAir';
            $additionalOptions = $this->changeAddionalOption($additionalOptions, $methodName);
            break;
        default:
            $methodName = 'specialOrder';
            $additionalOptions = $this->changeAddionalOption($additionalOptions, $methodName);
        }

        return $additionalOptions;
    }

    /**
     * Change Additional Options
     *
     * @param array  $additionalOptions AdditionalOptions
     * @param string $methodName        Shipping Method
     *
     * @return mixed
     */
    protected function changeAddionalOption($additionalOptions, $methodName)
    {
        $eddExpected = ['standard', 'twoDayAir', 'nextDayAir', 'specialOrder'];
        $remainElement = 0;
        foreach ($additionalOptions as $key => $options) {
            if ($methodName == $options['label']) {
                $remainElement = $key;
            }
        }

        foreach ($additionalOptions as $key => $options) {
            if ($remainElement !== $key) {
                if (in_array($options['label'], $eddExpected)) {
                    unset($additionalOptions[$key]);
                }
            }
        }

        return $additionalOptions;
    }
}
