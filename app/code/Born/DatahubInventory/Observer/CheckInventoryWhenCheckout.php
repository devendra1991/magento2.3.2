<?php


namespace Born\DatahubInventory\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;


/**
 * Class CheckInventoryWhenCheckout
 * @package Born\DatahubInventory\Observer
 */
class CheckInventoryWhenCheckout implements ObserverInterface
{
    /**
     *
     */
    const REDIRECT_URL = "born_dbi_extension/dbi_inventory_section/dbi_inventory_redirect_url";

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    private $actionFlag;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    private $redirect;


    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    private $checkInventory;

    /**
     * CheckInventoryWhenCheckout constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\ActionFlag $actionFlag
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Born\DatahubInventory\Model\CheckInventory $checkInventory
    )
    {
        $this->messageManager = $messageManager;
        $this->actionFlag = $actionFlag;
        $this->redirect = $redirect;
        $this->scopeConfig = $scopeConfig;
        $this->checkInventory = $checkInventory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $can_place_order = true;
        $messages = [];

        //Check Inventory
        $this->checkInventory->checkInventory($messages, $can_place_order);

        if (!$can_place_order) {
            //add error massages
            // TODO
            // If redirect to external URL ,add error message may not work, will pass messages as param to redirect page
            foreach ($messages as $message) {
                $this->messageManager->addErrorMessage($message);
            }
            $this->messageManager->addErrorMessage("Please change quantity in cart");
            $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $url = $this->scopeConfig->getValue(self::REDIRECT_URL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $observer->getControllerAction()->getResponse()->setRedirect($url)->sendResponse();
            return;

        }

    }

}