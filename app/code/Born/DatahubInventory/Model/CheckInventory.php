<?php


namespace Born\DatahubInventory\Model;


/**
 * Class CheckInventory
 * @package Born\DatahubInventory\Model
 */
class CheckInventory
{
    /**
     *
     */
    CONST REQUESTING_SYSTEM_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_system";

    /**
     *
     */
    CONST REQUESTING_COUNTRY_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_country";

    /**
     *
     */
    CONST DBI_INVENTORY_CHECK_ENABLE = "born_dbi_extension/dbi_inventory_section/enable";

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Born\DbiConnector\Model\InventoryEDDFromDatahub
     */
    private $getFromDatahub;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * CheckInventory constructor.
     * @param \Magento\Checkout\Model\Session $session
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Born\DbiConnector\Model\InventoryEDDFromDatahub $getFromDatahub
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Born\DbiConnector\Model\InventoryEDDFromDatahub $getFromDatahub,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->cart = $cart;
        $this->session = $session;
        $this->messageManager = $messageManager;
        $this->getFromDatahub = $getFromDatahub;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param array $messages
     * @param bool $can_place_order
     */
    public function checkInventory(&$messages = [], &$can_place_order = true)
    {
        $enable = $this->scopeConfig->getValue(self::DBI_INVENTORY_CHECK_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $requestSystem = $this->scopeConfig->getValue(self::REQUESTING_SYSTEM_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $requestCountry = $this->scopeConfig->getValue(self::REQUESTING_COUNTRY_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        // Check Inventory when function set enable at admin
        if ($enable) {
            $items = $this->session->getQuote()->getAllVisibleItems();
            foreach ($items as $item) {
                //Get params send to Datahub
                $params = [];
                $params['requestingSystem'] = $requestSystem;
                $params['requestingCountry'] = $requestCountry;
                $params["skus"] = [$item->getSku()];

                //Send request to Datahub
                $result = $this->getFromDatahub->getinventoryFromDatahub($params);


                //Check result
                if ($result == null) {
                    $can_place_order = false;
                    $messages[] = "Can't get inventory from DataHub , Please try again! ";
                    break;
                }

                //Check Sku and Inventory
                try {
                    if (!$result['sku'] || !$result['available']) {
                        $can_place_order = false;
                        $messages[] = $item->getName() . "  is no longer there";
                    } else {

                        $sum = ($result['dcAvailable'] ?? 0) + ($result['storeAvailable'] ?? 0);
                        if ($item->getQty() > $sum) {
                            $can_place_order = false;
                            if ($sum == 0) {
                                $messages[] = $item->getName() . "  is no longer there";
                            } else {
                                $messages[] = $item->getName() . "  : only " . $sum . " left ";
                            }
                        }
                    }
                } catch (Exception $e) {
                    $can_place_order = false;
                    echo $e->getMessage();
                }
            }

        }
    }


}