<?php
/**
 * NewsletterOptionsData Newsletter GraphQl Class for data provider.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_NewsletterGraphQl
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\NewsletterGraphQl\Model\DataProvider;

use Born\DbiNewsletter\Model\ResourceModel\Newsletter\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Psr\Log\LoggerInterface;

class NewsletterOptionsData
{
    /**
     * @var Newsletter collection Factory
     */
    private $collectionFactory;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * NewsletterOptionsData constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param LoggerInterface $logger
     */

    public function __construct(
        CollectionFactory $collectionFactory,
        LoggerInterface $logger
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
    }

    /**
     * @return array
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     */
    public function getData($used_in): array
    {
        $result = [];
        try {
            $newsletterData = $this->collectionFactory->create();

            if ($used_in == '1') {
                $newsletterData->addFieldToFilter("used_in_checkout", ['eq' => "1"]);
            } else {
                $newsletterData->addFieldToFilter("used_in_registration", ['eq' => "1"]);
            }
            if (!empty($newsletterData->getItems())) {
                $data = [];
                foreach ($newsletterData->getItems() as $record) {
                    $data[] = [
                        'newsletter_id' => $record->getNewsletterId(),
                        'title' => $record->getCompanyName() . '-' . $record->getCategory(),
                        'company_name' => $record->getCompanyName(),
                        'category' => $record->getCategory(),
                        'source' => $record->getSource(),
                        'status_type' => $record->getStatusType(),
                        'status' => ($record->getStatus() == 0) ? 'false' : 'true',
                        'used_in_registration' => $record->getUsedInRegistration(),
                        'used_in_checkout' => $record->getUsedInCheckout()
                    ];
                }
                $result['items'] = $data;
            } else {
                throw new GraphQlInputException(
                    __('Newsletter does not contain the data.')
                );
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(
                __(
                    $e->getMessage()
                )
            );
        }
        return $result;
    }
}
