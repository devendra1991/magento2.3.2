<?php
/**
 * NewsletterOptions Newsletter GraphQl Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_NewsletterGraphQl
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\NewsletterGraphQl\Model\Resolver;

use Born\NewsletterGraphQl\Model\DataProvider\NewsletterOptionsData;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class NewsletterOptions implements ResolverInterface
{
    /**
     * @var NewsletterOptionsData
     */
    private $newsletterOptionsData;

    /**
     * NewsletterOptions constructor.
     *
     * @param NewsletterOptionsData $newsletterOptionsData
     */

    public function __construct(
        NewsletterOptionsData $newsletterOptionsData
    ) {
        $this->newsletterOptionsData = $newsletterOptionsData;
    }

    /**
     * Fetches the data from persistence models and
     * format it according to the GraphQL schema.
     *
     * @param  \Magento\Framework\GraphQl\Config\Element\Field $field
     * @param  ContextInterface $context
     * @param  ResolveInfo $info
     * @param  array|null $value
     * @param  array|null $args
     * @throws \Exception
     * @return mixed|Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        try {
            $argument = $this->getArguments($args);
            $data = $this->newsletterOptionsData->getData($argument);
            return !empty($data) ? $data : [];
        } catch (ValidatorException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        } catch (InputMismatchException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }
    }

    /**
     * @param array $args
     * @return int
     * @throws GraphQlInputException
     */
    private function getArguments(array $args): int
    {
        if ($args['used_in'] == 0 || $args['used_in'] == 1) {
            return (int)$args['used_in'];
        } else {
            throw new GraphQlInputException(
                __('used_in value must be specified as 0 => For register OR 1 => For checkout options category')
            );
        }
    }
}
