<?php
/**
 * Born_CartSummaryGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CartSummaryGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\CartSummaryGraphQl\Model\Resolver;

use Born\DbiConnector\Helper\Data;
use Born\DbiConnector\Model\InventoryEDDFromDatahub;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Item;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Helper\Cart as CartHelper;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class UpdateCartItems
 *
 * @category  PHP
 * @package   Born\CartSummaryGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class UpdateCartItems implements ResolverInterface
{
    const REQUESTING_SYSTEM_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_system";

    const REQUESTING_COUNTRY_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_country";

    const DBI_INVENTORY_CHECK_ENABLE = "born_dbi_extension/dbi_inventory_section/enable";

    /**
     * Cart
     *
     * @var Cart
     */
    protected $cart;

    /**
     * RequestQuantityProcessor
     *
     * @var RequestQuantityProcessor
     */
    protected $quantityProcessor;

    /**
     * CartRepositoryInterface
     *
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * Data
     *
     * @var Data
     */
    protected $dataHelper;

    /**
     * Session
     *
     * @var Session
     */
    protected $session;

    /**
     * InventoryEDDFromDatahub
     *
     * @var InventoryEDDFromDatahub
     */
    protected $getFromDatahub;

    /**
     * CartHelper
     *
     * @var CartHelper
     */
    protected $cartHelper;

    /**
     * CartItemRepositoryInterface
     *
     * @var CartItemRepositoryInterface
     */
    protected $cartItemRepository;

    /**
     * ScopeConfigInterface
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * UpdateCartItems constructor.
     *
     * @param Cart                        $cart               Cart
     * @param RequestQuantityProcessor    $quantityProcessor  RequestQuantityProcessor
     * @param CartRepositoryInterface     $cartRepository     CartRepositoryInterface
     * @param GetCartForUser              $getCartForUser     GetCartForUser
     * @param Session                     $session            Session
     * @param InventoryEDDFromDatahub     $getFromDatahub     InventoryEDDFromDatahub
     * @param CartHelper                  $cartHelper         CartHelper
     * @param CartItemRepositoryInterface $cartItemRepository CartItemRepositoryInterface
     * @param ScopeConfigInterface        $scopeConfig        ScopeConfigInterface
     */
    public function __construct(
        Cart $cart,
        RequestQuantityProcessor $quantityProcessor,
        CartRepositoryInterface $cartRepository,
        GetCartForUser $getCartForUser,
        Session $session,
        InventoryEDDFromDatahub $getFromDatahub,
        CartHelper $cartHelper,
        CartItemRepositoryInterface $cartItemRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->cart = $cart;
        $this->quantityProcessor = $quantityProcessor;
        $this->cartRepository = $cartRepository;
        $this->getCartForUser = $getCartForUser;
        $this->session = $session;
        $this->getFromDatahub = $getFromDatahub;
        $this->cartHelper = $cartHelper;
        $this->cartItemRepository = $cartItemRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Updating Shopping cart and checked inventory from datahub
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   Value
     * @param array|null       $args    argument
     *
     * @return array|Value|mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     * @throws NoSuchEntityException
     * @throws GraphQlAuthorizationException
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($args['input']['cart_id']) || empty($args['input']['cart_id'])) {
            throw new GraphQlInputException(__('Required parameter "cart_id" is missing.'));
        }

        $maskedCartId = $args['input']['cart_id'];

        if (!isset($args['input']['cart_items']) || empty($args['input']['cart_items'])
            || !is_array($args['input']['cart_items'])
        ) {
            throw new GraphQlInputException(__('Required parameter "cart_items" is missing.'));
        }
        $cartItems = $args['input']['cart_items'];

        $userId = $context->getUserId();
        if ($userId == 0) {
            $cart = $this->getCartForUser->execute($maskedCartId, $context->getUserId());
        } else {
            $cart = $this->cartRepository->getActiveForCustomer($userId);
        }

        try {
            $this->processCartItems($cart, $cartItems);
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        } catch (LocalizedException $e) {
            throw new GraphQlInputException(__($e->getMessage()), $e);
        }
        return [
            'cart' => [
                'model' => $cart,
            ],
        ];
    }

    /**
     * @param Quote $cart  Quote
     * @param array $items Item
     *
     * @return void
     * @throws GraphQlInputException
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     * @throws InputException
     */
    private function processCartItems(Quote $cart, array $items): void
    {
        foreach ($items as $item) {
            if (!isset($item['cart_item_id']) || empty($item['cart_item_id'])) {
                throw new GraphQlInputException(__('Required parameter "cart_item_id" for "cart_items" is missing.'));
            }
            $itemId = $item['cart_item_id'];

            if (!isset($item['quantity'])) {
                throw new GraphQlInputException(__('Required parameter "quantity" for "cart_items" is missing.'));
            }
            $quantity = (float)$item['quantity'];

            $cartItem = $cart->getItemById($itemId);

            if (!$cartItem) {
                throw new GraphQlInputException(
                    __('Could not find cart item with id: %1.', $item['cart_item_id'])
                );
            }

            if ($quantity <= 0) {
                $this->cartItemRepository->deleteById((int)$cart->getId(), $itemId);
            } else {
                $isAvailable = $this->checkInventory($cartItem, $itemId, $quantity);
                if ($isAvailable) {
                    $cartItem->setQty($quantity);
                    $this->validateCartItem($cartItem);
                    $this->cartItemRepository->save($cartItem);
                }
            }
        }
    }

    /**
     * Checking inventory from datahub
     *
     * @param $cartItem  Item      cart Items
     * @param $itemId    Item      Item Id
     * @param $quantity  Quantity  Quantity
     *
     * @throws GraphQlInputException
     */
    protected function checkInventory($cartItem, $itemId, $quantity)
    {
        $enable = $this->scopeConfig->getValue(self::DBI_INVENTORY_CHECK_ENABLE, ScopeInterface::SCOPE_STORE);
        $messages = " ";
        if ($enable) {
            $requestSystem = $this->scopeConfig->getValue(self::REQUESTING_SYSTEM_PATH, ScopeInterface::SCOPE_STORE);
            $requestCountry = $this->scopeConfig->getValue(self::REQUESTING_COUNTRY_PATH, ScopeInterface::SCOPE_STORE);
            $canPlaceOrder = true;

            $params = [
                'requestingSystem' => $requestSystem,
                'requestingCountry' => $requestCountry,
                'skus' => [$cartItem->getSku()]
            ];

            $result = $this->getFromDatahub->getinventoryFromDatahub($params);

            if ($result == null) {
                $canPlaceOrder = false;
                $messages .= $cartItem->getName() . " can't get data from DataHub!  ";
            }

            try {

                if (!isset($result['available']) || $result['available'] == "" || $result['available'] == null) {

                    $canPlaceOrder = false;
                    $messages .= $cartItem->getName() ."(". $cartItem->getId() .")". "  is no longer exist!  ";
                } else {
                    $totalQty = $result['dcAvailable'] + $result['storeAvailable'];
                    if ($quantity <= $totalQty) {
                        $quoteItem = [
                            $cartItem->getId() => ['qty' => $quantity],
                        ];
                        $quoteItem = $this->cart->suggestItemsQty($quoteItem);
                        $quoteItem = $this->quantityProcessor->process($quoteItem);

                        return true;
                    } else {
                        $canPlaceOrder = false;
                        $messages .= "The requested qty for " . $cartItem->getName()
                            . "(" . $cartItem->getId() . ")" . " is exceeds the qty from Datahub! "
                            . $totalQty . "  Items left only in Datahub!";
                    }
                }
            } catch (\Exception $e) {
                $canPlaceOrder = false;
                echo $e->getMessage();
            }

            if (!$canPlaceOrder) {
                throw new GraphQlInputException(__($messages));
            } else {
                if ($this->cartHelper->getItemsCount() === 0) {
                    $messages .= 'No items in a cart!';
                    throw new GraphQlInputException(__($messages));
                }
            }
        } else {
            $messages .= 'Please check inventory enable status from admin!';
            throw new GraphQlInputException(__($messages));
        }
    }

    /**
     * Validate cart item
     *
     * @param  Item $cartItem Item
     * @throws GraphQlInputException
     * @return void
     */
    private function validateCartItem(Item $cartItem): void
    {
        if ($cartItem->getHasError()) {
            $errors = [];
            foreach ($cartItem->getMessage(false) as $message) {
                $errors[] = $message;
            }

            if (!empty($errors)) {
                throw new GraphQlInputException(
                    __(
                        'Could not update the product with SKU %sku: %message',
                        ['sku' => $cartItem->getSku(), 'message' => __(implode("\n", $errors))]
                    )
                );
            }
        }
    }

}
