<?php
/**
 * class CartItems
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\CartSummaryGraphQl\Plugin\CartItems
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\CartSummaryGraphQl\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Json\Helper\Data;

class CartItems
{
    /**
     * @var Data
     */
    private $jsonHelper;

    /**
     * CartItems constructor.
     * @param Data $jsonHelper
     */
    public function __construct(
        Data $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * @param \Magento\QuoteGraphQl\Model\Resolver\CartItems $subject
     * @param $result
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws LocalizedException
     */

    public function aroundResolve(
        \Magento\QuoteGraphQl\Model\Resolver\CartItems $subject,
        $result,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }
        $cart = $value['model'];

        $itemsData = [];
        foreach ($cart->getAllVisibleItems() as $cartItem) {
            $product = $cartItem->getProduct();
            $options = $cartItem->getProduct()->getTypeInstance(true)->getOrderOptions($cartItem->getProduct());

            $optionArray = [];
            if (isset($options['attributes_info'])) {
                $getAttributeOptions = $options['attributes_info'];
                foreach ($getAttributeOptions as $options) {
                    $optionArray['config']
                    [str_replace(' ', '', $options['label'])] = $options['value'];
                }
            }

            /**
             * @var QuoteItem $cartItem
             */
            $productData = $cartItem->getProduct()->getData();
            $productData['model'] = $cartItem->getProduct();
            $subTotal = ($cartItem->getPrice() * $cartItem->getQty());
            $itemsData[] = [
                'id' => $cartItem->getItemId(),
                'quantity' => $cartItem->getQty(),
                'product' => $productData,
                'itemOptions' => $this->jsonHelper->jsonEncode($optionArray),
                'price' => [
                    'value' => $cartItem->getPrice(),
                    'currency' => "USD"
                ],
                'subTotal' => [
                    'value' => $subTotal,
                    'currency' => "USD"
                ],
                'model' => $cartItem,
            ];
        }
        return $itemsData;
    }
}
