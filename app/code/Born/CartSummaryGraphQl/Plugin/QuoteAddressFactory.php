<?php
/**
 * class QuoteAddressFactory
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\CartSummaryGraphQl\Plugin\QuoteAddressFactory
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
namespace Born\CartSummaryGraphQl\Plugin;

use Magento\Customer\Helper\Address as AddressHelper;
use Magento\Quote\Model\Quote\AddressFactory as BaseQuoteAddressFactory;

class QuoteAddressFactory
{
    /**
     * @var AddressHelper
     */
    private $addressHelper;
    /**
     * @var BaseQuoteAddressFactory
     */
    private $quoteAddressFactory;

    public function __construct(
        BaseQuoteAddressFactory $quoteAddressFactory,
        AddressHelper $addressHelper
    ) {
        $this->addressHelper = $addressHelper;
        $this->quoteAddressFactory = $quoteAddressFactory;
    }

    public function aroundCreateBasedOnInputData(
        \Magento\QuoteGraphQl\Model\Cart\QuoteAddressFactory $subject,
        $result,
        array $addressInput
    ) {
        $addressInput['country_id'] = $addressInput['country_code'] ?? '';

        $maxAllowedLineCount =  $this->addressHelper->getStreetLines();
        if (isset($addressInput['street']) && is_array($addressInput['street']) && count($addressInput['street']) > $maxAllowedLineCount) {
            throw new GraphQlInputException(
                __('"Street Address" cannot contain more than %1 lines.', $maxAllowedLineCount)
            );
        }

        $quoteAddress = $this->quoteAddressFactory->create();
        $quoteAddress->addData($addressInput);
        return $quoteAddress;
    }
}
