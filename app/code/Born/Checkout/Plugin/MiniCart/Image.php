<?php
namespace Born\Checkout\Plugin\MiniCart;



class Image
{
    private $productRepository;

    private $catalogProductTypeConfigurable;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
    )
    {
        $this->productRepository = $productRepository;
        $this->catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
    }

    public function aroundGetItemData(\Magento\Checkout\CustomerData\AbstractItem $subject,  $proceed, $item)
    {
        $result = $proceed($item);
        $originProduct = $this->productRepository->get($item->getSku());
        $parentId = $this->catalogProductTypeConfigurable->getParentIdsByChild($originProduct->getId());
        $parentImgUrl = '';
        if ($parentId) {
            $parentProduct = $this->productRepository->getById($parentId['0']);
            $parentImgUrl = $parentProduct->getData('descriptive_image');
        }
        $imageUrl = $originProduct->getData('descriptive_image');  //TODO update logic for Multi-Image


        if(!empty($imageUrl)){ // updating Image URl
            $result['product_image']['src'] = $imageUrl;
        } elseif (!empty($parentImgUrl)) {
            $result['product_image']['src'] = $parentImgUrl;
        }


        return $result;

    }
}