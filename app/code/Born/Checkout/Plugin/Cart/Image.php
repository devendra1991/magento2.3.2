<?php

namespace Born\Checkout\Plugin\Cart;

class Image
{

    private $productRepository;

    private $catalogProductTypeConfigurable;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $catalogProductTypeConfigurable
    )
    {
        $this->productRepository = $productRepository;
        $this->catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
    }

    public function afterGetImage($item, $result, $product)
    {


        //$parentProduct = $this->productRepository->getById($parentId);
        //var_dump($parentProduct->getSku());
        $originProduct = $this->productRepository->get($product->getSku());
        $parentId = $this->catalogProductTypeConfigurable->getParentIdsByChild($originProduct->getId());
        $parentImgUrl = '';
        if ($parentId) {
            $parentProduct = $this->productRepository->getById($parentId['0']);
            $parentImgUrl = $parentProduct->getData('descriptive_image');
        }

        $imageUrl = $originProduct->getData('descriptive_image');  //TODO update logic for Multi-Image


        if (!empty($imageUrl)) {
            $result->setImageUrl($imageUrl);
        } elseif (!empty($parentImgUrl)) {
            $result->setImageUrl($parentImgUrl);
        }


        return $result;
    }
}