<?php


namespace Born\Checkout\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

/**
 * Class CartThreshold
 * @package Born\Checkout\Observer
 */
class CartThreshold implements ObserverInterface
{

    /**
     * @var \Born\Checkout\Model\Config
     */
    private $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * CartThreshold constructor.
     * @param \Born\Checkout\Model\Config $scopeConfig
     * @param LoggerInterface $log
     * @param \Magento\Checkout\Model\Cart $cart
     */
    public function __construct(
        \Born\Checkout\Model\Config $scopeConfig,
        \Psr\Log\LoggerInterface $log,
        \Magento\Checkout\Model\Cart $cart
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->cart = $cart;
        $this->log = $log;
    }


    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getData('product');
        $this->log->debug($product->getSku());
//        if (($this->getCartLineItems() >= $this->scopeConfig->getCartThreshold()) && !$this->AlreadyIncart($product->getSku())) {
//            throw new \Magento\Framework\Exception\LocalizedException(
//                __('We can only support ' . $this->scopeConfig->getCartThreshold() . '  line items in Cart')
//            );
//        }


    }

    /**
     * @return int|mixed|null
     */
    private function getCartLineItems()
    {
        return $this->cart->getQuote()->getItemsCount();

    }

    /**
     * @param $productSku
     * @return bool
     *
     */
    private function AlreadyIncart($productSku)
    {
        $items = $this->cart->getQuote()->getAllVisibleItems();

        foreach ($items as $item) {
            if ($productSku == $item->getSku())
                return true;
        }
        return false;
    }

}