<?php


namespace Born\Checkout\Model;


use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterfaceAlias;

/**
 * Class Config
 * @package Born\Checkout\Model
 */
class Config
{
    /**
     *
     */
    const CARTTHRESHOLD = 'checkout/cart/shopping_cart_threshold';

    /**
     * @var ScopeConfigInterfaceAlias
     */
    private $scopeConfig;

    /**
     * Config constructor.
     * @param ScopeConfigInterfaceAlias $scopeConfig
     */
    public function __construct(
        ScopeConfigInterfaceAlias $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return int
     */
    public function getCartThreshold()
    {
        return (int) $this->scopeConfig->getValue(
            self::CARTTHRESHOLD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}