<?php
/**
 * ProductLinks GraphQl Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_ProductDetailsGraphQl
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\ProductDetailsGraphQl\Model\Resolver\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductLink\Link;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Catalog\Pricing\Price\RegularPrice;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Pricing\Amount\AmountInterface;
use Magento\Framework\Pricing\PriceInfo\Factory as PriceInfoFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @inheritdoc
 *
 * Format the product links information to conform to GraphQL schema representation
 */
class ProductLinks implements ResolverInterface
{
    /**
     * @var string[]
     */
    private $linkTypes = ['related', 'upsell', 'crosssell'];
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var PriceInfoFactory
     */
    private $priceInfoFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * ProductLinks constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param ProductRepositoryInterface $productRepository
     * @param PriceInfoFactory $priceInfoFactory
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        PriceInfoFactory $priceInfoFactory
    ) {
        $this->productRepository = $productRepository;
        $this->priceInfoFactory = $priceInfoFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritdoc
     *
     * Format product links data to conform to GraphQL schema
     *
     * @param \Magento\Framework\GraphQl\Config\Element\Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @throws \Exception
     * @return null|array
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var Product $product */
        $product = $value['model'];
        $links = null;
        if ($product->getProductLinks()) {
            $links = [];
            /** @var Link $productLink */
            foreach ($product->getProductLinks() as $key => $productLink) {
                if (in_array($productLink->getLinkType(), $this->linkTypes)) {
                    $linkedProduct = $this->loadProductBySku($productLink->getLinkedProductSku());
                    $links[] = $productLink->getData();
                    $links[$key]["id"] = $linkedProduct->getId();
                    $links[$key]["name"] = $linkedProduct->getName();
                    $links[$key]["price"] = $this->getPrice($linkedProduct);
                    $links[$key]["image"] = $this->getProductImage($linkedProduct->getId(), 'image');
                    $links[$key]["small_image"] = $this->getProductImage($linkedProduct->getId(), 'thumbnail');
                    $links[$key]["thumbnail"] = $this->getProductImage($linkedProduct->getId(), 'thumbnail');
                    $links[$key]["linked_product_url"] = $this->getProductUrl($linkedProduct);
                }
            }
        }

        return $links;
    }

    /**
     * @param $sku
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function loadProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }

    /**
     * @param $product
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPrice($product)
    {
        /** @var Product $product */
        $product->unsetData('minimal_price');
        $priceInfo = $this->priceInfoFactory->create($product);
        /** @var \Magento\Catalog\Pricing\Price\FinalPriceInterface $finalPrice */
        $finalPrice = $priceInfo->getPrice(FinalPrice::PRICE_CODE);
        $minimalPriceAmount = $finalPrice->getMinimalPrice();
        $maximalPriceAmount = $finalPrice->getMaximalPrice();
        $regularPriceAmount = $priceInfo->getPrice(RegularPrice::PRICE_CODE)->getAmount();

        $prices = [
            'minimalPrice' => $this->createAdjustmentsArray($priceInfo->getAdjustments(), $minimalPriceAmount),
            'regularPrice' => $this->createAdjustmentsArray($priceInfo->getAdjustments(), $regularPriceAmount),
            'maximalPrice' => $this->createAdjustmentsArray($priceInfo->getAdjustments(), $maximalPriceAmount)
        ];

        return $prices;
    }

    /**
     * @param array $adjustments
     * @param AmountInterface $amount
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function createAdjustmentsArray(array $adjustments, AmountInterface $amount): array
    {
        /** @var \Magento\Store\Model\Store $store */
        $store = $this->storeManager->getStore();

        $priceArray = [
            'amount' => [
                'value' => $amount->getValue(),
                'currency' => $store->getCurrentCurrencyCode()
            ],
            'adjustments' => []
        ];
        $priceAdjustmentsArray = [];
        foreach ($adjustments as $adjustmentCode => $adjustment) {
            if ($amount->hasAdjustment($adjustmentCode) && $amount->getAdjustmentAmount($adjustmentCode)) {
                $priceAdjustmentsArray[] = [
                    'code' => strtoupper($adjustmentCode),
                    'amount' => [
                        'value' => $amount->getAdjustmentAmount($adjustmentCode),
                        'currency' => $store->getCurrentCurrencyCode(),
                    ],
                    'description' => $adjustment->isIncludedInDisplayPrice() ?
                        'INCLUDED' : 'EXCLUDED'
                ];
            }
        }
        $priceArray['adjustments'] = $priceAdjustmentsArray;
        return $priceArray;
    }

    /**
     * @param $productID
     * @param $imageType
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getProductImage($productID, $imageType)
    {
        $product = $this->productRepository->getById($productID);
        return [
            'model' => $product,
            'image_type' => $imageType
        ];
    }

    private function getProductUrl($product){
        return $product->getUrlModel()->getUrl($product);
    }
}
