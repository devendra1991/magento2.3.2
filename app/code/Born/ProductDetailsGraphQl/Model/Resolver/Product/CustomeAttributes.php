<?php
/**
 * ProductDetailsGraphQl resolver Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_ProductDetailsGraphQl
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\ProductDetailsGraphQl\Model\Resolver\Product;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class CustomeAttributes
 * @package Born\ProductDetailsGraphQl\Model\Resolver\Product
 */
class CustomeAttributes implements ResolverInterface
{

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param \Magento\Framework\GraphQl\Config\Element\Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @throws \Exception
     * @return mixed|Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    )
    {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }
        /** @var Product $product */
        $product = $value['model'];
        $optionText = null;
        if ($product->getFacetChannelAvailability()) {
            $attribute = $product->getResource()->getAttribute('facet_channel_availability');
            if ($attribute->usesSource()) {
                $optionText = $attribute->getSource()->getOptionText($product->getFacetChannelAvailability());
            }
        }
        return $optionText;
    }
}
