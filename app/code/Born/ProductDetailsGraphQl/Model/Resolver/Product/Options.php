<?php
/**
 * Options resolver Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_ProductDetailsGraphQl
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\ProductDetailsGraphQl\Model\Resolver\Product;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Swatches\Helper\Data;

/**
 * Class Options
 * @package Born\ProductDetailsGraphQl\Model\Resolver\Product
 */
class Options implements ResolverInterface
{
    /**
     * @var Data
     */
    private $swatchHelper;

    /**
     * Options constructor.
     * @param Data $swatchHelper
     */
    public function __construct(Data $swatchHelper)
    {
        $this->swatchHelper = $swatchHelper;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param \Magento\Framework\GraphQl\Config\Element\Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @throws \Exception
     * @return mixed|Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['value_index'])) {
            return null;
        }

        $optionIds[] = $value['value_index'];
        $hashcodeData = $this->swatchHelper->getSwatchesByOptionsId($optionIds);
        return $hashcodeData[$value['value_index']]['value'] ?? 'No';
    }
}
