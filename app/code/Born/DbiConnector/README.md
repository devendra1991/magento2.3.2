Magento 2 Born DBI Connector module used for to generate the token, based on host.
It requires the host details and username and the password to connect and generate the token.

Follow below steps to install and configured the module.

=> copy this module into the app/code/ directory.

=> Born should be the Vendor and DbiConnector should be the module name.

=> after placing the module in the right place next step is the enable module using following command.

php bin/magento module:enable Born_DbiConnector

=> Next, after that run following command.

php bin/magento setup:upgrade

=> And the last one is to deploy by using following command.

php bin/magento setup:static-content:deploy -f


Now module has been enabled successfully. Now it's time to configure it, for this we required the hostname, username and password.

Steps to configure and Enable the module:

Go to Stores => Configuration => BORN DBI EXTENTIONS => DBI API Auth => DBI Connector Configuration

Enter the details given and test.


Usage of token authorization module is as follows, just follow the below steps.

1. Inject the helper file into your class i.e \Born\DbiConnector\Helper\Data $data_helper and create the variable.
2. And assign the $data_helper veriable like this
        $this->_data_helper = $data_helper;
        
3. Into you function just call the method with the help of object defined into the constructor i.e
        $this->_data_helper->getAccessToken();
        
 With the help of this it will return the Token from the third party host.