<?php


namespace Born\DbiConnector\Model;

use Born\DbiConnector\Helper\Data;
use Born\DatahubOrders\Model\Config;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Message\ManagerInterface;



class InventoryEDDFromDatahub
{
    const XPATH_FOR_DBI_INVENTORY = "born_dbi_extension/dbi_inventory_section/dbi_inventory_url";
    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var $dbiConnector
     */
    private $dbiConnector;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Message
     */
    private $message;

    /**
     * OrderExportApi constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param Data $dataHelper
     * @param LoggerInterface $logger
     * @param Request $request
     * @param Json $jsonHelper
     * @param SessionManagerInterface $coreSession
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        Data $dbiConnector,
        LoggerInterface $logger,
        Request $request,
        Json $json,
        SessionManagerInterface $coreSession,
        ManagerInterface $messageManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->dbiConnector = $dbiConnector;
        $this->logger = $logger;
        $this->request = $request;
        $this->json = $json;
        $this->coreSession = $coreSession;
        $this->message = $messageManager;
    }

    /**
     * @param $method
     * @param array $params
     * @return $response  || array
     *      $response[] = {
     *      sku: string;
            available: boolean;

            nextDayAir?: string;
            twoDayAir?: string;
            standard?: string;

            storeStandard?: string;

            purchaseOrderStandard?: string;

            specialOrder?: string;

            dcAvailable: number;
            storeAvailable: number;
            purchaseOrderAvailable: number;

            handlingBuffer?: number;

            facilities?: Facility[];
     * }
     */
    public function getDataFromDatahub($params = [])
    {
        $end_point = $this->scopeConfig->getValue(self::XPATH_FOR_DBI_INVENTORY,  \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $response = $this->CallDataHub($params, $end_point, "POST");

        if (!empty($response)) {
            return $this->json->unserialize($response);
        }

        return array(['response' => 'false', "Message" => "Please try again, no response from the API."]);
    }

    /**
     * @param array $params
     * @param $end_point
     * @param string $method
     * @return bool
     */
    private function CallDataHub($params = [], $end_point, $method = "POST")
    {
        try {
            if (!empty($params) && $params != '') {

                /**
                 * @var TYPE_NAME $accessToken
                 */

                $accessToken = $this->dbiConnector->getAccessToken();
                $token = $this->json->unserialize($accessToken);

                $postFields = '';


                if ($method == 'POST') {
                    $postFields = $this->json->serialize($params);

                }

                $this->curl->addHeader('authorizationToken', $token['token']);

                $this->curl->post($end_point,$postFields);

                return $this->curl->getBody();

            } else {
                $this->logger->error(__('Request don\'t have data.'));
            }
            $this->logger->error(__('Something went wrong.'));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return false;
    }


    public function getEDDFromDatahub($param = [])
    {
        $response = $this->getDataFromDatahub($param);

        if (!empty($response['errorMessage']) && $response['errorMessage']) {
            $this->message->addErrorMessage($response['errorMessage']);
            return null;
        }
        try{
            $result = [];
            $result['sku'] = $response['0']['sku']  ?? null;
            $result['available'] = $response['0']['available']  ?? null;
            $result['nextDayAir'] = $response['0']['nextDayAir']  ?? null;
            $result['twoDayAir'] = $response['0']['twoDayAir']  ?? null;
            $result['standard'] = $response['0']['standard']  ?? null;
            $result['storeStandard'] = $response['0']['storeStandard']  ?? null;
            $result['specialOrder'] = $response['0']['specialOrder']  ?? null;
            $result['purchaseOrderStandard'] = $response['0']['purchaseOrderStandard']  ?? null;

            return $result;


        }catch (\Exception $exception){
            echo $exception->getMessage();
        }
    }

    public function getinventoryFromDatahub($param = [])
    {
        $response = $this->getDataFromDatahub($param);
        if (!empty($response['errorMessage']) && $response['errorMessage']) {
            $this->message->addErrorMessage($response['errorMessage']);
            return null;
        }
        try{
            $result = [];
            $result['sku'] = $response['0']['sku']  ?? null;
            $result['available'] = $response['0']['available']  ?? null;
            $result['dcAvailable'] = $response['0']['dcAvailable']  ?? null;
            $result['storeAvailable'] = $response['0']['storeAvailable'] ?? null;
            $result['purchaseOrderAvailable'] = $response['0']['purchaseOrderAvailable'] ?? null;
            $result['handlingBuffer'] = $response['0']['handlingBuffer'] ?? null;
            return $result;


        }catch (\Exception $exception){
            echo $exception->getMessage();
        }
    }

}

