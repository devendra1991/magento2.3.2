<?php

/**
 * @copyright: Copyright © 2019 Born
 * @author   : Sushil Zore <sushil.zore@borngroup.com>
 */
namespace Born\DbiConnector\Api;


interface TokenInterface
{
    /**
     * @return mixed
     */
    public function getAccessToken();
}