<?php
/**
 * Created by PhpStorm.
 *
 * Date: 21/5/19
 * Time: 2:08 PM

 * Generate Access Token From DATA Hub Login Call
 * Generated Access Token will be used for further
 * communication with Data Hub
 *
 * @author Born Team <suraj.tupe@wearefmg.net>
 */
namespace Born\DbiConnector\Helper;

use Born\DbiConnector\Api\TokenInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class Data extends AbstractHelper implements TokenInterface
{
    const XPATH_FOR_QUEUE_PROCESSING = 'born_dbi_general/general/enable_queue';
    const XPATH_FOR_DBI_CONNECTOR_END_POINT = 'born_dbi_extension/general/api_end_point';
    const XPATH_FOR_DBI_CONNECTOR_USERNAME = 'born_dbi_extension/general/username';
    const XPATH_FOR_DBI_CONNECTOR_PASSWORD = 'born_dbi_extension/general/password';
    const XPATH_FOR_DBI_CONNECTOR_BEARER_TOKEN = 'born_dbi_extension/general/bearertoken';
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var EncryptorInterface
     */
    private $encryptorType;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var ScopeInterface
     */
    protected $scopeConfig;

    /**
     * Data constructor.
     * @param Context $context
     * @param Curl $curl
     * @param EncryptorInterface $encryptorType
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Curl $curl,
        EncryptorInterface $encryptorType,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->scopeConfig = $context->getScopeConfig();
        $this->curl = $curl;
        $this->encryptorType = $encryptorType;
        $this->logger = $logger;
    }
    /**
     * Get Access Token from DataHub Login Call
     * @return mixed
     */
    public function getAccessToken()
    {
        try {
            $this->curl->addHeader(
                "username",
                $this->scopeConfig->getValue(self::XPATH_FOR_DBI_CONNECTOR_USERNAME, ScopeInterface::SCOPE_STORE)
            );
            $this->curl->addHeader(
                "password",
                $this->encryptorType->decrypt(
                    $this->scopeConfig->getValue(self::XPATH_FOR_DBI_CONNECTOR_PASSWORD, ScopeInterface::SCOPE_STORE)
                )
            );
            $this->curl->addHeader(
                "bearertoken",
                $this->encryptorType->decrypt(
                    $this->scopeConfig->getValue(
                        self::XPATH_FOR_DBI_CONNECTOR_BEARER_TOKEN,
                        ScopeInterface::SCOPE_STORE
                    )
                )
            );
            $this->curl->get(
                $this->scopeConfig->getValue(self::XPATH_FOR_DBI_CONNECTOR_END_POINT, ScopeInterface::SCOPE_STORE)
            );
            $response = $this->curl->getBody();
            if (!empty($response)) {
                return $response;
            }
            $this->logger->addDebug(__('Unable to Get Access Token'));
        } catch (\Exception $e) {
            $this->logger->addDebug($e->getMessage());
        }
        return false;
    }


    /**
     * @return int
     */
    public function isQueueProcessingEnable()
    {
        return (int)$this->scopeConfig->getValue(self::XPATH_FOR_QUEUE_PROCESSING, ScopeInterface::SCOPE_STORE);
    }
}
