<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Setup;

use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\Data\ConfigData;
use Magento\Framework\Config\File\ConfigFilePool;
use Magento\Framework\Setup\ConfigOptionsListInterface;
use Magento\Framework\Setup\Option\TextConfigOption;

/**
 * ConnectionOptionList
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class ConfigOptionsList implements ConfigOptionsListInterface
{
    /**
     * Input key for the options
     */
    const INPUT_KEY_QUEUE_STOMP_HOST     = 'stomp-host';
    const INPUT_KEY_QUEUE_STOMP_PORT    = 'stomp-port';
    const INPUT_KEY_QUEUE_STOMP_LOGIN = 'stomp-login';
    const INPUT_KEY_QUEUE_STOMP_SECRET = 'stomp-password';
    const INPUT_KEY_QUEUE_STOMP_TARGET = 'stomp-target';
    const INPUT_KEY_QUEUE_STOMP_SSL = 'stomp-ssl-on';
    const INPUT_KEY_QUEUE_STOMP_SYNC = 'stomp-sync';
    const INPUT_KEY_QUEUE_STOMP_LAZY = 'stomp-lazy';
    const INPUT_KEY_QUEUE_STOMP_CONNTIMEOUT = 'stomp-connection-timeout';

    /**
     * Path to the values in the deployment config
     */
    const CONFIG_PATH_QUEUE_STOMP_HOST     = 'queue/stomp/host';
    const CONFIG_PATH_QUEUE_STOMP_PORT    = 'queue/stomp/port';
    const CONFIG_PATH_QUEUE_STOMP_LOGIN = 'queue/stomp/login';
    const CONFIG_PATH_QUEUE_STOMP_SECRET = 'queue/stomp/password';
    const CONFIG_PATH_QUEUE_STOMP_TARGET = 'queue/stomp/target';
    const CONFIG_PATH_QUEUE_STOMP_SSL = 'queue/stomp/ssl_on';
    const CONFIG_PATH_QUEUE_STOMP_SYNC = 'queue/stomp/sync';
    const CONFIG_PATH_QUEUE_STOMP_LAZY = 'queue/stomp/lazy';
    const CONFIG_PATH_QUEUE_STOMP_CONNTIMEOUT = 'queue/stomp/connection_timeout';


    /**
     * Default values
     */
    const DEFAULT_STOMP_HOST     = 'b-6c5b0225-e18d-49cd-801d-7b5e94687e5b-1.stomp.us-east-1.amazonaws.com';
    const DEFAULT_STOMP_PORT     = '61614';
    const DEFAULT_STOMP_LOGIN = '';
    const DEFAULT_STOMP_SECRET = '';
    const DEFAULT_STOMP_TARGET = 'activemq';
    const DEFAULT_STOMP_SSL = true;
    const DEFAULT_STOMP_SYNC = true;
    const DEFAULT_STOMP_LAZY = false;
    const DEFAULT_STOMP_CONNTIMEOUT = 10;

    /**
     * @var ConnectionValidator
     */
    private $connectionValidator;


    /**
     * Constructor
     *
     * @param ConnectionValidator $connectionValidator Connection Check
     */
    public function __construct(ConnectionValidator $connectionValidator)
    {
        $this->connectionValidator = $connectionValidator;
    }//end __construct()


    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
                new TextConfigOption(
                    self::INPUT_KEY_QUEUE_STOMP_HOST,
                    TextConfigOption::FRONTEND_WIZARD_TEXT,
                    self::CONFIG_PATH_QUEUE_STOMP_HOST,
                    'STOMP Host',
                    self::DEFAULT_STOMP_HOST
                ),
                new TextConfigOption(
                    self::INPUT_KEY_QUEUE_STOMP_PORT,
                    TextConfigOption::FRONTEND_WIZARD_TEXT,
                    self::CONFIG_PATH_QUEUE_STOMP_PORT,
                    'STOMP Port',
                    self::DEFAULT_STOMP_PORT
                ),
                new TextConfigOption(
                    self::INPUT_KEY_QUEUE_STOMP_LOGIN,
                    TextConfigOption::FRONTEND_WIZARD_TEXT,
                    self::CONFIG_PATH_QUEUE_STOMP_LOGIN,
                    'STOMP Login',
                    self::DEFAULT_STOMP_LOGIN
                ),
                new TextConfigOption(
                    self::INPUT_KEY_QUEUE_STOMP_SECRET,
                    TextConfigOption::FRONTEND_WIZARD_TEXT,
                    self::CONFIG_PATH_QUEUE_STOMP_SECRET,
                    'STOMP Password',
                    self::DEFAULT_STOMP_SECRET
                )
        ];
    }//end getOptions()


    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function createConfig(array $data, DeploymentConfig $deploymentConfig)
    {
        $configData = new ConfigData(ConfigFilePool::APP_ENV);

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_HOST])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_HOST, $data[self::INPUT_KEY_QUEUE_STOMP_HOST]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_PORT])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_PORT, $data[self::INPUT_KEY_QUEUE_STOMP_PORT]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_LOGIN])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_LOGIN, $data[self::INPUT_KEY_QUEUE_STOMP_LOGIN]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_SECRET])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_SECRET, $data[self::INPUT_KEY_QUEUE_STOMP_SECRET]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_TARGET])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_TARGET, $data[self::INPUT_KEY_QUEUE_STOMP_TARGET]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_SSL])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_SSL, $data[self::INPUT_KEY_QUEUE_STOMP_SSL]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_SYNC])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_SYNC, $data[self::INPUT_KEY_QUEUE_STOMP_SYNC]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_LAZY])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_LAZY, $data[self::INPUT_KEY_QUEUE_STOMP_LAZY]);
        }

        if (isset($data[self::INPUT_KEY_QUEUE_STOMP_CONNTIMEOUT])) {
            $configData->set(self::CONFIG_PATH_QUEUE_STOMP_CONNTIMEOUT, $data[self::INPUT_KEY_QUEUE_STOMP_CONNTIMEOUT]);
        }
        return [$configData];
    }//end createConfig()


    /**
     * {@inheritdoc}
     */
    public function validate(array $options, DeploymentConfig $deploymentConfig)
    {
        $errors = [];

        if (isset($options[self::INPUT_KEY_QUEUE_STOMP_LOGIN])
            && $options[self::INPUT_KEY_QUEUE_STOMP_SECRET] !== ''
        ) {
            $result = $this->connectionValidator->isConnectionValid(
                $options[self::INPUT_KEY_QUEUE_STOMP_HOST],
                $options[self::INPUT_KEY_QUEUE_STOMP_PORT],
                $options[self::INPUT_KEY_QUEUE_STOMP_LOGIN],
                $options[self::INPUT_KEY_QUEUE_STOMP_SECRET]
            );

            if (!$result) {
                $errors[] = 'Could not connect to the STOMP Service.';
            }
        }

        return $errors;
    }//end validate()

}//end class
