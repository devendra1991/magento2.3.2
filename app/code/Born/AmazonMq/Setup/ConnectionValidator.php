<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Setup;

use Enqueue\Stomp\StompConnectionFactory;

/**
 * ConnectionValidator
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class ConnectionValidator
{

    /**
     * Checks STOMP Connection
     *
     * @param string $host     STOMP HOST
     * @param string $port     STOMP PORT
     * @param string $login    STOMP LOGIN
     * @param string $password STOMP PASSWORD
     *
     * @return bool
     */
    public function isConnectionValid($host, $port, $login, $password)
    {
        try {
            $stompConnection = new StompConnectionFactory([
                'host' => $host,
                'port' => $port,
                'login' => $login,
                'password' => $password,
                'target' => ConfigOptionsList::DEFAULT_STOMP_TARGET,
                'ssl_on' => ConfigOptionsList::DEFAULT_STOMP_SSL,
                'sync' => ConfigOptionsList::DEFAULT_STOMP_SYNC,
                'lazy' => ConfigOptionsList::DEFAULT_STOMP_LAZY,
                'connection_timeout' => ConfigOptionsList::DEFAULT_STOMP_CONNTIMEOUT
            ]);

            if(is_object($stompConnection))
            {
                return true;
            }else{
                throwException('connection is not valid');
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }//end isConnectionValid()

}//end class
