<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\ExchangeInterface;

/**
 * Exchange
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Exchange implements ExchangeInterface
{
    /**
     * @var QueueFactory
     */
    protected $queueFactory;

    /**
     * @var array
     */
    protected $queues = [];


    /**
     * Exchange constructor.
     *
     * @param QueueFactory $queueFactory Queue Factory
     */
    public function __construct(
        QueueFactory $queueFactory
    ) {
        $this->queueFactory = $queueFactory;
    }//end __construct()


    /**
     * {@inheritdoc}
     */
    public function enqueue($topic, EnvelopeInterface $envelope)
    {

        $queue = $this->createQueue($topic);
        $queue->push($envelope);

        return null;
    }//end enqueue()

    /**
     * Create Queue
     *
     * @param string $topicName same as queue name
     *
     * @return Queue
     */
    protected function createQueue($topicName)
    {
        if (array_key_exists($topicName, $this->queues)) {
            return $this->queues[$topicName];
        }
        $this->queues[$topicName] = $this->queueFactory->create($topicName);

        return $this->queues[$topicName];
    }
}
