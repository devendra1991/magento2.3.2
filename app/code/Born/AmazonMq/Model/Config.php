<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Enqueue\Stomp\StompConnectionFactory;
use Magento\Framework\App\DeploymentConfig;

/**
 * Data
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Config
{
    /**
     * Queue config key
     */
    const QUEUE_CONFIG = 'queue';

    /**
     * Sqs config key
     */
    const MQ_CONFIG = 'stomp';
    const HOST     = 'host';
    const PORT    = 'port';
    const LOGIN = 'login';
    const SECRETKEY = 'password';
    const PREFIX     = 'prefix';

    /**
     * Deployment configuration
     *
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var MqClient
     */
    private $connection;

    /**
     * Associative array of MQ configuration
     *
     * @var array
     */
    private $data;


    /**
     * Constructor
     *
     * Example environment config:
     * <code>
     * 'queue' =>
     *     [
     *         'stomp' => [
     *             'host' => 'b-6c5b0225-e18d-49cd-801d-7b5e94687e5b-1.mq.us-east-1.amazonaws.com',
     *             'port' => 61614,
     *             'login' => 'DBI-DEV-AMQP-Admin',
     *             'password' => 'HeSjb[J4q7&xCnLU',
     *             'target' => 'activemq',
     *             'ssl_on' => true,
     *             'sync' => true,
     *             'lazy' => false,
     *             'connection_timeout' => 10
     *         ],
     *     ],
     * </code>
     *
     * @param DeploymentConfig $config Configuration Instance
     */
    public function __construct(DeploymentConfig $config)
    {
        $this->deploymentConfig = $config;
    }//end __construct()


    /**
     * Return MQ client
     * @return \Enqueue\Stomp\StompContext
     */
    public function getConnection()
    {
        if (!isset($this->connection)) {
            $this->connection = (
                new StompConnectionFactory(
                    [
                        'host' => $this->getValue(Config::HOST),
                        'vhost' => $this->getValue(Config::HOST),
                        'port' => $this->getValue(Config::PORT),
                        'login' => $this->getValue(Config::LOGIN),
                        'password' => $this->getValue(Config::SECRETKEY),
                        'target' => 'activemq',
                        'ssl_on' => true,
                        'sync' => true,
                        'lazy' => false,
                        'connection_timeout' => 10
                    ]
                )
            )->createContext();
        }

        return $this->connection;
    }//end getConnection()


    /**
     * Returns the configuration set for the key.
     *
     * @param string $key Secret Key
     * @return string
     */
    public function getValue($key)
    {
        $this->load();

        return isset($this->data[$key]) ? $this->data[$key] : null;
    }//end getValue()


    /**
     * Load the configuration for SQS
     *
     * @return void
     */
    private function load()
    {
        if (null === $this->data) {
            $queueConfig = $this->deploymentConfig->getConfigData(self::QUEUE_CONFIG);
            $this->data  = isset($queueConfig[self::MQ_CONFIG]) ? $queueConfig[self::MQ_CONFIG] : [];
        }
    }//end load()
}//end class
