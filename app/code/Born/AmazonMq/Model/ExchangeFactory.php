<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\AmazonMq\Model;

use Magento\Framework\MessageQueue\ExchangeFactoryInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * ExchangeFactory
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class ExchangeFactory implements ExchangeFactoryInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    private $instanceName = null;


    /**
     * Initialize dependencies.
     *
     * @param ObjectManagerInterface $objectManager Object Manager
     * @param string                 $instanceName  Instance Name
     *
     * @since 100.0.0
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = Exchange::class
    ) {
        $this->objectManager = $objectManager;
        $this->instanceName  = $instanceName;
    }//end __construct()


    /**
     * Create
     *
     * @param string $connectionName Connection
     * @param array  $data           Data
     *
     * @return ExchangeInterface|mixed
     */
    public function create($connectionName, array $data = [])
    {

        return $this->objectManager->create($this->instanceName);
    }//end create()
}//end class
