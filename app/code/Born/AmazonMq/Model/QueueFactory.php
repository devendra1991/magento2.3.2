<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Magento\Framework\MessageQueue\QueueFactoryInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * QueueFactory
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class QueueFactory implements QueueFactoryInterface
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    private $objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    private $instanceName = null;


    /**
     * QueueFactory constructor.
     *
     * @param ObjectManagerInterface $objectManager Object Manger
     * @param string                 $instanceName  Instance Name
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = Queue::class
    ) {
        $this->objectManager = $objectManager;
        $this->instanceName  = $instanceName;
    }//end __construct()


    /**
     * Create
     *
     * @param string $queueName      Queue Name
     * @param string $connectionName Connection Name
     *
     * @return QueueInterface|mixed
     */
    public function create($queueName, $connectionName = '')
    {
        return $this->objectManager->create(
            $this->instanceName,
            [
                'queueName' => $queueName
            ]
        );
    }//end create()
}//end class
