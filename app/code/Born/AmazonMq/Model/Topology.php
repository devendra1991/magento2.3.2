<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Psr\Log\LoggerInterface;
use Magento\Framework\Communication\ConfigInterface as CommunicationConfig;
use Magento\Framework\MessageQueue\ConfigInterface as QueueConfig;

/**
 * Topology
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Topology
{
    /**
     * Type of exchange
     */
    const TOPIC_EXCHANGE = 'topic';

    /**
     * MQ connection
     */
    const MQ_CONNECTION = 'stomp';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Config
     */
    private $mqConfig;

    /**
     * @var QueueConfig
     */
    private $queueConfig;

    /**
     * @var CommunicationConfig
     */
    private $communicationConfig;


    /**
     * Topology constructor.
     *
     * @param Config              $mqConfig           Mq Config
     * @param QueueConfig         $queueConfig         Queue Config
     * @param CommunicationConfig $communicationConfig Communication Config
     * @param LoggerInterface     $logger              Logger
     */
    public function __construct(
        Config $mqConfig,
        QueueConfig $queueConfig,
        CommunicationConfig $communicationConfig,
        LoggerInterface $logger
    ) {
        $this->mqConfig          = $mqConfig;
        $this->queueConfig         = $queueConfig;
        $this->communicationConfig = $communicationConfig;
        $this->logger              = $logger;
    }//end __construct()


    /**
     * Install SQS Exchanges, Queues and bind them
     *
     * @return void
     *
     * @throws \Exception
     */
    public function install()
    {
        $availableQueues    = $this->getQueuesList(self::MQ_CONNECTION);
        $availableExchanges = $this->getExchangesList(self::MQ_CONNECTION);
        foreach ($this->queueConfig->getBinds() as $bind) {
            $queueName    = $bind[QueueConfig::BIND_QUEUE];
            $exchangeName = $bind[QueueConfig::BIND_EXCHANGE];
            if (in_array($queueName, $availableQueues) && in_array($exchangeName, $availableExchanges)) {
                try {
                    $this->declareQueue($queueName);
                } catch (\Exception $e) {
                    $this->logger->error(
                        sprintf(
                            'There is a problem with creating or binding queue "%s" and an exchange "%s". Error: %s',
                            $queueName,
                            $exchangeName,
                            $e->getTraceAsString()
                        )
                    );
                }
            }
        }
    }//end install()


    /**
     * Create MQ Queues
     *
     * @param string $queueName Queue Name
     *
     * @return void
     *
     * @throws \Exception
     */
    public function create($queueName = '')
    {
        if ($queueName) {
            $availableQueues[] = $queueName;
        } else {
            $availableQueues = $this->getQueuesList(self::MQ_CONNECTION);
        }

        foreach ($availableQueues as $queue) {
            try {
                $this->declareQueue($queue);
            } catch (\Exception $e) {
                $this->logger->error(
                    sprintf(
                        'There is a problem with creating queue "%s". Error: %s',
                        $queue,
                        $e->getMessage()
                    )
                );
            }
        }
    }//end create()


    /**
     * Delete MQ Queues
     *
     * @param string $queueName Queue Name
     *
     * @return void
     *
     * @throws \Exception
     */
    public function delete($queueName = '')
    {
        if ($queueName) {
            $availableQueues[] = $queueName;
        } else {
            $availableQueues = $this->getQueuesList(self::MQ_CONNECTION);
        }

        foreach ($availableQueues as $queue) {
            try {
                $this->deleteQueue($queue);
            } catch (\Exception $e) {
                $this->logger->error(
                    sprintf(
                        'There is a problem with removing queue "%s". Error: %s',
                        $queue,
                        $e->getMessage()
                    )
                );
            }
        }
    }//end delete()


    /**
     * Purge MQ Queues
     *
     * @param string $queueName Queue Name
     *
     * @return void
     *
     * @throws \Exception
     */
    public function purge($queueName = '')
    {
        if ($queueName) {
            $availableQueues[] = $queueName;
        } else {
            $availableQueues = $this->getQueuesList(self::MQ_CONNECTION);
        }

        foreach ($availableQueues as $queue) {
            try {
                $this->purgeQueue($queue);
            } catch (\Exception $e) {
                $this->logger->error(
                    sprintf(
                        'There is a problem with purging queue "%s". Error: %s',
                        $queue,
                        $e->getMessage()
                    )
                );
            }
        }
    }//end purge()


    /**
     * Get Queue Name
     *
     * @param string $queueName Queue Name
     *
     * @return string
     */
    protected function getQueueName($queueName)
    {
        return $this->mqConfig->getValue(Config::PREFIX) . '_' . $queueName;
    }//end getQueueName()


    /**
     * Return list of queue names,
     * that are available for connection
     *
     * @param string $connection Connection
     *
     * @return array List of queue names
     *
     * @throws \Exception
     */
    private function getQueuesList($connection)
    {
        $queues = [];
        foreach ($this->queueConfig->getConsumers() as $consumer) {
            if ($consumer[QueueConfig::CONSUMER_CONNECTION] === $connection) {
                $queues[] = $consumer[QueueConfig::CONSUMER_QUEUE];
            }
        }
        foreach (array_keys($this->communicationConfig->getTopics()) as $topicName) {
            if ($this->queueConfig->getConnectionByTopic($topicName) === $connection) {
                $queues = array_merge($queues, $this->queueConfig->getQueuesByTopic($topicName));
            }
        }
        return array_unique($queues);
    }//end getQueuesList()


    /**
     * Return list of exchange names, that are available for connection
     *
     * @param string $connection Connection
     *
     * @return array List of exchange names
     */
    private function getExchangesList($connection)
    {
        $exchanges   = [];
        $this->queueConfig = $this->queueConfig->getPublishers();
        foreach (queueConfig as $publisher) {
            if ($publisher[QueueConfig::PUBLISHER_CONNECTION] === $connection) {
                $exchanges[] = $publisher[QueueConfig::PUBLISHER_EXCHANGE];
            }
        }
        return array_unique($exchanges);
    }//end getExchangesList()


    /**
     * Declare MQ Queue
     *
     * @param string $queueName Queue Name
     *
     * @return void
     */
    private function declareQueue($queueName)
    {
        $mqQueueName = $this->getConnection()->createQueue($this->getQueueName($queueName));
        $this->getConnection()->declareQueue($mqQueueName);
    }//end declareQueue()


    /**
     * Delete MQ Queue
     *
     * @param string $queueName Queue Name
     *
     * @return void
     */
    private function deleteQueue($queueName)
    {
        $mqQueueName = $this->getConnection()->createQueue($this->getQueueName($queueName));
        $this->getConnection()->deleteQueue($mqQueueName);
    }//end deleteQueue()


    /**
     * Purge MQ Queue
     *
     * @param string $queueName Queue Name
     *
     * @return void
     */
    private function purgeQueue($queueName)
    {
        $sqsQueueName = $this->getConnection()->createQueue($this->getQueueName($queueName));
        $this->getConnection()->purgeQueue($sqsQueueName);
    }//end purgeQueue()


    /**
     * Return MQ connection
     *
     * @return \Enqueue\Stomp\StompContext
     */
    private function getConnection()
    {
        return $this->mqConfig->getConnection();
    }//end getConnection()
}//end class
