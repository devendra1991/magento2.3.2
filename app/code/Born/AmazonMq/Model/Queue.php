<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Enqueue\Stomp\StompMessage;
use Magento\Framework\MessageQueue\EnvelopeFactory;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\QueueInterface;
use Psr\Log\LoggerInterface;

/**
 * Queue
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Queue implements QueueInterface
{
    const TIMEOUT_PROCESS = 1000;
    const MSG_PROPERTIES = 'properties';
    const MSG_RECEIPT_HANDLE = 'receiptHandle';

    /**
     * @var Config
     */
    private $mqConfig;

    /**
     * @var string
     */
    private $queueName;

    /**
     * @var EnvelopeFactory
     */
    private $envelopeFactory;

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var \Enqueue\Stomp\StompConsumer
     */
    private $consumer;

    /**
     * Queue constructor.
     *
     * @param Config          $mqConfig       Mq Config
     * @param EnvelopeFactory $envelopeFactory Envelope Factory
     * @param string          $queueName       Queue Name
     * @param LoggerInterface $logger          Logger
     */
    public function __construct(
        Config $mqConfig,
        EnvelopeFactory $envelopeFactory,
        $queueName,
        LoggerInterface $logger
    ) {
        $this->mqConfig       = $mqConfig;
        $this->queueName       = $queueName;
        $this->envelopeFactory = $envelopeFactory;
        $this->logger          = $logger;
    }//end __construct()

    /**
     * {@inheritdoc}
     */
    public function dequeue()
    {
        /**
         * @var \Enqueue\Stomp\StompMessage $message
         */
        $message = $this->createConsumer()->receive(self::TIMEOUT_PROCESS);
        if (null !== $message) {
            return $this->createEnvelop($message);
        }
        return null;
    }//end dequeue()

    /**
     * @return \Enqueue\Stomp\StompConsumer
     */
    public function createConsumer()
    {
        if (!$this->consumer) {
            $this->consumer = $this->mqConfig->getConnection()->createConsumer($this->getQueue());
        }
        return $this->consumer;
    }//end createConsumer()

    /**
     * @return \Enqueue\Stomp\StompDestination
     */
    public function getQueue()
    {
        return $this->mqConfig->getConnection()->createQueue($this->getQueueName());
    }//end getQueue()

    /**
     * @return string
     */
    protected function getQueueName()
    {
        return $this->mqConfig->getValue(Config::PREFIX) . '_' . $this->queueName;
    }//end getQueueName()

    /**
     * Create Envelop
     *
     * @param StompMessage $message
     *
     * @return \Magento\Framework\MessageQueue\Envelope
     */
    protected function createEnvelop(StompMessage $message)
    {
        return $this->envelopeFactory->create([
            'body' => $message->getBody(),
            self::MSG_PROPERTIES => [
                self::MSG_PROPERTIES => $message->getProperties(),
                self::MSG_RECEIPT_HANDLE => $message->getFrame(),
                'topic_name' => $this->queueName,
                'message_id' => $message->getFrame()->getHeaders()['message-id']
            ]
        ]);
    }//end createEnvelop()

    /**
     * {@inheritdoc}
     */
    public function acknowledge(EnvelopeInterface $envelope)
    {
        $message = $this->createMessage($envelope);
        $this->createConsumer()->acknowledge($message);
        // @codingStandardsIgnoreEnd
    }//end acknowledge()

    /**
     * @param EnvelopeInterface $envelope
     * @return \Enqueue\Stomp\StompMessage
     */
    protected function createMessage(EnvelopeInterface $envelope)
    {
        $mergerProperties = $envelope->getProperties();
        $properties       = array_key_exists(self::MSG_PROPERTIES, $mergerProperties) ? $mergerProperties[self::MSG_PROPERTIES] : [];
        $receiptHandler   = array_key_exists(self::MSG_RECEIPT_HANDLE, $mergerProperties)
            ? $mergerProperties[self::MSG_RECEIPT_HANDLE]
            : null;
        $message          = $this->mqConfig->getConnection()->createMessage($envelope->getBody(), $properties);
        if ($receiptHandler) {
            $message->setFrame($receiptHandler);
        }
        return $message;
    }//end createMessage()

    /**
     * {@inheritdoc}
     */
    public function subscribe($callback, int $qtyOfMessages = null)
    {
        $index = 0;
        while (true) {
            /**
             * @var \Enqueue\Stomp\StompMessage $message
             */
            if ($message = $this->createConsumer()->receive(self::TIMEOUT_PROCESS)) {
                $index++;
                $envelope = $this->createEnvelop($message);

                if ($callback instanceof \Closure) {
                    $callback($envelope);
                } else {
                    call_user_func($callback, $envelope);
                }
                if (null !== $qtyOfMessages && $index >= $qtyOfMessages) {
                    break;
                }
            }
        }
    }//end subscribe()


    /**
     * (@inheritdoc)
     */
    public function reject(EnvelopeInterface $envelope, $requeue = true, $rejectionMessage = null)
    {
        $message  = $this->createMessage($envelope);
        $this->consumer = $this->createConsumer();
        $this->consumer->reject($message, $requeue);
    }//end reject()


    /**
     * (@inheritdoc)
     */
    public function push(EnvelopeInterface $envelope)
    {
        $message = $this->createMessage($envelope);
        $this->mqConfig->getConnection()->createProducer()->send($this->getQueue(), $message);
    }//end push()


    /**
     * @return \Enqueue\Stomp\StompContext
     */
    public function getConnection()
    {
        return $this->mqConfig->getConnection();
    }//end getConnection()
}//end class
