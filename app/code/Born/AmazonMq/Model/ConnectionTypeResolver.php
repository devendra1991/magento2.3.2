<?php
/**
 * Born_AmazonMq
 *
 * PHP version 7.x
 *
 * @category  PHP
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AmazonMq\Model;

use Magento\Framework\MessageQueue\ConnectionTypeResolverInterface;
use Magento\Framework\App\DeploymentConfig;

/**
 * ConnectionTypeResolver
 *
 * @package   Born\AmazonMq
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class ConnectionTypeResolver implements ConnectionTypeResolverInterface
{
    const CONNECTION = 'connections';

    /**
     * AWS MQ connection names.
     *
     * @var string[]
     */
    private $mqConnectionName = [];

    /**
     * Initialize dependencies.
     *
     * @param DeploymentConfig $deploymentConfig Deployment Configuration
     *
     */
    public function __construct(DeploymentConfig $deploymentConfig)
    {
        $queueConfig = $deploymentConfig->getConfigData(Config::QUEUE_CONFIG);
        if (isset($queueConfig[self::CONNECTION]) && is_array($queueConfig[self::CONNECTION])) {
            $this->mqConnectionName = array_keys($queueConfig[self::CONNECTION]);
        }
        if (isset($queueConfig[Config::MQ_CONFIG])) {
            $this->mqConnectionName[] = Config::MQ_CONFIG;
        }
    }//end __construct()


    /**
     * {@inheritdoc}
     * @since 100.0.0
     */
    public function getConnectionType($connectionName)
    {
        return in_array($connectionName, $this->mqConnectionName) ? 'stomp' : null;
    }//end getConnectionType()
}//end class
