<?php
/**
 * OptionSelectBuilder Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_ConfigurableCatalog
 * @author     Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */


namespace Born\ConfigurableCatalog\Plugin;

class OptionSelectBuilder
{
    /**
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilder $subject
     * @param $result
     * @return mixed
     */

    public function afterGetSelect(
        \Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilder $subject,
        $result
    ) {
        return $result->order('attribute_option.option_id ASC');
    }
}
