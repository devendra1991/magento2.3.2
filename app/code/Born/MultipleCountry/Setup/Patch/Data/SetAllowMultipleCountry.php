<?php
/**
 * Born_MultipleCountry
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\MultipleCountry
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\MultipleCountry\Setup\Patch\Data;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Component\ComponentRegistrarInterface;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\File\Csv as CsvProcessor;

/**
 * Class SetAllowMultipleCountry
 *
 * @category  PHP
 * @package   Born\SetAllowMultipleCountry\Setup\Patch\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class SetAllowMultipleCountry implements DataPatchInterface
{
    /**
     * Set the Constant CONFIG_PATH
     *
     * CONFIG_PATH
     */
    const CONFIG_PATH = 'general/country/allow';

    /**
     * Set the Constant CSV_FILE_PATH
     *
     * CSV_FILE_PATH
     */
    const CSV_FILE_PATH = '/Setup/file/allowed_country.csv';

    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * TypeListInterface
     *
     * @var TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * WriteInterface
     *
     * @var WriterInterface
     */
    protected $writer;

    /**
     * ComponentRegistrarInterface
     *
     * @var ComponentRegistrarInterface
     */
    protected $componentRegistrar;

    /**
     * CsvProcessor
     *
     * @var CsvProcessor
     */
    protected $csvProcessor;

    /**
     * SetDefaultConfig constructor.
     *
     * @param ModuleDataSetupInterface    $moduleDataSetup    ModuleSetupInterface
     * @param TypeListInterface           $cacheTypeList      TypeListInterface
     * @param WriterInterface             $writer             WriterInterface
     * @param ComponentRegistrarInterface $componentRegistrar ComponentRegistrarInterface
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer,
        ComponentRegistrarInterface $componentRegistrar,
        CsvProcessor $csvProcessor
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->cacheTypeList = $cacheTypeList;
        $this->writer = $writer;
        $this->componentRegistrar = $componentRegistrar;
        $this->csvProcessor = $csvProcessor;
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     * @throws \Exception
     */
    public function apply()
    {
        $dirPath = $this->componentRegistrar->getPath(
            ComponentRegistrar::MODULE,
            'Born_MultipleCountry'
        );
        $filePath = $dirPath . self::CSV_FILE_PATH;
        $csvData = $this->csvProcessor->getData($filePath);
        foreach ($csvData as $key => $value) {
            $csv[] = implode(',', $value);
        }
        $implodeValues = implode(',', $csv);
        $this->moduleDataSetup->startSetup();
        /* Save the default allowed countries */
        $this->writer->save(
            self::CONFIG_PATH,
            $implodeValues,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
        // Clean Config type Cache
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }
}
