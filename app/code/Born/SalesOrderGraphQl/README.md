# About Born_SalesOrderGraphql
**SalesOrderGraphQl** provides type and resolver information for the GraphQl module
to generate sales orders information.

Please follow below steps:-

=> Born should be the Vendor and SalesOrderGraphQl should be the module name.

=> after placing the module in the right place next step is the enable module using following command

php bin/magento module:enable Born_SalesOrderGraphql

=> Next, after that run following command.

php bin/magento cache:flush 

=> Next, after that run following command.

php bin/magento setup:upgrade


=> You can check your GraphQL query response by installing chrome extension ChromeiQL or Altair GraphQL addon.
=> Put URL like "http://yourwebsiteurl/graphql"(EX: http://local.magentoce.com/graphql) to get order data 
=> In Request Body, You need to pass the required data(field) to request payload for getting a response of Sales Order record,
=> We have pass order increment id as 000000007 (response order increment id on success page) for fetch records of Sales Order increment Id 000000007. Based on define id in request result will be shown data of order of specific id.
=> Request Payload,

{
  customerPlacedOrder(order_increment_id: "000000007") {
    allOrderRecords {
     increment_id
     customer_name
     sub_total
     grand_total
     is_guest_customer
     shipping_method
     shipping_amount
     tax_amount
     discount_amount
     created_at
      items {
        image
        itemoptions
        pid
        price
        qty
        sku
        title
      }
    }
  }
}
=>Result for Order Increment id 000000007, I have added two products for order increment id 000000007 so in response they display both items in items array with Item  info:

{
  "data": {
    "customerPlacedOrder": {
      "allOrderRecords": [
        {
          "increment_id": "000000007",
          "customer_name": "manisha bansode",
          "sub_total": "40.0000",
          "grand_total": "45.0000",
          "is_guest_customer": false,
          "shipping_method": "flatrate_flatrate",
          "shipping_amount": "5.0000",
          "tax_amount": "0.0000",
          "discount_amount": "0.0000",
          "created_at": "2019-06-04 12:32:40",
          "items": [
            {
              "image": "http://dbi.magento231ee.com/media/catalog/product/i/m/images_1_.jpeg",
              "itemoptions": "",
              "pid": "2",
              "price": 20,
              "qty": "1.0000",
              "sku": "sample  product",
              "title": "sample  product"
            },
            {
              "image": "http://dbi.magento231ee.com/media/catalog/product/i/m/images_2_.jpeg",
              "itemoptions": "{\"config\":{\"Color\":\"blue\"}}",
              "pid": "5",
              "price": 20,
              "qty": "1.0000",
              "sku": "Test configurable product-blue",
              "title": "Test configurable product"
            }
          ]
        }
      ]
    }
  }
}


