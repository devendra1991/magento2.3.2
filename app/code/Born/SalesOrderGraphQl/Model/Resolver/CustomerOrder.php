<?php

/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_SalesOrderGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\SalesOrderGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\SalesOrderGraphQl\Model\Resolver;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * CustomerOrder
 * Class CustomerOrder
 *
 * @category  PHP
 * @package   Born\SalesOrderGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class CustomerOrder implements ResolverInterface
{
    /**
     * OrderRepositoryInterface
     *
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * SearchCriteriaBuilder
     *
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;


    /**
     * CustomerOrder constructor.
     *
     * @param OrderRepositoryInterface $orderRepository       OrderRepositoryInterface
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder SearchCriteriaBuilder
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository       = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }//end __construct()


    /**
     * Fetches the data and format it according to the GraphQL schema.
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   value
     * @param array|null       $args    arguments
     *
     * @throws \Exception
     * @return mixed|Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        $orderIncrementId = $this->getOrderIncrementId($args);
        $orderData        = $this->getOrderData($orderIncrementId);

        return $orderData;
    }//end resolve()


    /**
     * Get Order Increemnt Id
     *
     * @param  array $args arguments
     * @return string
     */
    public function getOrderIncrementId(array $args): string
    {

        if (!isset($args['order_increment_id'])) {
            throw new GraphQlInputException(__('"Order increment id must be specified'));
        }

        return (string) $args['order_increment_id'];
    }//end getOrderIncrementId()


    /**
     * Get Order Data
     *
     * @param string $orderIncrementId OrderIncrementId
     *
     * @return array
     */
    public function getOrderData(string $orderIncrementId): array
    {
        try {
            /* filter for all customer orders */

            $searchCriteria = $this->searchCriteriaBuilder->addFilter(
                'increment_id',
                $orderIncrementId,
                'eq'
            )->create();
            $orders         = $this->orderRepository->getList($searchCriteria);
            $optionArray    = [];
            $orderData      = [];
            $itemsData      = [];
            $customOption   = '';

            foreach ($orders as $order) {
                $orderId = $order->getId();
                $getOrderData = $this->orderRepository->get($orderId);
                if (isset($getOrderData)) {
                    foreach ($getOrderData->getAllVisibleItems() as $item) {
                        $itemsData['product_id']  = $item->getProductId();
                        $itemsData['sku']         = $item->getSku();
                        $itemsData['name']        = $item->getName();
                        $itemsData['type']        = $item->getProductType();
                        $itemsData['qty_ordered'] = $item->getQtyOrdered();
                        $itemsData['price']       = $item->getPrice();
                        $itemsData['image']       = $item->getProductImage();
                        if ($item->getProductType() == Configurable::TYPE_CODE) {
                            $getAttributeOptions = [];
                            $itemsConfig = $item->getProductOptions();
                            $getAttributeOptions = $itemsConfig['attributes_info'];
                            if (!empty($getAttributeOptions)) {
                                foreach ($getAttributeOptions as $options) {
                                    $optionArray['config'][$options['label']] = $options['value'];
                                }
                            }
                            $customOption = json_encode($optionArray);
                        }
                        $getAttributeItems[] = array_merge($itemsData, ['itemoptions' => $customOption]);
                    }
                    if ($order->getCustomerName() === 'Guest') {
                        $billingAddress = $order->getBillingAddress();
                        $customerName   = $billingAddress['firstname'] . ' ' . $billingAddress['lastname'];
                    } else {
                        $customerName = $order->getCustomerName();
                    }
                    $allOrderRecords = "allOrderRecords";
                    $orderData[$allOrderRecords][$orderId]['customer_name']     = $customerName;
                    $orderData[$allOrderRecords][$orderId]['created_at']        = $order->getCreatedAt();
                    $orderData[$allOrderRecords][$orderId]['shipping_method']   = $order->getShippingMethod();
                    $orderData[$allOrderRecords][$orderId]['increment_id']      = $order->getIncrementId();
                    $orderData[$allOrderRecords][$orderId]['grand_total']       = $order->getGrandTotal();
                    $orderData[$allOrderRecords][$orderId]['shipping_amount']   = $order->getShippingAmount();
                    $orderData[$allOrderRecords][$orderId]['discount_amount']   = $order->getDiscountAmount();
                    $orderData[$allOrderRecords][$orderId]['sub_total']         = $order->getSubtotal();
                    $orderData[$allOrderRecords][$orderId]['tax_amount']        = $order->getTaxAmount();
                    $orderData[$allOrderRecords][$orderId]['is_guest_customer'] = !empty($order->getCustomerIsGuest()) ? 1 : 0;
                    $orderData[$allOrderRecords][$orderId]['items']             = $getAttributeItems;
                }
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }

        return $orderData;
    }//end getOrderData()


}//end class
