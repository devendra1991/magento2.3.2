<?php

/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_SalesOrderGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\SalesOrderGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\SalesOrderGraphQl\Model\Resolver;


use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\GraphQl\Query\Resolver\ValueFactory;


/**
 * CustomerOrder
 * Class Items
 *
 * @category  PHP
 * @package   Born\SalesOrderGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class Items implements ResolverInterface
{

    /**
     * ProductRepository
     *
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductFactory
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ValueFactory
     *
     * @var ValueFactory
     */
    protected $valueFactory;

    /**
     * Items constructor.
     *
     * @param ProductRepository     $productRepository ProductRepository
     * @param ProductFactory        $productFactory    ProductFactory
     * @param StoreManagerInterface $storeManager      StoreManagerInterface
     * @param ValueFactory          $valueFactory      ValueFactory
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        StoreManagerInterface $storeManager,
        ValueFactory $valueFactory
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->valueFactory = $valueFactory;
    }

    /**
     * Fetches the data and format it according to the GraphQL schema.
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   array
     * @param array|null       $args    array
     *
     * @throws \Exception
     * @return mixed|Value
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        
        if (!isset($value['items'])) {

            return null;
        }
        $itemArray = function () use ($value) {
            foreach ($value['items'] as $key => $item)
            {

                $product = $this->productFactory->create()->load($item['product_id']);
                $store = $this->storeManager->getStore();
                $productImageUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product'
                    . $product->getImage();
                $itemArray[$key]['sku'] = $item['sku'];
                $itemArray[$key]['title'] = $item['name'];
                $itemArray[$key]['price'] = $item['price'];
                $itemArray[$key]['qty'] = $item['qty_ordered'];
                $itemArray[$key]['pid'] = $item['product_id'];
                $itemArray[$key]['image'] = $productImageUrl;
                $itemArray[$key]['itemoptions'] = $item['itemoptions'];
            }
            return $itemArray;
        };
        return $this->valueFactory->create($itemArray);

    }

}
