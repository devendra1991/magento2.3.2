<?php
/**
 * Born_BulkDiscount
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\BulkDiscount
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\BulkDiscount\Model\Resolver\Product;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\TierPrice;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use phpDocumentor\Reflection\Types\Float_;

/**
 * Class BulkDiscount
 *
 * @category  PHP
 * @package   Born\BulkDiscount\Model\Resolver\Product
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class TierPrices implements ResolverInterface
{
    /**
     * Format product's tier price data to conform to GraphQL schema
     *
     * @param \Magento\Framework\GraphQl\Config\Element\Field $field   Field
     * @param ContextInterface                                $context ContextInterface
     * @param ResolveInfo                                     $info    ResolveInfo
     * @param array|null                                      $value   Value
     * @param array|null                                      $args    Arguments
     *
     * @throws \Exception
     * @return null|array
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        // @var Product $product
        $product = $value['model'];
        $basePrice = $product->getPrice();
        $tierPrices = null;
        if ($product->getTierPrices()) {
            $tierPrices = [];
            // @var TierPrice $tierPrice
            foreach ($product->getTierPrices() as $tierPrice) {
                $percentageValue = $this->getPercentageValue($basePrice, $tierPrice->getValue());
                $tierPrice->setData('percentage_value', $percentageValue);
                $tierPrices[] = $tierPrice->getData();
            }
        }

        return $tierPrices;
    }

    /**
     * Calculate the percentage difference between two product price.
     *
     * @param Float $basePrice Product Base Price
     * @param Float $tierPrice Product Tier Price
     *
     * @return float|int|string
     */
    protected function getPercentageValue($basePrice, $tierPrice)
    {
        $percentageValue = '';
        if ($basePrice > $tierPrice) {
            $percentageValue = round((($basePrice - $tierPrice) / ($basePrice)) * 100);
        }

        return $percentageValue;
    }
}
