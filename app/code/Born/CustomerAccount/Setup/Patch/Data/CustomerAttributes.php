<?php
/**
 * Born_CustomerAccount
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerAccount
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
namespace Born\CustomerAccount\Setup\Patch\Data;


use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\Frontend\Datetime as FrontendDatetime;
use Magento\Eav\Model\Entity\Attribute\Backend\Datetime as BackendDatetime;
use Born\CustomerAccount\Model\Customer\Attribute\Source\EventRole;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;

/**
 * Class CustomerAttributes
 *
 * @category  PHP
 * @package   Born\CustomerAccount
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerAttributes implements DataPatchInterface
{
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * CustomerSetupFactory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * AttributeSetFactory
     *
     * @var AttributeSetFactory
     */
    protected $attributeSetFactory;


    /**
     * CustomerAttributes constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup      ModuleDataSetupInterface
     * @param CustomerSetupFactory     $customerSetupFactory CustomerSetupFactory
     * @param AttributeSetFactory      $attributeSetFactory  AttributeSetFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->moduleDataSetup      = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory  = $attributeSetFactory;
    }//end __construct()


    /**
     * Create the custom attributes for Customer
     *
     * @return DataPatchInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply()
    {
        $attributeEntities = $this->getCustomerAttributes();
        foreach ($attributeEntities as $entity => $values) {
            $customerSetup    = $this->customerSetupFactory
                ->create(['setup' => $this->moduleDataSetup]);
            $customerEntity   = $customerSetup->getEavConfig()
                ->getEntityType(Customer::ENTITY);
            $attributeSetId   = $customerEntity->getDefaultAttributeSetId();
            $attributeSet     = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
            $customerSetup->addAttribute(
                Customer::ENTITY,
                $entity,
                $values
            );
            $attribute = $customerSetup->getEavConfig()->getAttribute(
                Customer::ENTITY,
                $entity
            );
            $attribute->addData(
                [
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => [
                        'adminhtml_customer',
                        'customer_account_create',
                        'customer_account_edit',
                    ],
                ]
            );
            $attribute->save();
        }
    }//end apply()


    /**
     * Get Dependency Module
     *
     * @return array|string[]
     */
    public static function getDependencies(): array
    {

        return [];
    }//end getDependencies()


    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases(): array
    {

        return [];
    }//end getAliases()


    /**
     * Get Customer Attributes
     *
     * @return array|string[]
     */
    private function getCustomerAttributes()
    {
        $attribute = [
            'event_role' => [
                'type' => 'varchar',
                'label' => 'Event Role',
                'input' => 'select',
                'source' => EventRole::class,
                'validate_rules' => '[]',
                'required' => false,
                'sort_order' => 125,
                'position' => 125,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
            ],
            'event_date' => [
                'type' => 'datetime',
                'label' => 'Event Date',
                'input' => 'date',
                'frontend' => FrontendDatetime::class,
                'backend' => BackendDatetime::class,
                'validate_rules' => '{"input_validation":"date"}',
                'required' => false,
                'sort_order' => 130,
                'position' => 130,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
            ],
            'engagement_date' => [
                'type' => 'datetime',
                'label' => 'Engagement Date',
                'input' => 'date',
                'frontend' => FrontendDatetime::class,
                'backend' => BackendDatetime::class,
                'validate_rules' => '{"input_validation":"date"}',
                'required' => false,
                'sort_order' => 135,
                'position' => 135,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
            ],
            'purchased_dress' => [
                'type' => 'int',
                'label' => 'Have you purchased your dress?',
                'input' => 'select',
                'source' => Boolean::class,
                'validate_rules' => '[]',
                'required' => false,
                'sort_order' => 140,
                'position' => 140,
                'visible' => true,
                'user_defined' => true,
                'unique' => false,
                'system' => false,
            ],
        ];

        return $attribute;
    }//end getCustomerAttributes()


}//end class
