<?php
/**
 * Born_CustomerAccount
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_CustomerAccount
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerAccount\Model\Customer\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class CustomerAttributes
 *
 * @category  PHP
 * @package   Born\CustomerAccount
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class EventRole extends AbstractSource
{


    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $options    = [
            'Bride' => 'Bride',
            'Bridesmaid' => 'Bridesmaid',
            'MotherofBrideGroom' => 'Mother of Bride/Groom',
            'PartyGoer' => 'Party Goer',
            'PromGirl' => 'Prom Girl',
            'Partner' => 'Partner',
            ];
        $arrOptions = [];
        foreach ($options as $key => $value) {
            $arrOptions[] = [
                'value' => $key,
                'label' => __($value),
            ];
        }

        return $arrOptions;
    }//end getAllOptions()


    /**
     * Get text of the option value
     *
     * @param string|integer $value value
     * @return string|bool
     */
    public function getOptionValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }

        return false;
    }//end getOptionValue()


}//end class
