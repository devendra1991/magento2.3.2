<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Model;

use Magento\Sales\Model\Order;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;

/**
 * Class CustomerManagementInterface
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Customer
{
    const BILLING_ADDRESS  = 'billing_address';
    const SHIPPING_ADDRESS = 'shipping_address';
    const REGION_ID        = 'region_id';
    const REGION           = 'region';
    const STREET           = 'street';

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var array
     */
    private $orderData;


    /**
     * Data constructor.
     * @param Order $order Order
     */
    public function __construct(
        Order $order
    ) {
        $this->order = $order;
    }//end __construct()


    /**
     * Get Customer account Information from the order
     * @param string $orderId string
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    public function prepareCustomerAccountData(string $orderId)
    {
        $addressData         = $this->getOrderData($orderId);
        $billingAddress      = $addressData[self::BILLING_ADDRESS];
        $accountData         = [];
        $customerAccountData = [
            'email',
            'firstname',
            'lastname',
            'middlename',
            'password',
        ];
        foreach ($customerAccountData as $data) {
            if (key_exists($data, $billingAddress)) {
                $accountData[$data] = $billingAddress[$data];
            }
        }

        return $accountData;
    }//end prepareCustomerAccountData()


    /**
     * Fetch customer data from the order
     * @param string $orderId string
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    public function getOrderData(string $orderId)
    {
        if (empty($this->orderData)) {
            $orderCollection = $this->order->loadByIncrementId($orderId);
            if (empty($orderCollection->getData())) {
                throw new GraphQlNoSuchEntityException(__('Invalid Order ID'));
            }
            $this->orderData = [
                self::BILLING_ADDRESS => $orderCollection->getShippingAddress()->getData(),
                self::SHIPPING_ADDRESS => $orderCollection->getBillingAddress()->getData(),
            ];
        }

        return $this->orderData;
    }//end getOrderData()


    /**
     * Get Billing Address from the Order
     * @param string $orderId string
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    public function getBillingAddress(string $orderId)
    {
        $regionData     = [
            self::REGION_ID,
            self::REGION,
        ];
        $addressData    = $this->getOrderData($orderId);
        $billingAddress = $addressData[self::BILLING_ADDRESS];
        foreach ($regionData as $region) {
            if (key_exists($region, $billingAddress)) {
                unset($billingAddress[$region]);
            }
        }
        $billingAddress[self::STREET] = [$billingAddress[self::STREET]];

        return $this->filterCustomerAddress($billingAddress);
    }//end getBillingAddress()


    /**
     * Get Billing Address Region Data
     * @param string $orderId string
     * @return array
     */
    public function getBillingRegionData(string $orderId)
    {
        $addressData    = $this->getOrderData($orderId);
        $billingAddress = $addressData[self::BILLING_ADDRESS];
        $regionData     = [
            self::REGION_ID,
            self::REGION,
        ];
        $regionValue    = [];
        foreach ($regionData as $region) {
            if (key_exists($region, $billingAddress)) {
                $regionValue[$region] = $billingAddress[$region];
            }
        }

        return $regionValue;
    }//end getBillingRegionData()


    /**
     * Get Shipping Address Region Data
     * @param string $orderId string
     * @return array
     */
    public function getShippingRegionData(string $orderId)
    {
        $addressData     = $this->getOrderData($orderId);
        $shippingAddress = $addressData[self::SHIPPING_ADDRESS];
        $regionData      = [
            self::REGION_ID,
            self::REGION,
        ];
        $regionValue     = [];
        foreach ($regionData as $region) {
            if (key_exists($region, $shippingAddress)) {
                $regionValue[$region] = $shippingAddress[$region];
            }
        }

        return $regionValue;
    }//end getShippingRegionData()


    /**
     * Get Shipping Address from the order
     * @param string $orderId string
     * @return array
     */
    public function getShippingAddress(string $orderId)
    {
        $addressData     = $this->getOrderData($orderId);
        $shippingAddress = $addressData[self::SHIPPING_ADDRESS];
        $regionData      = [
            self::REGION_ID,
            self::REGION,
        ];
        foreach ($regionData as $region) {
            if (key_exists($region, $shippingAddress)) {
                unset($shippingAddress[$region]);
            }
        }
        $shippingAddress[self::STREET] = [$shippingAddress[self::STREET]];

        return $this->filterCustomerAddress($shippingAddress);
    }//end getShippingAddress()


    /**
     * Remove unnecessary address item from order address item
     * @param array $address array
     * @return mixed
     */
    public function filterCustomerAddress(array $address)
    {
        /** Allowed address field (Magento\Customer\Api\Data\AddressInterface) **/
        $filters = [
            'country_id',
            'street',
            'company',
            'telephone',
            'fax',
            'postcode',
            'city',
            'firstname',
            'lastname',
            'middlename',
            'prefix',
            'suffix',
            'vat_id',
            'default_billing',
            'default_shipping',
        ];
        foreach ($address as $key => $value) {
            if (!in_array($key, $filters)) {
                unset($address[$key]);
            }
        }

        return $address;
    }//end filterCustomerAddress()


}//end class
