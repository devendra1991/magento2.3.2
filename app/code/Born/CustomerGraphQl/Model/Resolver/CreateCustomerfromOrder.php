<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Model\Resolver;

use Born\CustomerGraphQl\Api\CustomerManagementInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Validator\Exception as ValidatorException;

/**
 * Class CustomerManagementInterface
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CreateCustomerfromOrder implements ResolverInterface
{
    /**
     * @var CustomerManagementInterface
     */
    private $customerManagementInterface;


    /**
     * CreateCustomerfromOrder constructor.
     * @param CustomerManagementInterface $customerManagementInterface CustomerManagementInterface
     */
    public function __construct(
        CustomerManagementInterface $customerManagementInterface
    ) {
        $this->customerManagementInterface = $customerManagementInterface;
    }//end __construct()


    /**
     * @param Field                                                      $field   Field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context ContextInterface
     * @param ResolveInfo                                                $info    ResolveInfo
     * @param array|null                                                 $value   array|null
     * @param array|null                                                 $args    array|null
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlInputException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['input']) || !is_array($args['input']) || empty($args['input'])) {
            throw new GraphQlInputException(__('"input" value should be specified'));
        }
        $data = [];
        if (array_key_exists('order_id', $args['input'])) {
            if ($args['input']['order_id'] && $args['input']['password']) {
                try {
                    $data = $this->customerManagementInterface->createCustomerFromOrder($args, $context);
                } catch (ValidatorException $e) {
                    throw new GraphQlInputException(__($e->getMessage()));
                }
            } else {
                throw new GraphQlInputException(__('"input order_id, password" value should be specified'));
            }
        }

        return ['customer' => $data];
    }//end resolve()


}//end class
