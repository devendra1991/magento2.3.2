<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Model\Resolver;

use Born\CustomerGraphQl\Model\Customer\CreateCustomer as CreateAccount;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Directory\Model\AllowedCountries;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CustomerGraphQl\Model\Customer\ExtractCustomerData;
use Magento\CustomerGraphQl\Model\Customer\Address\CreateCustomerAddress as CreateCustomerAddressModel;
use Magento\CustomerGraphQl\Model\Customer\Address\ExtractCustomerAddressData;

/**
 * Class CreateCustomer
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CreateCustomer implements ResolverInterface
{
    const FIRST_NMAE   = 'firstname';
    const LAST_NMAE    = 'lastname';
    const EMAIL        = 'email';
    const PHONE_NUMBER = 'phone_number';
    const COUNTRY      = 'country';
    const STATE        = 'state';
    const FIELD_NAME   = 'fieldName';
    const INPUT        = 'input';

    /**
     * @var ExtractCustomerAddressData
     */
    private $extractCustomerAddressData;

    /**
     * @var CreateCustomerAddressModel
     */
    private $createCustomerAddress;

    /**
     * @var ExtractCustomerData
     */
    private $extractCustomerData;

    /**
     * CreateAccount
     *
     * @var CreateAccount
     */
    protected $createAccount;

    /**
     * AddressInterfaceFactory
     *
     * @var AddressInterfaceFactory
     */
    protected $addressInterfaceFactory;

    /**
     * DataObjectHelper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * RegionFactory
     *
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * AllowedCountries
     *
     * @var AllowedCountries
     */
    protected $allowedCountriesReader;


    /**
     * CreateCustomer constructor.
     *
     * @param CreateAccount              $createAccount              CreateAccount
     * @param AddressInterfaceFactory    $addressInterfaceFactory    AddressInterfaceFactory
     * @param DataObjectHelper           $dataObjectHelper           DataObjectHelper
     * @param RegionFactory              $regionFactory              RegionFactory
     * @param StoreManagerInterface      $storeManager               StoreManagerInterface
     * @param AllowedCountries           $allowedCountriesReader     AllowedCountries
     * @param ExtractCustomerData        $extractCustomerData        ExtractCustomerData
     * @param CreateCustomerAddressModel $createCustomerAddress      CreateCustomerAddressModel
     * @param ExtractCustomerAddressData $extractCustomerAddressData ExtractCustomerAddressData
     */
    public function __construct(        // NOSONAR.
        CreateAccount $createAccount,
        AddressInterfaceFactory $addressInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        RegionFactory $regionFactory,
        StoreManagerInterface $storeManager,
        AllowedCountries $allowedCountriesReader,
        ExtractCustomerData $extractCustomerData,
        CreateCustomerAddressModel $createCustomerAddress,
        ExtractCustomerAddressData $extractCustomerAddressData
    ) {
        $this->createAccount              = $createAccount;
        $this->addressInterfaceFactory    = $addressInterfaceFactory;
        $this->dataObjectHelper           = $dataObjectHelper;
        $this->regionFactory              = $regionFactory;
        $this->storeManager               = $storeManager;
        $this->allowedCountriesReader     = $allowedCountriesReader;
        $this->extractCustomerData        = $extractCustomerData;
        $this->createCustomerAddress      = $createCustomerAddress;
        $this->extractCustomerAddressData = $extractCustomerAddressData;
    }//end __construct()


    /**
     * Resolver Interface
     * @param Field            $field   Field
     * @param ContextInterface $context Context
     * @param ResolveInfo      $info    Resolver Info
     * @param array|null       $value   Value
     * @param array|null       $args    Arguments
     *
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlInputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Validate_Exception
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args[self::INPUT]) || !is_array($args[self::INPUT]) || empty($args[self::INPUT])) {
            throw new GraphQlInputException(__('"input" value should be specified'));
        }
        $input        = $args[self::INPUT];
        $customerKeys = [
            self::FIRST_NMAE,
            self::LAST_NMAE,
            self::EMAIL,
            'password',
            self::PHONE_NUMBER,
        ];
        $customerInfo = [];
        foreach ($customerKeys as $key) {
            $customerInfo[$key] = $input[$key];
        }
        $region  = $this->regionFactory->create();
        $storeId = (int) $this->storeManager->getStore()->getId();
        /* Validate Country */
        $this->validateCountry($input[self::COUNTRY], $storeId);
        $region->loadByName($input[self::STATE], $input[self::COUNTRY]);
        /* Get the Region Id by State/Region */
        $regionId = (int) $region->getId();
        if (!$regionId) {
            throw new GraphQlInputException(
                __(
                    '"%state" is not matched with "%country". Invalid state "%state".',
                    [
                        self::FIELD_NAME => 'state',
                        self::STATE => htmlspecialchars($input[self::STATE]),
                        self::COUNTRY => htmlspecialchars($input[self::COUNTRY]),
                    ]
                )
            );
        }
        $addressInfo = $this->getAddressData($input, $regionId);

        return $this->saveCustomerAddress($customerInfo, $addressInfo);
    }//end resolve()


    /**
     * Set the Required Address data to save.
     *
     * @param array $input    Request Parameter
     * @param int   $regionId Region Id
     *
     * @return array
     */
    protected function getAddressData(array $input, int $regionId): array
    {
        $addressKeys = [
            'street',
            'postcode',
            'city',
        ];
        $addressInfo = [];
        foreach ($addressKeys as $key) {
            $addressInfo[$key] = $input[$key];
        }
        $addressRequiredInfo                     = [];
        $addressRequiredInfo[self::FIRST_NMAE]   = $input[self::FIRST_NMAE];
        $addressRequiredInfo[self::LAST_NMAE]    = $input[self::LAST_NMAE];
        $addressRequiredInfo[self::PHONE_NUMBER] = $input[self::PHONE_NUMBER];
        $addressRequiredInfo['telephone']        = $input[self::PHONE_NUMBER];
        $addressRequiredInfo['country_id']       = $input[self::COUNTRY];
        $addressRequiredInfo['region_id']        = $regionId;
        $addressRequiredInfo['default_billing']  = true;
        $addressRequiredInfo['default_shipping'] = true;

        return array_merge($addressInfo, $addressRequiredInfo);
    }//end getAddressData()


    /**
     * Save Customer Information (Customer and Address)
     *
     * @param array $customerInfo Customer Information
     * @param array $addressInfo  Address Information
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function saveCustomerAddress(array $customerInfo, array $addressInfo): array
    {
        $address = $this->addressInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $address,
            $addressInfo,
            AddressInterface::class
        );
        /* Create Customer Account */
        $customer     = $this->createCustomerAccount($customerInfo);
        $customerId   = (int) $customer->getId();
        $customer     = $this->extractCustomerData->execute($customer);
        $customerData = [
            self::EMAIL => $customer[self::EMAIL],
            self::FIRST_NMAE => $customer[self::FIRST_NMAE],
            self::LAST_NMAE => $customer[self::LAST_NMAE],
            self::PHONE_NUMBER => $customer[self::PHONE_NUMBER],
        ];
        $address->setCustomerId($customerId);
        /* Create Customer Address */
        $address                = $this->createCustomerAddress->execute((int) $customerId, $addressInfo);
        $address                = $this->extractCustomerAddressData->execute($address);
        $region                 = $this->regionFactory->create()->load($address['region_id']);
        $address[self::STATE]   = $region->getName();
        $address[self::COUNTRY] = $address['country_id'];
        $addressData            = ['addressInfo' => $address];
        $customerResult         = array_merge($customerData, $addressData);

        return $customerResult;
    }//end saveCustomerAddress()


    /**
     * Create Customer Account
     *
     * @param array $customerInfo Customer Information
     *
     * @return object
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function createCustomerAccount(array $customerInfo)
    {

        return $this->createAccount->execute($customerInfo);
    }//end createCustomerAccount()


    /**
     * Validate Country by country code on request parameter
     *
     * @param string $countryId CountryId
     * @param int    $storeId   Store Id
     *
     * @return null
     * @throws GraphQlInputException
     * @throws \Zend_Validate_Exception
     */
    protected function validateCountry(string $countryId, int $storeId)
    {
        if (!\Zend_Validate::is($countryId, 'NotEmpty')) {
            throw new GraphQlInputException(
                __(
                    '"%fieldName" is required. Enter and try again.',
                    [self::FIELD_NAME => 'countryId']
                )
            );
        }
        if (!in_array($countryId, $this->getWebsiteAllowedCountries($storeId), true)) {
            throw new GraphQlInputException(
                __(
                    'Invalid value of "%value" provided for the %fieldName field.',
                    [
                        self::FIELD_NAME => 'countryId',
                        'value' => htmlspecialchars($countryId),
                    ]
                )
            );
        }
    }//end validateCountry()


    /**
     * Get WebsiteAllowed Country
     *
     * @param int $storeId Store Id
     *
     * @return array
     */
    protected function getWebsiteAllowedCountries(int $storeId): array
    {
        return $this->allowedCountriesReader->getAllowedCountries(
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }//end getWebsiteAllowedCountries()


}//end class
