<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHPCustomer
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Model\Customer;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CreateCustomer
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Model\Customer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CreateCustomer
{
    /**
     * DataObjectHelper
     *
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * CustomerInterfaceFactory
     *
     * @var CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * AccountManagementInterface
     *
     * @var AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;


    /**
     * CreateCustomer constructor.
     *
     * @param DataObjectHelper           $dataObjectHelper  DataObjectHelper
     * @param CustomerInterfaceFactory   $customerFactory   CustomerInterfaceFactory
     * @param StoreManagerInterface      $storeManager      StoreManagerInterface
     * @param AccountManagementInterface $accountManagement AccountManagementInterface
     */
    public function __construct(
        DataObjectHelper $dataObjectHelper,
        CustomerInterfaceFactory $customerFactory,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement
    ) {
        $this->dataObjectHelper  = $dataObjectHelper;
        $this->customerFactory   = $customerFactory;
        $this->accountManagement = $accountManagement;
        $this->storeManager      = $storeManager;
    }//end __construct()


    /**
     * Create new customer account
     *
     * @param array $customerInfo request parameter
     *
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(array $customerInfo): CustomerInterface
    {
        $customerDataObject = $this->customerFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $customerDataObject,
            $customerInfo,
            CustomerInterface::class
        );
        $store = $this->storeManager->getStore();
        $customerDataObject->setWebsiteId($store->getWebsiteId());
        $customerDataObject->setStoreId($store->getId());
        $password = array_key_exists('password', $customerInfo) ? $customerInfo['password'] : null;

        return $this->accountManagement->createAccount($customerDataObject, $password);
    }//end execute()


}//end class
