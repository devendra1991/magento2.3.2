<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Model;

use Born\CustomerGraphQl\Api\CustomerManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterface;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\CustomerGraphQl\Model\Customer\ChangeSubscriptionStatus;
use Magento\Authorization\Model\UserContextInterface;
use Magento\CustomerGraphQl\Model\Customer\CreateCustomerAccount;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\GraphQl\Model\Query\Resolver\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\CustomerGraphQl\Model\Customer\GetCustomer;

/**
 * Class CustomerManagementInterface
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerManagement implements CustomerManagementInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var GetCustomer
     */
    private $getCustomer;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var AddressInterfaceFactory
     */
    private $addressInterfaceFactory;

    /**
     * @var RegionInterfaceFactory
     */
    private $regionInterfaceFactory;

    /**
     * @var ChangeSubscriptionStatus
     */
    private $changeSubscriptionStatus;

    /**
     * @var CreateCustomerAccount
     */
    private $createAccount;

    /**
     * @var Customer
     */
    private $customer;


    /**
     * CustomerManagement constructor.
     *
     * @param OrderRepositoryInterface   $orderRepository          OrderRepositoryInterface
     * @param SearchCriteriaBuilder      $searchCriteriaBuilder    SearchCriteriaBuilder
     * @param RegionInterfaceFactory     $regionInterfaceFactory   RegionInterfaceFactory
     * @param Customer                   $customer                 Customer
     * @param ChangeSubscriptionStatus   $changeSubscriptionStatus ChangeSubscriptionStatus
     * @param CreateCustomerAccount      $createAccount            CreateCustomerAccount
     * @param DataObjectHelper           $dataObjectHelper         DataObjectHelper
     * @param AddressRepositoryInterface $addressRepository        AddressRepositoryInterface
     * @param AddressInterfaceFactory    $addressInterfaceFactory  AddressInterfaceFactory
     * @param GetCustomer                $getCustomer              GetCustomer
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RegionInterfaceFactory $regionInterfaceFactory,
        Customer $customer,
        ChangeSubscriptionStatus $changeSubscriptionStatus,
        CreateCustomerAccount $createAccount,
        DataObjectHelper $dataObjectHelper,
        AddressRepositoryInterface $addressRepository,
        AddressInterfaceFactory $addressInterfaceFactory,
        GetCustomer $getCustomer
    ) {
        $this->orderRepository          = $orderRepository;
        $this->searchCriteriaBuilder    = $searchCriteriaBuilder;
        $this->regionInterfaceFactory   = $regionInterfaceFactory;
        $this->customer                 = $customer;
        $this->changeSubscriptionStatus = $changeSubscriptionStatus;
        $this->createAccount            = $createAccount;
        $this->dataObjectHelper         = $dataObjectHelper;
        $this->addressRepository        = $addressRepository;
        $this->addressInterfaceFactory  = $addressInterfaceFactory;
        $this->getCustomer              = $getCustomer;
    }//end __construct()


    /**
     * @param array   $args    array
     * @param Context $context Context
     * @return array|mixed
     * @throws GraphQlInputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException
     */
    public function createCustomerFromOrder(array $args, Context $context)
    {
        $orderId = $args['input']['order_id'];
        try {
            $customerAccountData             = $this->customer->prepareCustomerAccountData($orderId);
            $customerAccountData['password'] = $args['input']['password'];
            $customerAccountData['gender']   = isset($args['input']['gender']) ? $args['input']['gender'] : '';
            $customer                        = $this->createAccount->execute($customerAccountData);
            $customerId                      = (int) $customer->getId();

            $context->setUserId((int) $customer->getId());
            $context->setUserType(UserContextInterface::USER_TYPE_CUSTOMER);

            if (array_key_exists('is_subscribed', $args['input'])) {
                if ($args['input']['is_subscribed']) {
                    $this->changeSubscriptionStatus->execute($customerId, true);
                }
            }
            $billingAddressData  = $this->customer->getBillingAddress($orderId);
            $shippingAddressData = $this->customer->getShippingAddress($orderId);
            $billingRegionData   = $this->customer->getBillingRegionData($orderId);

            $currentUserType = $context->getUserType();

            if ($billingAddressData === $shippingAddressData) {
                $this->getCustomer->execute($context);
                $this->createCustomerAddress($customerId, $billingAddressData, $billingRegionData);
            } else {
                $shippingRegionData = $this->customer->getShippingRegionData($orderId);
                $this->getCustomer->execute($context);
                $this->createCustomerAddress($customerId, $billingAddressData, $billingRegionData);
                $this->createCustomerAddress($customerId, $shippingAddressData, $shippingRegionData);
            }
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter(
                    'increment_id',
                    $orderId,
                    'eq'
                )->create();
            // @codingStandardsIgnoreStart
            $order          = $this->orderRepository->getList($searchCriteria)->getFirstItem();
            // @codingStandardsIgnoreEnd
            if ($order->getId() && !$order->getCustomerId()) {
                $order->setCustomerId($customerId);
                $order->setCustomerIsGuest(0);
                $this->orderRepository->save($order);
            }
        } catch (ValidatorException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        } catch (InputMismatchException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }

        return ['id' => $customerId];
    }//end createCustomerFromOrder()


    /**
     * Create customer address
     *
     * @param  int   $customerId  int
     * @param  array $addressData array
     * @param  array $regionData  array
     * @return AddressInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function createCustomerAddress(int $customerId, array $addressData, array $regionData) : AddressInterface
    {
        /** @var AddressInterface $address */
        $address = $this->addressInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray($address, $addressData, AddressInterface::class);

        $region = $this->regionInterfaceFactory->create();
        $this->dataObjectHelper->populateWithArray($region, $regionData, RegionInterface::class);
        $address->setRegion($region);

        $address->setCustomerId($customerId);
        $address->setIsDefaultBilling(true);
        $address->setIsDefaultShipping(true);
        try {
            $address = $this->addressRepository->save($address);
        } catch (GraphQlInputException $e) {
            throw new GraphQlInputException(__($e->getMessage()), $e);
        }

        return $address;
    }//end createCustomerAddress()


}//end class
