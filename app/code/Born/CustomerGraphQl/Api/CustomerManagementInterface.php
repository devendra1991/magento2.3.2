<?php
/**
 * Born_CustomerGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CustomerGraphQl\Api;

use Magento\GraphQl\Model\Query\Resolver\Context;

/**
 * Interface CustomerManagementInterface
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CustomerManagementInterface
{


    /**
     * Create Customer from order ID
     *
     * @param  array   $args    array
     * @param  Context $context Context
     * @return mixed
     */
    public function createCustomerFromOrder(array $args, Context $context);


}//end interface
