<?php
/**
 * Born_MageConf
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\MageConf
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\MageConf\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class SetDefaultConfig
 *
 * @category  PHP
 * @package   Born\MageConf\Setup\Patch\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class SetDefaultInventoryStock implements DataPatchInterface
{
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * TypeListInterface
     *
     * @var TypeListInterface
     */
    protected $cacheTypeList;
    /**
     * WriteInterface
     *
     * @var WriterInterface
     */
    protected $writer;
    /**
     * SetDefaultConfig constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup ModuleSetupInterface
     * @param TypeListInterface        $cacheTypeList   TypeListInterface
     * @param WriterInterface          $writer          WriterInterface
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->cacheTypeList = $cacheTypeList;
        $this->writer = $writer;
    }

    /**
     * Save Default Config Values
     *
     * @param array $configValues pass the array
     *
     * @return mixed
     */
    public function saveDefaultConfigValue($configValues)
    {
        foreach ($configValues as $key => $value) {
            $this->writer->save(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                0
            );
        }
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $configValues
            = [
                'cataloginventory/item_options/manage_stock' => 0,
                'currency/options/default' => 'USD',
                'general/country/allow' => 'US'
            ];
        $this->moduleDataSetup->startSetup();
        $this->saveDefaultConfigValue($configValues);
        // Clean Config type Cache
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
