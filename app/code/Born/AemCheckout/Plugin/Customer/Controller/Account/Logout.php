<?php
/**
 * Born_AemCheckout
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\AemCheckout\Plugin\Customer\Controller\Account;

use Magento\Customer\Controller\Account\Logout as MagentoLogout;
use Born\AemCheckout\Model\ReadCookieData;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Logout
{
    /**
     * @var ReadCookieData
     */
    protected $readCookieData;


    /**
     * AttributeSets constructor.
     * @param ReadCookieData $readCookieData ReadCookieData
     */
    public function __construct(
        ReadCookieData $readCookieData
    ) {
        $this->readCookieData = $readCookieData;
    }//end __construct()


    /**
     * @param MagentoLogout $subject MagentoLogout
     * @param mixed         $result  mixed
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterExecute(
        MagentoLogout $subject,
        $result
    ) {
        $this->readCookieData->deleteCifCookie();

        return $result;
    }//end afterExecute()


}//end class
