<?php
/**
 * Born_AemCheckout
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\AemCheckout\Plugin\Customer\Controller\Account;

use Magento\Customer\Controller\Account\LoginPost as MagentoLoginPost;
use Born\AemCheckout\Model\ReadCookieData;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Model\Quote;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class LoginPost
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;
    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteMaskFactory;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var ReadCookieData
     */
    protected $readCookieData;
    /**
     * @var Quote
     */
    protected $quote;


    /**
     * LoginPost constructor.
     * @param ReadCookieData     $readCookieData   ReadCookieData
     * @param CustomerSession    $customerSession  CustomerSession
     * @param CheckoutSession    $checkoutSession  CheckoutSession
     * @param QuoteIdMaskFactory $quoteMaskFactory QuoteIdMaskFactory
     * @param Quote              $quote            Quote
     */
    public function __construct(
        ReadCookieData $readCookieData,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        QuoteIdMaskFactory $quoteMaskFactory,
        Quote $quote
    ) {
        $this->readCookieData   = $readCookieData;
        $this->customerSession  = $customerSession;
        $this->checkoutSession  = $checkoutSession;
        $this->quoteMaskFactory = $quoteMaskFactory;
        $this->quote            = $quote;
    }//end __construct()


    /**
     * @param MagentoLogout $subject MagentoLogout
     * @param mixed         $result  mixed
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterExecute(
        MagentoLoginPost $subject,
        $result
    ) {
        $customerId = $this->customerSession->getId();
        $quoteAll   = $this->quote->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('is_active', 1)->getData();
        $quoteId    = $quoteAll[0]['entity_id'] ?? '';

       /** $quoteId = $this->checkoutSession->getQuoteId(); */
        if (!empty($quoteId)) {
            $quoteIdMask     = $this->quoteMaskFactory->create()->load($quoteId, 'quote_id');
            $quoteMaskedData = $quoteIdMask->getMaskedId();
            $quoteMaskedData = $quoteId.'#'.$quoteMaskedData;
            $this->readCookieData->updateCifCookie($quoteMaskedData);
        }

        return $result;
    }//end afterExecute()


}//end class
