<?php
/**
 * Born_AemCheckout
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AemCheckout\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Customer\Model\VisitorFactory;

/**
 * Class CreateGuestQuote
 * @package Born\AemCheckout\Model
 */
class CreateGuestQuote
{
    /**
     * @var VisitorFactory
     */
    protected $visitor;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartQuoteRepository;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Customer
     */
    protected $customerModel;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteMaskFactory;


    /**
     * CreateGuestQuote constructor.
     * @param CustomerSession         $customerSession     CustomerSession
     * @param CheckoutSession         $checkoutSession     CheckoutSession
     * @param CartRepositoryInterface $cartQuoteRepository CartRepositoryInterface
     * @param Customer                $customerModel       Customer
     * @param QuoteIdMaskFactory      $quoteMaskFactory    QuoteIdMaskFactory
     * @param VisitorFactory          $visitor             VisitorFactory
     */
    public function __construct(
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        CartRepositoryInterface $cartQuoteRepository,
        Customer $customerModel,
        QuoteIdMaskFactory $quoteMaskFactory,
        VisitorFactory $visitor
    ) {
        $this->customerSession     = $customerSession;
        $this->checkoutSession     = $checkoutSession;
        $this->cartQuoteRepository = $cartQuoteRepository;
        $this->customerModel       = $customerModel;
        $this->quoteMaskFactory    = $quoteMaskFactory;
        $this->visitor             = $visitor;
    }//end __construct()


    /**
     * @param int $quoteMaskedId int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setGuestQuoteByCookie($quoteMaskedId)
    {
        /** @var $quoteIdMask QuoteIdMask */
        $quoteIdMask = $this->quoteMaskFactory->create()->load($quoteMaskedId, 'masked_id');
        $quoteId     = $quoteIdMask->getQuoteId();
        if (!empty($quoteId)) {
            $flag = $this->createCustomerSession($quoteId);
            if ($flag && $quoteId != $this->checkoutSession->getQuoteId()) {
                $this->visitor->create();
                $_SESSION['default']['visitor_data']['quote_id'] = $quoteIdMask->getQuoteId(); // @codingStandardsIgnoreLine
                $quote                                           = $this->cartQuoteRepository->get($quoteId);
                $quote->setIsActive(1);
                $this->cartQuoteRepository->save($quote);
                $this->checkoutSession->replaceQuote($quote);
            }
        }
    }//end setGuestQuoteByCookie()


    /**
     * @param int $quoteId cart quote id
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createCustomerSession($quoteId)
    {
        $quoteRepo  = $this->cartQuoteRepository->get($quoteId);
        $customerId = $quoteRepo->getCustomer()->getId();
        if ($customerId) {
            $customer = $this->customerModel->load($customerId);
            $this->customerSession->setCustomerAsLoggedIn($customer);

            return false;
        }

        return true;
    }//end createCustomerSession()


}//end class
