<?php
/**
 * Born_AemCheckout
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AemCheckout\Model;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\Config\ConfigInterface;

/**
 * Class ReadCookieData
 * @package Born\AemCheckout\Model
 */
class ReadCookieData
{
    /**
     * Set Cookie Name
     *
     * COOKIE_NAME
     */
    public const COOKIE_NAME       = 'cif_cart';
    public const COOKIE_WRITE_NAME = 'cif.cart';

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var ConfigInterface
     */
    private $sessionConfig;

    /**
     * Cookie manager
     *
     * @var CookieManagerInterface
     */
    protected $cookieManager;


    /**
     * ReadCookieData constructor.
     * @param CookieManagerInterface $cookieManager         CookieManagerInterface
     * @param CookieMetadataFactory  $cookieMetadataFactory CookieMetadataFactory
     * @param ConfigInterface        $sessionConfig         ConfigInterface
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ConfigInterface $sessionConfig
    ) {
        $this->cookieManager         = $cookieManager;
        $this->sessionConfig         = $sessionConfig;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
    }//end __construct()


    /**
     * Get cookie
     *
     * @return string|null
     */
    public function getCookie()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }//end getCookie()


    /**
     * Update Quote Mask ID
     * @param string $quoteMaskedData Quote Mask ID
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function updateCifCookie($quoteMaskedData)
    {
        setrawcookie(           // @codingStandardsIgnoreLine
            self::COOKIE_WRITE_NAME,
            $quoteMaskedData,
            time() + (86400 * 30),
            '/'
        );
    }//end updateCifCookie()


    /**
     * Delete Quote Mask ID
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function deleteCifCookie()
    {
        setrawcookie(           // @codingStandardsIgnoreLine
            self::COOKIE_WRITE_NAME,
            '',
            time() + (86400 * 30),
            '/'
        );
    }//end deleteCifCookie()


}//end class
