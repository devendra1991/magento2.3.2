<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_AemCheckout
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\AemCheckout
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\AemCheckout\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Born\AemCheckout\Model\ReadCookieData;
use Born\AemCheckout\Model\CreateGuestQuote;

/**
 * Remember Me Account Management
 * Class CustomerRemember
 *
 * @category  PHP
 * @package   Born\AemCheckout\Observer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CreateCheckoutSession implements ObserverInterface
{
    /**
     * @var ReadCookieData
     */
    protected $readCookieData;

    /**
     * @var CreateGuestQuote
     */
    protected $createGuestQuote;


    /**
     * CreateCheckoutSession constructor.
     * @param ReadCookieData   $readCookieData   ReadCookieData
     * @param CreateGuestQuote $createGuestQuote CreateGuestQuote
     */
    public function __construct(
        ReadCookieData $readCookieData,
        CreateGuestQuote $createGuestQuote
    ) {
        $this->readCookieData   = $readCookieData;
        $this->createGuestQuote = $createGuestQuote;
    }//end __construct()


    /**
     * @param Observer $observer Observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        /** Create guest session */
        $cartData = trim($this->readCookieData->getCookie());
        if (!empty($cartData)) {
            $cartData      = explode('#', $cartData);
            $quoteMaskedId = $cartData[1] ?? '';
            $this->createGuestQuote->setGuestQuoteByCookie($quoteMaskedId);
        }
    }//end execute()


}//end class
