<?php

/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_RememberMe
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\RememberMe
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\RememberMe\Plugin\Customer\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Authentication as CustomerAuthentication;
use Born\RememberMe\Model\RememberMe;
use Magento\Persistent\Block\Form\Remember;

/**
 * Customer Authentication
 * Class Authentication
 *
 * @category  PHP
 * @package   Born\RememberMe\Plugin\Customer\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Authentication
{
    /**
     * Request
     *
     * @var RequestInterface
     */
    protected $request;

    /**
     * RememberMe Model
     *
     * @var RememberMe
     */
    protected $remember;

    /**
     * Checked RememberMe or not
     *
     * @var Remember
     */
    protected $checked;

    /**
     * Authentication constructor.
     *
     * @param RequestInterface $request  RequestInterface
     * @param RememberMe       $remember RememberMe
     * @param Remember         $checked  Remember
     */
    public function __construct(
        RequestInterface $request,
        RememberMe $remember,
        Remember $checked
    ) {
        $this->request = $request;
        $this->remember = $remember;
        $this->checked = $checked;
    }

    /**
     * Plugin for Authentication
     *
     * @param CustomerAuthentication $subject    Input
     * @param \Closure               $proceed    Call Main Method
     * @param int                    $customerId Customer Id
     * @param string                 $password   Customer Address
     *
     * @return mixed
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function aroundAuthenticate(
        CustomerAuthentication $subject,
        \Closure $proceed,
        $customerId,
        $password
    ) {
        $result = $proceed($customerId, $password);
        $value = $this->request->getPost('persistent_remember_me');
        $this->remember->initRememberToken(
            $customerId,
            $value,
            $this->isSecure()
        );

        return $result;
    }

    /**
     * Is Request Secure
     *
     * @return bool
     */
    private function isSecure()
    {
        return $this->request->isSecure();
    }
}
