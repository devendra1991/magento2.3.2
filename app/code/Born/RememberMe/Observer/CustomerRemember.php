<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_RememberMe
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\RememberMe
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\RememberMe\Observer;

use Born\RememberMe\Model\RememberMe;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Remember Me Account Management
 * Class CustomerRemember
 *
 * @category  PHP
 * @package   Born\RememberMe\Observer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CustomerRemember implements ObserverInterface
{
    /**
     * RememberMe
     *
     * @var RememberMe
     */
    protected $rememberMe;

    /**
     * CustomerSession
     *
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * CustomerFactory
     *
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * CustomerRepositoryInterface
     *
     * @var customerRepository
     */
    protected $customerRepository;

    /**
     * CustomerRemember constructor.
     *
     * @param RememberMe                  $rememberMe         RememberMe
     * @param CustomerSession             $customerSession    CustomerSession
     * @param CustomerRepositoryInterface $customerRepository CustomerRepositoryInterface
     * @param CustomerFactory             $customerFactory    CustomerFactory
     */
    public function __construct(
        RememberMe $rememberMe,
        CustomerSession $customerSession,
        CustomerRepositoryInterface $customerRepository,
        CustomerFactory $customerFactory
    ) {
        $this->rememberMe = $rememberMe;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Check the Remember Me cookie, If Not Exist Customer will logout.
     *
     * @param Observer $observer Observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $cookieData = $this->rememberMe->getRememberMeToken(RememberMe::COOKIE_NAME);
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomer()->getId();
            $customer = $this->customerFactory->create()->load($customerId);
            $rememberToken = $customer->getData('remember_token');
            if ($rememberToken && empty($cookieData)) {
                $this->customerSession->logout();
            }
        }
    }
}