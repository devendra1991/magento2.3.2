<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_RememberMe
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\RememberMe
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\RememberMe\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Persistent\Helper\Data;

/**
 * Remember Me Account Management
 * Class RememberMe
 *
 * @category  PHP
 * @package   Born\RememberMe\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class RememberMe
{
    /**
     * Set Cookie Name
     *
     * COOKIE_NAME
     */
    const COOKIE_NAME = 'remember_me';

    /**
     * Random Math Instance
     *
     * @var Random
     */
    protected $mathRandom;

    /**
     * Customer Repository
     *
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Cookie manager
     *
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * Cookie metadata factory
     *
     * @var CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * Resource Connection
     *
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Customer Session
     *
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * DB connection.
     *
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * Persistent Helper
     *
     * @var Data
     */
    protected $persistentData;

    /**
     * RememberMe constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository    CustomerRepositoryInterface
     * @param Random                      $mathRandom            Random
     * @param ResourceConnection          $resourceConnection    ResourceConnection
     * @param CookieMetadataFactory       $cookieMetadataFactory CookieMetadataFactory
     * @param CookieManagerInterface      $cookieManager         CookieManagerInterface
     * @param CustomerSession             $customerSession       CustomerSession
     * @param Data                        $persistentData        Data
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        Random $mathRandom,
        ResourceConnection $resourceConnection,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager,
        CustomerSession $customerSession,
        Data $persistentData
    ) {
        $this->mathRandom = $mathRandom;
        $this->connection = $resourceConnection->getConnection();
        $this->resourceConnection = $resourceConnection;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->persistentData = $persistentData;
    }

    /**
     * Intialize Remember token and updated in DB
     *
     * @param integer $customerId customer Id
     * @param string  $value      Remember-be check value
     * @param bool    $secure     Is request secure
     *
     * @return void
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function initRememberToken($customerId, $value, $secure)
    {
        $token = $this->mathRandom->getUniqueHash();
        $duration = $this->persistentData->getLifeTime();
        $cookiePath = $this->customerSession->getCookiePath();
        if ($value) {
            $this->connection->update(
                $this->getCustomerEntityTable(),
                ['remember_token' => $token],
                ['entity_id = ?' => $customerId]
            );
            $this->setCookie($token, $duration, $cookiePath, $secure);
        } else {
            $this->connection->update(
                $this->getCustomerEntityTable(),
                ['remember_token' => ''],
                ['entity_id = ?' => $customerId]
            );
        }
    }

    /**
     * Regenerate Customer Seesion
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function regenerateCustomerSession()
    {
        $token = $this->getRememberMeToken();
        if (!$token) {
            return false;
        }
        $customerId = $this->connection->fetchOne(
            $this->connection->select()->from(
                $this->getCustomerEntityTable(),
                'entity_id'
            )->where('remember_token = ?', $token)->limit(1)
        );
        if (!$customerId) {
            return false;
        }
        $customer = $this->customerRepository->getById($customerId);
        $this->customerSession->setCustomerDataAsLoggedIn($customer);

        return true;
    }

    /**
     * Get Customer Entity Table
     *
     * @return string
     */
    protected function getCustomerEntityTable()
    {
        return $this->resourceConnection->getTableName('customer_entity');
    }

    /**
     * Get Remember Me Token
     *
     * @return string|null
     */
    public function getRememberMeToken()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    /**
     * Forget Customer
     *
     * @param bool $secure Is Request Secure
     *
     * @return void
     * @throws InputException
     * @throws FailureToSendException
     */
    public function forgetCustomer($secure)
    {
        $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setPath($this->customerSession->getCookiePath())
            ->setSecure($secure)
            ->setHttpOnly(true);
        $this->cookieManager->deleteCookie(self::COOKIE_NAME, $metadata);
    }

    /**
     * Set Cookie
     *
     * @param string $value    Token value or cookie value
     * @param int    $duration Cookie duration same as persistent lifetime
     * @param string $path     Cookie Path
     * @param bool   $secure   is Request Secure
     *
     * @return void
     * @throws InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function setCookie($value, $duration, $path, $secure)
    {
        $publicCookieMetadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($path)
            ->setSecure($secure)
            ->setHttpOnly(true);
        $this->cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $value,
            $publicCookieMetadata
        );
    }
}
