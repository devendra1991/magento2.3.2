<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Api\Data;

/**
 * Interface CategoryFacetsOptionInterface
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CategoryFacetsOptionInterface
{
    const ID                      = 'id';
    const FACETS_ID               = 'facets_id';
    const OPTION_ID               = 'option_id';
    const OPTION_VALUE            = 'option_value';
    const OPTION_POSITION         = 'option_position';
    const CATEGORY_ID             = 'category_id';
    const IS_ACTIVE               = 'is_active';


    /**
     * Get Id.
     *
     * @return int|null
     */
    public function getId();


    /**
     * Get Facets Id.
     *
     * @return int|null
     */
    public function getFacetsId();


    /**
     * Get option Id.
     *
     * @return int|null
     */
    public function getOptionId();


    /**
     * Get Option Value.
     *
     * @return int|null
     */
    public function getOptionValue();


    /**
     * Get Category Id.
     *
     * @return int|null
     */
    public function getCategoryId();


    /**
     * Get Option Position.
     *
     * @return int|null
     */
    public function getOptionPosition();


    /**
     * Get IsActive.
     *
     * @return int|null
     */
    public function getIsActive();


    /**
     * Set Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setId($id);


    /**
     * Set Facets Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setFacetsId($id);


    /**
     * Set Option Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setOptionId($id);


    /**
     * Set Product Attribute Id.
     *
     * @param string $value string
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setOptionValue($value);


    /**
     * Set Category Id.
     * @param int $value category Id
     * @return int|null
     */
    public function setCategoryId($id);


    /**
     * Set Option Position
     *
     * @param int $order int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setOptionPosition($order);


    /**
     * Set IsActive.
     *
     * @param int $status int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface
     */
    public function setIsActive($status);


}//end interface
