<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Api\Data;

/**
 * Interface CategoryFacetsInterface
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CategoryFacetsInterface
{
    const ID                     = 'id';
    const STORE_ID               = 'store_id';
    const CATEGORY_ID            = 'category_id';
    const PRODUCT_ATTRIBUTE_ID   = 'product_attribute_id';
    const FACETS_POSITION        = 'facets_position';
    const IS_ACTIVE              = 'is_active';


    /**
     * Get Id.
     *
     * @return int|null
     */
    public function getId();


    /**
     * Get Store Id.
     *
     * @return int|null
     */
    public function getStoreId();


    /**
     * Get Category Id.
     *
     * @return int|null
     */
    public function getCategoryId();


    /**
     * Get Product Attribute Id.
     *
     * @return int|null
     */
    public function getProductAttributeId();


    /**
     * Get Facets Position.
     *
     * @return int|null
     */
    public function getFacetsPosition();


    /**
     * Get IsActive.
     *
     * @return int|null
     */
    public function getIsActive();


    /**
     * Set Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setId($id);


    /**
     * Set Store Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setStoreId($id);


    /**
     * Set Category Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setCategoryId($id);


    /**
     * Set Product Attribute Id.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setProductAttributeId($id);


    /**
     * Set Facets Position
     *
     * @param int $order int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setFacetsPosition($order);


    /**
     * Set IsActive.
     *
     * @param int $status int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     */
    public function setIsActive($status);


}//end interface
