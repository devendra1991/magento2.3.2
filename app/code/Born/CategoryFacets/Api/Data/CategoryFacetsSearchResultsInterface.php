<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CategoryFacetsSearchResultsInterface
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CategoryFacetsSearchResultsInterface extends SearchResultsInterface
{


    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();


    /**
     * @param array $items array
     * @return SearchResultsInterface
     */
    public function setItems(array $items);


}//end interface
