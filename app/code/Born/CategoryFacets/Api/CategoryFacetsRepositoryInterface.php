<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types = 1);

namespace Born\CategoryFacets\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterface;

/**
 * Interface CategoryFacetsRepositoryInterface
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface CategoryFacetsRepositoryInterface
{


    /**
     * Create or update a promotion lebel.
     *
     * @param \Born\CategoryFacets\Api\Data\CategoryFacetsInterface $data CategoryFacetsInterface
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     */
    public function save(CategoryFacetsInterface $data);


    /**
     * Returns Table details.
     *
     * @param int $id int
     * @return \Born\CategoryFacets\Api\Data\CategoryFacetsInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);


    /**
     * Removes company entity and all the related links from the system.
     *
     * @param \Devendra\Promotion\Api\Data\CategoryFacetsInterface $data CategoryFacetsInterface
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(CategoryFacetsInterface $data);


    /**
     * Delete a company. Customers belonging to a company are not deleted with this request.
     *
     * @param int $id int
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);


    /**
     * Returns the list of companies. The list is an array of objects, and detailed information about item attributes
     * might not be included.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria SearchCriteriaInterface
     * @return \Magento\Company\Api\Data\CategoryFacetsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);


}//end interface
