<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Plugin\Catalog\Controller\Adminhtml\Product\Attribute;

use Magento\Eav\Model\Config;
use Magento\Catalog\Controller\Adminhtml\Product\Attribute\Save as MagentoAttributeSave;
use Magento\Framework\App\Request\Http\Proxy;
use Magento\Framework\Serialize\Serializer\FormData;
use Magento\Framework\App\ObjectManager;
use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterfaceFactory as ModelFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tabs;
use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterfaceFactory as OptionModelFactory;
use Born\CategoryFacets\Api\CategoryFacetsOptionRepositoryInterface as OptionModelRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as AttributeModel;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Save
{
    public const CATEGORY_ID          = 'category_id';
    public const PRODUCT_ATTRIBUTE_ID = 'product_attribute_id';
    public const CATALOG_PRODUCT      = 'catalog_product';
    public const OPTION_ID            = 'option_id';
    public const FACETS_ID            = 'facets_id';

    /**
     * @var AttributeModel
     */
    protected $attributeModel;
    /**
     * @var OptionModelFactory
     */
    private $optionModelFactory;
    /**
     * @var OptionModelRepositoryInterface
     */
    private $optionModelRepositoryInterface;
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;
    /**
     * @var Tabs
     */
    protected $tabs;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var FormData|null
     */
    private $formDataSerializer;
    /**
     * @var Proxy
     */
    private $request;

    /**
     * @var Config
     */
    protected $eavConfig;


    /**
     * Save constructor.
     * @param Proxy                          $request                        Proxy
     * @param Config                         $eavConfig                      Config
     * @param ModelFactory                   $modelFactory                   ModelFactory
     * @param ModelRepositoryInterface       $modelRepositoryInterface       ModelRepositoryInterface
     * @param SearchCriteriaBuilder          $searchCriteriaBuilder          SearchCriteriaBuilder
     * @param CollectionFactory              $categoryCollectionFactory      CollectionFactory
     * @param OptionModelRepositoryInterface $optionModelRepositoryInterface OptionModelRepositoryInterface
     * @param OptionModelFactory             $optionModelFactory             OptionModelFactory
     * @param Tabs                           $tabs                           Tabs
     * @param AttributeModel                 $attributeModel                 AttributeModel
     * @param FormData|null                  $formDataSerializer             FormData
     */
    public function __construct(
        Proxy $request,
        Config $eavConfig,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionFactory $categoryCollectionFactory,
        OptionModelRepositoryInterface $optionModelRepositoryInterface,
        OptionModelFactory $optionModelFactory,
        Tabs $tabs,
        AttributeModel $attributeModel,
        FormData $formDataSerializer = null
    ) {
        $this->request                        = $request;
        $this->eavConfig                      = $eavConfig;
        $this->modelFactory                   = $modelFactory;
        $this->modelRepositoryInterface       = $modelRepositoryInterface;
        $this->searchCriteriaBuilder          = $searchCriteriaBuilder;
        $this->categoryCollectionFactory      = $categoryCollectionFactory;
        $this->optionModelFactory             = $optionModelFactory;
        $this->optionModelRepositoryInterface = $optionModelRepositoryInterface;
        $this->tabs                           = $tabs;
        $this->attributeModel                 = $attributeModel;
        $this->formDataSerializer             = $formDataSerializer
            ?: ObjectManager::getInstance()->get(FormData::class); // @codingStandardsIgnoreLine
    }//end __construct()


    /**
     * @param MagentoAttributeSave $subject MagentoAttributeSave
     * @param mixed                $result  mixed
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterExecute(
        MagentoAttributeSave $subject,
        $result
    ) {
        $data                 = $this->request->getParams();
        $isFilterable         = $data['is_filterable'] ?? 0;
        $isFilterableInSearch = $data['is_filterable_in_search'] ?? 0;

        if ($isFilterable || $isFilterableInSearch) {
            $optionData   = $this->formDataSerializer
                ->unserialize($this->request->getParam('serialized_options', '[]'));
            $inputType    = $data['swatch_input_type'] ?? '';
            $deleteOption = [];
            if ($inputType === 'dropdown' || $inputType === '') {
                $deleteOption = $optionData['option']['delete'] ?? [];
            }
            if ($inputType === 'visual') {
                $deleteOption = $optionData['optionvisual']['delete'] ?? [];
            }
            if ($inputType === 'text') {
                $deleteOption = $optionData['optiontext']['delete'] ?? [];
            }
            $attributeId = $data['attribute_id'] ?? '';
            if (empty($attributeId)) {
                $attributeCode  = $this->getAttributeCode($data);
                $attributeModel = $this->eavConfig->getAttribute('catalog_product', $attributeCode);
                $attributeId    = $attributeModel->getAttributeId();
            }
            $this->updateFacetAttribute($attributeId, $deleteOption);
        }

        return $result;
    }//end afterExecute()


    /**
     * @param array $data Attribute paramData
     * @return mixed|string
     */
    private function getAttributeCode($data):string
    {
        $attributeLabel = $data['frontend_label'][0] ?? '';
        $attributeCode  = $data['attribute_code'] ?? '';
        if (empty($attributeCode)) {
            $attributeLabel = str_replace(' ', '_', $attributeLabel);
            $attributeCode  = strtolower($attributeLabel);
        }

        return $attributeCode;
    }//end getAttributeCode()


    /**
     * Add facets Attribute for each category
     * @param int   $attributeId  Product Attribute Id
     * @param array $deleteOption Product Attribute Options
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function updateFacetAttribute($attributeId, $deleteOption):void
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::PRODUCT_ATTRIBUTE_ID, $attributeId, 'eq')
            ->create();

        $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
        $result        = $searchResults->getItems();
        if (empty($result)) {
            $this->makeAttributeEntry($attributeId);
        } else {
            $this->updateAttributeEntry($attributeId, $deleteOption);
        }
    }//end updateFacetAttribute()


    /**
     * @param int $attributeId Attribute Id
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function makeAttributeEntry($attributeId):void
    {
        $categoryCollection = $this->categoryCollectionFactory->create();
        foreach ($categoryCollection as $category) {
            $categoryId = (int) $category->getEntityId();
            if (!empty($categoryId) && $categoryId !== 1) {
                $data            = [
                    'store_id' => 0,
                    'category_id' => $categoryId,
                    'product_attribute_id' => $attributeId,
                    'facets_position' => $this->getFacetsPosition($categoryId),
                    'is_active' => 1,
                ];
                $modelFactoryObj = $this->modelFactory->create();
                $objData         = $modelFactoryObj->addData($data);
                $savedData = $this->modelRepositoryInterface->save($objData); // @codingStandardsIgnoreLine
                $this->tabs->enableAttributeOptions(
                    $savedData->getId(),
                    $savedData->getProductAttributeId(),
                    $categoryId
                );
            }
        }
    }//end makeAttributeEntry()


    /**
     * @param int $categoryId Category Id
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getFacetsPosition($categoryId):int
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::CATEGORY_ID, $categoryId, 'eq')
            ->create();

        $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
        $result        = $searchResults->getItems();

        return count($result);
    }//end getFacetsPosition()


    /**
     * @param int $attributeId  Product attributeId
     * @param int $deleteOption Options
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function updateAttributeEntry($attributeId, $deleteOption):void
    {
        /** remove deleted attribute options */
        foreach ($deleteOption as $key => $value) {
            if ($value) {
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter(self::OPTION_ID, $key, 'eq')
                    ->create();
                $searchResults  = $this->optionModelRepositoryInterface->getList($searchCriteria);
                $result         = $searchResults->getItems();
                foreach ($result as $data) {
                    $this->deleteOption($data['id']);
                }
            }
        }

        /** update/add attribute options */
        $attributeModel = $this->attributeModel;
        $attributeModel->load($attributeId);
        $attributeCode = $attributeModel->getAttributeCode();
        $eavModel      = $this->eavConfig;
        $attribute     = $eavModel->getAttribute(self::CATALOG_PRODUCT, $attributeCode);
        $options       = $attribute->getSource()->getAllOptions();

        $searchCriteria1 = $this->searchCriteriaBuilder
            ->addFilter(self::PRODUCT_ATTRIBUTE_ID, $attributeId, 'eq')
            ->create();
        $searchResults    = $this->modelRepositoryInterface->getList($searchCriteria1);
        $results          = $searchResults->getItems();
        foreach ($results as $key => $value) {
            foreach ($options as $option) {
                $optionValue = $option['value'] ?? '';
                $optionLabel = $option['label'] ?? '';
                if (!empty($optionValue)) {
                    $this->addUpdateOptions($value['id'], $optionValue, $optionLabel);
                }
            }
        }
    }//end updateAttributeEntry()


    /**
     * @param int    $facetId     Facets Id
     * @param int    $optionId    Option Id
     * @param string $optionLabel option Label
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function addUpdateOptions($facetId, $optionId, $optionLabel):void
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::FACETS_ID, $facetId, 'eq')
            ->addFilter(self::OPTION_ID, $optionId, 'eq')
            ->create();
        $searchResults  = $this->optionModelRepositoryInterface->getList($searchCriteria);
        $result         = $searchResults->getItems();

        if (!empty($result)) {
            /** Update data */
            $data                 = [
                'id' => $result[0]['id'],
                'option_value' => $optionLabel,
            ];
            $modelFactoryResource = $this->optionModelFactory->create();
            $objData              = $modelFactoryResource->addData($data);
            $this->optionModelRepositoryInterface->save($objData);
        } else {
            /** Add data for each category */
            $categoryCollection = $this->categoryCollectionFactory->create();
            foreach ($categoryCollection as $category) {
                $categoryId = (int) $category->getEntityId();
                if (!empty($categoryId) && $categoryId !== 1) {
                    $data            = [
                        'facets_id' => $facetId,
                        'option_id' => $optionId,
                        'option_value' => $optionLabel,
                        'option_position' => $this->getOptionPosition($facetId, $categoryId),
                        'category_id' => $categoryId,
                        'is_active' => 1,
                    ];
                    $modelFactoryObj = $this->optionModelFactory->create();
                    $objData         = $modelFactoryObj->addData($data);
                    $this->optionModelRepositoryInterface->save($objData); // @codingStandardsIgnoreLine
                }
            }
        }
    }//end addUpdateOptions()


    /**
     * Remove Attribute Option
     * @param int $optionId Attribute Option Id
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function deleteOption($optionId):void
    {
        if (!empty($optionId)) {
            $this->optionModelRepositoryInterface->deleteById($optionId);
        }
    }//end deleteOption()


    /**
     * @param int $facetId    Facets Id
     * @param int $categoryId Category Id
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getOptionPosition($facetId, $categoryId):int
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::FACETS_ID, $facetId, 'eq')
            ->addFilter(self::CATEGORY_ID, $categoryId, 'eq')
            ->create();
        $searchResults  = $this->optionModelRepositoryInterface->getList($searchCriteria);
        $result         = $searchResults->getItems();

        return count($result);
    }//end getOptionPosition()


}//end class
