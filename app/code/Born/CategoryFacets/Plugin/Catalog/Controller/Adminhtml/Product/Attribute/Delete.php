<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Plugin\Catalog\Controller\Adminhtml\Product\Attribute;

use Magento\Catalog\Controller\Adminhtml\Product\Attribute\Delete as MagentoAttributeDelete;
use Magento\Framework\App\Request\Http\Proxy;
use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterfaceFactory as ModelFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Delete
{
    public const CATEGORY_ID          = 'category_id';
    public const PRODUCT_ATTRIBUTE_ID = 'product_attribute_id';
    public const OPTION_ID            = 'option_id';

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var Proxy
     */
    private $request;


    /**
     * Save constructor.
     * @param Proxy                    $request                  Proxy
     * @param ModelFactory             $modelFactory             ModelFactory
     * @param ModelRepositoryInterface $modelRepositoryInterface ModelRepositoryInterface
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder    SearchCriteriaBuilder
     */
    public function __construct(
        Proxy $request,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->request                  = $request;
        $this->modelFactory             = $modelFactory;
        $this->modelRepositoryInterface = $modelRepositoryInterface;
        $this->searchCriteriaBuilder    = $searchCriteriaBuilder;
    }//end __construct()


    /**
     * @param MagentoAttributeDelete $subject MagentoAttributeDelete
     * @param mixed                  $result  mixed
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterExecute(
        MagentoAttributeDelete $subject,
        $result
    ) {
        $data        = $this->request->getParams();
        $attributeId = $data['attribute_id'];
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::PRODUCT_ATTRIBUTE_ID, $attributeId, 'eq')
            ->create();
        $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
        $resultData    = $searchResults->getItems();
        foreach ($resultData as $data) {
            $this->modelRepositoryInterface->deleteById($data['id']);
        }

        return $result;
    }//end afterExecute()


}//end class
