/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

define([
    "jquery",
    'Magento_Ui/js/modal/modal'
], function ($, modal) {
    "use strict";

    return {
        saveDataAjaxUrl: '',
        categoryId: '',
        gridAjaxUrl: '',
        atrributeIdAjaxUrl: '',
        optionGridUrl: '',
        saveOptionGridUrl: '',

        /**
         * Update Facets Attributes Id
         */
        getFacetsAttributesId: function () {
            $.ajax({
                url: this.atrributeIdAjaxUrl,
                data: {
                    form_key: window.FORM_KEY
                },
                type: 'post',
                dataType: 'json',
                showLoader: true,
                success: function (data) {
                    $("#jsonFacetsResponse").val(JSON.stringify(data));
                }
            });
        },
        /**
         * Update grid UI
         */
        applyFacetGrid : function () {
            if ($('select[name=category_attribute_set] > option').length > 0) {
                var category_attribute_set = $('select[name=category_attribute_set] option:selected').val();
                var that = this;
                $.ajax({
                    url: this.gridAjaxUrl,
                    data: {
                        form_key: window.FORM_KEY,
                        category_attribute_set: category_attribute_set
                    },
                    type: 'post',
                    dataType: 'html',
                    showLoader: true,
                    success: function (data) {
                        $("#categoryfacets_tab_grid").html(data);
                        that.getFacetsAttributesId();
                    }
                });
            }
        },
        /**
         * Function for Save data
         * @param element
         * @param positionValue
         */
        saveRowData: function (element, positionValue) {
            let action = 'update_position';
            if (element.hasClass('row_action')) {
                action = 'update_status';
            }
            let categoryId = this.categoryId;
            let attribute_id = $.trim(element.parent().parent().parent().find('.col-attribute_id').text());
            let facets_position = $.trim(positionValue);
            let is_enable = element.parent().parent().parent().find('input[type="checkbox"]').prop("checked");
            if (is_enable === true) {
                is_enable = 1;
            } else {
                is_enable = 0;
            }
            var jsonFacetsResponse = $('#jsonFacetsResponse').val();
            var that = this;
            $.ajax({
                url: this.saveDataAjaxUrl,
                data: {
                    form_key: window.FORM_KEY,
                    attribute_id: attribute_id,
                    facets_position: facets_position,
                    category_id: categoryId,
                    is_enable: is_enable,
                    jsonFacetsResponse: jsonFacetsResponse,
                    action: action
                },
                type: 'post',
                dataType: 'html',
                showLoader: true,
                success: function (data) {
                    $("#categoryfacets_tab_grid").html(data);
                    that.getFacetsAttributesId();
                }
            });
        },
        /**
         * Function for Show Option grid
         * @param element
         * @param attribute_id
         */
        showOptionGrid: function (element, attribute_id) {
            $.ajax({
                url: this.optionGridUrl,
                type: 'POST',
                showLoader: 'true',
                data: {
                    form_key: window.FORM_KEY,
                    attribute_id: attribute_id
                },
                success: function (data) {
                    var optionPopup = $('.attribute-option-popup').html(data).modal({
                        responsive: true,
                        innerScroll: false,
                        title: false,
                        buttons: [{
                            text: 'Close',
                            class: 'button',
                            click: function () {
                                this.closeModal();
                            }
                        }]
                    });
                    $('.attribute-option-popup').trigger('contentUpdated');
                    optionPopup.modal("openModal");
                },
                error: function (error) {
                    console.log(error);
                }
            });

            return false;
        },
        /**
         * Function for Save Option grid Data
         * @param element
         * @param facets_value_id
         */
        saveOptionGrid: function (element, facets_value_id) {
            let action = 'update_position';
            let option_position = '';
            if (element.hasClass('opt_row_action')) {
                action = 'update_status';
            } else {
                option_position = element.parent().parent().parent().find('input[type="text"]').val();
            }
            let is_enable = element.parent().parent().parent().find('input[type="checkbox"]').prop("checked");
            if (is_enable === true) {
                is_enable = 1;
            } else {
                is_enable = 0;
            }

            $.ajax({
                url: this.saveOptionGridUrl,
                type: 'POST',
                showLoader: 'true',
                data: {
                    form_key: window.FORM_KEY,
                    facets_value_id: facets_value_id,
                    option_position: option_position,
                    is_enable: is_enable,
                    action: action
                },
                success: function (data) {
                    var optionPopup = $('.attribute-option-popup').html(data).modal({
                        responsive: true,
                        innerScroll: false,
                        title: false,
                        buttons: [{
                            text: 'Close',
                            class: 'button',
                            click: function () {
                                this.closeModal();
                            }
                        }]
                    });
                    $('.attribute-option-popup').trigger('contentUpdated');
                    optionPopup.modal("openModal");
                },
                error: function (error) {
                    console.log(error);
                }
            });

            return false;
        }
    }
});