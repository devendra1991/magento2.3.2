<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\Number;
use Magento\Framework\DataObject;

/**
 * Class FacetOptionPosition
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class FacetOptionPosition extends Number
{


    /**
     * Renders grid column
     *
     * @param DataObject $row DataObject
     * @return string
     */
    public function render(DataObject $row):string
    {
        $input  = $this->_getInputValueElement($row);
        $val     = $row->getData('id');
        return <<<HTML
<div class="position">
    <a href="javascript:void(0)" data-value-id="$val" class="move-top icon-backward backward optPositionBtn"></a>
    {$input}
    <a href="javascript:void(0)" data-value-id="$val" class="move-bottom icon-forward forward optPositionBtn"></a>
</div>
HTML;

    }//end render()


}//end class
