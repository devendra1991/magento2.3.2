<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\Number;
use Magento\Framework\DataObject;

/**
 * Class OptionCheckBox
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class OptionCheckBox extends Number
{


    /**
     * @param \Magento\Framework\DataObject $row DataObject
     * @return string
     */
    public function render(DataObject $row):string
    {
        $checked = '';
        $val     = $row->getData($this->getColumn()->getIndex());
        $id     = $row->getData('id');
        if ($val == 1) {
            $checked = 'checked="checked" ';
        }
        $input = '<label class="data-grid-checkbox-cell-inner">
                  <input type="checkbox"
                         data-value-id="' . $id . '"
                         class="opt_row_action admin__control-checkbox' . $this->getColumn()->getValidateClass() . '" 
                         name="' . $this->getColumn()->getId() . '"
                         value="' . $val . '" ' . $checked . '                         
                         /><label> </label></label>';

        return <<<HTML
{$input}
HTML;
    }//end render()


}//end class
