<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab;

use Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer\FacetPosition;
use Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer\Options;
use Born\CategoryFacets\Block\Adminhtml\Widget\Grid\Column\Renderer\Checkbox;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tabs;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;

/**
 * Class Attributes
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Attributes extends Extended
{
    const HEADER    = 'header';
    const SORTABLE  = 'sortable';
    const INDEX     = 'index';
    const CSS_CLASS = 'col-id';

    /**
     * @var Data
     */
    private $backendHelper;

    /**
     * @var array
     */
    private $data;


    /**
     * Grid constructor.
     * @param Context  $context       Context
     * @param Data     $backendHelper Data
     * @param Tabs     $tabs          Tabs
     * @param array    $data          array
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Tabs $tabs,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->backendHelper = $backendHelper;
        $this->data          = $data;
        $this->tabs          = $tabs;
    }//end __construct()


    /**
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('categoryfacets_tab_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        $this->setSortable(true);
    }//end _construct()


    /**
     * Disable click
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row DataObject
     * @return bool|string
     */
    public function getRowUrl($row):bool
    {
        return false;
    }//end getRowUrl()


    /**
     * @return string
     */
    public function getGridUrl():string
    {
        return $this->getUrl('facets/*/grid', ['_current' => true]);
    }//end getGridUrl()


    /**
     * @return Extended
     */
    protected function _prepareCollection():Extended
    {
        $this->tabs->enableFilterAttribute();
        $attributeCollection = $this->tabs->getFacetsCollections();
        $this->setCollection($attributeCollection);

        return parent::_prepareCollection();
    }//end _prepareCollection()


    /**
     * @return Extended
     * @throws \Exception
     */
    protected function _prepareColumns():Extended
    {
        $this->addColumn(
            'attribute_id',
            [
                self::HEADER => __('Attribute Id'),
                self::SORTABLE => true,
                self::INDEX => 'attribute_id',
                'filter_index' => 'main_table.attribute_id',
                'header_css_class' => self::CSS_CLASS,
                'column_css_class' => self::CSS_CLASS,
            ]
        );

        $this->addColumn(
            'attribute_set_id',
            [
                self::HEADER => __('Attribute Set Id'),
                self::SORTABLE => true,
                self::INDEX => 'attribute_set_id',
                'header_css_class' => self::CSS_CLASS,
                'column_css_class' => self::CSS_CLASS,
            ]
        );

        $this->addColumn(
            'attribute_code',
            [
                self::HEADER => __('Attribute Code'),
                self::SORTABLE => true,
                self::INDEX => 'attribute_code',
            ]
        );
        $this->addColumn(
            'frontend_label',
            [
                self::HEADER => __('Attribute Name'),
                self::INDEX => 'frontend_label',
            ]
        );

        $this->addColumn(
            'facets_position',
            [
                self::HEADER => __('Position'),
                'type' => 'number',
                 self::INDEX => 'facets_position',
                'renderer' => FacetPosition::class,
            ]
        );
        $this->addColumn(
            'is_active',
            [
                self::HEADER => __('Visibility'),
                'type'       => 'number',
                'filter'     => false,
                'sortable'   => false,
                 self::INDEX => 'is_active',
                'renderer'   => Checkbox::class,
            ]
        );
        $this->addColumn(
            'button',
            [
                self::HEADER => __('Options'),
                'type'       => 'number',
                'filter'     => false,
                'sortable'   => false,
                'renderer'   => Options::class,
            ]
        );

        return parent::_prepareColumns();
    }//end _prepareColumns()


}//end class
