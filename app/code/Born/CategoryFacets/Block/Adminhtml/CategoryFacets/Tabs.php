<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Block\Adminhtml\CategoryFacets;

use Born\CategoryFacets\Api\CategoryFacetsOptionRepositoryInterface as OptionModelRepositoryInterface;
use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterfaceFactory as ModelFactory;
use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterfaceFactory as OptionModelFactory;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab\Attributes;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as AttributeModel;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\Helper\Data as jsonData;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Registry;
use Magento\Framework\Session\SessionManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Tabs
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Tabs extends Template
{
    const CURRENT            = '_current';
    const FACETS_CATEGORY_ID = 2;
    const ATTRIBUTE_SET_ID   = 0;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var AttributeModel
     */
    protected $attributeModel;
    /**
     * @var Config
     */
    protected $eavConfig;
    /**
     * @var string
     */
    protected $_template = 'category/tab.phtml';
    /**
     * @var \Magento\Catalog\Block\Adminhtml\Category\Tab\Product
     */
    protected $blockGrid;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var EncoderInterface
     */
    protected $jsonEncoder;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var OptionModelFactory
     */
    private $optionModelFactory;
    /**
     * @var OptionModelRepositoryInterface
     */
    private $optionModelRepositoryInterface;
    /**
     * @var Attribute
     */
    private $attribute;
    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;
    /**
     * @var jsonData
     */
    private $jsonData;
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * Tabs constructor.
     * @param Context                  $context                   Context
     * @param Registry                 $registry                  Registry
     * @param EncoderInterface         $jsonEncoder               EncoderInterface
     * @param Attribute                $attribute                 Attribute
     * @param SessionManagerInterface  $sessionManager            SessionManagerInterface
     * @param jsonData                 $jsonData                  jsonData
     * @param ModelFactory             $modelFactory              ModelFactory
     * @param ModelRepositoryInterface $modelRepositoryInterface  ModelRepositoryInterface
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder     SearchCriteriaBuilder
     * @param CollectionFactory        $categoryCollectionFactory CollectionFactory
     * @param OptionModelRepositoryInterface        $categoryCollectionFactory OptionModelRepositoryInterface
     * @param OptionModelFactory        $categoryCollectionFactory OptionModelFactory
     * @param AttributeModel        $attributeModel AttributeModel
     * @param Config        $eavConfig Config
     * @param LoggerInterface        $logger LoggerInterface
     * @param array                    $data                      array
     */
    public function __construct(// NOSONAR.
        Context $context,
        Registry $registry,
        EncoderInterface $jsonEncoder,
        Attribute $attribute,
        SessionManagerInterface $sessionManager,
        jsonData $jsonData,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionFactory $categoryCollectionFactory,
        OptionModelRepositoryInterface $optionModelRepositoryInterface,
        OptionModelFactory $optionModelFactory,
        AttributeModel $attributeModel,
        Config $eavConfig,
        LoggerInterface $logger,
        array $data = []
    ) {
        $this->registry                       = $registry;
        $this->jsonEncoder                    = $jsonEncoder;
        $this->attribute                      = $attribute;
        $this->sessionManager                 = $sessionManager;
        $this->jsonData                       = $jsonData;
        $this->modelFactory                   = $modelFactory;
        $this->modelRepositoryInterface       = $modelRepositoryInterface;
        $this->searchCriteriaBuilder          = $searchCriteriaBuilder;
        $this->_categoryCollectionFactory     = $categoryCollectionFactory;
        $this->optionModelFactory             = $optionModelFactory;
        $this->optionModelRepositoryInterface = $optionModelRepositoryInterface;
        $this->attributeModel = $attributeModel;
        $this->eavConfig = $eavConfig;
        $this->logger = $logger;
        parent::__construct($context, $data);
    }//end __construct()


    /**
     * Return HTML of grid block
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getGridHtml():string
    {
        return $this->getBlockGrid()->toHtml();
    }//end getGridHtml()

    /**
     * Retrieve instance of grid block
     * @return \Magento\Catalog\Block\Adminhtml\Category\Tab\Product|\Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid():Attributes
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                Attributes::class,
                'categoryfacets.categoryfacets.tab.grid'
            );
        }

        return $this->blockGrid;
    }//end getBlockGrid()

    /**
     * Retrieve current category instance
     *
     * @return object|null
     */
    public function getCategory():object
    {
        return $this->registry->registry('category');
    }//end getCategory()


    /**
     * Enable Filter Attribute by Default
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function enableFilterAttribute():void
    {
        $categoryCollection = $this->_categoryCollectionFactory->create();
        foreach ($categoryCollection as $category) {
            $categoryId = (int) $category->getEntityId();
            if (!empty($categoryId) && $categoryId !== 1) {
                $this->enableAttributesForFilter(
                    $categoryId
                );
            }
        }
    }//end enableFilterAttribute()

    /**
     * set default sort order and position for attributes
     *
     * @param int $categoryId Category Id
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function enableAttributesForFilter(int $categoryId):void
    {
        if (!empty($categoryId)) {
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('category_id', $categoryId, 'eq')
                ->create();

            $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
            $result        = $searchResults->getItems();

            if (empty($result)) {
                $facetsCollections   = $this->getFacetsCollections();
                $attributeCollection = [];
                foreach ($facetsCollections as $data) {
                    $attributeCollection[$data->getAttributeId()] = $data->getAttributeCode();
                }
                ksort($attributeCollection);
                $counter = 0;
                foreach ($attributeCollection as $key => $value) {   // NOSONAR.
                    $data = [
                        'store_id' => 0,
                        'category_id' => $categoryId,
                        'product_attribute_id' => $key,
                        'facets_position' => $counter,
                        'is_active' => 1,
                    ];

                    try {
                        $modelFactoryObj = $this->modelFactory->create();
                        $objData         = $modelFactoryObj->addData($data);
                        $savedData = $this->modelRepositoryInterface->save($objData); // @codingStandardsIgnoreLine
                        $this->enableAttributeOptions(
                            $savedData->getId(),
                            $savedData->getProductAttributeId(),
                            $categoryId
                        );
                    } catch (\Exception $e) {
                        $this->logger->debug('Facets Error: ' . $e->getMessage());
                    }
                    $counter++;
                }
            }
        }
    }//end enableAttributesForFilter()


    /**
     * @param int $facetId     facetId
     * @param int $attributeId attributeId
     * @param int $categoryId  categoryId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function enableAttributeOptions($facetId, $attributeId, $categoryId)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('facets_id', $facetId, 'eq')
            ->create();
        $searchResults = $this->optionModelRepositoryInterface->getList($searchCriteria);
        $result        = $searchResults->getItems();
        if (empty($result)) {
            $attributeModel = $this->attributeModel;
            $attributeModel->load($attributeId);
            $attributeCode = $attributeModel->getAttributeCode();
            $eavModel = $this->eavConfig;
            $attribute = $eavModel->getAttribute('catalog_product', $attributeCode);
            $options = $attribute->getSource()->getAllOptions();
            $counter = 0;
            foreach ($options as $opt) {
                if (!empty($opt['value']) && $opt['value'] != '') {
                    $facetsData['facets_id'] = $facetId;
                    $facetsData['option_id'] = $opt['value'];
                    $facetsData['option_value'] = $opt['label'];
                    $facetsData['category_id'] = $categoryId;
                    $facetsData['option_position'] = $counter;
                    $facetsData['is_active'] = 1;
                    $counter++;
                }
                try {
                    if (!empty($facetsData)) {
                        $modelFactoryObj = $this->optionModelFactory->create();
                        $objData         = $modelFactoryObj->addData($facetsData);
                        $savedData = $this->optionModelRepositoryInterface->save($objData); // @codingStandardsIgnoreLine
                    }
                } catch (\Exception $e) {
                    $this->logger->debug('Facets Error: ' . $e->getMessage());
                }

            }

        }
    }//end enableAttributeOptions()


    /**
     * get category facet collection
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getFacetsOptionCollections()
    {
        $attributeId      = 0;
        $facetsCategoryId = self::FACETS_CATEGORY_ID;
        if ($this->sessionManager->getFacetsAttributeId()) {
            $attributeId = $this->sessionManager->getFacetsAttributeId();
        }
        if ($this->sessionManager->getFacetsCategoryId()) {
            $facetsCategoryId = $this->sessionManager->getFacetsCategoryId();
        }
        $attributeCollection = $this->attribute->getCollection();

        $attributeCollection->addFieldToSelect('attribute_code');
        $attributeCollection->addFieldToSelect('frontend_label');

        $attributeCollection->getSelect()->joinInner(
            [
                'eav_att_opt' => $attributeCollection->getTable('eav_attribute_option'),
            ],
            'main_table.attribute_id = ' . $attributeId . ' AND
                  eav_att_opt.attribute_id = ' . $attributeId,
            [
                'eav_att_opt.option_id AS opt_id',
                'eav_att_opt.sort_order',
            ]
        );
        $attributeCollection->getSelect()->joinLeft(
            ['eav_att_opt_val' => $attributeCollection->getTable('eav_attribute_option_value')],
            'eav_att_opt.option_id = eav_att_opt_val.option_id',
            [ 'eav_att_opt_val.value' ]
        );
        $attributeCollection->getSelect()->joinLeft(
            ['cat_facet' => $attributeCollection->getTable('born_category_facets_attribute')],
            'cat_facet.product_attribute_id = ' . $attributeId . ' AND
                 cat_facet.category_id = ' . $facetsCategoryId,
            ['cat_facet.category_id']
        );

        $attributeCollection->getSelect()->joinLeft(
            ['cat_facet_options' => $attributeCollection->getTable('born_category_facets_options')],
            'cat_facet.id = cat_facet_options.facets_id AND 
                   eav_att_opt.option_id = cat_facet_options.option_id AND
                   cat_facet_options.category_id = ' . $facetsCategoryId,
            [
                'cat_facet_options.id',
                'cat_facet_options.option_id',
                'cat_facet_options.option_value',
                'cat_facet_options.option_position',
                'cat_facet_options.is_active',
            ]
        );
        $attributeCollection->getSelect()->order('cat_facet_options.option_position');

        return $attributeCollection;
    }//end getFacetsOptionCollections()


    /**
     * Get OptionGrid URL
     * @return string
     */
    public function getOptionGridUrl()
    {
        return $this->getUrl('facets/*/OptionGrid', [self::CURRENT => true]);
    }//end getOptionGridUrl()


    /**
     * get category facet collection
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getFacetsCollections():AbstractCollection
    {
        $attributeSetId   = self::ATTRIBUTE_SET_ID;
        $facetsCategoryId = self::FACETS_CATEGORY_ID;
        if ($this->sessionManager->getCategoryAttributeSet()) {
            $attributeSetId = $this->sessionManager->getCategoryAttributeSet();
        }
        if ($this->sessionManager->getFacetsCategoryId()) {
            $facetsCategoryId = $this->sessionManager->getFacetsCategoryId();
        }
        $attributeCollection = $this->attribute->getCollection();

        if ($attributeSetId != 0) {
            $attributeCollection->setAttributeSetFilter($attributeSetId);
        }
        $attributeCollection->addFieldToSelect('attribute_id');
        $attributeCollection->addFieldToSelect('attribute_code');
        $attributeCollection->addFieldToSelect('frontend_label');

        $attributeCollection->getSelect()->joinInner(
            ['eav' => $attributeCollection->getTable('catalog_eav_attribute')],
            'main_table.attribute_id = eav.attribute_id',
            [
                'eav.is_filterable',
                'eav.is_filterable_in_search',
            ]
        );

        $attributeCollection->getSelect()->joinLeft(
            ['cat_facet' => $attributeCollection->getTable('born_category_facets_attribute')],
            'main_table.attribute_id = cat_facet.product_attribute_id ' . ' ' . ' AND
                 cat_facet.category_id = ' . $facetsCategoryId,
            [
                'cat_facet.facets_position',
                'cat_facet.is_active',
            ]
        );
        $attributeCollection->addFieldToFilter('is_filterable', '1');
        $attributeCollection->getSelect()->order('facets_position ASC');

        return $attributeCollection;
    }//end getFacetsCollections()


    /**
     * @param array $facetsArray array
     * @return string\
     */
    public function encodeData($facetsArray = []):string
    {
        return $this->jsonData->jsonEncode($facetsArray);
    }//end encodeData()


    /**
     * @param string $facetsString string
     * @return array/mixed
     */
    public function decodeData($facetsString = ''):array
    {
        return $this->jsonData->jsonDecode($facetsString);
    }//end decodeData()


    /**
     * @return string
     */
    public function getAjaxUrl():string
    {
        return $this->getUrl('facets/*/grid', [self::CURRENT => true]);
    }//end getAjaxUrl()


    /**
     * @return string
     */
    public function getSaveDataAjaxUrl():string
    {
        return $this->getUrl('facets/*/save', [self::CURRENT => true]);
    }//end getSaveDataAjaxUrl()


    /**
     * @return string
     */
    public function getAtrributeIdAjaxUrl():string
    {
        return $this->getUrl('facets/*/facetesposition', [self::CURRENT => true]);
    }//end getAtrributeIdAjaxUrl()


    /**
     * @return string
     */
    public function getSaveOptionGridUrl():string
    {
        return $this->getUrl('facets/*/saveoptions', [self::CURRENT => true]);
    }//end getSaveOptionGridUrl()


}//end class
