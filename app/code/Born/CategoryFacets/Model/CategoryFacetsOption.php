<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface;
use Magento\Framework\Model\AbstractModel;
use Born\CategoryFacets\Model\ResourceModel\CategoryFacetsOption as CategoryFacetsResourceModel;

/**
 * class CategoryFacets
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CategoryFacetsOption extends AbstractModel implements CategoryFacetsOptionInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'categoryfacetsoption';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'categoryfacetsoption';


    /**
     * Initialize resource model.
     *
     * @return void
     */
    public function _construct():void
    {
        $this->_init(CategoryFacetsResourceModel::class);
    }//end _construct()


    /**
     * Get facets id
     * @return CategoryFacetsOptionInterface|int|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }//end getId()


    /**
     * Get facets id
     * @return CategoryFacetsOptionInterface/int/null
     */
    public function getFacetsId()
    {
        return $this->getData(self::FACETS_ID);
    }//end getFacetsId()


    /**
     * @return int|mixed|null
     */
    public function getOptionId()
    {
        return $this->getData(self::OPTION_ID);
    }//end getOptionId()


    /**
     * Get Facets Option Value
     * @return CategoryFacetsOptionInterface|string|mixed|null
     */
    public function getOptionValue()
    {
        return $this->getData(self::OPTION_VALUE);
    }//end getOptionValue()


    /**
     * Get Category Id.
     *
     * @return int|null
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }//end getCategoryId()


    /**
     * Get Option SortOrder
     * @return CategoryFacetsOptionInterface|int|mixed|null
     */
    public function getOptionPosition()
    {
        return $this->getData(self::OPTION_POSITION);
    }//end getOptionPosition()


    /**
     * Get Facets Status
     * @return CategoryFacetsOptionInterface|int|mixed|null
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }//end getIsActive()


    /**
     * Set id
     * @param int|mixed $id facets id
     * @return CategoryFacetsOptionInterface|CategoryFacets|AbstractModel
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }//end setId()


    /**
     * Set facets Id
     * @param int $id StoreId
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setFacetsId($id)
    {
        return $this->setData(self::FACETS_ID, $id);
    }//end setFacetsId()


    /**
     * Set facets Option Id
     * @param int $id Option Id
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setOptionId($id)
    {
        return $this->setData(self::OPTION_ID, $id);
    }//end setOptionId()


    /**
     * Set Attribute Option Value
     * @param string $value Attribute Value
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setOptionValue($value)
    {
        return $this->setData(self::OPTION_VALUE, $value);
    }//end setOptionValue()


    /**
     * Set Category Id
     * @param iint $value Category Id
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setCategoryId($id)
    {
        return $this->setData(self::CATEGORY_ID, $id);
    }//end setCategoryId()

    /**
     * Set Option SortOrder
     * @param int $id SortOrder
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setOptionPosition($id)
    {
        return $this->setData(self::OPTION_POSITION, $id);
    }//end setOptionPosition()


    /**
     * Set facets status
     * @param int $status status
     * @return CategoryFacetsOptionInterface|CategoryFacets
     */
    public function setIsActive($status)
    {
        return $this->setData(self::IS_ACTIVE, $status);
    }//end setIsActive()


}//end class
