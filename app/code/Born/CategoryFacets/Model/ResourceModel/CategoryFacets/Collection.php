<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model\ResourceModel\CategoryFacets;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Born\CategoryFacets\Model\CategoryFacets as CategoryFacetsModel;
use Born\CategoryFacets\Model\ResourceModel\CategoryFacets as CategoryFacetsResourceModel;

/**
 * class CategoryFacets
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model\ResourceModel\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Collection extends AbstractCollection
{

    /**
     * Standard collection initialization.
     *
     * @return void
     */
    protected function _construct():void
    {
        $this->_init(
            CategoryFacetsModel::class,
            CategoryFacetsResourceModel::class
        );
    }//end _construct()


}//end class
