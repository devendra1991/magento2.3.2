<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Interface CategoryFacetsOption
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model\ResourceModel
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CategoryFacetsOption extends AbstractDb
{
    const TABLE_NAME   = 'born_category_facets_options';
    const PRIIMARY_KEY = 'id';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct():void
    {
        $this->_init(self::TABLE_NAME, self::PRIIMARY_KEY);
    }//end _construct()


}//end class
