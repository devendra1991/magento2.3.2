<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface;
use Born\CategoryFacets\Model\ResourceModel\CategoryFacets\CollectionFactory;
use Born\CategoryFacets\Api\Data\CategoryFacetsSearchResultsInterfaceFactory;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
/**
 * class CategoryFacetsRepository
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class CategoryFacetsRepository implements CategoryFacetsRepositoryInterface
{
    /**
     * @var \Born\CategoryFacets\Model\CategoryFacetsFactory
     */
    private $categoryFacetsFactory;
    /**
     * @var ResourceModel\CategoryFacets\CollectionFactory
     */
    private $categoryFacetsCollectionFactory;
    /**
     * @var DonationsSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;


    /**
     * DevpromoRepository constructor.
     * @param CategoryFacetsFactory                $categoryFacetsFactory CategoryFacetsFactory
     * @param CollectionFactory                    $collectionFactory     CollectionFactory
     * @param CategoryFacetsSearchResultsInterfaceFactory $searchResultsFactory  CategoryFacetsSearchResultsInterfaceFactory
     */
    public function __construct(
        CategoryFacetsFactory $categoryFacetsFactory,
        CollectionFactory $collectionFactory,
        CategoryFacetsSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->categoryFacetsFactory           = $categoryFacetsFactory;
        $this->categoryFacetsCollectionFactory = $collectionFactory;
        $this->searchResultsFactory            = $searchResultsFactory;
    }//end __construct()


    /**
     * {@inheritdoc}
     */
    public function save(CategoryFacetsInterface $data):CategoryFacets
    {
        try {
            $data->getResource()->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Record: %1',
                $exception->getMessage()
            ));
        }

        return $data;
    }//end save()


    /**
     * {@inheritdoc}
     */
    public function getById($id):CategoryFacets
    {
        $categoryFacetsModel = $this->categoryFacetsFactory->create();
        $categoryFacetsModel->getResource()->load($categoryFacetsModel, $id);
        if (!$categoryFacetsModel->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $id));
        }

        return $categoryFacetsModel;
    }//end getById()


    /**
     * {@inheritdoc}
     */
    public function delete(CategoryFacetsInterface $data):bool
    {
        try {
            $data->getResource()->delete($data);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Donations: %1',
                $exception->getMessage()
            ));
        }

        return true;
    }//end delete()


    /**
     * {@inheritdoc}
     */
    public function deleteById($id):bool
    {

        return $this->delete($this->getById($id));
    }//end deleteById()


    /**
     * {@inheritDoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria):SearchResults
    {
        $collection = $this->categoryFacetsCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getData());

        return $searchResults;
    }//end getList()


}//end class
