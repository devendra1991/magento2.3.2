<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

/**
 * Class Attributes
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class ArrayManipulation
{


    /**
     * Add/Change item position in array
     * Shifting elements up/down depending on available positions
     * Priority for shifting down
     * @param array $array    array
     * @param int   $element  int
     * @param int   $position int
     * @return array
     */
    public function addItemChangePosition(array &$array, int $element, int $position = null):array
    {
        if (($key = array_search($element, $array)) !== false) {
            unset($array[$key]);
        }
        if (count($array) == 0) {
            $array[] = $element;
        } elseif (is_numeric($position) && $position < 0) {
            if ((count($array) + $position) < 0) {
                $array = array_insert($array, $element, 0);
            } else {
                $array[count($array) + $position] = $element;
            }
        } elseif (is_numeric($position) && isset($array[$position])) {
            $part1 = array_slice($array, 0, $position, true);
            $part2 = array_slice($array, $position, null, true);
            $array = array_merge($part1, [$position => $element], $part2);
            foreach ($array as $key => $item) {
                if (is_null($item)) {
                    unset($array[$key]);
                }
            }
        } elseif (is_null($position)) {
            $array[] = $element;
        } elseif (!isset($array[$position])) {
            $array[$position] = $element;
        }

        return array_merge($array);
    }//end addItemChangePosition()


}//end class
