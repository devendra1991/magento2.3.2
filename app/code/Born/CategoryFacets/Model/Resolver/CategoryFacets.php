<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\Catalog\Model\Product\AttributeSet\Options;
use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterfaceFactory as ModelFactory;
use Born\CategoryFacets\Api\CategoryFacetsOptionRepositoryInterface as OptionModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterfaceFactory as OptionModelFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;

/**
 * Class CategoryFacets
 *
 * @category  PHP
 * @package   Born\CustomerGraphQl\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CategoryFacets implements ResolverInterface
{

    const CATEGORY_ID = 'category_id';
    const IS_ACTIVE   = 'is_active';
    const FACETS_ID   = 'facets_id';

    /**
     * @var Attribute
     */
    protected $attribute;
    /**
     * @var Options
     */
    protected $_attributeSetCollectionFactory;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var OptionModelFactory
     */
    private $optionModelFactory;
    /**
     * @var OptionModelRepositoryInterface
     */
    private $optionModelRepositoryInterface;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Born\CustomerGraphQl\Api\CustomerManagementInterface
     */
    private $customerManagementInterface;


    /**
     * CategoryFacets constructor.
     * @param Options                        $attributeSetCollectionFactory  Options
     * @param ModelFactory                   $modelFactory                   ModelFactory
     * @param ModelRepositoryInterface       $modelRepositoryInterface       ModelRepositoryInterface
     * @param SearchCriteriaBuilder          $searchCriteriaBuilder          SearchCriteriaBuilder
     * @param Attribute                      $attribute                      Attribute
     * @param OptionModelFactory             $optionModelFactory             OptionModelFactory
     * @param OptionModelRepositoryInterface $optionModelRepositoryInterface OptionModelRepositoryInterface
     */
    public function __construct(
        Options $attributeSetCollectionFactory,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Attribute $attribute,
        OptionModelFactory $optionModelFactory,
        OptionModelRepositoryInterface $optionModelRepositoryInterface
    ) {
        $this->_attributeSetCollectionFactory = $attributeSetCollectionFactory;
        $this->modelFactory                   = $modelFactory;
        $this->modelRepositoryInterface       = $modelRepositoryInterface;
        $this->searchCriteriaBuilder          = $searchCriteriaBuilder;
        $this->attribute                      = $attribute;
        $this->optionModelFactory             = $optionModelFactory;
        $this->optionModelRepositoryInterface = $optionModelRepositoryInterface;
    }//end __construct()


    /**
     * @param Field                                                      $field   Field
     * @param \Magento\Framework\GraphQl\Query\Resolver\ContextInterface $context ContextInterface
     * @param ResolveInfo                                                $info    ResolveInfo
     * @param array|null                                                 $value   array|null
     * @param array|null                                                 $args    array|null
     * @return array|\Magento\Framework\GraphQl\Query\Resolver\Value|mixed
     * @throws GraphQlInputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['category_id'])) {
            throw new GraphQlInputException(__('"Category Id" value should be specified'));
        }
        $data = $this->getFacetsData($args['category_id']);

        return $data;
    }//end resolve()


    /**
     * Get Category Facets Data
     * @param int $categoryId int
     * @return array
     * @throws GraphQlInputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFacetsData($categoryId)
    {
        try {
            /** $data = $this->customerManagementInterface->createCustomerFromOrder($args, $context); **/

            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter(self::CATEGORY_ID, $categoryId, 'eq')
                ->addFilter(self::IS_ACTIVE, 1, 'eq')
                ->create();
            $searchResults  = $this->modelRepositoryInterface->getList($searchCriteria);
            $result         = $searchResults->getItems();

            if (empty($result)) {
                throw new GraphQlInputException(__('Invalid Category Id'));
            }
            $attributeData = [];
            foreach ($result as $data) {
                $attributeData['attributes'][$data['facets_position']] = $this->getFacetAttributeJsonData(
                    $data['product_attribute_id'],
                    $data['facets_position'],
                    $data['id']
                );
            }

            return $attributeData;
        } catch (ValidatorException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }
    }//end getFacetsData()


    /**
     * @param int $attributeId product attribute id
     * @param int $position    product attribute position
     * @param int $id          Facet Id
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFacetAttributeJsonData($attributeId, $position, $id)
    {
        $attributeData = $this->attribute->load($attributeId);

        return [
           'attribute_id' => $attributeData->getAttributeId(),
           'attribute_code' => $attributeData->getAttributeCode(),
           'attribute_name' => $attributeData->getFrontendLabel(),
           'sort_order' => $position,
           'attribute_option' => $this->getAttributeOptionsJsonData($id),
        ];
    }//end getFacetAttributeJsonData()


    /**
     * @param int $id Facets id
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAttributeOptionsJsonData($id)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(self::FACETS_ID, $id, 'eq')
            ->addFilter(self::IS_ACTIVE, 1, 'eq')
            ->create();
        $searchResults  = $this->optionModelRepositoryInterface->getList($searchCriteria);
        $result         = $searchResults->getItems('option_position');

        $attributeOptionData = [];
        if (!empty($result)) {
            foreach ($result as $data) {
                $attributeOptionData[$data['option_position']] = [
                    'option_id' => $data['option_id'],
                    'option_value' => $data['option_value'],
                    'option_position' => $data['option_position'],
                ];
            }
        }

        return json_encode($attributeOptionData);
    }//end getAttributeOptionsJsonData()


}//end class
