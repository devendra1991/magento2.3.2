<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Born\CategoryFacets\Api\CategoryFacetsOptionRepositoryInterface;
use Born\CategoryFacets\Model\ResourceModel\CategoryFacetsOption\CollectionFactory;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;

/**
 * class CategoryFacetsOptionRepository
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class CategoryFacetsOptionRepository implements CategoryFacetsOptionRepositoryInterface
{
    /**
     * @var \Born\CategoryFacets\Model\CategoryFacetsOptionFactory
     */
    private $categoryFacetsFactory;
    /**
     * @var ResourceModel\CategoryFacetsOption\CollectionFactory
     */
    private $categoryFacetsCollectionFactory;
    /**
     * @var SearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;


    /**
     * CategoryFacetsOptionRepository constructor.
     * @param CategoryFacetsOptionFactory                       $categoryFacetsOptionFactory CategoryFacetsOption
     * @param CollectionFactory                                 $collectionFactory           CollectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory        SearchResultsInterface
     */
    public function __construct(
        CategoryFacetsOptionFactory $categoryFacetsOptionFactory,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->categoryFacetsFactory           = $categoryFacetsOptionFactory;
        $this->categoryFacetsCollectionFactory = $collectionFactory;
        $this->searchResultsFactory            = $searchResultsFactory;
    }//end __construct()


    /**
     * {@inheritdoc}
     */
    public function save(CategoryFacetsOptionInterface $data):CategoryFacetsOption
    {
        try {
            $data->getResource()->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Record: %1',
                $exception->getMessage()
            ));
        }

        return $data;
    }//end save()


    /**
     * {@inheritdoc}
     */
    public function getById($id):CategoryFacetsOption
    {
        $categoryFacetsModel = $this->categoryFacetsFactory->create();
        $categoryFacetsModel->getResource()->load($categoryFacetsModel, $id);
        if (!$categoryFacetsModel->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $id));
        }

        return $categoryFacetsModel;
    }//end getById()


    /**
     * {@inheritdoc}
     */
    public function delete(CategoryFacetsOptionInterface $data):bool
    {
        try {
            $data->getResource()->delete($data);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Donations: %1',
                $exception->getMessage()
            ));
        }

        return true;
    }//end delete()


    /**
     * {@inheritdoc}
     */
    public function deleteById($id):bool
    {

        return $this->delete($this->getById($id));
    }//end deleteById()


    /**
     * {@inheritDoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria):SearchResults
    {
        $collectionData = $this->categoryFacetsCollectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collectionData->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collectionData->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collectionData->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collectionData->setCurPage($searchCriteria->getCurrentPage());
        $collectionData->setPageSize($searchCriteria->getPageSize());
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setTotalCount($collectionData->getSize());
        $searchResults->setItems($collectionData->getData());

        return $searchResults;
    }//end getList()


}//end class
