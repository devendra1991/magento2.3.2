<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

use Magento\Catalog\Model\Product\AttributeSet\Options;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class AttributeSets
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Api\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class AttributeSets implements OptionSourceInterface
{
    const LABEL = 'label';
    const VALUE = 'value';
    /**
     * @var Options
     */
    protected $_attributeSetCollectionFactory;


    /**
     * AttributeSets constructor.
     * @param Options $attributeSetCollectionFactory Options
     */
    public function __construct(
        Options $attributeSetCollectionFactory
    ) {
        $this->_attributeSetCollectionFactory = $attributeSetCollectionFactory;
    }//end __construct()


    /**
     * DropDown option data
     * @return array
     */
    public function toOptionArray():array
    {
        $coll         = $this->_attributeSetCollectionFactory;
        $attributeSet = [
                [
                    self::LABEL => 'All Attributes',
                    self::VALUE => '',
                ],
            ];
        foreach ($coll->toOptionArray() as $d) {
            $attributeSet[] = [
                self::LABEL => $d[self::LABEL],
                self::VALUE => $d[self::VALUE],
            ];
        }

        return $attributeSet;
    }//end toOptionArray()


}//end class
