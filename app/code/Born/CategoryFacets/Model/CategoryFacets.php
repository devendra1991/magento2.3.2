<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Model;

use Born\CategoryFacets\Api\Data\CategoryFacetsInterface;
use Magento\Framework\Model\AbstractModel;
use Born\CategoryFacets\Model\ResourceModel\CategoryFacets as CategoryFacetsResourceModel;

/**
 * class CategoryFacets
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class CategoryFacets extends AbstractModel implements CategoryFacetsInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'categoryfacets';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'categoryfacets';


    /**
     * Initialize resource model.
     *
     * @return void
     */
    public function _construct():void
    {
        $this->_init(CategoryFacetsResourceModel::class);
    }//end _construct()


    /**
     * Get facets id
     * @return CategoryFacetsInterface|int|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }//end getId()


    /**
     * Get facets store id
     * @return CategoryFacetsInterface/int/null
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }//end getStoreId()


    /**
     * @return int|mixed|null
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }//end getCategoryId()


    /**
     * Get Facets Product Attribute Id
     * @return CategoryFacetsInterface|int|mixed|null
     */
    public function getProductAttributeId()
    {
        return $this->getData(self::PRODUCT_ATTRIBUTE_ID);
    }//end getProductAttributeId()


    /**
     * Get Facets SortOrder
     * @return CategoryFacetsInterface|int|mixed|null
     */
    public function getFacetsPosition()
    {
        return $this->getData(self::FACETS_POSITION);
    }//end getSortOrder()


    /**
     * Get Facets Status
     * @return CategoryFacetsInterface|int|mixed|null
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }//end getIsActive()


    /**
     * Set facets id
     * @param int|mixed $id facets id
     * @return CategoryFacetsInterface|CategoryFacets|AbstractModel
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }//end setId()


    /**
     * Set facets StoreId
     * @param int $id StoreId
     * @return CategoryFacetsInterface|CategoryFacets
     */
    public function setStoreId($id)
    {
        return $this->setData(self::STORE_ID, $id);
    }//end setStoreId()


    /**
     * Set facets Category Id
     * @param int $id Category Id
     * @return CategoryFacetsInterface|CategoryFacets
     */
    public function setCategoryId($id)
    {
        return $this->setData(self::CATEGORY_ID, $id);
    }//end setCategoryId()


    /**
     * Set facets Product Attribute Id
     * @param int $id Product Attribute Id
     * @return CategoryFacetsInterface|CategoryFacets
     */
    public function setProductAttributeId($id)
    {
        return $this->setData(self::PRODUCT_ATTRIBUTE_ID, $id);
    }//end setProductAttributeId()


    /**
     * Set facets SortOrder
     * @param int $id SortOrder
     * @return CategoryFacetsInterface|CategoryFacets
     */
    public function setFacetsPosition($id)
    {
        return $this->setData(self::FACETS_POSITION, $id);
    }//end setSortOrder()


    /**
     * Set facets status
     * @param int $status status
     * @return CategoryFacetsInterface|CategoryFacets
     */
    public function setIsActive($status)
    {
        return $this->setData(self::IS_ACTIVE, $status);
    }//end setIsActive()


}//end class
