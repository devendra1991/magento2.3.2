<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Controller\Adminhtml\Category;

use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tabs;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Grid
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Controller\Adminhtml\Category
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class FacetesPosition extends Action
{
    /**
     * @var Tabs
     */
    private $tabs;


    /**
     * FacetesPosition constructor.
     *
     * @param Context $context Context
     * @param Tabs    $tabs    Tabs
     */
    public function __construct(
        Context $context,
        Tabs $tabs
    ) {
        parent::__construct($context);
        $this->tabs = $tabs;
    }//end __construct()


    /**
     * @return bool|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|string
     */
    public function execute()
    {
        try {
            $attributCollections = $this->tabs->getFacetsCollections();
            if (!empty($attributCollections)) {
                $facetsPositions = [];
                foreach ($attributCollections as $key => $value) {      //NOSONAR.
                    $facetsPositions[$value->getFacetsPosition()] = $value->getAttributeId();
                }
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData((object) $facetsPositions);

                return $resultJson;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();              // @codingStandardsIgnoreLine
        }
    }//end execute()


}//end class
