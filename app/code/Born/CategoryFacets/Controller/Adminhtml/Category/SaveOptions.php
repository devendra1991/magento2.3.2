<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Controller\Adminhtml\Category;

use Born\CategoryFacets\Api\CategoryFacetsOptionRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsOptionInterfaceFactory as ModelFactory;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab\Options;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tabs;
use Magento\Backend\App\Action;
use Born\CategoryFacets\Model\ArrayManipulation;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\LayoutFactory;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Controller\Adminhtml\Category
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class SaveOptions extends Action
{
    const FACETS_VALUE_KEY = 'facets_value_id';
    const OPTION_POSITION  = 'option_position';
    const FACETS_VALUE_ID  = 'id';
    const IS_ENABLE        = 'is_enable';
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;
    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ArrayManipulation
     */
    private $arrayManipulation;
    /**
     * @var Tabs
     */
    private $tabs;


    /**
     * @param Context                  $context                  Context
     * @param RawFactory               $resultRawFactory         RawFactory
     * @param LayoutFactory            $layoutFactory            LayoutFactory
     * @param RequestInterface         $request                  RequestInterface
     * @param ModelFactory             $modelFactory             ModelFactory
     * @param ModelRepositoryInterface $modelRepositoryInterface ModelRepositoryInterface
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder    SearchCriteriaBuilder
     * @param ArrayManipulation        $arrayManipulation        ArrayManipulation
     * @param Tabs                     $tabs                     Tabs
     */
    public function __construct(        // NOSONAR.
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory,
        RequestInterface $request,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ArrayManipulation $arrayManipulation,
        Tabs $tabs
    ) {
        parent::__construct($context);
        $this->resultRawFactory         = $resultRawFactory;
        $this->layoutFactory            = $layoutFactory;
        $this->request                  = $request;
        $this->modelFactory             = $modelFactory;
        $this->modelRepositoryInterface = $modelRepositoryInterface;
        $this->searchCriteriaBuilder    = $searchCriteriaBuilder;
        $this->arrayManipulation        = $arrayManipulation;
        $this->tabs                     = $tabs;
    }//end __construct()


    /**
     * Save data action
     * @return mixed
     */
    public function execute()
    {
        $paramData = $this->getRequest()->getParams();
        try {
            if (!empty($paramData)) {
                $facets_value_id = isset($paramData[self::FACETS_VALUE_KEY]) ? $paramData[self::FACETS_VALUE_KEY] : '';
                $facets_value_id = (int) trim($facets_value_id);
                $facets_position = isset($paramData[self::OPTION_POSITION]) ? $paramData[self::OPTION_POSITION] : '';
                $facets_position = (int) trim($facets_position);
                $isActive        = isset($paramData[self::IS_ENABLE]) ? $paramData[self::IS_ENABLE] : '';
                $isActive        = (int) trim($isActive);
                $action          = trim($paramData['action']);

                $arrFacetsResponse = $this->getGridArrayData();
                $searchCriteria    = $this->searchCriteriaBuilder
                    ->addFilter(self::FACETS_VALUE_ID, $facets_value_id, 'eq')
                    ->create();

                $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
                $result        = $searchResults->getItems();

                if ($action === 'update_status') {
                    $data['is_active'] = $isActive;
                } else {
                    $data['option_position'] = $facets_position;
                }
                if (!empty($result) && !empty($facets_value_id)) {
                    $data['id'] = $facets_value_id;
                    $modelFactoryResource = $this->modelFactory->create();
                    $objData              = $modelFactoryResource->addData($data);
                    $this->modelRepositoryInterface->save($objData);
                }

                if (!empty($result) && $action === 'update_position') {
                    $sortedPosition = $this->arrayManipulation->addItemChangePosition(
                        $arrFacetsResponse,
                        $facets_value_id,
                        $facets_position
                    );
                    $this->updatePosition($sortedPosition);
                }

                $resultRaw = $this->resultRawFactory->create();

                return $resultRaw->setContents(
                    $this->layoutFactory->create()->createBlock(
                        Options::class,
                        'categoryfacets.categoryfacets.option.grid'
                    )->toHtml()
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();  // @codingStandardsIgnoreLine
        }
    }//end execute()


    /**
     * Prepare data to update option position
     * @return array
     */
    public function getGridArrayData()
    {
        $collection = $this->tabs->getFacetsOptionCollections();
        $result = [];
        foreach ($collection as $data) {
            $result[$data['option_position']] = $data['id'];
        }

        return $result;
    }//end getGridArrayData()


    /**
     * Update sort position
     * @param array $sortedPosition array
     */
    private function updatePosition(array $sortedPosition):void
    {
        foreach ($sortedPosition as $position => $attributeId) {
            try {
                $updateSortOrder['id']                  = $attributeId;
                $updateSortOrder[self::OPTION_POSITION] = $position;

                $modelFactoryRes                        = $this->modelFactory->create();
                $objData                                = $modelFactoryRes->addData($updateSortOrder);
                $this->modelRepositoryInterface->save($objData);        // @codingStandardsIgnoreLine
            } catch (\Exception $e) {
                echo $e->getMessage(); // @codingStandardsIgnoreLine
            }
        }
    }//end updatePosition()


}//end class
