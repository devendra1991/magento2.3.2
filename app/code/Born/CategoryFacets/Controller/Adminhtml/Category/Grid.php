<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Controller\Adminhtml\Category;

use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab\Attributes;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Class Grid
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Controller\Adminhtml\Category
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Grid extends Action
{
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;


    /**
     * Grid constructor.
     * @param Context                 $context          Context
     * @param RawFactory              $resultRawFactory RawFactory
     * @param LayoutFactory           $layoutFactory    LayoutFactory
     * @param RequestInterface        $request          RequestInterface
     * @param SessionManagerInterface $sessionManager   SessionManagerInterface
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory,
        RequestInterface $request,
        SessionManagerInterface $sessionManager
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory    = $layoutFactory;
        $this->request          = $request;
        $this->sessionManager   = $sessionManager;
    }//end __construct()


    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $params    = $this->request->getParams();
        if (isset($params['category_attribute_set'])) {
            $this->sessionManager->setCategoryAttributeSet($params['category_attribute_set']);
            $this->sessionManager->setFacetsCategoryId($params['id']);
        }

        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                Attributes::class,
                'categoryfacets.categoryfacets.tab.grid'
            )->toHtml()
        );
    }//end execute()


}//end class
