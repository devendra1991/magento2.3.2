<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Controller\Adminhtml\Category;

use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab\Options;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Class OptionGrid
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Controller\Adminhtml\Category
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class OptionGrid extends Action
{
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;


    /**
     * Grid constructor.
     * @param Context                 $context          Context
     * @param RawFactory              $resultRawFactory RawFactory
     * @param LayoutFactory           $layoutFactory    LayoutFactory
     * @param RequestInterface        $request          RequestInterface
     * @param SessionManagerInterface $sessionManager   SessionManagerInterface
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory,
        RequestInterface $request,
        SessionManagerInterface $sessionManager
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory    = $layoutFactory;
        $this->request          = $request;
        $this->sessionManager   = $sessionManager;
    }//end __construct()


    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $params    = $this->request->getParams();
        if (isset($params['attribute_id'])) {
            $this->sessionManager->setFacetsAttributeId($params['attribute_id']);
        }

        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                Options::class,
                'categoryfacets.categoryfacets.option.grid'
            )->toHtml()
        );
    }//end execute()


}//end class
