<?php
/**
 * Born_CategoryFacets
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CategoryFacets
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types = 1);

namespace Born\CategoryFacets\Controller\Adminhtml\Category;

use Born\CategoryFacets\Api\CategoryFacetsRepositoryInterface as ModelRepositoryInterface;
use Born\CategoryFacets\Api\Data\CategoryFacetsInterfaceFactory as ModelFactory;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tab\Attributes;
use Born\CategoryFacets\Block\Adminhtml\CategoryFacets\Tabs;
use Magento\Backend\App\Action;
use Born\CategoryFacets\Model\ArrayManipulation;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\LayoutFactory;

/**
 * Class Save
 *
 * @category  PHP
 * @package   Born\CategoryFacets\Controller\Adminhtml\Category
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class Save extends Action
{
    const CATEGORY_ID          = 'category_id';
    const ATTRIBUTE_ID         = 'attribute_id';
    const FACETS_POSITION      = 'facets_position';
    const PRODUCT_ATTRIBUTE_ID = 'product_attribute_id';
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;
    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var ModelFactory
     */
    private $modelFactory;
    /**
     * @var ModelRepositoryInterface
     */
    private $modelRepositoryInterface;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ArrayManipulation
     */
    private $arrayManipulation;
    /**
     * @var Tabs
     */
    private $tabs;


    /**
     * @param Context                  $context                  Context
     * @param RawFactory               $resultRawFactory         RawFactory
     * @param LayoutFactory            $layoutFactory            LayoutFactory
     * @param RequestInterface         $request                  RequestInterface
     * @param ModelFactory             $modelFactory             ModelFactory
     * @param ModelRepositoryInterface $modelRepositoryInterface ModelRepositoryInterface
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder    SearchCriteriaBuilder
     * @param ArrayManipulation        $arrayManipulation        ArrayManipulation
     * @param Tabs                     $tabs                     Tabs
     */
    public function __construct(        // NOSONAR.
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory,
        RequestInterface $request,
        ModelFactory $modelFactory,
        ModelRepositoryInterface $modelRepositoryInterface,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ArrayManipulation $arrayManipulation,
        Tabs $tabs
    ) {
        parent::__construct($context);
        $this->resultRawFactory         = $resultRawFactory;
        $this->layoutFactory            = $layoutFactory;
        $this->request                  = $request;
        $this->modelFactory             = $modelFactory;
        $this->modelRepositoryInterface = $modelRepositoryInterface;
        $this->searchCriteriaBuilder    = $searchCriteriaBuilder;
        $this->arrayManipulation        = $arrayManipulation;
        $this->tabs                     = $tabs;
    }//end __construct()


    /**
     * Save data action
     * @return mixed
     */
    public function execute()
    {
        $paramData = $this->getRequest()->getParams();
        try {
            if (!empty($paramData)) {
                $category_id          = (int) trim($paramData[self::CATEGORY_ID]);
                $product_attribute_id = (int) trim($paramData[self::ATTRIBUTE_ID]);
                $facets_position      = (int) trim($paramData[self::FACETS_POSITION]);
                $action               = trim($paramData['action']);
                $arrFacetsResponse    = $this->tabs->decodeData($paramData['jsonFacetsResponse']);

                /** check if data exist */
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter(self::CATEGORY_ID, $category_id, 'eq')
                    ->addFilter(self::PRODUCT_ATTRIBUTE_ID, $product_attribute_id, 'eq')
                    ->create();

                $searchResults = $this->modelRepositoryInterface->getList($searchCriteria);
                $result        = $searchResults->getItems();

                $data = [
                    'store_id' => 0,
                    self::CATEGORY_ID => $category_id,
                    self::PRODUCT_ATTRIBUTE_ID => $product_attribute_id,
                    self::FACETS_POSITION => $facets_position,
                    'is_active' => (int) trim($paramData['is_enable']),
                ];
                if (!empty($result)) {
                    $id         = $result[0]['id'];
                    $data['id'] = $id;
                }

                $modelFactoryResource = $this->modelFactory->create();
                $objData              = $modelFactoryResource->addData($data);
                $this->modelRepositoryInterface->save($objData);

                if (!empty($result) && $action === 'update_position') {
                    $sortedPosition = $this->arrayManipulation->addItemChangePosition(
                        $arrFacetsResponse,
                        $product_attribute_id,
                        $facets_position
                    );
                    $this->updatePosition($sortedPosition, (int) $category_id);
                }

                $resultRaw = $this->resultRawFactory->create();

                return $resultRaw->setContents(
                    $this->layoutFactory->create()->createBlock(
                        Attributes::class,
                        'categoryfacets.categoryfacets.tab.grid'
                    )->toHtml()
                );
            }
        } catch (\Exception $e) {
            echo $e->getMessage();  // @codingStandardsIgnoreLine
        }
    }//end execute()


    /**
     * Update sort position
     * @param array $sortedPosition   array
     * @param int   $category_id      int
     */
    private function updatePosition(array $sortedPosition, int $category_id):void
    {
        foreach ($sortedPosition as $position => $attributeId) {
            try {
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter(self::CATEGORY_ID, $category_id, 'eq')
                    ->addFilter(self::PRODUCT_ATTRIBUTE_ID, $attributeId, 'eq')
                    ->create();
                $searchResults  = $this->modelRepositoryInterface->getList($searchCriteria)->getItems();

                $updateSortOrder['id']                  = $searchResults[0]['id'];
                $updateSortOrder[self::FACETS_POSITION] = $position;

                $modelFactoryRes                        = $this->modelFactory->create();
                $objData                                = $modelFactoryRes->addData($updateSortOrder);
                $this->modelRepositoryInterface->save($objData);        // @codingStandardsIgnoreLine
            } catch (\Exception $e) {
                echo $e->getMessage(); // @codingStandardsIgnoreLine
            }
        }
    }//end updatePosition()


}//end class
