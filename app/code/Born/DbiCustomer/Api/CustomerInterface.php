<?php
/**
 * Interface CustomerInterface
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiCustomer\Api\CustomerInterface
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiCustomer\Api;

/**
 * Delegating customer action
 */
interface CustomerInterface
{
    /**
     * Costumer Contacts method
     *
     * @param  $method
     * @param  array $params
     * @return mixed
     */
    public function customerContacts($method, $params = []);

    /**
     * Customer Information Method enterprise customer api
     *
     * @param  $method
     * @param  array $params
     * @return mixed
     */
    public function customerInformation($method, $params = []);

    /**
     * Search enterprise customer information
     *
     * @param  $method
     * @param  array $params
     * @return mixed
     */
    public function customerInformationSearch($method, $params = []);
}
