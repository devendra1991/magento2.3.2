<?php
/**
 * Class CustomerObserver
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiCustomer\Observer\Customer\CustomerObserver
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiCustomer\Observer\Customer;

use Born\DbiConnector\Helper\Data as DbiHelper;
use Born\DbiCustomer\Model\CustomerApi;
use Born\DbiNewsletter\Model\ResourceModel\Newsletter\CollectionFactory;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\MessageQueue\PublisherInterface;
use Psr\Log\LoggerInterface;

/**
 * Delegating to CustomerObserver to get the data of customer
 */
class CustomerObserver implements ObserverInterface
{
    const TOPIC_NAME = 'dbicustomer_contacts';

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var PublisherInterface
     */
    private $publisher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $customerList = [];
    /**
     * @var Helper
     */
    private $dbiConnectorHelper;
    /**
     * @var CustomerApi
     */
    private $dbiCustomerApi;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Newsletter
     */
    private $newsletter;

    /**
     * CustomerObserver constructor.
     *
     * @param JsonHelper $jsonHelper
     * @param PublisherInterface $publisher
     * @param LoggerInterface $logger
     * @param CustomerRepositoryInterface $customerRepository
     * @param Http $request
     * @param DbiHelper $dbiConnectorHelper
     * @param CustomerApi $dbiCustomerApi
     * @param CollectionFactory $newsletterCollectionFactory
     */
    public function __construct(
        JsonHelper $jsonHelper,
        PublisherInterface $publisher,
        // use for publish message in RabbitMQ
        LoggerInterface $logger,
        CustomerRepositoryInterface $customerRepository,
        Http $request,
        DbiHelper $dbiConnectorHelper,
        CustomerApi $dbiCustomerApi,
        CollectionFactory $newsletterCollectionFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->publisher = $publisher;
        $this->request = $request;

        $this->logger = $logger;
        $this->dbiConnectorHelper = $dbiConnectorHelper;
        $this->dbiCustomerApi = $dbiCustomerApi;
        $this->customerRepository = $customerRepository;
        $this->newsletterCollectionFactory = $newsletterCollectionFactory;
    }

    /**
     * @param Observer $observer
     * @return string
     */
    public function execute(Observer $observer)
    {
        try {
            $customer = $observer->getCustomerDataObject();
            if (in_array($customer->getEmail(), $this->customerList)) {
                return;
            }
            $customerData = [];
            $phoneNumber = "";
            $newsletterOptsArray = [];

            if (!empty($customer)) {
                if ($customer->getCustomAttribute('phone_number')) {
                    $phoneNumber = $customer->getCustomAttribute('phone_number')->getValue();
                }

                if ($customer->getCustomAttribute('newsletter_options')) {
                    $newsletterOptions = $customer->getCustomAttribute('newsletter_options')->getValue();
                    $newsletterCollection = $this->newsletterCollectionFactory->create()
                        ->addFieldToFilter(['newsletter_id'], [['in' => $newsletterOptions]]);
                    foreach ($newsletterCollection->getItems() as $record) {
                        $newsletterOptsArray[] = [
                            'optStatusCompanyName' => $record->getCompanyName(),
                            'optStatusCategory' => $record->getCategory(),
                            'optStatusSource' => ($record->getSource()) ? $record->getSource() : '',
                            'optStatusType' => $record->getStatusType(),
                            'optStatus' => ($record->getStatus() == 0) ? 'false' : 'true'
                        ];
                    }
                }
                $customerData['user_id'] = $customer->getId();
                $customerData['email'] = $customer->getEmail();
                if ($customer->getPrefix() != '') {
                    $customerData['prefix'] = $customer->getPrefix();
                }
                $customerData['firstName'] = $customer->getFirstName();
                if ($customer->getMiddleName() != '') {
                    $customerData['middleName'] = $customer->getMiddleName();
                }
                $customerData['lastName'] = $customer->getLastName();

                if ($phoneNumber != '') {
                    $customerData['phone'] = $phoneNumber;
                }

                if (!empty($newsletterOptsArray) && $newsletterOptsArray != '') {
                    $customerData['optStatus'] = $newsletterOptsArray;
                }

                $this->customerList[] = $customer->getEmail();
                if ($this->dbiConnectorHelper->isQueueProcessingEnable()) {
                    $publishData = $this->jsonHelper->jsonEncode($customerData);
                    /**
                     * @var TYPE_NAME $publishData
                     */
                    $this->publisher->publish(self::TOPIC_NAME, $publishData);
                } else {

                    if ($customer->getCustomAttribute('customer_uuid')) {
                        $customerData['customerUuid'] = $customer->getCustomAttribute(
                            'customer_uuid'
                        )->getValue();
                        $apiMethod = 'PATCH';
                        $customerData['updatedBy'] = "Consumer";
                        $customerContactsArray['email'] = $customerData['email'];

                        if (isset($customerData['optStatus'])) {
                            $customerContactsArray['optStatus'] = $customerData['optStatus'];
                        }
                        $customerContactsArray["updatedBy"] = "Consumer";
                        $customerContactsArray["customerUuid"] = $customer->getCustomAttribute(
                            'customer_uuid'
                        )->getValue();

                        /** @var TYPE_NAME $addInfoReturns */
                        $addInfoReturns = $this->dbiCustomerApi->customerContacts(
                            $apiMethod,
                            $customerContactsArray
                        );
                        $this->logger->info("Address Info : " . $addInfoReturns);
                    } else {
                        $apiMethod = 'POST';
                        $customerData['createdBy'] = "Consumer";
                    }

                    // removed following keys from the array,
                    // because customer information is not accepting these parameters
                    unset($customerData['optStatus']);
                    unset($customerData['user_id']);

                    $returns = $this->dbiCustomerApi->customerInformation($apiMethod, $customerData);

                    $data = $this->jsonHelper->jsonDecode($returns);
                    if (isset($data['response']) && $data['response'] == 'false') {
                        $this->logger->error(__('Empty response from DataHub'));
                    }
                    /*
                    * Return data from the Data Hub API and
                    * updating the Uuid of the registered customer after customer registered on datahub
                    * This is used basically after creating the customer.
                    * */
                    $this->logger->info("Customer Info : " . $returns);
                    if ($apiMethod == "POST") {
                        $customerUuid = $data['customerUuid'];
                        if (!$customer->getCustomAttribute('customer_uuid')) {
                            $customer->setCustomAttribute('customer_uuid', $customerUuid);
                            $this->customerRepository->save($customer);
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            $result = ['error' => $exception->getMessage()];
            $this->logger->error($this->jsonHelper->jsonEncode($result));
        }
    }
}
