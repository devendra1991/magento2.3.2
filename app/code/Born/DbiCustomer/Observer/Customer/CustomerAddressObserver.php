<?php
/**
 * Class CustomerAddressObserver
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiCustomer\Observer\Customer\CustomerAddressObserver
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiCustomer\Observer\Customer;

use Born\DbiConnector\Helper\Data as DbiHelper;
use Born\DbiCustomer\Model\CustomerApi;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\MessageQueue\PublisherInterface;
use Psr\Log\LoggerInterface;

/**
 * Delegating to CustomerAddressObserver to
 * get the data of customer address information
 */
class CustomerAddressObserver implements ObserverInterface
{
    const TOPIC_NAME = 'dbicustomer_contacts';

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var PublisherInterface
     */
    private $publisher;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var DbiHelper
     */
    private $dbiConnectorHelper;
    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var customerList
     */
    private $customerList = [];

    /**
     * CustomerAddressObserver constructor.
     *
     * @param JsonHelper $jsonHelper
     * @param PublisherInterface $publisher
     * @param CustomerApi $customerApi
     * @param LoggerInterface $logger
     * @param CustomerRepositoryInterface $customerRepository
     * @param CountryFactory $countryFactory
     */
    public function __construct(
        JsonHelper $jsonHelper,
        PublisherInterface $publisher,
        // use for publish message in RabbitMQ
        CustomerApi $customerApi,
        LoggerInterface $logger,
        CustomerRepositoryInterface $customerRepository,
        CountryFactory $countryFactory,
        DbiHelper $dbiConnectorHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->publisher = $publisher;
        $this->customerApi = $customerApi;

        $this->logger = $logger;
        $this->customerRepository = $customerRepository;
        $this->dbiConnectorHelper = $dbiConnectorHelper;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @param Observer $observer
     * @return string
     */
    public function execute(Observer $observer)
    {
        try {
            $customerAddress = $observer->getEvent()->getCustomerAddress();

            $customerData = [];
            $addressDetails = [];
            if (!empty($customerAddress)) {
                $userData = $this->customerRepository->getById($customerAddress->getCustomerId());
                $email = ($customerAddress->getEmail()) ? $customerAddress->getEmail() : $userData->getEmail();
                if (in_array($email, $this->customerList)) {
                    return;
                }
                $customerData['user_id'] = $customerAddress->getCustomerId();
                $addressDetails['firstName'] = ($customerAddress->getFirstname() != '') ?
                    $customerAddress->getFirstname() :
                    $userData->getFirstname();

                $addressDetails['lastName'] = ($customerAddress->getLastname() != '') ?
                    $customerAddress->getLastname() :
                    $userData->getLastname();

                $customerData['email'] = ($customerAddress->getEmail() != '') ?
                    $customerAddress->getEmail() :
                    $userData->getEmail();

                $addressDetails['address1'] = ($customerAddress->getStreet() != '') ?
                    implode(',', $customerAddress->getStreet()) : "";

                $addressDetails['city'] = $customerAddress->getCity();
                $addressDetails['state'] = $customerAddress->getRegion();
                $addressDetails['postalCode'] = $customerAddress->getPostcode();
                $countryName = $this->countryFactory->create()->loadByCode($customerAddress->getCountryId())->getName();
                $addressDetails['country'] = $countryName;
                if ($addressDetails['address1'] != '') {
                    if ($customerAddress->getIsDefaultShipping() == 1) {
                        $addressDetails['addressType'] = "Shipping";
                        $customerData['address'][] = $addressDetails;
                    }
                    if ($customerAddress->getIsDefaultBilling() == 1) {
                        $addressDetails['addressType'] = "Billing";
                        $customerData['address'][] = $addressDetails;
                    }
                }

                $phoneDetails['phone'] = $customerAddress->getTelephone();
                $phoneDetails['countryCode'] = $customerAddress->getCountryId();
                $customerData['phone'][] = $phoneDetails;
                $this->customerList[] = $customerAddress->getEmail();

                if ($this->dbiConnectorHelper->isQueueProcessingEnable()) {
                    $publishData = $this->jsonHelper->jsonEncode($customerData);
                    $this->publisher->publish(self::TOPIC_NAME, $publishData);
                } else {
                    if ($userData->getCustomAttribute('customer_uuid')) {
                        $customerData['customerUuid'] = $userData->getCustomAttribute('customer_uuid')->getValue();
                        $apiMethod = 'PATCH';
                        $customerData['updatedBy'] = "Consumer";
                    } else {
                        $apiMethod = 'POST';
                        $customerData['createdBy'] = "Consumer";
                    }
                    unset($customerData['user_id']);
                    $returns = $this->customerApi->customerContacts($apiMethod, $customerData);

                    $data = $this->jsonHelper->jsonDecode($returns);
                    if (isset($data['response']) && $data['response'] == 'false') {
                        $this->logger->error(__('Empty response from DataHub'));
                    }
                    $this->logger->info("Address Info : " . $returns);

                    /*
                    * Return data from the Data Hub API and
                    * updating the Uuid of the registered customer after customer registered on datahub
                    * This is used basically after creating the customer.
                    * */

                    if ($apiMethod == "POST") {
                        $customerUuid = $data['customerUuid'];
                        if (!$userData->getCustomAttribute('customer_uuid')) {
                            $userData->setCustomAttribute('customer_uuid', $customerUuid);
                            $this->customerRepository->save($userData);
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            $result = ['error' => $exception->getMessage()];
            $this->logger->error($this->jsonHelper->jsonEncode($result));
        }
    }
}
