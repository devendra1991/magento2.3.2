<?php
/**
 * Class MassConsumer
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiCustomer\Model\MassConsumer
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiCustomer\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\MessageQueue\CallbackInvoker;
use Magento\Framework\MessageQueue\ConsumerConfigurationInterface;
use Magento\Framework\MessageQueue\ConsumerInterface;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\MessageController;
use Magento\Framework\MessageQueue\QueueInterface;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
 * Delegating to MassConsumer to consume the data from the queue
 */
class MassConsumer implements ConsumerInterface
{
    const XPATH_FOR_DBI_CUSTOMER_LOG_ENABLED = 'born_dbi_extension/general/enable_logs';

    /**
     * @var \Born\DbiCustomer\Model\CustomerApi
     */
    private $customerApi;

    /**
     * MassConsumer constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param CallbackInvoker $invoker
     * @param ResourceConnection $resource
     * @param MessageController $messageController
     * @param ConsumerConfigurationInterface $configuration
     * @param CustomerApi $processQueueMsgCustomerApi
     * @param LoggerInterface $logger
     * @param JsonHelper $jsonHelper
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CallbackInvoker $invoker,
        ResourceConnection $resource,
        MessageController $messageController,
        ConsumerConfigurationInterface $configuration,
        CustomerApi $processQueueMsgCustomerApi,
        LoggerInterface $logger,
        JsonHelper $jsonHelper,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->invoker = $invoker;
        $this->resource = $resource;
        $this->messageController = $messageController;
        $this->configuration = $configuration;
        $this->processQueueMsgCustomerApi = $processQueueMsgCustomerApi;
        $this->logger = $logger;
        $this->jsonHelper = $jsonHelper;
        $this->customerRepository = $customerRepository;
    }

    /**
     * Connects to a queue, consumes a message on the queue,
     * and invoke a method to process the message contents.
     *
     * @param int|null $maxNumberOfMessages
     * if not specified - process all queued incoming messages and terminate,
     * otherwise terminate execution after processing the specified number of messages
     */
    public function process($maxNumberOfMessages = null)
    {
        $queue = $this->configuration->getQueue();
        if (!isset($maxNumberOfMessages)) {
            $queue->subscribe($this->getTransactionCallback($queue));
        } else {
            $this->invoker->invoke($queue, $maxNumberOfMessages, $this->getTransactionCallback($queue));
        }
    }

    /**
     * Get transaction callback. This handles the case of async.
     *
     * @param  QueueInterface $queue
     * @return \Closure
     */
    private function getTransactionCallback(QueueInterface $queue)
    {
        /**
         * @param EnvelopeInterface $message
         * @var TYPE_NAME $queue
         */
        return function (EnvelopeInterface $message) use ($queue) {
            $lock = null;
            $customerContactsArray = [];
            try {
                $isLogEnabled = $this->scopeConfig->getValue(
                    self::XPATH_FOR_DBI_CUSTOMER_LOG_ENABLED,
                    ScopeInterface::SCOPE_STORE
                );

                $lock = $this->messageController->lock($message, $this->configuration->getConsumerName());
                $messageBody = $message->getBody();
                $decodedArray = $this->jsonHelper->jsonDecode($this->jsonHelper->jsonDecode($messageBody));
                $userId = $decodedArray['user_id'];
                $customerObject = $this->customerRepository->getById($userId);
                unset($decodedArray['user_id']);
                if ($customerObject->getCustomAttribute('customer_uuid')) {
                    $decodedArray['customerUuid'] = $customerObject->getCustomAttribute(
                        'customer_uuid'
                    )->getValue();
                    $apiMethod = 'PATCH';
                    $decodedArray['updatedBy'] = "Consumer";

                    // Customer Contacts API parameters
                    $customerContactsArray["customerUuid"] = $customerObject->getCustomAttribute(
                        'customer_uuid'
                    )->getValue();
                } else {
                    $apiMethod = 'POST';
                    $decodedArray['createdBy'] = "Consumer";
                }

                $customerContactsArray['email'] = $decodedArray['email'];
                if (isset($decodedArray['address'])) {
                    $customerContactsArray['address'] = $decodedArray['address'];
                    unset($decodedArray['address']);
                }
                if (isset($decodedArray['phone']) && is_array($decodedArray['phone'])) {
                    $customerContactsArray['phone'] = $decodedArray['phone'];
                    unset($decodedArray['phone']);
                }
                if (isset($decodedArray['optStatus'])) {
                    $customerContactsArray['optStatus'] = $decodedArray['optStatus'];
                    unset($decodedArray['optStatus']);
                }
                // removed following keys from the array,
                // because customer information API is not accepting these parameters
                $returns = $this->processQueueMsgCustomerApi->customerInformation($apiMethod, $decodedArray);

                if ($isLogEnabled) {
                    $this->logger->info($returns);
                }

                //converting response to array
                $data = $this->jsonHelper->jsonDecode($returns);

                /*
                 * Return data from the Data Hub API and
                 * updating the Uuid of the registered customer after customer registered on datahub
                 * This is used basically after creating the customer.
                 * */

                if (isset($data['customerUuid']) && $apiMethod == "POST") {
                    $customerUuid = $data['customerUuid'];
                    if (!$customerObject->getCustomAttribute('customer_uuid')) {
                        $customerObject->setCustomAttribute('customer_uuid', $customerUuid);
                        $this->customerRepository->save($customerObject);
                    }
                    $customerContactsArray["customerUuid"] = $data['customerUuid'];
                }

                // Calling Customer Contacts API after the customer contact information
                if (isset($customerContactsArray["customerUuid"])) {
                    $customerContactsArray["updatedBy"] = "Consumer";
                    $addInfoReturns = $this->processQueueMsgCustomerApi->customerContacts(
                        "PATCH",
                        $customerContactsArray
                    );

                    if ($isLogEnabled) {
                        $this->logger->info("Address Info : " . $addInfoReturns);
                    }
                }

                if (isset($data['response']) && $data['response'] == 'false') {
                    $queue->reject($message); // if get error in message process
                }

                $queue->acknowledge($message); // send acknowledge to queue
            } catch (MessageLockException $exception) {
                $queue->acknowledge($message);
            } catch (ConnectionLostException $e) {
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            } catch (NotFoundException $e) {
                $queue->acknowledge($message);
                $this->logger->warning($e->getMessage());
            } catch (\Exception $e) {
                $queue->reject($message, false, $e->getMessage());
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            }
        };
    }
}
