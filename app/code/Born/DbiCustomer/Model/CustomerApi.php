<?php
/**
 * Class CustomerContactsApi
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiCustomer\Model\ApiCaller
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\DbiCustomer\Model;

use Born\DbiConnector\Helper\Data;
use Born\DbiCustomer\Api\CustomerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Json\Helper\Data as jsonHelper;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

/**
 * Delegating customer api action
 *
 * @property SessionManagerInterface coreSession
 * @property jsonHelper jsonHelper
 * @property Request request
 * @property CurlFactory curlFactory
 */
class CustomerApi implements CustomerInterface
{
    const XPATH_FOR_DBI_CUSTOMER_END_POINT = 'born_dbi_extension/dbi_customer_section/customer_end_point';
    const XPATH_FOR_DBI_CUSTOMER_INFORMATION_END_POINT =
        'born_dbi_extension/dbi_customer_information_section/customer_information_end_point';
    const XPATH_FOR_DBI_CUSTOMER_INFORMATION_SEARCH_END_POINT =
        'born_dbi_extension/dbi_customer_information_search_section/customer_information_search_end_point';
    /**
     * @var Curl
     */
    private $curlFactory;

    /**
     * @var LoggerInterface
     */

    private $logger;
    /**
     * @var ScopeInterface
     */
    private $scopeConfig;

    /**
     * @var ScopeInterface
     */
    private $data_helper;

    /**
     * ApiCaller constructor.
     *
     * @param ScopeConfigInterface    $scopeConfig
     * @param CurlFactory             $curlFactory
     * @param Data                    $dataHelper
     * @param LoggerInterface         $logger
     * @param Request                 $request
     * @param jsonHelper              $jsonHelper
     * @param SessionManagerInterface $coreSession
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurlFactory $curlFactory,
        Data $dataHelper,
        LoggerInterface $logger,
        Request $request,
        jsonHelper $jsonHelper,
        SessionManagerInterface $coreSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
        $this->data_helper = $dataHelper;
        $this->logger = $logger;
        $this->request = $request;
        $this->jsonHelper = $jsonHelper;
        $this->coreSession = $coreSession;
    }

    /**
     * This method is used to Customer Contact API, it contains the customer contact which is used to create,
     * update and get the Information about the customer
     *
     * Method contains GET,POST and PATCH
     *
     * @param  string $method
     * @param  array  $params
     * @return string
     */
    public function customerContacts($method, $params = [])
    {
        $end_point = $this->scopeConfig->getValue(
            self::XPATH_FOR_DBI_CUSTOMER_END_POINT,
            ScopeInterface::SCOPE_STORE
        );
        $apiCaller = $this->_apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->jsonEncode([
            'response' => 'false',
            "Message" => "Please try again, no response from the API."
        ]);
    }

    /**
     * This method is used to Customer Information API, it contains the customer information which is used to CREATE
     * and GET the Information about the customer
     *
     * Contact Information contains POST METHOD
     *
     * @param  string $method
     * @param  array  $params
     * @return mixed|string
     */
    public function customerInformation($method, $params = [])
    {
        $end_point = $this->scopeConfig->getValue(
            self::XPATH_FOR_DBI_CUSTOMER_INFORMATION_END_POINT,
            ScopeInterface::SCOPE_STORE
        );
        $apiCaller = $this->_apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->jsonEncode([
            'response' => 'false',
            "Message" => "Please try again, no response from the API."
        ]);
    }

    /**
     * This method is used to Customer Search Information API,
     * it contains the customer information which is used to CREATE,
     * UPDATE and GET the Information about the customer
     *
     * Contact Information Search contain POST, GET, PATCH METHOD
     *
     * @param  string $method
     * @param  array  $params
     * @return mixed|string
     */
    public function customerInformationSearch($method, $params = [])
    {
        $end_point = $this->scopeConfig->getValue(
            self::XPATH_FOR_DBI_CUSTOMER_INFORMATION_SEARCH_END_POINT,
            ScopeInterface::SCOPE_STORE
        );
        $apiCaller = $this->_apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->jsonEncode([
            'response' => 'false',
            "Message" => "Please try again, no response from the API."
        ]);
    }

    /**
     * _apiCaller
     *
     * Private method which call the curl for create, update
     * and get the information about the customer from the third party host.
     *
     * @param  array  $params
     * @param  string $end_point
     * @param  string $method
     * @return string
     */
    private function _apiCaller($params = [], $end_point, $method = "GET")
    {
        try {
            if (!empty($params) && $params != '') {

                /**
                 * @var TYPE_NAME $accessToken
                 */

                $accessToken = $this->_getAccessHeader();

                /* Create curl factory */
                $httpAdapter = $this->curlFactory->create();

                $postFields = '';

                if ($method == 'GET') {
                    $end_point .= '?' . http_build_query($params);
                }

                if ($method == 'POST') {
                    $postFields = $this->jsonHelper->jsonEncode($params);
                }

                if ($method == 'PATCH') {
                    $postFields = $this->jsonHelper->jsonEncode($params);
                    $setOpt = [
                        CURLOPT_CUSTOMREQUEST => "PATCH",
                        CURLOPT_POSTFIELDS => $postFields
                    ];
                    $httpAdapter->setOptions($setOpt);
                }

                $headers = ['authorizationToken:' . $accessToken];
                $httpAdapter->write($method, $end_point, '1.1', $headers, $postFields);
                return $httpAdapter->read();
            } else {
                $this->logger->error(__('Request don\'t have data.'));
            }
            $this->logger->error(__('Something went wrong.'));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return false;
    }

    /**
     * This method is used to create the access token
     *
     * @return array|mixed
     */
    private function _getAccessHeader()
    {
        $tokenResponse = $this->data_helper->getAccessToken();
        $json_response = $this->jsonHelper->jsonDecode($tokenResponse);

        if (isset($json_response['code']) && $json_response['code'] != '') {
            $this->logger->error(__('Unable to get the token', $json_response['body']));
            return $tokenResponse;
        } else {
            return $json_response['token'];
        }
    }

    /**
     * This method is used to create customer Unique user
     * identification which returns the hexadecimal string
     *
     * @return string
     * @throws \Exception
     */
    public function generateUuid()
    {
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
