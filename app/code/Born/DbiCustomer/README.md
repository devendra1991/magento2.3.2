Magento 2.3 Born_DBICustomer Module contains the API's which is used for to create, update and get the information of the customer.
This module contains the System configuration file which it needs to add the HOST URL's.

Follow below steps to install and configured the module.

=> copy this module into the app/code/ directory.

=> Born should be the Vendor and DbiCustomer should be the module name.

=> after placing the module in the right place next step is the enable module using following command.

php bin/magento module:enable Born_DbiCustomer

=> Next, after that run following command.

php bin/magento setup:upgrade

=> And the last one is to deploy by using following command.

php bin/magento setup:static-content:deploy -f


Now module has been enabled successfully. Now it's time to configure it, for this we required the HOST URL's.

Steps to configure and Enable the module:

Go to Stores => Configuration => DBI DATAHUB CONFIGURATIONS => DBI Connector.

there have 3 tabse as named is : 
    1) DBI Customer Contacts : which contains the host url of the customer contacts
    2) DBI Customer Information : which contains the host url of the customer information
    3) DBI Customer Information Search : which contains the host url of the customer information search.


IMPORTANT NOTES : 
                1) DbiConnector Module is the dependency of this module, because based on token will get generate.
                2) Above hosts urls is mandatory, Based on these host url it will handle all the api requests.