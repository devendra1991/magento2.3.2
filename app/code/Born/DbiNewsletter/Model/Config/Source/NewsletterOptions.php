<?php
/**
 * NewsletterOptions Model.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Model\Config\Source;

use Born\DbiNewsletter\Block\NewsletterOpts;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class NewsletterOptions
 * @package Born\DbiNewsletter\Model\Config\Source
 */
class NewsletterOptions extends AbstractSource
{
    /**
     * @var NewsletterOpts
     */
    private $newsletterOpts;

    public function __construct(
        NewsletterOpts $newsletterOpts
    ) {
        $this->newsletterOpts = $newsletterOpts;
    }

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = $this->newsletterOpts->getAllOptions();
        }
        return $this->_options;
    }

    /**
     * Get text of the option value
     *
     * @param  string|integer $value
     * @return string|bool
     */
    public function getOptionValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
}
