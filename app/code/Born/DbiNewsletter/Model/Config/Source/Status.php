<?php
/**
 * Model Status Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package Born\DbiNewsletter\Model\Config\Source
 */
class Status implements OptionSourceInterface
{
    /**
     * Get Newsletter row status type labels array.
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = ['1' => __('True'), '0' => __('False')];
        return $options;
    }

    /**
     * Get Newsletter is used to checkout form labels array.
     *
     * @return array
     */
    public function getOptionArrayForRegistration()
    {
        $options = ['1' => __('Yes'), '0' => __('No')];
        return $options;
    }

    /**
     * Get Newsletter is used to checkout form type labels array.
     *
     * @return array
     */
    public function getOptionArrayForCheckout()
    {
        $options = ['1' => __('Yes'), '0' => __('No')];
        return $options;
    }

    /**
     * Get Newsletter row status labels array with empty value for option element.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

    /**
     * Get Newsletter row type array for option element.
     *
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getOptions();
    }
}
