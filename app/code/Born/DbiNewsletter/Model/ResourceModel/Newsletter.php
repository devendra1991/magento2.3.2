<?php

/**
 * Newsletter Model.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Newsletter mysql resource.
 */
class Newsletter extends AbstractDb
{
    /**
     * @var string
     */
    protected $_idFieldName = 'newsletter_id';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('dbi_newsletter', 'newsletter_id');
    }
}
