<?php

/**
 * Newsletter Model.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Model\ResourceModel\Newsletter;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Born\DbiNewsletter\Model\ResourceModel\Newsletter
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'newsletter_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Born\DbiNewsletter\Model\Newsletter',
            'Born\DbiNewsletter\Model\ResourceModel\Newsletter'
        );
    }
}
