<?php

/**
 * Newsletter Model.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Model;

use Born\DbiNewsletter\Api\Data\NewsletterInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Newsletter
 * @package Born\DbiNewsletter\Model
 */
class Newsletter extends AbstractModel implements NewsletterInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'dbi_newsletter_records';

    /**
     * @var string
     */
    protected $_cacheTag = 'dbi_newsletter_records';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'dbi_newsletter_records';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Born\DbiNewsletter\Model\ResourceModel\Newsletter');
    }

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Company Name.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->getData(self::COMPANY_NAME);
    }

    /**
     * Set Company Name.
     *
     * @param  $companyName
     * @return newsletter
     */
    public function setCompanyName($companyName)
    {
        return $this->setData(self::COMPANY_NAME, $companyName);
    }

    /**
     * Get Category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->getData(self::CATEGORY);
    }

    /**
     * Set Category Name
     *
     * @param  $category
     * @return string
     */
    public function setCategory($category)
    {
        return $this->setData(self::CATEGORY, $category);
    }

    /**
     * Get Source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->getData(self::SOURCE);
    }

    /**
     * Set Source Name.
     *
     * @param  $source
     * @return newsletter
     */
    public function setSource($source)
    {
        return $this->setData(self::SOURCE, $source);
    }

    /**
     * Get Status Type.
     *
     * @return string
     */
    public function getStatusType()
    {
        return $this->getData(self::STATUS_TYPE);
    }

    /**
     * Set Status Type.
     *
     * @param  $statusType
     * @return newsletter
     */
    public function setStatusType($statusType)
    {
        return $this->setData(self::STATUS_TYPE, $statusType);
    }

    /**
     * Get Status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status.
     *
     * @param  int $status
     * @return newsletter
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get CreatedAt.
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     *
     * @param  $createdAt
     * @return newsletter
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
