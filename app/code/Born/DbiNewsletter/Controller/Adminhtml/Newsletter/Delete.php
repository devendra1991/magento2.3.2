<?php
/**
 * Delete Newsletter Controller Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Controller\Adminhtml\Newsletter;

use Born\DbiNewsletter\Model\Newsletter;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Delete
 * @package Born\DbiNewsletter\Controller\Adminhtml\Newsletter
 */
class Delete extends Action
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context    $context
     * @param Newsletter $collection
     */
    public function __construct(
        Context $context,
        Newsletter $collection
    ) {
        $this->collection = $collection;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $collection = $this->collection->load($id);
                $collection->delete();
                $this->messageManager->addSuccess(
                    __('Record has been deleted successfully.')
                );
            } catch (\Exception $exception) {
                $this->messageManager->addError(__($exception->getMessage()));
            }
        }
        return $this->resultFactory->create(
            ResultFactory::TYPE_REDIRECT
        )->setPath('*/*/index');
    }

    /**
     * Check delete Permission.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Born_DbiNewsletter::row_data_delete'
        );
    }
}
