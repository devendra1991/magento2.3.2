<?php
/**
 * AddRow Newsletter Controller Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Controller\Adminhtml\Newsletter;

use Born\DbiNewsletter\Model\NewsletterFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;

/**
 * Class AddRow
 * @package Born\DbiNewsletter\Controller\Adminhtml\Newsletter
 */
class AddRow extends Action
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var Born\DbiNewsletter\Model\NewsletterFactory
     */
    private $newsletterFactory;
    /**
     * @var Context
     */
    private $context;

    /**
     * @param Context           $context
     * @param Registry          $coreRegistry
     * @param NewsletterFactory $newsletterFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        NewsletterFactory $newsletterFactory
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->newsletterFactory = $newsletterFactory;
        $this->context = $context;
        parent::__construct($context);
    }

    /**
     * Mapped Newsletter List page.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rowId = (int)$this->getRequest()->getParam('id');
        $rowData = $this->newsletterFactory->create();
        /**
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $title = $rowData->getCompanyName();
            if (!$rowData->getNewsletterId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('newsletter/newsletter/rowdata');
                return;
            }
        }

        $this->coreRegistry->register('row_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Edit Newsletter ' . $title) : __('Add Newsletter');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Born_DbiNewsletter::add_row');
    }
}
