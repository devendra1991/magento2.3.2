<?php

/**
 * Save Controller Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Controller\Adminhtml\Newsletter;

use Born\DbiNewsletter\Model\NewsletterFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

/**
 * Class Save
 * @package Born\DbiNewsletter\Controller\Adminhtml\Newsletter
 */
class Save extends Action
{
    /**
     * @var Born\DbiNewsletter\Model\NewsletterFactory
     */
    public $newsletterFactory;

    /**
     * Save constructor.
     *
     * @param Context           $context
     * @param NewsletterFactory $newsletterFactory
     */

    public function __construct(
        Context $context,
        NewsletterFactory $newsletterFactory
    ) {
        $this->newsletterFactory = $newsletterFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('newsletter/newsletter/addrow');
            return;
        }
        try {
            $rowData = $this->newsletterFactory->create();
            $rowData->setData($data);
            if (isset($data['newsletter_id'])) {
                $rowData->setEntityId($data['newsletter_id']);
                $this->messageManager->addSuccess(__('Newsletter has been successfully updated.'));
            } else {
                $this->messageManager->addSuccess(__('Newsletter has been successfully saved.'));
            }
            $rowData->save();
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('newsletter/newsletter/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Born_DbiNewsletter::save');
    }
}
