<?php
/**
 * DbiNewsletter NewsletterInterface.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\DbiNewsletter\Api\Data;

interface NewsletterInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */

    const ENTITY_ID = 'id';
    const COMPANY_NAME = 'company_name';
    const CATEGORY = 'category';
    const SOURCE = 'source';
    const STATUS_TYPE = 'status_type';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATE_AT = 'updated_at';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId();

    /**
     *  Set EntityId.
     *
     * @param int $entityId
     */
    public function setEntityId($entityId);

    /**
     * Get Company Name.
     *
     * @return string
     */
    public function getCompanyName();

    /**
     * Set Company Name.
     * @param $companyName
     */
    public function setCompanyName($companyName);

    /**
     * Get Category
     *
     * @return string
     */
    public function getCategory();

    /**
     * Set Category Name
     *
     * @param $category
     * @return string
     */
    public function setCategory($category);

    /**
     * Get Source.
     *
     * @return string
     */
    public function getSource();

    /**
     * Set Source Name.
     * @param $source
     */
    public function setSource($source);

    /**
     * Get Status Type.
     *
     * @return string
     */
    public function getStatusType();

    /**
     * Set Status Type.
     * @param $statusType
     */
    public function setStatusType($statusType);

    /**
     * Get Status.
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set Status.
     * @param int $status
     */
    public function setStatus($status);


    /**
     * Get CreatedAt.
     *
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     * @param $createdAt
     */
    public function setCreatedAt($createdAt);
}
