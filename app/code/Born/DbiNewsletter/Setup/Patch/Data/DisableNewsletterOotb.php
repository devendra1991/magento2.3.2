<?php
/**
 * DisableNewsletterOotb Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\DbiNewsletter\Setup\Patch\Data;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class DisableNewsletterOotb
 * @package Born\DbiNewsletter\Setup\Patch\Data
 */
class DisableNewsletterOotb implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;
    /**
     * @var WriterInterface
     */
    private $writer;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->cacheTypeList = $cacheTypeList;
        $this->writer = $writer;
    }

    /**
     * Save Default Config Values
     *
     * @param array $configValues pass the array
     *
     * @return mixed
     */
    public function saveDefaultConfigValue($configValues)
    {
        foreach ($configValues as $key => $value) {
            $this->writer->save(
                $key,
                $value,
                \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                0
            );
        }
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $configValues
            = [
            'newsletter/general/active' => 0
            ];
        $this->moduleDataSetup->startSetup();
        $this->saveDefaultConfigValue($configValues);
        // Clean Config type Cache
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
