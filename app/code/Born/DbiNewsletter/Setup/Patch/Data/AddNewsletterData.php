<?php
/**
 * AddNewsletterData Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Setup\Patch\Data;

use Born\DbiNewsletter\Model\NewsletterFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

/**
 * Class AddData
 *
 * @package Born\DbiNewsletter\Setup\Patch\Data
 */
class AddNewsletterData implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var Newsletter
     */
    private $newsletterFactory;
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    public function __construct(
        NewsletterFactory $newsletterFactory,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->newsletterFactory = $newsletterFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */

    public function apply()
    {
        $newsletterData = [
            [
                "company_name" => "David's Bridal",
                "category" => "Email",
                "status_type" => "Bride",
                "status" => 0, // 1 => True, 0 => False
                "used_in_registration" => 1, // 1 => Yes, 0 => No
                "used_in_checkout" => 1 // 1 => Yes, 0 => No
            ],
            [
                "company_name" => "David's Bridal",
                "category" => "Appt SMS",
                "status_type" => "Bride",
                "status" => 0, // 1 => True, 0 => False
                "used_in_registration" => 1, // 1 => Yes, 0 => No
                "used_in_checkout" => 1 // 1 => Yes, 0 => No
            ],
            [
                "company_name" => "David's Bridal",
                "category" => "Direct Mail",
                "status_type" => "Bride",
                "status" => 0, // 1 => True, 0 => False
                "used_in_registration" => 1, // 1 => Yes, 0 => No
                "used_in_checkout" => 1 // 1 => Yes, 0 => No
            ],
            [
                "company_name" => "David's Bridal",
                "category" => "Phone",
                "status_type" => "Bride",
                "status" => 0, // 1 => True, 0 => False
                "used_in_registration" => 1, // 1 => Yes, 0 => No
                "used_in_checkout" => 1 // 1 => Yes, 0 => No
            ],
            [
                "company_name" => "Blue Print Registry",
                "category" => "Email",
                "status_type" => "Bride",
                "status" => 0, // 1 => True, 0 => False
                "used_in_registration" => 1, // 1 => Yes, 0 => No
                "used_in_checkout" => 1 // 1 => Yes, 0 => No
            ]
        ];

        $this->moduleDataSetup->startSetup();
        foreach ($newsletterData as $data) {
            $this->newsletterFactory->create()->setData($data)->save();
        }
        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */

    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */

    public static function getVersion()
    {
        return '2.0.0';
    }

    /**
     * {@inheritdoc}
     */

    public function getAliases()
    {
        return [];
    }
}
