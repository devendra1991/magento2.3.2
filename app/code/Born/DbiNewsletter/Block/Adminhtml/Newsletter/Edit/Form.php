<?php

/**
 * Form Block Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author     Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Block\Adminhtml\Newsletter\Edit;

use Born\DbiNewsletter\Model\Config\Source\Status;
use Born\DbiNewsletter\Model\Config\Source\UsedIn;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;

/**
 * Adminhtml Add New Row Form.
 */
class Form extends Generic
{
    /**
     * @var Context
     */
    private $context;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var FormFactory
     */
    private $formFactory;
    /**
     * @var array
     */
    private $data;

    /**
     * Form Constructor
     *
     * @param Context $context ,
     * @param Registry $registry ,
     * @param FormFactory $formFactory ,
     * @param Status $options ,
     * @param UsedIn $optionsUsedIn
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Status $options,
        UsedIn $optionsUsedIn,
        array $data = []
    ) {
        $this->options = $options;
        $this->optionsUsedIn = $optionsUsedIn;
        $this->context = $context;
        $this->registry = $registry;
        $this->formFactory = $formFactory;
        $this->data = $data;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('row_data');
        $form = $this->_formFactory->create(
            ['data' => [
                            'id' => 'edit_form',
                            'action' => $this->getData('action'),
                            'method' => 'post'
                        ]
            ]
        );

        $form->setHtmlIdPrefix('dbiNewsletter_');
        if ($model->getNewsletterId()) {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit Newsletter'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField(
                'newsletter_id',
                'hidden',
                ['name' => 'newsletter_id']
            );
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Add Newsletter'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField(
            'company_name',
            'text',
            [
                'name' => 'company_name',
                'label' => __('Company Name'),
                'id' => 'company_name',
                'title' => __('Company Name'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'category',
            'text',
            [
                'name' => 'category',
                'label' => __('Category'),
                'id' => 'category',
                'title' => __('Category'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'source',
            'text',
            [
                'name' => 'source',
                'label' => __('Status Source'),
                'id' => 'source',
                'title' => __('Status Source'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'status_type',
            'text',
            [
                'name' => 'status_type',
                'label' => __('Type'),
                'id' => 'status_type',
                'title' => __('Type'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'id' => 'status',
                'title' => __('Status'),
                'values' => $this->options->getOptionArray(),
                'class' => 'status',
            ]
        );

        $fieldset->addField(
            'used_in_registration',
            'select',
            [
                'name' => 'used_in_registration',
                'label' => __('Used In Registration'),
                'id' => 'used_in_registration',
                'title' => __('Used In Registration'),
                'values' => $this->optionsUsedIn->getUsedInOptionArray(),
                'class' => 'status',
            ]
        );

        $fieldset->addField(
            'used_in_checkout',
            'select',
            [
                'name' => 'used_in_checkout',
                'label' => __('Used In Checkout'),
                'id' => 'used_in_checkout',
                'title' => __('Used In Checkout'),
                'values' => $this->optionsUsedIn->getUsedInOptionArray(),
                'class' => 'status',
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
