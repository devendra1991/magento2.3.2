<?php
/**
 * AddRow Block Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiNewsletter
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiNewsletter\Block\Adminhtml\Newsletter;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Framework\Registry;

/**
 * Class AddRow
 * @package Born\DbiNewsletter\Block\Adminhtml\Newsletter
 */
class AddRow extends Container
{
    /**
     * Core registry.
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * AddRow constructor.
     *
     * @param Context  $context
     * @param Registry $registry
     * @param array    $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Add Row constructor
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Born_DbiNewsletter';
        $this->_controller = 'adminhtml_newsletter';
        parent::_construct();
        if ($this->_isAllowedAction('Born_DbiNewsletter::add_row')) {
            $this->buttonList->update('save', 'label', __('Save'));
        } else {
            $this->buttonList->remove('save');
        }
        $this->buttonList->remove('reset');
    }

    /**
     * Set Header Text
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getHeaderText()
    {
        return __('Add Newsletter');
    }

    /**
     * Check permission for passed action.
     *
     * @param string $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Get form action URL.
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        if ($this->hasFormActionUrl()) {
            return $this->getData('form_action_url');
        }

        return $this->getUrl('*/*/save');
    }
}
