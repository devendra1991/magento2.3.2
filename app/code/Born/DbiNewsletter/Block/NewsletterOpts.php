<?php
/**
 * NewsletterOpts Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_NewsletterGraphQl
 * @author     Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);
namespace Born\DbiNewsletter\Block;

use Born\DbiNewsletter\Model\ResourceModel\Newsletter\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class NewsletterOpts
 * @package Born\DbiNewsletter\Block
 */
class NewsletterOpts extends Template
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * NewsletterOpts constructor.
     *
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $arrOptions = [];
        $collection = $this->collectionFactory->create()
            ->addFieldToFilter(
                ['used_in_checkout', 'used_in_registration'],
                [['eq' => '1'], ['eq' => '1']]
            );
        if ($collection->getItems()) {
            foreach ($collection->getItems() as $record) {
                $arrOptions[] = [
                    "value" => $record->getNewsletterId(),
                    "label" => __($record->getCompanyName() . ' - ' . $record->getCategory())
                ];
            }
        }
        return $this->_options = $arrOptions;
    }
}
