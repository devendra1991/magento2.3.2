<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_QuoteItemGraphql
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphql
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\QuoteItemGraphQl\Model\Resolver;


use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\TypeResolverInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use \Magento\Quote\Api\Data\CartItemInterfaceFactory;
use Magento\Quote\Api\GuestCartItemRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;


/**
 * TotalsObject
 * Class TotalsObject
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class TotalsObject implements TypeResolverInterface
{
    /**
     * CartItemRepositoryInterface
     *
     * @var CartItemRepositoryInterface
     */
    protected $cartItemRepository;

    /**
     * CartItemInterfaceFactory
     *
     * @var CartItemInterfaceFactory
     */
    protected $cartItemFactory;

    /**
     * GuestCartItemRepositoryInterface
     *
     * @var GuestCartItemRepositoryInterface
     */
    protected $guestCartItemRepository;

    /**
     * QuoteIdMaskFactory
     *
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * CartRepositoryInterface
     *
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * SaveCartItem constructor.
     *
     * @param CartItemRepositoryInterface      $cartItemRepository      CartItemRep
     * @param CartItemInterfaceFactory         $cartItemFactory         CartItemInt
     * @param GuestCartItemRepositoryInterface $guestCartItemRepository GuestCartItem
     * @param QuoteIdMaskFactory               $quoteIdMaskFactory      QuoteIdMask
     * @param CartRepositoryInterface          $quoteRepository         Cart
     */
    public function __construct(
        CartItemRepositoryInterface $cartItemRepository,
        CartItemInterfaceFactory $cartItemFactory,
        GuestCartItemRepositoryInterface $guestCartItemRepository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->cartItemRepository = $cartItemRepository;
        $this->cartItemFactory = $cartItemFactory;
        $this->guestCartItemRepository = $guestCartItemRepository;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
    }


    /**
     * Determine a concrete GraphQL type based off the given data.
     *
     * @param array $data Array
     *
     * @return string
     */
    public function resolveType(array $data): string
    {
        //TODO: Implement type resolver
        return '';
    }
}