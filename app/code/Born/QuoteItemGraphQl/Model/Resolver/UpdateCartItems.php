<?php
/**
 * Born_QuoteItemGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\QuoteItemGraphQl\Model\Resolver;

use Born\DbiConnector\Helper\Data;
use Born\DbiConnector\Model\InventoryEDDFromDatahub;
use Klarna\Kp\Model\QuoteRepository;
use Magento\Checkout\Exception;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Json\Helper\Data as jsonHelper;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface;
use Psr\Log\LoggerInterface;
use Magento\Checkout\Helper\Cart as CartHelper;

/**
 * Class UpdateCartItems
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class UpdateCartItems implements ResolverInterface
{
    const REQUESTING_SYSTEM_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_system";

    const REQUESTING_COUNTRY_PATH = "born_dbi_extension/dbi_inventory_section/dbi_inventory_request_country";

    const DBI_INVENTORY_CHECK_ENABLE = "born_dbi_extension/dbi_inventory_section/enable";

    /**
     * MaskedQuoteIdToQuoteIdInterface
     *
     * @var MaskedQuoteIdToQuoteIdInterface
     */
    protected $mask;

    /**
     * LoggerInterface
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Cart
     *
     * @var Cart
     */
    protected $cart;

    /**
     * RequestQuantityProcessor
     *
     * @var RequestQuantityProcessor
     */
    protected $quantityProcessor;

    /**
     * CartRepositoryInterface
     *
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * Data
     *
     * @var Data
     */
    protected $dataHelper;

    /**
     * JsonHelper
     *
     * @var jsonHelper
     */
    protected $jsonHelper;

    /**
     * CurlFactory
     *
     * @var CurlFactory
     */
    protected $curlFactory;

    /**
     * Session
     *
     * @var Session
     */
    protected $session;

    /**
     * InventoryEDDFromDatahub
     *
     * @var InventoryEDDFromDatahub
     */
    protected $getFromDatahub;

    /**
     * CartHelper
     *
     * @var CartHelper
     */
    protected $cartHelper;

    protected $scopeConfig;

    /**
     * UpdateCartItems constructor.
     *
     * @param Cart                            $cart              Cart
     * @param RequestQuantityProcessor        $quantityProcessor RequestQuantityProcessor
     * @param CartRepositoryInterface         $quoteRepository   CartRepositoryInterface
     * @param MaskedQuoteIdToQuoteIdInterface $mask              MaskedQuoteIdToQuoteIdInterface
     * @param Data                            $dataHelper        Data
     * @param jsonHelper                      $jsonHelper        jsonHelper
     * @param CurlFactory                     $curlFactory       CurlFactory
     * @param LoggerInterface                 $logger            LoggerInterface
     * @param Session                         $session           Session
     * @param InventoryEDDFromDatahub         $getFromDatahub    InventoryEDDFromDatahub
     * @param CartHelper                      $cartHelper        CartHelper
     */
    public function __construct(
        Cart $cart,
        RequestQuantityProcessor        $quantityProcessor,
        CartRepositoryInterface         $quoteRepository,
        MaskedQuoteIdToQuoteIdInterface $mask,
        Data                            $dataHelper,
        jsonHelper                      $jsonHelper,
        CurlFactory                     $curlFactory,
        LoggerInterface                 $logger,
        Session                         $session,
        InventoryEDDFromDatahub         $getFromDatahub,
        CartHelper                      $cartHelper,
        ScopeConfigInterface            $scopeConfig

    ) {
        $this->cart              = $cart;
        $this->quantityProcessor = $quantityProcessor;
        $this->quoteRepository   = $quoteRepository;
        $this->mask              = $mask;
        $this->dataHelper        = $dataHelper;
        $this->jsonHelper        = $jsonHelper;
        $this->curlFactory       = $curlFactory;
        $this->logger            = $logger;
        $this->session           = $session;
        $this->getFromDatahub    = $getFromDatahub;
        $this->cartHelper        = $cartHelper;
        $this->scopeConfig       = $scopeConfig;
    }
    /**
     * Updating Shopping cart and checked inventory from datahub
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   Value
     * @param array|null       $args    argument
     *
     * @return array|Value|mixed
     * @throws LocalizedException
     * @throws GraphQlInputException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $maskId = $this->getMaskId($args);
        $itemId = $this->getItemId($args);
        $customerId = $context->getUserId();
        $updatedData = $this->updateCartItems($maskId, $itemId, $customerId, $args);
        return ['cart_details' => $updatedData];
    }

    /**
     * Get quote mask id
     *
     * @param array|null $args argument
     *
     * @return string
     * @throws GraphQlInputException
     */
    private function getMaskId($args): string
    {
        if (!isset($args['input']['mask_id'])) {
            throw new GraphQlInputException(__('"Quote id should be specified!'));
        }
        return (string)$args['input']['mask_id'];
    }

    /**
     * Get cart item id
     *
     * @param array|null $args argument
     *
     * @return int
     * @throws GraphQlInputException
     */
    private function getItemId($args): int
    {
        foreach ($args['input']['cart_details'] as $item) {
            if (!isset($item['item_id'])) {
                throw new GraphQlInputException(__('"Quote Item id should be specified!'));
            }
            return (int)$item['item_id'];
        }
    }

    /**
     * Update Cart Items
     *
     * @param string     $mask_id    Quote mask id
     * @param int        $itemId     Quote Item Id
     * @param int        $customerId Customer Id
     * @param array|null $args       argument
     *
     * @throws LocalizedException
     * @throws GraphQlInputException
     * @return array
     */
    protected function updateCartItems($mask_id, $itemId, $customerId, array $args): array
    {

        if (!isset($args['input']) || !is_array($args['input']) || empty($args['input'])) {
            throw new GraphQlInputException(__('"input" value should be specified'));
        }
        try {
            if (!$customerId) {
                $cart_id = $this->mask->execute($mask_id);
                $result = $this->updateShoppingCart($cart_id, $args);
            } else {
                $quotedata = $this->quoteRepository->getActiveForCustomer($customerId);
                $id = $quotedata->getId();
                $result = $this->updateShoppingCart($id, $args);
            }
        } catch (\Exception $exception) {
            throw new GraphQlInputException(
                __(' response: %1', $exception->getMessage())
            );
        }
        return $result;
    }

    /**
     * Updating Shopping Cart quantity
     *
     * @param int        $cart_id QuoteRepository
     * @param array|null $args    Request parameter
     *
     * @throws GraphQlInputException
     * @throws LocalizedException
     * @return array
     */
    protected function updateShoppingCart($cart_id, $args)
    {
        $enable = $this->scopeConfig->getValue(self::DBI_INVENTORY_CHECK_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if($enable){
            $requestSystem = $this->scopeConfig->getValue(self::REQUESTING_SYSTEM_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $requestCountry = $this->scopeConfig->getValue(self::REQUESTING_COUNTRY_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $messages = "";
            $canPlaceOrder = true;
            $items = $this->session->getQuote()->getAllVisibleItems();
            $response = [];
            foreach ($items as $item) {
                $params = [
                    'requestingSystem' => $requestSystem,
                    'requestingCountry' => $requestCountry,
                    'skus' => [$item->getSku()]
                ];

                $result = $this->getFromDatahub->getinventoryFromDatahub($params);

                if ($result == null) {
                    $canPlaceOrder = false;
                    $messages .= $item->getName() . " can't get data from DataHub!  ";
                    continue;
                }
                try {
                    if (!$result['available']) {
                        $canPlaceOrder = false;
                        $messages .= $item->getName() . "  is no longer there!  ";
                    } else {
                        $sum = $result['dcAvailable'] + $result['storeAvailable'];
                        foreach ($args['input']['cart_details'] as $cartItem) {
                            if ($item->getId() == $cartItem['item_id']) {
                                if ($sum >= $cartItem['qty']) {
                                    $canPlaceOrder = true;
                                    $quote = $this->quoteRepository->getActive($cart_id);
                                    $quoteItem = [
                                        $cartItem['item_id'] => ['qty' => $cartItem['qty']],
                                    ];
                                    $quoteItem = $this->cart->suggestItemsQty($quoteItem);
                                    $quoteItem = $this->quantityProcessor->process($quoteItem);
                                    $this->cart->updateItems($quoteItem)->save();
                                    $quote->collectTotals();
                                    $response[] = [
                                        'item_id' => $item->getId(),
                                        'quote_id' => $cart_id,
                                        'message' => 'Shopping cart is updated!',
                                        'product_name' => $item->getName()
                                    ];
                                } else {
                                    $canPlaceOrder = false;
                                    $messages .= ". The requested qty for " . $item->getName()
                                        . "(".$item->getId().")"." is exceeds the qty from Datahub! ".$sum. "  Items left only in Datahub!";
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    $canPlaceOrder = false;
                    echo $e->getMessage();
                }
            }
            if (!$canPlaceOrder) {
                throw new GraphQlInputException(__($messages));
            } else {
                if ($this->cartHelper->getItemsCount() === 0) {
                    $messages .= 'No items in a cart!';
                    throw new GraphQlInputException(__($messages));
                } else {
                    return $response;
                }
            }

        } else {
            $response [] = [
                'message'=> 'Please check datahub inventory enable status in admin!'
            ];
            return $response;
        }

    }
}
