<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_CartItemGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\CartItemGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\QuoteItemGraphQl\Model\Resolver;

use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\GuestCartRepositoryInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Webapi\Controller\Rest\ParamOverriderCustomerId;
use Magento\Customer\Model\Session;

/**
 * GetCartItem
 * Class GetCartItem
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class GetCartItem implements ResolverInterface
{
    /**
     * QuoteManagement
     *
     * @var QuoteManagement
     */
    protected $quoteManagement;

    /**
     * ParamOverriderCustomerId
     *
     * @var ParamOverriderCustomerId
     */
    protected $overriderCustomerId;

    /**
     * GuestCartRepositoryInterface
     *
     * @var GuestCartRepositoryInterface
     */
    protected $guestCartRepository;

    /**
     * Configurable
     *
     * @var Configurable
     */
    protected $configurable;

    /**
     * ProductFactory
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * Session
     *
     * @var Session
     */
    protected $session;


    /**
     * GetCartItem constructor.
     *
     * @param ParamOverriderCustomerId     $overriderCustomerId CustomerId
     * @param CartManagementInterface      $quoteManagement     CartManagement
     * @param GuestCartRepositoryInterface $guestCartRepository GuestCart
     * @param Configurable                 $configurable        Configurable
     * @param ProductFactory               $productFactory      ProductFactory
     * @param Session                      $session             Session
     */
    public function __construct(
        ParamOverriderCustomerId $overriderCustomerId,
        CartManagementInterface $quoteManagement,
        GuestCartRepositoryInterface $guestCartRepository,
        Configurable $configurable,
        ProductFactory $productFactory,
        Session $session
    ) {
        $this->quoteManagement = $quoteManagement;
        $this->overriderCustomerId = $overriderCustomerId;
        $this->guestCartRepository = $guestCartRepository;
        $this->configurable = $configurable;
        $this->productFactory = $productFactory;
        $this->session = $session;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   Value
     * @param array|null       $args    argument
     *
     * @return Value|\Magento\Quote\Api\Data\CartInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {



        if ($this->session->isLoggedIn()) {
            // at this point we assume customer is session
            $cart = $this->quoteManagement->getCartForCustomer(
                $this->overriderCustomerId->getOverriddenValue()
            );
        } else if ($args['CartId']) {
            // At this point we assume this is guest cart

            $cart = $this->guestCartRepository->get($args['CartId']);

        } else {
            // at this point we assume it is mine cart
            $cart = $this->quoteManagement->getCartForCustomer(
                $this->overriderCustomerId->getOverriddenValue()
            );
        }

        $itemsData = [];
        $optionArray = [];

        foreach ($cart->getItems() as $item) {
            $product = $item->getProduct();
            $parentIds = $this->configurable->getParentIdsByChild($product->getId());
            $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());

            $getAttributeOptions = [];
            $optionArray = [];
            if (isset($options['attributes_info'])) {
                $getAttributeOptions = $options['attributes_info'];
                foreach ($getAttributeOptions as $options) {
                    $optionArray['config'][$options['label']] = $options['value'];
                }

            }
            $itemOptions = json_encode($optionArray);

                $itemsData[] = array_merge(
                    $item->getData(),
                    ['product' =>
                        array_merge(
                            $product->getData(),
                            ['model' => $product]
                        )
                    ],
                    array('addtionaloption' => 'No'),
                    array('readytoship' => '4-10 Bussiness Days'),
                    array('itemoptions'=> $itemOptions)
                );

        }

        return array_merge(
            $cart->getData(),
            ['items' => $itemsData]
        );
    }
}