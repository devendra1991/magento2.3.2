<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_QuoteItemGraphql
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphql
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\QuoteItemGraphQl\Model\Resolver;


use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Api\GuestCartItemRepositoryInterface;
use Magento\Quote\Model\Webapi\ParamOverriderCartId;
use Magento\Customer\Model\Session;

/**
 * RemoveCartItem
 * Class RemoveCartItem
 *
 * @category  PHP
 * @package   Born\QuoteItemGraphQl\Model\Resolver
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class RemoveCartItem implements ResolverInterface
{
    /**
     * GuestCartItemRepositoryInterface
     *
     * @var GuestCartItemRepositoryInterface
     */
    protected $guestCartItemRepository;

    /**
     * CartItemRepositoryInterface
     *
     * @var CartItemRepositoryInterface
     */
    protected $cartItemRepository;

    /**
     * ParamOverriderCartId
     *
     * @var ParamOverriderCartId
     */
    protected $overriderCartId;

    /**
     * Session
     *
     * @var Session
     */
    protected $session;

    /**
     * RemoveCartItem constructor.
     *
     * @param CartItemRepositoryInterface      $cartItemRepository      CartItem
     * @param GuestCartItemRepositoryInterface $guestCartItemRepository GuestCart
     * @param ParamOverriderCartId             $overriderCartId         ParamCart
     * @param Session                          $session                 Session
     */
    public function __construct(
        CartItemRepositoryInterface $cartItemRepository,
        GuestCartItemRepositoryInterface $guestCartItemRepository,
        ParamOverriderCartId $overriderCartId,
        Session $session
    ) {
        $this->overriderCartId = $overriderCartId;
        $this->cartItemRepository = $cartItemRepository;
        $this->guestCartItemRepository = $guestCartItemRepository;
        $this->session = $session;
    }


    /**
     * Fetches the data from persistence models
     *
     * @param Field            $field   Field
     * @param ContextInterface $context ContextInterface
     * @param ResolveInfo      $info    ResolveInfo
     * @param array|null       $value   array|null
     * @param array|null       $args    argument
     *
     * @return array|Value|mixed
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        ['item_id' => $itemId] = $args;

        if ($this->session->isLoggedIn()) {
            // at this point we assume customer is session
            $this->cartItemRepository->deleteById($this->overriderCartId->getOverriddenValue(), $itemId);
        } else if (isset($args['CartId'])) {
            // At this point we assume this is guest cart
            $this->guestCartItemRepository->deleteById($args['CartId'], $itemId);
        } else {
            // at this point we assume it is mine cart
            $this->cartItemRepository->deleteById($this->overriderCartId->getOverriddenValue(), $itemId);
        }

        return [];
    }
}