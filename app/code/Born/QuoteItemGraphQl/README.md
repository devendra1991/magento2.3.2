# About Born_QuoteItemGraphQl

Request:: Quote Item

{
  getCartItem(CartId: "PErYtAMexL5XrQSg8d2Cs5PGWPwyvxCD") {
    id
    tax_amount
    subtotal
    discount_amount
    subtotal_with_discount
    grand_total
    items {
      item_id
      qty
      name
      sku
      options
      addtionaloption
      readytoship
      itemoptions
      product {
        thumbnail {
          label
          url
        }
        categories {
          id
        }
        special_price        
        price {
          regularPrice {
            amount {
              value
              currency
            }
          }
        }
        price {
          maximalPrice {
            amount {
              value
              currency
            }
            adjustments {
              code
              amount {
                value
                currency
              }
            }
          }
        }
      }
    }
  }
}

Response:: QuoteItem

{
  getCartItem(CartId: "BS6lJVa3YDRdWw2zaS07o3BfYBrursKt") {
    id
    tax_amount
    subtotal
    discount_amount
    subtotal_with_discount
    grand_total
    items {
      item_id
      qty
      name
      sku
      options
      addtionaloption
      readytoship
      itemoptions
      product {
        thumbnail {
          label
          url
        }
        categories {
          id
        }
        special_price        
        price {
          regularPrice {
            amount {
              value
              currency
            }
          }
        }
        price {
          maximalPrice {
            amount {
              value
              currency
            }
            adjustments {
              code
              amount {
                value
                currency
              }
            }
          }
        }
      }
    }
  }
}

Request : RemoveCartItem

mutation{ 
  removeCartItem(item_id: 409, CartId: "uKhtKEP4TENprzmnO9rkoyyJ39FU015i") {
        getCartItem(CartId: "uKhtKEP4TENprzmnO9rkoyyJ39FU015i") {
            id
            tax_amount
            subtotal
            discount_amount
            subtotal_with_discount
            grand_total
            items {                
      item_id
      qty
      name
      sku
      options
      addtionaloption
      readytoship
      itemoptions
      product {
        thumbnail {
          label
          url
        }
        categories {
          id
        }
        special_price        
        price {
          regularPrice {
            amount {
              value
              currency
            }
          }
        }
        price {
          maximalPrice {
            amount {
              value
              currency
            }
            adjustments {
              code
              amount {
                value
                currency
              }
            }
          }
        }
      }
    }
        }
    }
}

Response : Remove Cart Item
{
  "data": {
    "removeCartItem": {
      "getCartItem": {
        "id": null,
        "tax_amount": null,
        "subtotal": 177,
        "discount_amount": null,
        "subtotal_with_discount": 177,
        "grand_total": 177,
        "items": [
          {
            "item_id": 402,
            "qty": 3,
            "name": " Dyeable Satin Mid Heel Crystal T Strap Sandal ",
            "sku": "DBTIA-Red-5",
            "options": null,
            "addtionaloption": "No",
            "readytoship": "4-10 Bussiness Days",
            "itemoptions": "{\"config\":{\"Color\":\"Red\",\"size\":\"5\"}}",
            "product": {
              "thumbnail": {
                "label": " Dyeable Satin Mid Heel Crystal T Strap Sandal ",
                "url": "http://local.dbi-magento/static/version1562669405/graphql/_view/en_US/Magento_Catalog/images/product/placeholder/thumbnail.jpg"
              },
              "categories": [
                {
                  "id": 2
                },
                {
                  "id": 3
                }
              ],
              "special_price": null,
              "price": {
                "regularPrice": {
                  "amount": {
                    "value": 55,
                    "currency": "USD"
                  }
                },
                "maximalPrice": {
                  "amount": {
                    "value": 55,
                    "currency": "USD"
                  },
                  "adjustments": []
                }
              }
            }
          },
          {
            "item_id": 408,
            "qty": 1,
            "name": "test 123",
            "sku": "test 123",
            "options": null,
            "addtionaloption": "No",
            "readytoship": "4-10 Bussiness Days",
            "itemoptions": "[]",
            "product": {
              "thumbnail": {
                "label": "test 123",
                "url": "http://local.dbi-magento/static/version1562669405/graphql/_view/en_US/Magento_Catalog/images/product/placeholder/thumbnail.jpg"
              },
              "categories": [
                {
                  "id": 2
                },
                {
                  "id": 3
                }
              ],
              "special_price": null,
              "price": {
                "regularPrice": {
                  "amount": {
                    "value": 12,
                    "currency": "USD"
                  }
                },
                "maximalPrice": {
                  "amount": {
                    "value": 12,
                    "currency": "USD"
                  },
                  "adjustments": []
                }
              }
            }
          }
        ]
      }
    }
  }
}

Request: Updating shopping cart

mutation{
  updateCart(input:{
    mask_id:"CBhferjPYtKc0ZMGtSMwxDPt3fkBZfQf",
    cart_details:[
      {
        item_id:260,
        qty:3
      },
      {
        item_id:258,
        qty:0
      }
    ]
  }){
    cart_details{
      item_id
      message
      product_name
      quote_id
    }
  }
}

Response : for success updation of shopping cart

{
  "data": {
    "updateCart": {
      "cart_details": [
        {
          "item_id": 260,
          "message": "Shopping cart is updated!",
          "product_name": "test product",
          "quote_id": 145
        }
      ]
    }
  }
}