<?php
/**
 * Born_PasswordRule
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\PasswordRule
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Born_PasswordRule',
    __DIR__
);
