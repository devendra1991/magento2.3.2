<?php
/**
 * Born_PasswordRule
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\PasswordRule
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\PasswordRule\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class SetPasswordRuleValue
 *
 * @category  PHP
 * @package   Born\PasswordRule\Setup\Patch\Data
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class SetPasswordRuleValue implements DataPatchInterface
{
    const PASSWORD_RULE_PATH = 'customer/password/required_character_classes_number';
    /**
     * ModuleDataSetupInterface
     *
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * TypeListInterface
     *
     * @var TypeListInterface
     */
    protected $cacheTypeList;
    /**
     * WriteInterface
     *
     * @var WriterInterface
     */
    protected $writer;

    /**
     * SetDefaultConfig constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup ModuleSetupInterface
     * @param TypeListInterface        $cacheTypeList   TypeListInterface
     * @param WriterInterface          $writer          WriterInterface
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        TypeListInterface $cacheTypeList,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->cacheTypeList = $cacheTypeList;
        $this->writer = $writer;
    }

    /**
     * Create the Data Patch
     *
     * @return DataPatchInterface|void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->writer->save(
            self::PASSWORD_RULE_PATH,
            4,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
        // Clean Config type Cache
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
        $this->moduleDataSetup->endSetup();
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
