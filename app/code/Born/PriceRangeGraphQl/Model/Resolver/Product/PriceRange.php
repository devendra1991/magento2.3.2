<?php
/**
 * Copyright © Born, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Born_PriceRangeGraphQl
 *
 * PHP version 5.x-7.x
 *
 * @category  PHP
 * @package   Born\PriceRangeGraphQl
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

namespace Born\PriceRangeGraphQl\Model\Resolver\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Serialize\SerializerInterface;



/**
 * Get the Min and Max SpecialPrice for Configurable Product
 *
 * Class PriceRange
 *
 * @category  PHP
 * @package   Born\PriceRangeGraphQl\Model\Resolver\Product
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */

class PriceRange implements ResolverInterface
{
    /**
     * SerializerInterface
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * PriceRange constructor.
     *
     * @param SerializerInterface $serializer SerializerInterface
     */
    public function __construct(

        SerializerInterface $serializer
    ) {

        $this->serializer = $serializer;

    }


    /**
     * ResoverFunction
     *
     * @param  Field            $field   Field
     * @param  ContextInterface $context ContextInterface
     * @param  ResolveInfo      $info    ResolveInfo
     * @param  array|null       $value   null
     * @param  array|null       $args    null
     * @return string
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }
        /**
         * ProductSku
         *
         * @var Product $product
         */
        $product = $value['model'];
        $productType = $product->getTypeId();

        if ($productType == 'configurable') {

            $specialChildProductPrice = [];
            $allChildProductPrice = [];
            $specialChildProduct
                = $product->getTypeInstance()->getUsedProducts($product);
            foreach ($specialChildProduct as $child) {
                $specialPrice = $child->getSpecialPrice();
                $actualPrice = $child->getPrice();
                $actualPrice = number_format($actualPrice, 2, '.', '');
                $allChildProductPrice[] = $actualPrice;
                if ($specialPrice != 0 && !is_null($specialPrice)) {
                    $specialPrice = number_format($specialPrice, 2, '.', '');
                    $specialChildProductPrice[] = $specialPrice;
                }
            }

            $priceRange = [];
            $maxIntRegular = max($allChildProductPrice);
            $minIntRegular = min($allChildProductPrice);
            $priceRange['RegularPrice']['MinPrice'] =  $minIntRegular;
            $priceRange['RegularPrice']['MaxPrice'] =  $maxIntRegular;

            $priceRangeJson = $this->serializer->serialize($priceRange);

            if (count($specialChildProductPrice) == 0) {
                return $priceRangeJson;
            }
            $maxIntSpecial = max($specialChildProductPrice);
            $minIntSpecial = min($specialChildProductPrice);
            if ($maxIntSpecial == 0) {
                return '';
            }
            if ($minIntSpecial == $maxIntSpecial) {

                $priceRange['SpecialPrice']['MinPrice'] =  $minIntSpecial;
                $priceRange['SpecialPrice']['MaxPrice'] =  $maxIntRegular;
                $priceRangeJson = $this->serializer->serialize($priceRange);
                return $priceRangeJson;
            } else {
                $priceRange['SpecialPrice']['MinPrice'] =  $minIntSpecial;
                $priceRange['SpecialPrice']['MaxPrice'] =  $maxIntSpecial;
                $priceRangeJson = $this->serializer->serialize($priceRange);
                return $priceRangeJson;
            }
        }
    }
}
