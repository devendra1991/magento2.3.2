# About Born_PriceRangeGraphQl

Request:: Product PriceRange

{
  products(filter: { sku: { eq: "DBI143Config" } }) {
    items {
      id
      attribute_set_id
      name
      sku
      type_id
      price_range    
      price {
        regularPrice {
          amount {
            value
            currency
          }
          
        }
          maximalPrice{             
              amount {
                 currency
                 value
               }
            }
            minimalPrice{
             amount {
                 currency
                 value
               }               
            }
        
        
      }
      categories {
        id
      }
      ... on ConfigurableProduct {
        configurable_options {
          id
          attribute_id
          label
          position
          use_default
          attribute_code
          values {
            value_index
            label
          }
          product_id
        }
        variants {
          product {
            id
            name
            sku
            attribute_set_id
            ... on PhysicalProductInterface {
              weight
            }
            price {
              regularPrice {
                amount {
                  value
                  currency
                }
              }
			   maximalPrice{             
              amount {
                 currency
                 value
               }
            }
            minimalPrice{
             amount {
                 currency
                 value
               }               
            }
            }
          
          }
                 
          
          attributes {
            label
            code
            value_index
          }
        }
      }
    }
  }

Response:: Product PriceRange

{
  "data": {
    "products": {
      "items": [
        {
          "id": 5407,
          "attribute_set_id": 4,
          "name": "DBI143Config",
          "sku": "DBI143Config",
          "type_id": "configurable",
          "price_range": "{\"RegularPrice\":{\"MinPrice\":\"95.00\",\"MaxPrice\":\"100.00\"},\"SpecialPrice\":{\"MinPrice\":\"50.00\",\"MaxPrice\":\"90.00\"}}",
          "price": {
            "regularPrice": {
              "amount": {
                "value": 100,
                "currency": "USD"
              }
            },
            "maximalPrice": {
              "amount": {
                "currency": "USD",
                "value": 50
              }
            },
            "minimalPrice": {
              "amount": {
                "currency": "USD",
                "value": 50
              }
            }
          },
          "categories": [
            {
              "id": 2
            },
            {
              "id": 3
            }
          ],
          "configurable_options": [
            {
              "id": 18,
              "attribute_id": "93",
              "label": "Color",
              "position": 1,
              "use_default": false,
              "attribute_code": "color",
              "values": [
                {
                  "value_index": 14,
                  "label": "Red"
                },
                {
                  "value_index": 15,
                  "label": "Blue"
                },
                {
                  "value_index": 818,
                  "label": "Black"
                }
              ],
              "product_id": 5407
            },
            {
              "id": 19,
              "attribute_id": "180",
              "label": "size",
              "position": 0,
              "use_default": false,
              "attribute_code": "size",
              "values": [
                {
                  "value_index": 16,
                  "label": "4"
                },
                {
                  "value_index": 17,
                  "label": "5"
                },
                {
                  "value_index": 18,
                  "label": "6"
                }
              ],
              "product_id": 5407
            }
          ],
          "variants": [
            {
              "product": {
                "id": 5398,
                "name": "DBI143Config-4-Red",
                "sku": "DBI143Config-4-Red",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 98,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 98
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 98
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Red",
                  "code": "color",
                  "value_index": 14
                },
                {
                  "label": "4",
                  "code": "size",
                  "value_index": 16
                }
              ]
            },
            {
              "product": {
                "id": 5399,
                "name": "DBI143Config-4-Blue",
                "sku": "DBI143Config-4-Blue",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 95,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 95
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 95
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Blue",
                  "code": "color",
                  "value_index": 15
                },
                {
                  "label": "4",
                  "code": "size",
                  "value_index": 16
                }
              ]
            },
            {
              "product": {
                "id": 5400,
                "name": "DBI143Config-4-Black",
                "sku": "DBI143Config-4-Black",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 99,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 99
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 99
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Black",
                  "code": "color",
                  "value_index": 818
                },
                {
                  "label": "4",
                  "code": "size",
                  "value_index": 16
                }
              ]
            },
            {
              "product": {
                "id": 5401,
                "name": "DBI143Config-5-Red",
                "sku": "DBI143Config-5-Red",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Red",
                  "code": "color",
                  "value_index": 14
                },
                {
                  "label": "5",
                  "code": "size",
                  "value_index": 17
                }
              ]
            },
            {
              "product": {
                "id": 5402,
                "name": "DBI143Config-5-Blue",
                "sku": "DBI143Config-5-Blue",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Blue",
                  "code": "color",
                  "value_index": 15
                },
                {
                  "label": "5",
                  "code": "size",
                  "value_index": 17
                }
              ]
            },
            {
              "product": {
                "id": 5403,
                "name": "DBI143Config-5-Black",
                "sku": "DBI143Config-5-Black",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Black",
                  "code": "color",
                  "value_index": 818
                },
                {
                  "label": "5",
                  "code": "size",
                  "value_index": 17
                }
              ]
            },
            {
              "product": {
                "id": 5404,
                "name": "DBI143Config-6-Red",
                "sku": "DBI143Config-6-Red",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 100
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Red",
                  "code": "color",
                  "value_index": 14
                },
                {
                  "label": "6",
                  "code": "size",
                  "value_index": 18
                }
              ]
            },
            {
              "product": {
                "id": 5405,
                "name": "DBI143Config-6-Blue",
                "sku": "DBI143Config-6-Blue",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 90
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 90
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Blue",
                  "code": "color",
                  "value_index": 15
                },
                {
                  "label": "6",
                  "code": "size",
                  "value_index": 18
                }
              ]
            },
            {
              "product": {
                "id": 5406,
                "name": "DBI143Config-6-Black",
                "sku": "DBI143Config-6-Black",
                "attribute_set_id": 4,
                "weight": 0,
                "price": {
                  "regularPrice": {
                    "amount": {
                      "value": 100,
                      "currency": "USD"
                    }
                  },
                  "maximalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 50
                    }
                  },
                  "minimalPrice": {
                    "amount": {
                      "currency": "USD",
                      "value": 50
                    }
                  }
                }
              },
              "attributes": [
                {
                  "label": "Black",
                  "code": "color",
                  "value_index": 818
                },
                {
                  "label": "6",
                  "code": "size",
                  "value_index": 18
                }
              ]
            }
          ]
        }
      ]
    }
  }
}