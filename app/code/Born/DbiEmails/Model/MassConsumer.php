<?php
/**
 * Class MassConsumer
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiEmails\Model\MassConsumer
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiEmails\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\MessageQueue\CallbackInvoker;
use Magento\Framework\MessageQueue\ConsumerConfigurationInterface;
use Magento\Framework\MessageQueue\ConsumerInterface;
use Magento\Framework\MessageQueue\EnvelopeInterface;
use Magento\Framework\MessageQueue\MessageController;
use Magento\Framework\MessageQueue\QueueInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Born\DbiEmails\Model\OrderEmailApi;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;

/**
 * Class Expedited
 *
 * @category  PHP
 * @package   Born\DbiEmails\Model
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
class MassConsumer implements ConsumerInterface
{
    const XPATH_FOR_DBI_CUSTOMER_LOG_ENABLED
        = 'born_dbi_extension/general/enable_logs';


    /**
     * OrderEmailApi
     *
     * @var \Born\DbiEmails\Model\OrderEmailApi
     */
    private $dbiEmailApi;

    /**
     * OrderRepositoryInterface
     *
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * SerializerJson
     *
     * @var SerializerJson
     */
    private $json;
    /**
     * CallbackInvoker
     *
     * @var CallbackInvoker
     */
    private $invoker;
    /**
     * ResourceConnection
     *
     * @var ResourceConnection
     */
    private $resource;
    /**
     * MessageController
     *
     * @var MessageController
     */
    private $messageController;
    /**
     * ConsumerConfigurationInterface
     *
     * @var ConsumerConfigurationInterface
     */
    private $configuration;
    /**
     * Order
     *
     * @var Order
     */
    private $order;

    /**
     * Initialize dependencies.
     *
     * @param Order                          $order             Order
     * @param CallbackInvoker                $invoker           CallbackInvoker
     * @param ResourceConnection             $resource          ResourceConnection
     * @param MessageController              $messageController MessageController
     * @param ConsumerConfigurationInterface $configuration     ConsumerConfiguration
     * @param LoggerInterface                $logger            LoggerInterface
     * @param OrderEmailApi                  $dbiEmailApi       OrderEmailApi
     * @param OrderRepositoryInterface       $orderRepository   OrderRepository
     * @param SerializerJson                 $json              SerializerJson
     */
    public function __construct(
        Order $order,
        CallbackInvoker $invoker,
        ResourceConnection $resource,
        MessageController $messageController,
        ConsumerConfigurationInterface $configuration,
        LoggerInterface $logger,
        OrderEmailApi $dbiEmailApi,
        OrderRepositoryInterface $orderRepository,
        SerializerJson $json
    ) {
        $this->invoker = $invoker;
        $this->order = $order;
        $this->resource = $resource;
        $this->messageController = $messageController;
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->dbiEmailApi = $dbiEmailApi;
        $this->orderRepository = $orderRepository;
        $this->json = $json;
    }

    /**
     * MaxNumberOfMessages
     *
     * @param null $maxNumberOfMessages maxNumberOfMessages
     */
    public function process($maxNumberOfMessages = null)
    {
        $queue = $this->configuration->getQueue();
        if (!isset($maxNumberOfMessages)) {
            $queue->subscribe($this->getTransactionCallback($queue));
        } else {
            $this->invoker->invoke($queue, $maxNumberOfMessages, $this->getTransactionCallback($queue));
        }
    }

    /**
     * GetTransactionCallback
     *
     * @param QueueInterface $queue QueueInterface
     *
     * @return \Closure
     */
    private function getTransactionCallback(QueueInterface $queue)
    {
        return function (EnvelopeInterface $message) use ($queue) {
            /** @var Order $order */
            $lock = null;
            try {
                $lock = $this->messageController->lock($message, $this->configuration->getConsumerName());
                $messageBody = $message->getBody();
                //$this->config->setDebugValue($messageBody);
                $orderArray = $this->json->unserialize($this->json->unserialize($messageBody));
                $apiMethod = 'POST';

                $response = $this->dbiEmailApi->orderEmail($apiMethod, $orderArray);
                $data = $this->json->unserialize($response);

                $orderId = $orderArray['orderData']['orderNumber'];
                $order = $this->order->load($orderId);
                $requestId = $data['requestId'] ?? null;
                if (!empty($requestId)) {
                    $order->setData('ordermail_request_id', $requestId);
                    $this->orderRepository->save($order);
                } else {
                    if (!empty($data['errorDetails'])) {
                        $orderId = $orderArray['orderData']['orderNumber'];
                        $order = $this->orderRepository->get($orderId);
                        $order->addCommentToStatusHistory('DataHub Warning : ' . implode(",\n", $data['errorDetails']), true);
                        $this->orderRepository->save($order);
                    }
                }

                if (isset($data['response']) && $data['response'] === false) {
                    $queue->reject($message); // if get error in message process
                }
                $queue->acknowledge($message); // send acknowledge to queue
            } catch (MessageLockException $exception) {
                $queue->acknowledge($message);
            } catch (ConnectionLostException $e) {
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            } catch (NotFoundException $e) {
                $queue->acknowledge($message);
                $this->logger->warning($e->getMessage());
            } catch (\Exception $e) {
                $queue->reject($message, false, $e->getMessage());
                $queue->acknowledge($message);
                if ($lock) {
                    $this->resource->getConnection()
                        ->delete($this->resource->getTableName('queue_lock'), ['id = ?' => $lock->getId()]);
                }
            }
        };
    }
}