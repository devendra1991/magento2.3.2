<?php
/**
 * OrderEmails OrderEmail Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiEmails
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiEmails\Model;


use Born\DbiConnector\Helper\Data;
use Born\DbiEmails\Api\EmailInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Json\Helper\Data as jsonHelper;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;


/**
 * Class OrderEmailApi
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiEmails\Model
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class OrderEmailApi implements EmailInterface
{
    const XPATH_FOR_DBI_ORDER_EMAIL_END_POINT
        = 'born_dbi_extension/dbi_order_email_section/order_email_end_point';


    /**
     * CurlFactory
     *
     * @var CurlFactory
     */
    private $curlFactory;
    /**
     * ScopeConfigInterface
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * Data
     *
     * @var Data
     */
    private $data_helper;
    /**
     * LoggerInterface
     *
     * @var LoggerInterface
     */
    private $logger;
    /**
     * jsonHelper
     *
     * @var jsonHelper
     */
    private $jsonHelper;


    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CurlFactory $curlFactory,
        Data $dataHelper,
        LoggerInterface $logger,
        Request $request,
        jsonHelper $jsonHelper,
        SessionManagerInterface $coreSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->curlFactory = $curlFactory;
        $this->data_helper = $dataHelper;
        $this->logger = $logger;
        $this->request = $request;
        $this->jsonHelper = $jsonHelper;
        $this->coreSession = $coreSession;
    }

    /**
     * OrderEmail RequestParam
     *
     * @param \Born\DbiEmails\Api\Email $method EmailParam
     * @param array                     $params mixed
     *
     * @return mixed|string
     */
    public function orderEmail($method, $params = [])
    {


        $end_point = $this->scopeConfig->getValue(
            self::XPATH_FOR_DBI_ORDER_EMAIL_END_POINT,
            ScopeInterface::SCOPE_STORE
        );
        $apiCaller = $this->_apiCaller($params, $end_point, $method);

        $response = \Zend_Http_Response::extractBody($apiCaller);
        if (!empty($response)) {
            return $response;
        }
        return $this->jsonHelper->jsonEncode([
            'response' => 'false',
            "Message" => 'Please try again, no response from the API.'
        ]);
    }

    /**
     * ApiCaller
     *
     * @param array     $params ArrayParam
     * @param $end_point
     * @param string    $method String
     *
     * @return bool
     */
    private function _apiCaller($params = [], $end_point, $method = "POST")
    {

        try {
            if (!empty($params) && $params != '') {

                $accessToken = $this->_getAccessHeader();
                /* Create curl factory */
                $httpAdapter = $this->curlFactory->create();

                $postFields = '';

                if ($method == 'POST') {
                    $postFields = $this->jsonHelper->jsonEncode($params);
                    //$logger->info('Json'.$postFields);
                }

                $headers = ['authorizationToken:' . $accessToken];
                $httpAdapter->write(
                    $method, $end_point, '1.1', $headers, $postFields
                );
                return $httpAdapter->read();

            } else {
                $this->logger->error(__('Request don\'t have data.'));
            }
            $this->logger->error(__('Something went wrong.'));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return false;
    }

    /**
     * This method is used to create the access token
     *
     * @return array|mixed
     */
    private function _getAccessHeader()
    {
        $tokenResponse = $this->data_helper->getAccessToken();
        $json_response = $this->jsonHelper->jsonDecode($tokenResponse);
        if (isset($json_response['code']) && $json_response['code'] != '') {
            $this->logger->error(
                __('Unable to get the token', $json_response['body'])
            );
            return $tokenResponse;
        } else {
            return $json_response['token'];
        }
    }
}