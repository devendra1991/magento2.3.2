<?php
/**
 * OrderEmails OrderEmail Class.
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiEmails
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

declare(strict_types=1);

namespace Born\DbiEmails\Observer;

use Born\DbiConnector\Helper\Data as DbiHelper;
use Magento\Framework\Event\Observer;
use Born\DbiEmails\Model\OrderEmailApi;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface as orderRepository;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Order ;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
/**
 * Class OrderEmail
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DatahubOrders\Plugin\Order/**
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

class  OrderEmail implements ObserverInterface
{

    const TOPIC_NAME = 'dbiorder_emails';
    const WEBSITE_URL
        = 'born_dbi_extension/dbi_order_email_section/order_product_url';
    /**
     * JsonHelper
     *
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * PublisherInterface
     *
     * @var PublisherInterface
     */
    protected $publisher;

    /**
     * LoggerInterface
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Helper
     *
     * @var Helper
     */
    protected $dbiConnectorHelper;
    /**
     * CustomerApi
     *
     * @var dbiEmailApi
     */
    protected $dbiEmailApi;
    /**
     * ProductFactory
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * StoreManagerInterface
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * Order
     *
     * @var Order
     */
    protected $order;
    /**
     * Http
     *
     * @var Http
     */
    protected $request;
    /**
     * OrderRepository
     *
     * @var orderRepository
     */
    protected $orderRepository;
    /**
     * ScopeConfigInterface
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * OrderEmail constructor.
     *
     * @param Order                 $order              Order
     * @param JsonHelper            $jsonHelper         JsonHelper
     * @param PublisherInterface    $publisher          PublisherInterface
     * @param LoggerInterface       $logger             LoggerInterface
     * @param orderRepository       $orderRepository    orderRepository
     * @param Http                  $request            Http
     * @param DbiHelper             $dbiConnectorHelper DbiHelper
     * @param ProductFactory        $productFactory     ProductFactory
     * @param StoreManagerInterface $storeManager       StoreManagerInterface
     * @param OrderEmailApi         $dbiEmailApi        OrderEmailApi
     * @param ScopeConfigInterface  $scopeConfig        ScopeConfigInterface
     */
    public function __construct(
        Order $order,
        JsonHelper $jsonHelper,
        PublisherInterface $publisher,
        LoggerInterface $logger,
        orderRepository $orderRepository,
        Http $request,
        DbiHelper $dbiConnectorHelper,
        ProductFactory $productFactory,
        StoreManagerInterface $storeManager,
        OrderEmailApi $dbiEmailApi,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->order = $order;
        $this->jsonHelper = $jsonHelper;
        $this->publisher = $publisher;
        $this->request = $request;
        $this->logger = $logger;
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->orderRepository = $orderRepository;
        $this->dbiConnectorHelper = $dbiConnectorHelper;
        $this->dbiEmailApi = $dbiEmailApi;
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * OrderEmail
     *
     * @param Observer $observer Observer
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->order->load($orderId);
        //get Order All Item
        $itemCollection = $order->getAllVisibleItems();

        $shippingAddress = $order->getShippingAddress();
        $line = [];

        foreach ($itemCollection as  $key => $item) {
            $options = $this->getItemOptions($item);
            $itemEdd = $this->getItemEdd($item);
            $urlOption = $this->getUrlOptions($item);
            $productImage = $this->getProductImage($item->getProductId());
            $productUrl = $this->getProductUrl($item->getProductId());
            if (!empty($urlOption)) {
                $productUrl = $productUrl.'?'.$urlOption;
            }

            $line[$key]['id'] = $item->getOrderId();
            $line[$key]['costAmount'] = $item->getPrice();
            $line[$key]['shipDescription'] = $order->getShippingDescription();
            $line[$key]['collectionId'] = $item->getItemId();
            $line[$key]['priceAmount'] = $item->getPrice();
            $line[$key]['quantity'] = $item->getQtyOrdered();
            $line[$key]['shipmentDate'] = $itemEdd;
            $line[$key]['shippingAmount'] = $item->getShippingAmount();
            $line[$key]['shippingTax'] = $item->getShippingTaxAmount();
            $line[$key]['taxAmount'] = $item->getTaxAmount();
            if($options) :
                foreach($options as  $option):
                    $line[$key][$option['label']] = $option['value'];
                endforeach ;
            endif ;
            $line[$key]['skuImageUrl']
                = $productImage;
            $line[$key]['productUrl']
                = $productUrl;
            $line[$key]['productName'] = $item->getName();
        }

        $apiMethod = 'POST';
        $orderEmailArray['firstName'] = $order->getCustomerFirstname();
        $orderEmailArray['lastName'] = $order->getCustomerLastname();
        $orderEmailArray['email'] = $order->getCustomerEmail();
        $orderEmailArray['orderData']['orderNumber']  = $order->getId();
        $orderEmailArray['orderData']['order']['orderId']  = $order->getId();
        $orderEmailArray['orderData']['order']['customerDiscount']
            =  $order->getDiscountAmount();
        $orderEmailArray['orderData']['order']['shippingAmount']
            = $order->getShippingAmount();
        $orderEmailArray['orderData']['order']['subtotalAmount']
            = $order->getSubtotal();
        $orderEmailArray['orderData']['order']['taxAmount']
            = $order->getTaxAmount();
        $orderEmailArray['orderData']['order']['totalAmount']
            = $order->getGrandTotal();
        $orderEmailArray['orderData']['order']['shipToFirstName']
            = $shippingAddress->getFirstName();
        $orderEmailArray['orderData']['order']['shipToLastName']
            = $shippingAddress->getLastName();
        $orderEmailArray['orderData']['order']['shipToEmail']
            = $shippingAddress->getEmail();
        $orderEmailArray['orderData']['order']['shipToPhone1']
            = $shippingAddress->getTelephone();
        $orderEmailArray['orderData']['order']['shipToAddress1']
            = implode(' ', $shippingAddress->getStreet());
        $orderEmailArray['orderData']['order']['shipToAddress2']
            = implode(' ', $shippingAddress->getStreet());
        $orderEmailArray['orderData']['order']['shipToCity']
            = $shippingAddress->getCity();
        $orderEmailArray['orderData']['order']['shipToCountry']
            = $shippingAddress->getCountryId();
        $orderEmailArray['orderData']['order']['shipToState']
            = $shippingAddress->getRegion();
        $orderEmailArray['orderData']['order']['shipToZip']
            = $shippingAddress->getPostcode();
        $orderEmailArray['orderData']['order']['lines']  = $line ;

        if ($this->dbiConnectorHelper->isQueueProcessingEnable()) {
            $publishData = $this->jsonHelper->jsonEncode($orderEmailArray);
            /**
             * PublishData
             *
             * @var TYPE_NAME $publishData
             */
            $this->publisher->publish(self::TOPIC_NAME, $publishData);
        } else {

            $addInfoReturns = $this->dbiEmailApi->orderEmail(
                $apiMethod,
                $orderEmailArray
            );
            $data = $this->jsonHelper->jsonDecode($addInfoReturns);


            $requestId = $data['requestId'] ?? null;
            if (!empty($requestId)) {
                $order->setData('ordermail_request_id', $requestId);
                $this->orderRepository->save($order);
            } else {
                if (!empty($data['errorDetails'])) {
                    $orderId = $orderEmailArray['orderData']['orderNumber'];
                    $order = $this->orderRepository->get($orderId);
                    $order->addCommentToStatusHistory('DataHub Warning : ' . implode(",\n", $data['errorDetails']), true);
                    $this->orderRepository->save($order);
                }
            }

            if ( isset($data['response']) && $data['response'] == 'false' ) {
                $this->logger->error(__('Empty response from DataHub'));

            }
        }

    }
    /**
     * GetItemOption ItemOptions
     *
     * @param Item $item Item
     *
     * @return array
     */
    public function getItemOptions($item)
    {
        $result = [];
        $option = $item->getProductOptions();
        if ($option) {
            if (isset($option['options'])) {
                $result = array_merge($result, $option['options']);
            }
            if (isset($option['additional_options'])) {
                $result = array_merge($result, $option['additional_options']);
            }
            if (isset($option['attributes_info'])) {
                $result = array_merge($result, $option['attributes_info']);
            }
        }
        return $result;
    }

    /**
     * GetProductImage Image
     *
     * @param ProductId $productid ProductId
     *
     * @return string
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductImage($productid)
    {

        $product = $this->productFactory->create()->load($productid);
        $productImage
            =  $product->getDescriptiveImage();
        return $productImage ;
    }

    /**
     * GetProductUrl Url
     *
     * @param Productid $productid Productid
     *
     * @return string
     */
    public function getProductUrl($productid)
    {
        $product = $this->productFactory->create()->load($productid);
        $productUrlKey = $product->getUrlKey();

        $webSiteUrl = $this->scopeConfig->getValue(
            self::WEBSITE_URL,
            ScopeInterface::SCOPE_STORE
        );
        $productUrl = $webSiteUrl.'product-id.'.$productUrlKey;
        return $productUrl;

    }

    /**
     * GetItemEdd
     *
     * @param Item $item Item
     *
     * @return string|null
     */
    public function getItemEdd($item)
    {
        $option = $item->getProductOptions();
        if ($option) {

            if (isset($option['info_buyRequest']['options']['specialOrder'])) {
                $result = $option['info_buyRequest']['options']['specialOrder'];
            } else if (isset($option['info_buyRequest']['options']['twoDayAir'])) {
                $result = $option['info_buyRequest']['options']['twoDayAir'];
            } else if (isset($option['info_buyRequest']['options']['nextDayAir'])) {
                $result = $option['info_buyRequest']['options']['nextDayAir'];
            } else if (isset($option['info_buyRequest']['options']['standard'])) {
                $result = $option['info_buyRequest']['options']['standard'];
            } else {
                $result = null;
            }

        }
        return $result;
    }

    /**
     * GetUrlOptions
     *
     * @param Item $item Item
     *
     * @return string|null
     */
    public function getUrlOptions($item)
    {

        $option = $item->getProductOptions();
        $urlString = [];
        if ($option) {
            if (isset($option['attributes_info'])) {
                $getAttributeOptions = $option['attributes_info'];
                foreach ($getAttributeOptions as $options) {
                    $optionLbl
                        = str_replace(' ', '_', strtolower($options['label']));
                    $optionValue = $options['value'];
                    $urlString[] = $optionLbl.'='.$optionValue;
                }
                $urlString = implode("&", $urlString);

            }
        }
        return $urlString;
    }

}