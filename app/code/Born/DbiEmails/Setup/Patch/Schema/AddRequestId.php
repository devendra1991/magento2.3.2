<?php
/**
 * Class AddRequestId
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiEmails\Setup\Patch\Schema
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */

namespace Born\DbiEmails\Setup\Patch\Schema;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;

/**
 * Class AddRequestId
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born\DbiEmails\Setup\Patch\Schema
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
class AddRequestId implements SchemaPatchInterface
{
    /**
     * SchemaSetupInterface
     *
     * @var SchemaSetupInterface
     */
    private $schemaSetup;

    /**
     * EnableSegmentation constructor.
     *
     * @param SchemaSetupInterface $schemaSetup SchemaSetupInterface
     */
    public function __construct(
        SchemaSetupInterface $schemaSetup
    ) {
        $this->schemaSetup = $schemaSetup;
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function apply()
    {
        $this->schemaSetup->startSetup();
        $setup = $this->schemaSetup;

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'ordermail_request_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => \Magento\Framework\DB\Ddl\Table::MAX_TEXT_SIZE,
                'nullable' => true,
                'comment' => 'Data Hub order mail request id'
            ]
        );


        $this->schemaSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}