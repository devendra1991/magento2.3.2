<?php
/**
 * OrderEmails EmailInterface .
 *
 * PHP version 7.2.X
 *
 * @category  PHP
 * @package   Born_DbiEmails
 * @author    Borngroup <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Borngroup
 * @link      https://www.davidsbridal.com/
 */
declare(strict_types=1);

namespace Born\DbiEmails\Api;

/**
 * Interface EmailInterface
 *
 * @category  PHP
 * @package   Born\DbiEmail\Api
 * @author    Born Support <support@borngroup.com>
 * @copyright 2019 Copyright BORN Commerce Pvt Ltd, https://www.borngroup.com/
 * @license   https://www.borngroup.com/ Private
 * @link      https://www.davidsbridal.com/
 */
interface EmailInterface
{
    /**
     * Order Email
     *
     * @param Email $method Email
     * @param array $params Email
     *
     * @return mixed
     */
    public function orderEmail($method, $params = []);

}
