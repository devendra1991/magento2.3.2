<?php
/**
 * Copyright © 2018 CyberSource. All rights reserved.
 * See accompanying LICENSE.txt for applicable terms of use and license.
 */

namespace CyberSource\Core\Cron;

use CyberSource\Core\Model\Config;
use CyberSource\Core\Model\LoggerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Payment\State\CommandInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\DataObject;
use Magento\Sales\Model\Order\Invoice;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;

class DecisionManagerReport
{
    const DM_ACCEPT = "ACCEPT";
    const PAYPAL_METHOD = 'cybersourcepaypal';
    const VISA_CHECKOUT_METHOD = 'chcybersourcevisa';

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    private $httpClientFactory;

    /**
     * @var \Magento\Framework\HTTP\ZendClient
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Config
     */
    private $gatewayConfig;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var CommandInterface
     */
    private $stateCommand;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * @var array
     */
    private $exceptions = [];

    /**
     * @var array
     */
    private static $skipStatuses = [
        Order::STATE_PROCESSING,
        Order::STATE_CANCELED,
        Order::STATE_CLOSED,
        Order::STATE_COMPLETE,
    ];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    private $processedMerchantIds = [];
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $emulation;
    /**
     * @var \Magento\Framework\App\Config\ScopeCodeResolver
     */
    private $scopeCodeResolver;

    /**
     * @var State
     */
    private $appState;

    /**
     * @var \CyberSource\Core\Service\CyberSourceSoapAPI
     */
    private $cybersourceApi;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var DataObject
     */
    private $postObject;

    /**
     * @var \CyberSource\Core\DM\TransactionProcessorInterface[]
     */
    private $transactionProcessors;

    /**
     * DecisionManagerReport constructor.
     * @param \Magento\Framework\HTTP\ZendClientFactory $clientFactory
     * @param LoggerInterface $logger
     * @param Config $config
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param CommandInterface $stateCommand
     * @param OrderRepository $orderRepository
     * @param InvoiceRepository $invoiceRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param DataObject $postObject
     * @param \CyberSource\Core\Service\CyberSourceSoapAPI $cybersourceApi
     * @param State $appState
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $emulation
     * @param \Magento\Framework\App\Config\ScopeCodeResolver $scopeCodeResolver
     * @param array $transactionProcessors
     */
    public function __construct(
        \Magento\Framework\HTTP\ZendClientFactory $clientFactory,
        LoggerInterface $logger,
        Config $config,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        CommandInterface $stateCommand,
        OrderRepository $orderRepository,
        InvoiceRepository $invoiceRepository,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        DataObject $postObject,
        \CyberSource\Core\Service\CyberSourceSoapAPI $cybersourceApi,
        State $appState,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\App\Config\ScopeCodeResolver $scopeCodeResolver,
        array $transactionProcessors = []
    ) {
        $this->httpClientFactory = $clientFactory;
        $this->logger = $logger;
        $this->gatewayConfig = $config;
        $this->orderFactory = $orderFactory;
        $this->stateCommand = $stateCommand;
        $this->orderRepository = $orderRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->postObject = $postObject;
        $this->cybersourceApi = $cybersourceApi;
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->emulation = $emulation;
        $this->scopeCodeResolver = $scopeCodeResolver;
        $this->transactionProcessors = $transactionProcessors;

        $this->createClient();
    }

    public function execute()
    {
        if (!$this->gatewayConfig->isDecisionManagerCronEnabled()) {
            $this->logInfo("Decision Manager Cron is not enabled.");
            return null;
        }

        foreach ($this->storeManager->getStores() as $store) {
            try {
                $request = null;
                $merchantId = null;

                $this->appState->emulateAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND,
                    function () use ($store, &$request, &$merchantId) {
                        $this->emulation->startEnvironmentEmulation(
                            $store->getId(),
                            \Magento\Framework\App\Area::AREA_FRONTEND, true
                        );
                        $this->scopeCodeResolver->clean(); // have to clean scope code resolver internal cache
                        $merchantId = $this->gatewayConfig->getMerchantId();
                        $request = $this->buildRequest();
                        $this->emulation->stopEnvironmentEmulation();
                        $this->scopeCodeResolver->clean(); // let's clean after our things
                    });

                if ($this->isMidProcessed($merchantId)) {
                    continue;
                }

                $this->runReport($request);
            } catch (\Exception $e) {
                $this->logger->error(
                    'An error occurred while running DM cron for Store Code '
                    . $store->getCode()
                    . ' Exception message: ' . $e->getMessage()
                );
            } finally {
                $this->setMidProcessed($merchantId);
            }
        }
    }

    private function isMidProcessed($merchantId) {
        return in_array($merchantId, $this->processedMerchantIds);
    }

    private function setMidProcessed($merchantId) {
        array_push($this->processedMerchantIds, $merchantId);
        return $this;
    }

    public function runReport($request)
    {
        $this->client->setParameterPost($request);

        try {
            $response = $this->client->request();
            $parsedXml = $this->parseResponse($response);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new LocalizedException(__("Response is invalid or empty."));
        }

        foreach ($parsedXml as $incrementId => $value) {
            $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
            $payment = $order->getPayment();

            if ($order && $payment && $payment->getCcTransId() == $value['transaction_id'] && !in_array($order->getState(), self::$skipStatuses)) {

                try {
                    $this->updatePayment($order, $value);
                } catch (\Exception $e) {
                    $this->buildExceptionOutput($order, $e->getMessage());
                    continue;
                }
            }
        }

        if (!empty($this->exceptions)) {
            $this->logInfo(json_encode($this->exceptions));
        }
    }

    private function updatePayment(\Magento\Sales\Model\Order $order, array $parsedXml)
    {
        if ($parsedXml['new_decision'] == self::DM_ACCEPT) {
            $payment = $order->getPayment();
            if ($payment instanceof \Magento\Sales\Model\Order\Payment) {
                $payment->setIsTransactionApproved(true);
                $transactionId = $payment->getLastTransId();
                $payment->setTransactionId($transactionId);

                if (
                    $this->gatewayConfig->getValue(
                        \CyberSource\Core\Model\AbstractGatewayConfig::KEY_ENABLED_DM_CRON_ACCEPTED_SETTLEMENT,
                        $order->getStoreId()
                    )
                ) {
                    $this->settleDmTransaction($payment);
                }

                $payment->update(false);
            }

            $this->orderRepository->save($order);
            return;
        }

        $this->processCancel($order);
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @return $this
     * @throws \Exception
     */
    private function settleDmTransaction($payment)
    {

        if (isset($this->transactionProcessors[$payment->getMethod()])) {
            $processor = $this->transactionProcessors[$payment->getMethod()];
            $processor->settle($payment);
            return $this;
        }

        $this->cybersourceApi->setPayment($payment);
        $result = (array)$this->cybersourceApi->captureOrder(
            $payment->getBaseAmountAuthorized()
        );

        if (
            !$result
            || !isset($result['decision'])
            || $result['decision'] != self::DM_ACCEPT
            || !isset($result['requestID'])
        ) {
            return $this;
        }

        $payment->setTransactionId($result['requestID']);
        return $this;
    }

    private function buildRequest()
    {
        $params = [];

        $params['merchantID'] = $this->gatewayConfig->getMerchantId();
        $params['username'] = $this->gatewayConfig->getReportUsername();
        $params['password'] = $this->gatewayConfig->getReportPassword();

        $startTimestamp = time()-23*3600;
        $endTimestamp = time();

        $params['startDate'] = gmdate('Y-m-d', $startTimestamp);
        $params['startTime'] = gmdate('H:i:s', $startTimestamp);

        $params['endDate'] = gmdate('Y-m-d', $endTimestamp);
        $params['endTime'] = gmdate('H:i:s', $endTimestamp);

        return $params;
    }

    private function parseResponse(\Zend_Http_Response $response)
    {
        if ($response->getBody())
        $xml = simplexml_load_string($response->getBody());
        $data = [];
        if (!empty($xml->Conversion)) {
            foreach ($xml->Conversion as $conversion) {
                $data[(string)$conversion['MerchantReferenceNumber']] = [
                    'decision' => (string) $conversion->OriginalDecision,
                    'new_decision' => (string) $conversion->NewDecision,
                    'transaction_id' => (string) $conversion['RequestID']
                ];
            }
        }
        return $data;
    }

    private function createClient()
    {
        $reportUrl = $this->gatewayConfig->getReportUrl();

        if ($reportUrl === null) {
            throw new LocalizedException(__("Invalid report url. Please, configure it on module config"));
        }

        $this->client = $this->httpClientFactory->create();
        $this->client->setMethod(Http::METHOD_POST);
        $this->client->setUri($reportUrl);
    }

    private function buildExceptionOutput($order, $message)
    {
        $this->exceptions[] = "Order increment_id: ".$order->getIncrementId() . " | Error Message: " .$message;
    }

    private function logInfo($message)
    {
        $this->logger->info(__CLASS__ . "|" . __METHOD__. " | " . $message);
    }

    private function processCancel(\Magento\Sales\Model\Order $order)
    {
        if (!$order->getId()) {
            return;
        }

        $payment = $order->getPayment();

        if (!$payment instanceof \Magento\Sales\Model\Order\Payment) {
            return;
        }

        $order->getPayment()
            ->setNotificationResult(true)
            ->setIsTransactionClosed(true)
            ->deny(false);

        try {
            $storeId = $order->getStoreId();
            $paymentMethod = $payment->getMethod();

            if (isset($this->transactionProcessors[$payment->getMethod()])) {
                $processor = $this->transactionProcessors[$payment->getMethod()];
                $processor->cancel($payment);
            } elseif ($paymentMethod == self::VISA_CHECKOUT_METHOD) {
                $this->cybersourceApi->reverseOrderPayment($storeId, true);
            } else {
                $this->cybersourceApi->setPayment($payment);
                $this->cybersourceApi->reverseOrderPayment($storeId);
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        $this->orderRepository->save($order);
        $this->sendEmail($order, $order->getStoreId());

    }

    public function sendEmail($order, $storeId)
    {
        $emailTempVariables = ['order' => $order];

        $sender = $this->scopeConfig->getValue(
            "payment/chcybersource/dm_fail_sender",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $senderName = $this->scopeConfig->getValue(
            "trans_email/ident_".$sender."/name",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $senderEmail = $this->scopeConfig->getValue(
            "trans_email/ident_".$sender."/email",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $email = $order->getCustomerEmail();
        $this->postObject->setData($emailTempVariables);
        $sender = [
            'name' => $senderName,
            'email' => $senderEmail,
        ];

        $emailTemplate = $this->scopeConfig->getValue(
                "payment/chcybersource/dm_fail_template",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
                );

        $this->appState->emulateAreaCode(Area::AREA_ADMINHTML, function (){});

        try{
            $transport = $this->transportBuilder->setTemplateIdentifier($emailTemplate)
                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => $storeId])
                ->setTemplateVars(['data' => $this->postObject])
                ->setFrom($sender)
                ->addTo($email)
                ->setReplyTo($senderEmail)
                ->getTransport();
            $transport->sendMessage();

        } catch(\Exception $e) {
            $this->logger->error($e->getMessage());

        }

        $this->logger->info("cancel email sent from store id " . $storeId . " to " . $email);
    }
}
