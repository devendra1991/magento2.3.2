<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace CyberSource\PayPal\Controller\Express;

use CyberSource\Core\Model\LoggerInterface;
use CyberSource\PayPal\Model\Config as CyberSourcePayPalConfig;
use CyberSource\PayPal\Model\Express\Checkout\Factory as CheckoutFactory;
use Magento\Quote\Api\Data\CartInterface;
/**
 * Abstract Express Checkout Controller
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class AbstractExpress extends \Magento\Paypal\Controller\Express\AbstractExpress
{
    /**
     * @var \CyberSource\PayPal\Model\Express\Checkout
     */
    protected $_checkout;

    /**
     * @var CheckoutFactory
     */
    private $cyberSourceCheckoutFactory;

    /**
     * @var CyberSourcePayPalConfig
     */
    public $gatewayConfig;

    /**
     * @var LoggerInterface
     */
    protected $cyberLogger;

    /**
     * AbstractExpress constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Paypal\Model\Express\Checkout\Factory $checkoutFactory
     * @param \Magento\Framework\Session\Generic $paypalSession
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param CheckoutFactory $cyberSourceCheckoutFactory
     * @param CyberSourcePayPalConfig $gatewayConfig
     * @param LoggerInterface $cyberLogger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Paypal\Model\Express\Checkout\Factory $checkoutFactory,
        \Magento\Framework\Session\Generic $paypalSession,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Customer\Model\Url $customerUrl,
        CheckoutFactory $cyberSourceCheckoutFactory,
        CyberSourcePayPalConfig $gatewayConfig,
        LoggerInterface $cyberLogger
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $orderFactory,
            $checkoutFactory,
            $paypalSession,
            $urlHelper,
            $customerUrl
        );
        $this->cyberSourceCheckoutFactory = $cyberSourceCheckoutFactory;
        $this->gatewayConfig = $gatewayConfig;
        $this->cyberLogger = $cyberLogger;
    }

    /**
     * Instantiate quote and checkout
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _initCheckout(CartInterface $quoteObject = null)
    {
        $quote = $this->_getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->getResponse()->setStatusHeader(403, '1.1', 'Forbidden');
            throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t initialize Express Checkout.'));
        }
        if (!isset($this->_checkoutTypes[$this->_checkoutType])) {
            $parameters = [
                'params' => [
                    'quote' => $quote,
                    'config' => $this->_config,
                    'gatewayConfig' => $this->gatewayConfig
                ],
            ];
            $this->_checkoutTypes[$this->_checkoutType] = $this->cyberSourceCheckoutFactory
                ->create($this->_checkoutType, $parameters);
        }
        $this->_checkout = $this->_checkoutTypes[$this->_checkoutType];
    }
}
