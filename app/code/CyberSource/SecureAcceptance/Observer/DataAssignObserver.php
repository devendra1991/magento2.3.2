<?php
/**
 * Copyright © 2018 CyberSource. All rights reserved.
 * See accompanying LICENSE.txt for applicable terms of use and license.
 */

namespace CyberSource\SecureAcceptance\Observer;

use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Checkout\Model\Session;
use CyberSource\SecureAcceptance\Model\Ui\ConfigProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;

class DataAssignObserver extends AbstractDataAssignObserver
{

    /**
     * @var  \Magento\Framework\Session\SessionManagerInterface
     */
    protected $session;

    /**
     * @var ScopeConfig
     */
    protected $scopeConfig;

    /**
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $session,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->session = $session;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);
        if (!$this->_isVaultCCMethod($data) || !$this->_isCvvEnabled()) {
            return;
        }
        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $additionalData = new DataObject($additionalData);
        if ($additionalData->getDataByKey('cvv')) {
            $this->session->setData('cvv', $additionalData->getDataByKey('cvv'));
        }

        if ($additionalData->getDataByKey('vault_cvv')) {
            $this->session->setData('cvv', $additionalData->getDataByKey('vault_cvv'));
        }
    }

    /**
     * @param Data
     * @return boolean
     */
    private function _isVaultCCMethod($data)
    {
        if ($data->getData(PaymentInterface::KEY_METHOD) != ConfigProvider::CC_VAULT_CODE) {
            return false;
        }
        return true;
    }

    /**
     * @return boolean
     */
    private function _isCvvEnabled()
    {
        return
            $this->scopeConfig->getValue("payment/chcybersource/enable_cvv", \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
            || $this->scopeConfig->getValue(\CyberSource\SecureAcceptance\Gateway\Config\Config::KEY_VAULT_ADMIN_ENABLE_CVV, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
