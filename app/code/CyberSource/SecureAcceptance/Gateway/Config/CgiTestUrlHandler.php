<?php

namespace CyberSource\SecureAcceptance\Gateway\Config;

class CgiTestUrlHandler implements \Magento\Payment\Gateway\Config\ValueHandlerInterface
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * CgiTestUrlHandler constructor.
     * @param Config $config
     */
    public function __construct(
        \CyberSource\SecureAcceptance\Gateway\Config\Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function handle(array $subject, $storeId = null)
    {
        $uri = '/pay';

        if ($this->config->isSilent()) {
            $uri = '/silent/pay';
        }

        return $this->config->getSopServiceUrlTest() . $uri;
    }
}

