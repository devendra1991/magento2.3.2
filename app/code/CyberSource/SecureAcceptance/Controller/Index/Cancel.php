<?php
/**
 * Copyright © 2018 CyberSource. All rights reserved.
 * See accompanying LICENSE.txt for applicable terms of use and license.
 */

namespace CyberSource\SecureAcceptance\Controller\Index;

use CyberSource\SecureAcceptance\Gateway\Config\Config;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\ResultFactory;

class Cancel extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $config;

    /**
     * Cancel constructor.
     * @param Context $context
     * @param Session $session
     * @param Config $config
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Session $session,
        Config $config
    ) {
        $this->config = $config;
        parent::__construct($context);
    }

    public function execute()
    {
        $url = $this->_url->getUrl('checkout');

        if (!$this->config->getUseIFrame()) {
            return $this->_redirect('checkout');
        }
        $html = '<html>
                    <body>
                        <script type="text/javascript">
                            window.onload = function() {
                                parent.window.location = "'.$url.'";
                            };
                        </script>
                    </body>
                </html>';
        /**
         * @var $result \Magento\Framework\Controller\ResultInterface
         */
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $result->setContents($html);
        return $result;
    }
}
