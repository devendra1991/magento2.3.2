<?php
/**
 * Copyright © 2018 CyberSource. All rights reserved.
 * See accompanying LICENSE.txt for applicable terms of use and license.
 */

namespace CyberSource\SecureAcceptance\Controller\Index;

use CyberSource\SecureAcceptance\Gateway\Config\Config;
use Magento\Framework\App\Action\Context;
use CyberSource\SecureAcceptance\Helper\RequestDataBuilder;
use CyberSource\SecureAcceptance\Helper\Vault;
use Magento\Framework\Exception\LocalizedException;

class LoadSilentData extends \Magento\Framework\App\Action\Action
{
    /**
     * @var RequestDataBuilder
     */
    protected $helper;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Vault
     */
    protected $vaultHelper;
    
    /**
     * \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    private $formKeyValidator;

    /**
     * LoadSilentData constructor.
     * @param Context $context
     * @param RequestDataBuilder $helper
     * @param Config $config
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param Vault $vaultHelper
     */
    public function __construct(
        Context $context,
        RequestDataBuilder $helper,
        Config $config,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Vault $vaultHelper,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
    ) {
        $this->helper = $helper;
        $this->config = $config;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->vaultHelper = $vaultHelper;
        parent::__construct($context);
        $this->formKeyValidator = $formKeyValidator;
    }

    public function execute()
    {
        $guestEmail = trim($this->_request->getParam('quoteEmail'));

        if (empty($guestEmail) || $guestEmail == 'null') {
            $guestEmail = null;
        }

        $cardType = $this->_request->getParam('cardType');
        $this->vaultHelper->setVaultEnabled($this->_request->getParam('vaultIsEnabled'));

        $result = $this->resultJsonFactory->create();

        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $result->setData(['error' => __('Invalid formkey.')]);
        }

        if ($this->config->isTestMode()) {
            $actionUrl = $this->config->getSopServiceUrlTest();
        } else {
            $actionUrl = $this->config->getSopServiceUrl();
        }

        $data = [];

        try {
            $data = [
                'form_data' => $this->helper->buildSilentRequestData($guestEmail, null, $cardType),
                'action_url' => $actionUrl . '/silent/pay'
            ];
        } catch (\Exception $e) {
            $data['error'] = $e->getMessage();
        }

        $result = $result->setData($data);
        return $result;
    }
}
