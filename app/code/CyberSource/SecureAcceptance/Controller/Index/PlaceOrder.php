<?php
/**
 * Copyright © 2018 CyberSource. All rights reserved.
 * See accompanying LICENSE.txt for applicable terms of use and license.
 */

namespace CyberSource\SecureAcceptance\Controller\Index;

use CyberSource\Core\Model\LoggerInterface;
use CyberSource\SecureAcceptance\Gateway\Config\Config;
use CyberSource\SecureAcceptance\Gateway\Request\AbstractRequest;
use CyberSource\SecureAcceptance\Helper\RequestDataBuilder;
use CyberSource\SecureAcceptance\Model\Ui\ConfigProvider;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\OrderRepository;

class PlaceOrder extends \CyberSource\Core\Action\CsrfIgnoringAction
{
    const LOCK_PREFIX = 'cyber_req_';

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Magento\Quote\Model\QuoteManagement $quoteManagement
     */
    private $quoteManagement;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @var RequestDataBuilder
     */
    private $requestDataBuilder;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var \CyberSource\SecureAcceptance\Service\Lock
     */
    private $lock;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * PlaceOrder constructor.
     * @param Context $context
     * @param SessionManagerInterface $checkoutSession
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param RequestDataBuilder $requestDataBuilder
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param Config $config
     * @param \CyberSource\SecureAcceptance\Service\Lock $lock
     * @param OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Registry $registry
     * @param LoggerInterface $logger
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     */
    public function __construct(
        Context $context,
        SessionManagerInterface $checkoutSession,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        RequestDataBuilder $requestDataBuilder,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        Config $config,
        \CyberSource\SecureAcceptance\Service\Lock $lock,
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Registry $registry,
        LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Event\ManagerInterface $eventManager

    ) {
        parent::__construct($context);

        $this->checkoutSession = $checkoutSession;
        $this->quoteManagement = $quoteManagement;
        $this->resultPageFactory = $resultPageFactory;
        $this->requestDataBuilder = $requestDataBuilder;
        $this->quoteRepository = $quoteRepository;
        $this->config = $config;
        $this->lock = $lock;
        $this->orderRepository = $orderRepository;
        $this->registry = $registry;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->eventManager = $eventManager;
    }

    public function execute()
    {
        /** @var array $cyberSourceResponse */
        $cyberSourceResponse = $this->getRequest()->getParams();

        $this->logger->debug(
            [
                'client' => static::class,
                'response' => (array) $cyberSourceResponse
            ]
        );

        $resultUrl = 'checkout/cart';

        /**
         * Validate cybersource signature before order placement to avoid data tampering
         */
        if(!$this->isValidSignature($cyberSourceResponse)) {
            $this->messageManager->addErrorMessage(__('Payment could not be processed.'));

            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($resultUrl, ['_secure' => true]);
        }

        try {

        if (!$this->lock->acquireLock($this->getLockName())) {
            return $this->processResponse( 'checkout/cart');
        }

        $quote = $this->getQuote();

        if (!$quote->getIsActive()) {
            $this->setSuccessOrder($quote);
            return $this->processResponse( 'checkout/onepage/success');
        }

        $quote->reserveOrderId();

        if ($this->requestDataBuilder->getCheckoutMethod($quote) === Onepage::METHOD_GUEST) {
            $quote->getBillingAddress()->setEmail($cyberSourceResponse['req_bill_to_email']);
            $this->requestDataBuilder->prepareGuestQuote($quote);
        }

        $quote->setPaymentMethod(ConfigProvider::CODE);
        $quote->setInventoryProcessed(false);

        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => ConfigProvider::CODE]);

        // register response for command response handlers
        $this->registry->register(AbstractRequest::TRANSPARENT_RESPONSE_KEY, $cyberSourceResponse);

        $quote->collectTotals();
        $this->quoteRepository->save($quote);

        $this->checkoutSession->setLastSuccessQuoteId($quote->getId());
        $this->checkoutSession->setLastQuoteId($quote->getId());
        $this->checkoutSession->clearHelperData();

            $order = $this->quoteManagement->submit($quote);
            $this->eventManager->dispatch(
                'cybersource_quote_submit_success',
                [
                    'order' => $order,
                    'quote' => $quote
                ]
            );
            $this->checkoutSession->setLastOrderId($order->getId());
            $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
            $this->checkoutSession->setLastOrderStatus($order->getStatus());

            $successValidator = $this->_objectManager->get('Magento\Checkout\Model\Session\SuccessValidator');

            if (!$successValidator->isValid()) {
                $resultUrl = 'checkout/cart';
            }

            $this->messageManager->addSuccessMessage('Your order has been successfully created!');
            $resultUrl = 'checkout/onepage/success';
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        } finally {
            $this->lock->releaseLock($this->getLockName());
        }

        if ($this->config->getUseIFrame()) {
            return $this->processResponse($resultUrl);
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($resultUrl, ['_secure' => true]);
    }


    /**
     * @return \Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getQuote()
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->checkoutSession->getQuote();

        try {
            if (!$quote->getId() && $quoteId = $this->getRequest()->getParam('req_' . \CyberSource\SecureAcceptance\Helper\RequestDataBuilder::KEY_QUOTE_ID)) {
                $quote = $this->quoteRepository->get($quoteId);
            }
        } catch(\Magento\Framework\Exception\NoSuchEntityException $e) {

        }

        if (!$quote->getId() || $quote->getId() != $this->getRequest()->getParam('req_' . \CyberSource\SecureAcceptance\Helper\RequestDataBuilder::KEY_QUOTE_ID)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Payment could not be processed.'));
        }

        return $quote;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     */
    private function setSuccessOrder($quote)
    {
        $order = $this->getQuoteOrder($quote);

        $this->checkoutSession->setLastSuccessQuoteId($quote->getId());
        $this->checkoutSession->setLastQuoteId($quote->getId());
        $this->checkoutSession->clearHelperData();
        $this->checkoutSession->setLastOrderId($order->getId());
        $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
        $this->checkoutSession->setLastOrderStatus($order->getStatus());
        $this->messageManager->addSuccessMessage('Your order has been successfully created!');
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    private function getQuoteOrder($quote)
    {
        $orderList = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->addFilters([
                    $this->filterBuilder->setField('quote_id')->setValue($quote->getId())->create()
                ]
            )->create()
        );

        $orders = $orderList->getItems();

        return array_shift($orders);
    }

    /**
     * @param $url
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processResponse($url)
    {
        $html = '<html>
                    <body>
                        <script type="text/javascript">
                            window.onload = function() {
                                window.top.location.href = "'.$this->_url->getUrl($url, ['_scope' => $this->storeManager->getStore()->getId()]).'";
                            };
                        </script>
                    </body>
                </html>';

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultRedirect->setContents($html);
        return $resultRedirect;
    }

    public function getLockName()
    {
        return self::LOCK_PREFIX . $this->getRequest()->getParam('req_transaction_uuid');
    }

    /**
     *
     * Validates signature of request
     *
     * @param $responses
     * @return bool
     */
    private function isValidSignature($responses)
    {
        if ($this->config->isSilent()) {
            $transactionKey = $this->config->getSopAuthSecretKey();
        } else {
            $transactionKey = $this->config->getAuthSecretKey();
        }

        if (!array_key_exists("signed_field_names", $responses) || empty($responses['signature'])) {
            return false;
        }

        $signedKey = $this->requestDataBuilder->sign($responses, $transactionKey);

        return hash_equals($signedKey, $responses['signature']); // mitigating potential timing attack
    }
}
