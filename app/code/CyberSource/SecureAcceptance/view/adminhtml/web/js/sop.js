define([
    'jquery',
    'CyberSource_SecureAcceptance/js/cc-inputs-enabler',
    'Magento_Payment/transparent'
], function ($, inputsEnabler) {
    'use strict';

    $.widget('cybersource.sop', $.mage.transparent, {

        _orderSave: function () {
            inputsEnabler(this.options.gateway, true);
            var postData = {
                'form_key': FORM_KEY,
                'cc_type': this.ccType(),
                'order_data': $(this.options.editFormSelector).serialize()
            };
            inputsEnabler(this.options.gateway, false);
            $.ajax({
                url: this.options.orderSaveUrl,
                type: 'post',
                context: this,
                data: postData,
                dataType: 'json',

                success: function (response) {
                    if (response.success && response[this.options.gateway]) {
                        this._postPaymentToGateway(response);
                    } else {
                        this._processErrors(response);
                    }
                },

                complete: function () {
                    $('body').trigger('processStop');
                }
            });
        }
    });

    return $.cybersource.sop;
});
