<?php

namespace CyberSource\SecureAcceptance\Plugin\Helper;

class RequestDataBuilderPlugin
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    /**
     * @var \CyberSource\SecureAcceptance\Gateway\Config\Config
     */
    private $config;

    /**
     * RequestDataBuilderPlugin constructor.
     * @param \CyberSource\SecureAcceptance\Gateway\Config\Config $config
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \CyberSource\SecureAcceptance\Gateway\Config\Config $config,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->config = $config;
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
    }

    /**
     *
     * Plugin method for \CyberSource\SecureAcceptance\Helper\RequestDataBuilder::buildSilentRequestData
     *
     * Appends override_custom_receipt_page field to the request and generates a new signature.
     * Erases device fingerprint and remote ip.
     *
     * @param \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject
     * @param $result
     * @return mixed
     */
    public function afterBuildSilentRequestData(
        \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject,
        $result
    ) {

        $result['override_custom_receipt_page'] = $this->urlBuilder->getUrl('chcybersource/transparent/response', ['_secure' => $this->request->isSecure()]);

        unset($result['device_fingerprint_id']);
        unset($result['customer_ip_address']);
        unset($result['signed_field_names']);
        unset($result['signature']);
        $result['access_key'] = $this->config->getSopAccessKey();
        $result['profile_id'] = $this->config->getSopProfileId();
        $result['signed_field_names'] = $subject->getSignedFields($result);
        $result['signature'] = $subject->sign($result, $this->config->getSopSecretKey());

        return $result;
    }

    /**
     *
     * Plugin method for \CyberSource\SecureAcceptance\Helper\RequestDataBuilder::buildSilentData
     *
     * Appends override_custom_receipt_page field to the request and generates a new signature.
     *
     * @param \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject
     * @param $result
     * @return mixed
     */
    public function afterBuildRequestData(
        \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject,
        $result
    ) {

        $result['override_custom_receipt_page'] = $this->urlBuilder->getUrl('chcybersource/transparent/response', ['_secure' => $this->request->isSecure()]);

        unset($result['signed_field_names']);
        unset($result['signature']);
        $result['access_key'] = $this->config->getAccessKey();
        $result['profile_id'] = $this->config->getProfileId();
        $result['signed_field_names'] = $subject->getSignedFields($result);
        $result['signature'] = $subject->sign($result, $this->config->getSecretKey());

        return $result;
    }

}
